package wine.autotest.mobile.test;

import java.util.ArrayList;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class SortOrderInNewArrivalAlerts extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String sortedArrivalLogin()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(3L);
			

			if(UIFoundation.isDisplayed(LoginPage.JoinNowButton)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.JoinNowButton));
				UIFoundation.waitFor(2L);
			}
			
			if(UIFoundation.isDisplayed(LoginPage.signInLinkAccount)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.signInLinkAccount));
			}
			
			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "newArrivalAlertEmail"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(6L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				//System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				//System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: validationForSortZtoA()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean verifyTheSortingInNewArrival()
	{
		ArrayList<String> arrayList=new ArrayList<String>();
		new ArrayList<String>();
		   boolean isSorted =false;
			String screenshotName = "Scenarios_verifyTheSortingInNewArrival.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
			     + screenshotName;
		try
		{
			
			String.valueOf(UIFoundation.javaScriptClick(LoginPage.btnMainNavAccTab));
			UIFoundation.waitFor(3L);
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccTabAfterSignIn));
			UIFoundation.waitFor(1L);			
			String.valueOf(UIFoundation.javaScriptClick(LoginPage.EmailPreferences));
			UIFoundation.waitFor(4L);
			String products1=UIFoundation.getText(ListPage.btnNewArrivalAlertFirstProduct);
			String products2=UIFoundation.getText(ListPage.btnNewArrivalAlertSecondProduct);
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) < 0) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	  String objDetail="New arrival alerts are sorted in descending order by default.";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			    	
					return true;
			    }
			    else
			    {
			    		String objDetail="New arrival alerts are not sorted in descending order by default.";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					return false;
			    }
		}catch(Exception e)
		{
			return false;
			
		}
	
	}

}
