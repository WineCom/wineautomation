
package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyErrorMsgForUsedGiftCard extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: usedGiftCardValidation()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String usedGiftCardValidation()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String expectedErrorMsg=null;
		String actualErrorMsg=null;
		
		String screenshotName = "Scenarios_ErrorMessage_Screenshot.jpeg";
				
		try
		{
			log.info("The execution of the method usedGiftCardValidation started here ...");
			System.out.println("============Order summary ===============");
			
			expectedErrorMsg = verifyexpectedresult.inValidGiftCardmsg;
			
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.chkGiftCheckbox);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.chkGiftCheckbox));
			objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "UsedGiftCertificate"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnGiftApply));
			UIFoundation.webDriverWaitForElement(CartPage.btnGiftCardApplyng, "Invisible", "", 50);
			actualErrorMsg=UIFoundation.getText(CartPage.txtGiftErrorCode);
			UIFoundation.waitFor(2L);
			if(actualErrorMsg.equalsIgnoreCase(expectedErrorMsg))
			{
				System.out.println(expectedErrorMsg);
				objStatus+=true;
			    String objDetail="Actual and expected error message are same";
			    getlogger().pass(objDetail);
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				System.err.println(actualErrorMsg);
				objStatus+=false;
			    String objDetail="Actual and expected error message are not same";
			    getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			    UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);			   
			}
			log.info("The execution of the method usedGiftCardValidation ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify Error Message for used GiftCard test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify Error Message for used GiftCard test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method usedGiftCardValidation "+ e);
			return "Fail";
			
		}
	}


}
