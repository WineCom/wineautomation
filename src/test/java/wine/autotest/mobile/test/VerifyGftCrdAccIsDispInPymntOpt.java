package wine.autotest.mobile.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyGftCrdAccIsDispInPymntOpt extends Mobile {
	


	/***************************************************************************
	 * Method Name : verifyGftCrdAccAndBalDispInPaymntOpt() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose     : Verifies gift card applied is displayed in payment option
	 ****************************************************************************
	 */

	public static String verifyGftCrdAccAndBalDispInPaymntOpt() {

		String objStatus=null;
		String screenshotName = "Scenarios_verifyGftCrdAccAndBalDispInPaymntOpt_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

		try {
			log.info("The execution of the verifyGftCrdAccAndBalDispInPaymntOpt method strated here");
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				UIFoundation.clickObject(FinalReviewPage.chkBillingAndShippingCheckbox);
				UIFoundation.waitFor(3L);
			}

			if(UIFoundation.isDisplayed(FinalReviewPage.txtDOBFormat)){
				objStatus+=true;
				ReportUtil.addTestStepsDetails("DOB format is in'MM / DD / YYYY'.", "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="DOB is not in format 'MM / DD / YYYY'.";
				
				UIFoundation.captureScreenShot(screenshotpath+screenshotName+"dobFormat", objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(6L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnCartEditButton));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "element", "", 50);
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.chkGiftCheckbox);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.chkGiftCheckbox));
			objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnGiftCardApplyng);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnGiftApply));
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(CartPage.txtGiftCertificateSuccessMsg)){								
				objStatus+=true;
				String objDetail="Verified gift card added sucessfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{								  
				  objStatus+=false;
				  String objDetail="Gift card did not add sucessfully";
				  ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				  getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				  UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}			
			UIFoundation.waitFor(6L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(FinalReviewPage.objGiftCardAct) && UIFoundation.isDisplayed(FinalReviewPage.objGiftCardBal)) {
						
				objStatus+=true;
				String objDetail="Gift card Account is displayed in payment option along with the Gift balance";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else{
								  
				  objStatus+=false;
				  String objDetail="Gift card Account is not displayed in payment option along with the Gift balance";
				  ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				  getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				  UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}	
			log.info("The execution of the method verifyGftCrdAccAndBalDispInPaymntOpt ended here ...");
			if (objStatus.contains("false"))
			{
				objStatus+=false;
				String objDetail="verifyGftCrdAccAndBalDispInPaymntOpt test scripts is failed";
				ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
			else
			{						
				
				String objDetail="verifyGftCrdAccAndBalDispInPaymntOpt test case excecuted succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}	
}
