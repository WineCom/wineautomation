package wine.autotest.mobile.test;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class HandleSoldInIncrementOfXProducts extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : addShipTodayProdTocrt() 
	 * Created By : Chandrashekhar
	 * Reviewed By : Ramesh.
	 *  Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addIncrementProdTocrt() {
        String objStatus = null;
        String screenshotName = "Scenarios_IncrementProd_Screenshot.jpeg";
           String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
        try {
               UIFoundation.waitFor(3L);
               getDriver().get("https://qwww.wine.com/product/tour-saint-christophe-futures-pre-sale-2018/520689");
               UIFoundation.waitFor(6L);
               objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
               UIFoundation.waitFor(4L);
               int prodLimit=Integer.parseInt(UIFoundation.getText(ListPage.btnCartCount));
               WebElement drop_down =getDriver().findElement(By.xpath("//select[@class='prodItemStock_quantitySelect']"));
               Select se = new Select(drop_down);
               for(int i=1 ;i<se.getOptions().size(); i++)
               {
                     int prodLimitInDropDown=Integer.parseInt(se.getOptions().get(i).getAttribute("value"));
                     if(prodLimit*(i)==prodLimitInDropDown)
                     {
                            objStatus+=true;

                      }
                     else
                     {
                            objStatus+=false;
                            String objDetail="roduct limit test case is failed";
                            UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
                            break;
                     }
               }

               if (objStatus.contains("false")) {
                     System.err.println("Product limit test case is failed");
                     return "Fail";
               } else {
                     System.out.println("Product limit test case is executed successfully");
                     return "Pass";
               }

        } catch (Exception e) {

               return "Fail";
        }

	}
	
}
