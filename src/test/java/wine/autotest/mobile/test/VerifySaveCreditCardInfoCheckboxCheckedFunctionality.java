package wine.autotest.mobile.test;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifySaveCreditCardInfoCheckboxCheckedFunctionality extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(2L);

			objStatus+=String.valueOf(UIBusinessFlow.addproductstocart(2));	
			UIFoundation.waitFor(4L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lblShowOutOfStock);
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: verifySaveCrdeitCardInfo()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifySaveCrdeitCardInfo() {

	String objStatus=null;

	String screenshotName = "Scenarios_CreditCard_Screenshot.jpeg";
	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
			+ screenshotName;
		try {
			log.info("The execution of the method verifySaveCrdeitCardInfo started here ...");
			UIFoundation.waitFor(4L);
			

          if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(8L);
           objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
            UIFoundation.waitFor(1L); 
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkBillPaymentEdit))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkBillPaymentEdit);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkBillPaymentEdit));
				UIFoundation.waitFor(1L);
			}
			String creditCardCount=UIFoundation.getText(FinalReviewPage.btnPaymentLinksCount);
			int creditCardCntBefore=Integer.parseInt(creditCardCount.replaceAll("\\(|\\)", ""));
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
			UIFoundation.waitFor(1L);
			}
			 
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
			UIFoundation.waitFor(2L);
			WebElement ele=getDriver().findElement(By.xpath("//input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
				UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
				UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			   {
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			   }
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);

			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			getDriver().navigate().refresh();
			UIFoundation.waitFor(3L);
			AddProdcutsToCartCaptureOrder.addprodTocrt();
			UIFoundation.waitFor(2L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
            UIFoundation.waitFor(3L); 
/*			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "selectRecipientAdr"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "RecipientContinue"));
			UIFoundation.waitFor(5L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkBillPaymentEdit))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkBillPaymentEdit);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkBillPaymentEdit));
				UIFoundation.waitFor(2L);
			}
			creditCardCount=UIFoundation.getText(FinalReviewPage.btnPaymentLinksCount);
			int creditCardCountAfter=Integer.parseInt(creditCardCount.replaceAll("\\(|\\)", ""));
			log.info("The execution of the method verifySaveCrdeitCardInfo ended here ...");
			
			if((creditCardCntBefore+1==creditCardCountAfter)){
				objStatus+=true;
			     String objDetail="Credit card count is verified";
			     ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}else{
				objStatus+=false;
			    String objDetail="Credit card count is not verified";
			    UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			    
			}
			if (objStatus.contains("false")) {
				System.out.println("Verify the 'Save credit card info for next time' check box functionality, if checked test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the 'Save credit card info for next time' check box functionality, if checked  test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method verifySaveCrdeitCardInfo "
					+ e);
			return "Fail";
		}
	}

}
