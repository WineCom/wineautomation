package wine.autotest.mobile.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.mobile.pages.*;

public class VerifyGiftCardFunctionalityinPaymentSecofPickedSubscriptionFlow extends Mobile {
	
	/***********************************************************************************************************
	 * Method Name : AddGiftcard() 
	 * Created By  : Chandrashekar K B
	 * Purpose     : The purpose of this method is to add 2 Gift Cards in cart page
	 *               
	 * 
	 ************************************************************************************************************
	 */
	public static String AddGiftcard() {
		String objStatus = null;
		String screenshotName = "Scenarios_enrollForPickedUpWineandValidateGiftcard_Screenshot.jpeg";
		try {
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			String giftCert2=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate2", 1, giftCert2);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGiftCheckbox));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(6L);	
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtGiftNumber));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate2"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(6L);	
			if(UIFoundation.isDisplayed(CartPage.txtGiftCertificateSuccessMsg)){
				objStatus+=true;
				String objDetail="Multiple gift cards are applied succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Multiple gift cards are not applied";		
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if (objStatus.contains("false"))
			{				
				String objDetail="Adding GiftCard in Cart Page is Failed ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{
				String objDetail="Adding GiftCard in Cart Page  is Succesfull ";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}	
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
	
	/***********************************************************************************************************
	 * Method Name : enrollForPickedUpWineandValidateGiftcard() 
	 * Created By  : Chandrashekar K B
	 * Purpose     : The purpose of this method is to navigate compass tiles and 'Proceed to Enrollment' and 
	 *               Validate Giftcard in Recipient page
	 * 
	 ************************************************************************************************************
	 */
	public static String enrollForPickedUpWineandValidateGiftcard() {
		String objStatus = null;
		String screenshotName = "Scenarios_enrollForPickedUpWineandValidateGiftcard_Screenshot.jpeg";
		try {
			log.info("Execution of the method VerifyGiftCardFunctionalityinPaymentSecofPickedSubscriptionFlow started here .........");
				driver.get("https://qwww.wine.com/");
				UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.imgpicked));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.imgpicked));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(PickedPage.cmdSignUpCompass)) 
			{
				objStatus+=true;
				String objDetail="User is navigated to Picked offer page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Picked offer page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdSignUpCompass));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkNoVoice));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkRedOnly));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNtVry));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSelQtyNxt));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(PickedPage.txtRecipientPge)) 
			{
				objStatus+=true;
				String objDetail="User is navigated to Recipient page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Recipient page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(PickedPage.dwnReceiveState, "State"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			if((UIFoundation.isDisplayed(PickedPage.spnGiftcard1))&&(UIFoundation.isDisplayed(PickedPage.spnGiftcard2))) 
			{
				objStatus+=true;
				String objDetail="Two Applied Gift Cards are displayed in Payment section ";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Two Applied Gift Cards are Not displayed in Payment section ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			if (objStatus.contains("false"))
			{				
				String objDetail="VerifyGiftCardFunctionalityinPaymentSecofPickedSubscriptionFlow  test case failed ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{
				String objDetail="VerifyGiftCardFunctionalityinPaymentSecofPickedSubscriptionFlow test case executed succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}	
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
}
