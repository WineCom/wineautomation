package wine.autotest.mobile.test;


import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class ModalLightBoxWithXIcon extends Mobile {

	
	/***************************************************************************
	 * Method Name			: modalLightBoxWithXIcon()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String modalLightBoxWithXIcon() {
		String objStatus=null;
		   String screenshotName = "Scenarios_modalLightBoxWithXIcon_.jpeg";
					
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtSearchProduct));
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtSearchProduct, "lightBoxProduct"));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtSearchTypeListWill));
			UIFoundation.waitFor(6L);
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.txtAllVintages);
			UIFoundation.waitFor(1L);
			if(UIFoundation.isDisplayed(CartPage.txtAllVintages)){
				 objStatus+=true;
			      String objDetail="'All vintages' Link is displayed in PIP";
			      getlogger().pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}else{
				 objStatus+=false;
				 String objDetail="'All vintages' Link is not displayed in PIP";	
				 getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"RecPaddrs", objDetail);
			}
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.txtAllVintages));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnklightBoxClose));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnCarouselContentItem));
			UIFoundation.waitFor(3L);		
			if(UIFoundation.isDisplayed(CartPage.lnklightBoxCloseIcon)){
				UIFoundation.waitFor(3L);
				UIFoundation.escapeBtn();
				UIFoundation.waitFor(2L);
				if(!UIFoundation.isDisplayed(CartPage.lnklightBoxCloseIcon)){
					 objStatus+=true;
				      String objDetail="Modal/Light Box closes on clicking 'ESC' key when the Modal/Light box has ' X' icon";
				      getlogger().pass(objDetail);
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					
				}else{
					 objStatus+=false;
					   String objDetail="Modal/Light Box is not closed on clicking 'ESC' key when the Modal/Light box has ' X' icon";
					   getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
					   UIFoundation.captureScreenShot(screenshotpath+screenshotName+"RecPaddrs", objDetail);
				}
			}else{
				objStatus+=false;
				   String objDetail="Light Box close icon is not displayed";
				   getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"RecPaddrs", objDetail);
			       
			}
		
			UIFoundation.scrollUp();
			UIFoundation.scrollUp();
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCrtCount));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
				
			}
			else
			{
				
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}
	/***************************************************************************
	 * Method Name			: lightBoxInRecipientPage()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String lightBoxInRecipientPage() {
		String objStatus=null;
		   String screenshotName = "Scenarios__LightBoxWindow.jpeg";
					
		try {
			log.info("The execution of the method lightBoxInRecipientPage started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}	
			if(UIFoundation.isDisplayed(FinalReviewPage.spnRecipientChangeAddress)){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.spnRecipientChangeAddress));
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkShippingAddressEdit));
			UIFoundation.waitFor(2L);
			UIFoundation.clearField(FinalReviewPage.txtShippingAddressFullNameClear);
			UIFoundation.waitFor(2L);
			UIFoundation.clckObject(FinalReviewPage.btnShippingOptionalClk);

			objStatus+=String.valueOf(UIFoundation.setObjectEditShippingAddress(FinalReviewPage.txtShippingAddressFullNameEnter, "firstName"));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnModalWindowIcon)){
				UIFoundation.waitFor(3L);
				UIFoundation.escapeBtn();
				UIFoundation.waitFor(2L);
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkShippingAddressEdit)){
									      
				      objStatus+=true;            	  
				      String objDetail="Modal/Light Box closes on clicking 'ESC' key for the Modal/Light box with  ' X' icon";
	                   getlogger().pass(objDetail);
	                   ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}else{
								       
				       objStatus+=false;
				       String objDetail="Modal/Light Box does not closed on clicking 'ESC' key for the Modal/Light box with  ' X' icon";                 
	                   UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
	                   getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
	                   
				}
			}else{
				   objStatus+=false;
				   String objDetail="Light Box close icon is not displayed";			      
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"RecPaddrs", objDetail);
			}

			log.info("The execution of the method lightBoxInRecipientPage ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
				
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
		
			log.error("there is an exception arised during the execution of the method lightBoxInRecipientPage "+ e);
			return "Fail";
		}
	}	
}
