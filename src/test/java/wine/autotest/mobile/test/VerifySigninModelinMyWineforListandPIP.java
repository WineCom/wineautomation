package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifySigninModelinMyWineforListandPIP extends Mobile {
	


	/***************************************************************************
	 * Method Name : VerifyMyWineSignInNEditLinkForGiftMessage 
	 * Created By  : Ramesh S 
	 * Reviewed By : Chandrashekhar
	 * Purpose     : TM-4132
	 ****************************************************************************
	 */

	public static String signInModalForMyWineforListandPIP() {

		String objStatus = null;
		String screenshotName = "Scenarios_signInModalForMyWineforListandPIP_Screenshot.jpeg";
		

		try {
			log.info("sign In Modal For My Wine Page method started here........");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgObj_MyWine));
			if (UIFoundation.isElementDisplayed(CartPage.txtSignInTextR)
					&& UIFoundation.isElementDisplayed(CartPage.txtSignInBtnR)) {
				objStatus += true;
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnAllLinkCloseIcon));
						
				String objDetail = "Sign modal is displayed when clicks on Mywine ";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				
			} else {
								 
				 objStatus+=false;
				 String objDetail = "Sign modal is not displayed when clicks on Mywine";
				 ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				 getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);		
								 
			}
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgObj_MyWine));
			if (UIFoundation.isElementDisplayed(CartPage.txtSignInTextR)
					&& UIFoundation.isElementDisplayed(CartPage.txtSignInBtnR)) {
				objStatus += true;
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnAllLinkCloseIcon));						
				String objDetail = "Sign modal is displayed when clicks on Mywine badge in PIP";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
				
			} else {
								 
				 objStatus+=false;
				 String objDetail = "Sign modal is not displayed when clicks on Mywine badge in PIP";
				 ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				 getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);				
			}
			log.info("sign In Modal For My Wine Page method ended here........");

			if (objStatus.contains("false")) {

				System.out.println("signInModalForMyWineforListandPIP test case is failed");

				return "Fail";
			} else {
				System.out.println("signInModalForMyWineforListandPIP test case executed succesfully");

				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";

		}
	}

}
