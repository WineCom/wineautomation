package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyLikeDislikeSectionNotDisplayedinVaritalPreference extends Mobile {
	

	
	/***********************************************************************************************************
	 * Method Name : LikeDisLikeNotSelectedinVarietalPage() 
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar
	 * Purpose     : The purpose of this method is to Verify the Like/Dislike section is not displayed in the varietal preference review page. 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String LikeDisLikeNotSelectedinVarietalPage() {
		String objStatus = null;
		String screenshotName = "Scenarios_LikeDisLikeNotSelectedinVarietalPage_Screenshot.jpeg";
		System.getProperty("user.dir");
		try {
			log.info("Execution of the method verifyUserIsAbleToEnrollForSubscription started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedCompassTiles));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSignUpPickedWine));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNovice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineJourneyNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizWineTypRed));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineTypeNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnVarietalTypNxt));
			UIFoundation.waitFor(1L);
			if(!(UIFoundation.isDisplayed(PickedPage.spnlikedheadertext) && UIFoundation.isDisplayed(PickedPage.spnDislikedHeaderText))) {
				String objDetail="Liked or Disliked Varietal were not displayed";
				System.out.println(objDetail);
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				objStatus+=true;
			}else {
				String objDetail="Liked or Disliked Varietal were displayed";
				System.out.println(objDetail);				
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				objStatus+=false;
			}
			
			if(UIFoundation.isDisplayed(PickedPage.spnAnythingelseQuest)) {
				String objDetail="Anything else question is present in the varietal review page";
				System.out.println(objDetail);
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				objStatus+=true;
			}else {
				String objDetail="Anything else question is present in the varietal review page";
				System.out.println(objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				objStatus+=false;
			}
			
			
			if (objStatus.contains("false"))
			{
				String objDetail="Verify non availability of selected varietal test case failed";
				System.out.println(objDetail);
				logger.fail(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Verify non availability of selected varietal test case executed successfully";
				System.out.println(objDetail);
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
}
