package wine.autotest.mobile.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class OrderCreationWithSpecialProductsForExistingUser extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: specialProducts()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String specialProducts()
	{
		String objStatus=null;
		try
		{
			log.info("The execution of method productFilteration started here");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.linSpecialsProducts));
			UIFoundation.waitFor(2L);
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."GreenWine"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.linsmallprod));			
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIBusinessFlow.addproductstocart(2));		
			UIFoundation.waitFor(3L);
			
	
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lblShowOutOfStock);
			UIFoundation.scrollUp();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method productFilteration "+e);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String checkoutProcess()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String totalBeforeTax=null;
		String screenshotName = "Scenarios_Sorting_Screenshot.jpeg";
		try
		{
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary in CART page===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			

           if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(LoginPage.signInLinkAccount))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.signInLinkAccount));
				UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernameMobile"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50); 
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."lnkAddNewAddressLink"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			
/*		if(UIFoundation.isDisplayed(FinalReviewPage."shippingAddressEdit")){
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."shippingAddressEdit"));
			UIFoundation.waitFor(4L);
			UIFoundation.clearField(FinalReviewPage."StreetAddress");
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage."StreetAddress", "Address1"));
			UIFoundation.clearField(FinalReviewPage."City");
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage."City", "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage."shipState", "State"));
			UIFoundation.javaScriptClick(FinalReviewPage."shipState");
			UIFoundation.clearField(FinalReviewPage."ZipCode");
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage."ZipCode", "ZipCode"));
			UIFoundation.clearField(FinalReviewPage."PhoneNum");
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage."PhoneNum", "PhoneNumber"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."shippingAddressSave"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage."VerifyContinueButton"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."RecipientContinue"));
			UIFoundation.waitFor(5L);
			}*/
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangePayment));
				UIFoundation.waitFor(1L);
			}
			
         //   objStatus+=String.valueOf(ApplicationDependent.existingUserOrderCreation(driver));
			
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."PaymentCheckoutEdit"));
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."AddPayment"));
			UIFoundation.waitFor(1L);*/
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard)){
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);	
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			}
/*			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."PaymentCheckoutEdit"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."PaymentEdit"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage."PaymentCVV", "CardCvid"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."PaymentSaveButton"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage."PaymentContinue"));*/
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			/*if(!UIFoundation.isDisplayed(FinalReviewPage."MainNavButton")){
				driver.navigate().refresh();
				UIFoundation.waitFor(2L);
			}*/
			//objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."MainNavButton"));
			//UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage."MainNavTabSignOut");
			//UIFoundation.waitFor(1L);
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."MainNavAccountTab"));
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."YourOrder"));
			UIFoundation.waitFor(3L);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
			System.out.println(orderNum);
		//	System.out.println(UIFoundation.getText(driver,"OrderDate"));
			UIFoundation.getOrderNumber(orderNum);
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false"))
			{
				
				System.err.println("Order creation with special products for existing user  test case is failedProducts are not listed in Alphabatical order");
		        objStatus+=false;
		        String objDetail="\"Order creation with special products for existing user  test case is failed";
		        logger.fail(objDetail);
		        UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
		        
				System.out.println("Order creation with special products for existing user  test case is failed");
				return "Fail";
			}
			else
			{
				
				System.out.println("Order creation with special products for existing user test case is executed successfully");
		    	objStatus+=true;
		        String objDetail="Order creation with special products for existing user test case is executed successfully";
		        logger.pass(objDetail);
		        ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	        
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method checkoutProcess of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";
			
		}
	}

	
	
	
}
