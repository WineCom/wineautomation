package wine.autotest.mobile.test;



import java.io.IOException;
import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyWarningMsgForRemoveShipTodayProductFromCart extends Mobile {

	/***************************************************************************
	 * Method Name : addShipTodayProdTocrt() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addShipTodayProdTocrt() {

		String objStatus = null;
		String expectedWarningMsg = null;
		String actualWarningMsg = null;

		String screenshotName = "Scenarios_WarningMessage_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtPinotNoir));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnShipToday);
			UIFoundation.waitFor(2L);
			
			
			/*addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnSecondProductToCart);
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnSecondProductToCart));
			}

			UIFoundation.waitFor(1L);
			addToCart3 = UIFoundation.getText(ListPage.btnFourthProductToCart);
			if (addToCart3.contains("Add to Cart")) {
			//	UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnSixthProductToCart);
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFourthProductToCart));
			}*/
			UIFoundation.waitFor(4L);			
			objStatus+=String.valueOf(UIBusinessFlow.addproductstocart(1));		
						
			UIFoundation.waitFor(2L); 
			
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkSrt);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			expectedWarningMsg = verifyexpectedresult.onlyShipTodayWarningMsg;
			actualWarningMsg = UIFoundation.getText(ListPage.txtShipTodayWarningMsg);
			if (actualWarningMsg.contains(expectedWarningMsg)) {
				System.out.println(actualWarningMsg);
				System.out.println("Verify warning message for only shiptoday products  test case is executed successfully ");
				objStatus+=true;
				String objDetail="Actual and expected Warning message are same";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");

			} else {
				System.out.println(actualWarningMsg);
				System.err.println("Verify warning message for only shiptoday products  test case is failed ");
				objStatus+=false;
				String objDetail="Actual and expected Warning message are not same";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			return "Fail";
		}
	}

}