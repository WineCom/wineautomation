package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyProductsAreRemovedInMyWine extends Mobile {



	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */

	public static String login()
	{
		String objStatus=null;

		try
		{
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(6L); 
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "ratingUsername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.waitFor(6L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}

		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";

		}
	}
	/***************************************************************************
	 * Method Name			: VerifyProductsAreRemovedInMyWine()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: TM-4110
	 ****************************************************************************
	 */
	public static String verifyProductsAreRemovedInMyWine() {

		String objStatus=null;
		String screenshotName = "VerifyProductsAreRemovedInMyWine.jpeg";
		
		try {
			log.info("The execution of the method VerifyProductsAreRemovedInMyWine started here ...");
			
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));		
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkAddProductToPIP));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.spnAddToMyWine));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.imgObj_MyWine));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isElementDisplayed(CartPage.spnRemove)) 
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnRemove));
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnObjRemove));

			log.info("The execution of the method VerifyProductsAreRemovedInMyWine ended here ...");
			if ( objStatus.contains("false")) {				
				objStatus+=false;
				String objDetail="Verify Products Are Removed In MyWine test case is failed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);				
				return "Fail";
			} else {				
				objStatus+=true;
				String objDetail="Verify Products Are Removed In MyWine test case executed succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method getOrderDetails "
					+ e);

			return "Fail";

		}
	}

}
