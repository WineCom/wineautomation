package wine.autotest.mobile.test;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;


import wine.autotest.mobile.test.Mobile;;

public class VerifyChooseStateDialogPrompted extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		
		
		String screenshotName = "Scenarios_ChooseState_Screenshot.jpeg";
		
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(2L);
			
			UIFoundation.waitFor(4L);			
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			UIFoundation.waitFor(2L);

						
			boolean elementPresent=UIFoundation.isDisplayed(ListPage.cboChooseYourState);
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			if(elementPresent==true){
				
				 objStatus+=false;
				System.out.println("Verify 'Choose state' dailog is prompted test case is failed");
				getlogger().fail("Verify 'Choose state' dailog is prompted test case is failed");
				 String objDetail="Choose state is displayed";
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				return "Fail";
				
				
			}else{
				 objStatus+=true;
				 System.out.println("Verify 'Choose state' dailog is prompted test case is executed successfully");
					getlogger().pass("Verify 'Choose state' dailog is prompted test case is executed successfully");					
			      String objDetail="Choose state is not displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      return "Pass";
			}		
			
		
		} catch (Exception e) {
			return "Fail";
		}
	}
	
	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */

	public static String checkoutProcess() {

		String expected=null;
		String actual=null;
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String screenshotName = "Scenarios_checkoutProcess_Screenshot.jpeg";
		

		try {
			log.info("The execution of the method checkoutProcess started here ...");
			expected = verifyexpectedresult.placeOrderConfirmation;
			UIFoundation.waitFor(2L);			
						
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
	
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckoutButton, "Invisible", "", 50);
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			
			
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnChangeDeliveryDate));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkSelectShippingMethodOption));
			/*Shipping=UIFoundation.getFirstSelectedValue(FinalReviewPage.dwnSelectShippingMethod);
			expectedShippingMethodCharges= Integer.parseInt(Shipping.replaceAll("[^0-9]",""));*/
			UIFoundation.waitFor(2L);
			

			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}

			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}		
	

			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);
			}
		
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
					UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
					UIFoundation.waitFor(3L);
				}
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			}

			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
		

			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(5L);
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
			if(orderNum!="Fail")
			{
				objStatus+=true;
				String objDetail="Order number is placed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				getlogger().pass(objDetail);
				System.out.println("Order number is placed successfully: "+orderNum);
			}else
			{
				objStatus+=false;
				String objDetail="Order number is null and Order cannot placed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method checkoutProcess ended here ...");
			if (!expected.equalsIgnoreCase(actual) && objStatus.contains("false")) {
			
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method checkoutProcess "
					+ e);
			return "Fail";
		}
	}	
	


}
