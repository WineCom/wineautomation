package wine.autotest.mobile.test;

import java.io.IOException;


import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;


import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class OrderCreationWithWhiteWineForNewUSer extends Mobile {

	

	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		
		try {
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(2L);
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkvarietal));
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkChardonnayWine);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkChardonnayWine));
			UIFoundation.waitFor(3L);

			objStatus+=String.valueOf(UIBusinessFlow.addproductstocart(2));		
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.txtChardonnay);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.webDriverWaitForElement(ListPage.btnCartCount, "Clickable", "", 50);

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
		
	}
	
	
	/***************************************************************************
	 * Method Name : addNewCreditCard() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh 
	 * Purpose     : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		
		
		try {
			
			log.info("The execution of the method addNewCreditCard started here ...");
			//UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtNameOnCard);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth, "Month"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear, "Year"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
		/*	UIFoundation.javaScriptClick(FinalReviewPage. "BillingAndShippingCheckbox");
			UIFoundation.waitFor(3L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			   {
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			   }
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "PaymentCheckoutEdit"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "PaymentEdit"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage. "PaymentCVV", "CardCvid"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "PaymentSaveButton"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage. "PaymentContinue"));
			UIFoundation.waitFor(5L);*/
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
			if(orderNum!="Fail")
			 {
			      objStatus+=true;
			      String objDetail="Order number is placed successfully";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      getlogger().pass(objDetail);
			      System.out.println("Order number is placed successfully: "+orderNum);   
			 }else
			 {
			       objStatus+=false;
			       String objDetail="Order number is null and Order cannot placed";
			       getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			 }
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				System.out.println(" Order creation with White wine products for new user test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with White wine products for new user test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}

}