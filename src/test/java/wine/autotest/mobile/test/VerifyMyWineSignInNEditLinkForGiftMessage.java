package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyMyWineSignInNEditLinkForGiftMessage extends Mobile {



	/***************************************************************************
	 * Method Name : VerifyMyWineSignInNEditLinkForGiftMessage 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh,
	 * Purpose     : TM-4132
	 ****************************************************************************
	 */

	public static String signInModalForMyWinePage() {

		String objStatus = null;
		String screenshotName = "signInModalForMyWinePage.jpeg";
		
		
		try {
			log.info("sign In Modal For My Wine Page method started here........");
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnObjMyWine));
			if (UIFoundation.isElementDisplayed(LoginPage.txtSignInTextR)
					&& UIFoundation.isElementDisplayed(LoginPage.btnSignInBtnR)) {
				objStatus += true;
				String objDetail = "Sign modal is displayed when clicks on Mywie";
				getlogger().pass(objDetail);
				
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "Sign modal is not displayed when clicks on Mywie";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");				
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);

			}
			log.info("sign In Modal For My Wine Page method ended here........");

			if (objStatus.contains("false")) {

				System.out.println("signInModalForMyWinePage test case is failed");

				return "Fail";
			} else {
				System.out.println("signInModalForMyWinePage test case executed succesfully");

				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";

		}
	}
	/***************************************************************************
	 * Method Name : VerifyMyWineSignInNEditLinkForGiftMessage 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose     : TM-4139
	 ****************************************************************************
	 */
	
	public static String editOptionForGiftMessage() {

		String objStatus = null;
		String screenshotName = "editOptionForGiftMessage.jpeg";
		
		try {
			log.info("edit Option For Gift Message method started here.......");
			getDriver().navigate().refresh();
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "userGiftMessage"));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtCaberNet));
			
			UIFoundation.waitFor(4L);			
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			UIFoundation.waitFor(2L);
			
			
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnCartCount));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(5L);	
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				UIFoundation.waitFor(3L);	
			}
			
		//	objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkaddGiftMessage));
			UIFoundation.clearField(FinalReviewPage.txtRecipientEmail);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientEmail, "addsaveolyonce"));
			UIFoundation.clearField(FinalReviewPage.txtRecipientGiftMessage);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientGiftMessage, "giftMessage"));	
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.cboRecipientGiftAdd));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnReciptContinue));
			UIFoundation.waitFor(4L);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.btnRecipientedit)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientedit));
				objStatus += true;
				String objDetail = "Edit button is displayed next to gift message";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "Edit button is  not displayed next to gift message";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.clickObject(FinalReviewPage.imgNoGiftBag);
			UIFoundation.clickObject(FinalReviewPage.btnReciptContinue);
			log.info("edit Option For Gift Message method ended here.......");
			if (objStatus.contains("false")) {
				System.out.println("editOptionForGiftMessage test case is failed");
				return "Fail";
			} else {
				System.out.println("editOptionForGiftMessage test case  executed succesfully");
				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";
		}
	}
}

