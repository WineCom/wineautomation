package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyShippingaddress extends Mobile {
	


	/*
	 *****************************************************************************************
	 * Method Name : VerifyFedexDisplaysProperGoogleAdresses 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh.
	 * Purpose     : TM-3966
	 *******************************************************************************************
	 */

	public static String verifyHomePickUpLocationAddress() {

		String objStatus = null;
		String screenshotName = "verifyHomePickUpLocationAddress.jpeg";
		

		try {
			log.info("verifyHomePickUpLocationAddress Method Started Here");
			
			 UIFoundation.waitFor(3L);
			 if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
				{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
				}else {
					objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
				}
			 UIFoundation.waitFor(5L);
			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkShipToFedex));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCodeFedEx"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearchButton));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtShipToThisLocation));
			if(UIFoundation.isDisplayed(FinalReviewPage.txtFedexFirstName)) {
				String objDetail="Verified FedEx Address Form displayed for the First time while adding address";
				System.out.println(objDetail);
				getlogger().pass(objDetail);
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	    
			   
			}else {
				String objDetail="Verified FedEx Address Form displayed for the First time while adding address";
				System.out.println(objDetail);
			    ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			    getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			    UIFoundation.captureScreenShot(screenshotpath+screenshotName+"txtFedexFirstName", objDetail);			    
			    
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCodeFedEx"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtFedexSaveButton));
			UIFoundation.waitFor(8L);
			System.out.println("Adding address for Second time");
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddressLink));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkShipToFedex));
			UIFoundation.waitFor(2L);
			UIFoundation.clearField(FinalReviewPage.txtFedexZipCode);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCodeFedEx"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearchButton));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
			
			if(UIFoundation.isDisplayed(FinalReviewPage.txtFedexFirstName)) {
				String objDetail="Verified FedEx Address Form displayed for the second time while adding address";
				System.out.println(objDetail);
				getlogger().pass(objDetail);
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else {
				String objDetail="Verified FedEx Address Form displayed for the second time while adding address";
				System.out.println(objDetail);
			    ReportUtil.addTestStepsDetails(objDetail, "Fail", "");	
			    getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			    UIFoundation.captureScreenShot(screenshotpath+screenshotName+"FedexFirstName", objDetail);
			}
			log.info("verifyHomePickUpLocationAddress Method Ended Here");
			if (objStatus.contains("false")) {

				System.out.println("Verification of  Address test case failed");
				return "fail";
			} else {
				System.out.println("Verification of  Address test case passed");
				return "Pass";

			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method execution " + e);
			return "Fail";

		}
	}	
}

