package wine.autotest.mobile.test;

import org.openqa.selenium.By;
import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifySuggestedShippingAddressWidget extends Mobile {

	
	/***************************************************************************
	 * Method Name : shippingDetails() 
	 * Created By  : Chandrashekhar. 
	 * Reviewed By : Ramesh.
	 * Purpose : The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 */

	public static String verifySuggestedAddress() {
		String objStatus = null;
		 String screenshotName = "Scenarios_verifySuggestedAddress_Screenshot.jpeg";

		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			

             if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          
			UIFoundation.waitFor(3L);
			boolean radioButtonShipToHome=UIFoundation.isDisplayed(FinalReviewPage.cboShippingAddrRadioButtonShipToHome);
			if(radioButtonShipToHome)
			{
				objStatus += true;
				String objDetail = "Ship to Home or Address' radio button is checked by default";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Ship to Home or Address' radio button is checked by default: ");				
				
			}
			else
			{
				objStatus += false;
				String objDetail = "Ship to Home or Address' radio button is not checked by default";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);				
				
			}
			boolean radioButtonShipToFedex=UIFoundation.isDisplayed(FinalReviewPage.cboShippingAddrRadioButtonShipToFedex);
			if(radioButtonShipToFedex)
			{
				objStatus += true;
				String objDetail = "Ship to FedEx Pickup Location radio button is displayed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Ship to FedEx Pickup Location radio button is displayed: ");
				
			}
			else
			{
				objStatus += false;
				String objDetail = "Ship to FedEx Pickup Location radio button is not displayed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				
			}
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.javaScriptClick(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			boolean shippingAddressTitle=UIFoundation.isDisplayed(FinalReviewPage.txtShippingHeaderTitle);
			if(shippingAddressTitle)
			{
				System.out.println("'Please Verify shipping Address' title is displayed");
				
			}else
			{
				System.out.println("'Please Verify shipping Address' title is not displayed");
				
			}
			String borderHighlighted = getDriver().findElement(By.xpath("(//li[@class='userDataCard_listItem shippingAddress_listItem js-is-selected'])[1]")).getCssValue("border-style");
			if(borderHighlighted.contains("solid"))
			{
				System.out.println("Suggested address dialog box is highlighted");
			}else
			{
				System.out.println("Suggested address dialog box is not highlighted");
			}
			boolean radioButton=UIFoundation.isSelected(FinalReviewPage.cboShippingAddrRadioButton);
	
			if(radioButton)
			{
				System.out.println("Suggested address radio button is selected by default");
			}
			else
			{
				System.out.println("Suggested address radio button is not selected by default");
			}
			boolean originalAddrsRadioButton=UIFoundation.isSelected(FinalReviewPage.cboOriginalAddrsRadioButton);
			if(originalAddrsRadioButton==false)
			{
				System.out.println("'Use this Shipping Address' checkbox is available and unmarked by default");
			}
			else
			{
				System.out.println("Use this Shipping Address' checkbox is available and marked by default");
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {				
				
				String objDetail = "Verify suggested address dialog test case is failed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);		
				
				return "Fail";
			} else {
				
				
				String objDetail = "Verify suggested address dialog test case is executed successfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verify suggested address dialog test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}
	}	
}
