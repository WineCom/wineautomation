package wine.autotest.mobile.test;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;


public class ProductListFilteration extends Mobile{
	
	static Pattern pattern;
	static String arrTestData[];
	static String arrObjectMap[];
	static String expectedText;
	static String expected,actual;
	static String value;
	static int expectedItemCount[]={1519,253,217,1623,271,64};
	static int actualItemCount[]=new int[expectedItemCount.length];
	static boolean isObjectPresent=false;

	
	/***************************************************************************
	 * Method Name			: verifyOnlyThreeFiltersAreVisible()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,
	 * Purpose				: The purpose of this method is to filter the product 
	 * 						  list by selecting some product,region,rate and price 
	 * 						  and checking for the count
	 * TM-3428
	 ****************************************************************************
	 */
	
	public static String verifyOnlyThreeFiltersAreVisible()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_verifyOnlyThreeFiltersAreVisible_Screenshot.jpeg";
		try
		{
			log.info("The execution of method productFilteration started here");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkvarietal));
			UIFoundation.waitFor(2L);			
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkVarietal));
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkRegion));
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkRatingAndPrice));
		
			if (objStatus.contains("false"))
			{
				
				String objDetail = "Verify only 3 filters are visible by default on tablet/desktop list page test case is failed";				
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);				
				return "Fail";				
				
			}
			else
			{				
				String objDetail = "Verify only 3 filters are visible by default on tablet/desktop list page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verify only 3 filters are visible by default on tablet/desktop list page");				
				
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method verifyOnlyThreeFiltersAreVisible "+e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: verifyMoreFiltersElementsAreVisible()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * TM-3429
	 ****************************************************************************
	 */
	
	public static String verifyMoreFiltersElementsAreVisible(WebDriver driver)
	{
		String objStatus=null;
		try
		{
			log.info("The execution of method productFilteration started here");
			/*objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkmoreFilters));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkreviewedBy));
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.spnsizeAndType));
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkfineWine));*/
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkVintage));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnHide));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(!UIFoundation.isDisplayed(ListPage.lnkVintage));
		//	objStatus+= String.valueOf(ApplicationDependent.isObjectExistForList(driver));
			if (objStatus.contains("false"))
			{
				
				System.out.println("Verify all the available filters are visible on clicking More filters test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify all the available filters are visible on clicking More filters test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method verifyMoreFiltersElementsAreVisible "+e);
			return "Fail";
		}
	}
	
	
	/***************************************************************************
	 * Method Name			: productFilteration()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to filter the product 
	 * 						  list by selecting some product,region,rate and price 
	 * 						  and checking for the count
	 ****************************************************************************
	 */
	
	public static String productFilteration(WebDriver driver)
	{
		String objStatus=null;
		try
		{
			log.info("The execution of method productFilteration started here");
			objStatus+=String.valueOf(wine.autotest.fw.utilities.UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			wine.autotest.fw.utilities.UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkvarietal));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIBusinessFlow.ClickObjectItems(ListPage.lnkPinotNoir));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkRegion));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkRatingAndPrice));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkPrice));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkRating));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIBusinessFlow.ClickObjectItems(ListPage.lnkDone));
			UIFoundation.waitFor(8L);
			if (objStatus.contains("false"))
			{
				
				System.out.println("Product Filteration test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Product Filteration test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method productFilteration "+e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: verifyPagination()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,
	 * Purpose				: The purpose of this method is to verify the pagination
	 ****************************************************************************
	 */
	
	public static String verifyPagination()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_verifyPagination_Screenshot.jpeg";		
		
		try
		{
			log.info("The execution of method verify pagination started here");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkBordexBlends));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkShowOutOfStock));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkShowOutOfStock));
			UIFoundation.waitFor(2L);

			objStatus+= String.valueOf(UIFoundation.scrollDown());
			log.info("The execution of method verify pagination ended here");
			if (objStatus.contains("false"))
			{	
				String objDetail = "Verify pagination test case is failed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);				
				return "Fail";
			}
			else
			{
				String objDetail = "Verify pagination test case is executed successfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verify pagination test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method verifyPagination "+e);
			return "Fail";
		}
	}
}
