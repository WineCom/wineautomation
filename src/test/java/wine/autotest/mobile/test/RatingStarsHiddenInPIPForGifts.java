package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class RatingStarsHiddenInPIPForGifts extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: VerifyRatingStarsHiddenInPIPForGifts()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,
	 * Purpose				: 
	 * TM-3287
	 ****************************************************************************
	 */
	
	public static String VerifyRatingStarsHiddenInPIPForGifts()
	{
		String screenshotName = "Scenarios_RatingStar_Screenshot.jpeg";
		String objStatus=null;
		try
		{

			log.info("The execution of method VerifyRatingStarsHiddenInPIPForGifts started here");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnGiftMainTab));
			UIFoundation.waitFor(2L);
			
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linBirthdayGifts));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkGiftProdutcAddToPIP));
			UIFoundation.waitFor(2L);
			
			if(!UIFoundation.isDisplayed(ListPage.cboStarsRating) && !UIFoundation.isDisplayed(ListPage.lnkshare)){
				  objStatus+=true;
			      String objDetail="Rating Star and Share is not displayed in PIP page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      getlogger().pass(objDetail);
			      System.out.println("Rating Star and Share is not displayed in PIP page");   
			}else{
				 objStatus+=false;
				 String objDetail="Rating Star and Share is displayed on PIP page";
				 getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				 UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			
			if (objStatus.contains("false"))
			{
				
				System.out.println("Verify the 'share' & 'rating stars' are hidden in PIP of Gift products test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the 'share' & 'rating stars' are hidden in PIP of Gift products test case is executed successfully");
				return "Pass";
			}
		
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method VerifyRatingStarsHiddenInPIPForGifts "+e);
			return "Fail";
		}
	}


}
