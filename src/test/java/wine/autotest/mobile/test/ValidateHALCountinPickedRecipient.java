package wine.autotest.mobile.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.*;

public class ValidateHALCountinPickedRecipient extends Mobile {
	/***********************************************************************************************************
	 * Method Name : enrollForPickedUpWineandvalidateHAlcount() 
	 * Created By  : Ramesh S
	 * Purpose     : The purpose of this method is to navigate compass tiles and 'Proceed to Enrollment' and 
	 *               Vaidate HAL count in Recipient page
	 * 
	 ************************************************************************************************************
	 */
	public static String enrollForPickedUpWineandvalidateHAlcount() {
		String objStatus = null;
		String screenshotName = "Scenarios_enrollPickedandvalHALCountRcpnt_Screenshot.jpeg";
		try {
			log.info("Execution of the method ValidateHALCountinPickedRecipient started here .........");
			objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.imgpicked));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.imgpicked));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(PickedPage.cmdSignUpCompass)) 
			{
				objStatus+=true;
				String objDetail="User is navigated to Picked offer page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Picked offer page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdSignUpCompass));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkNoVoice));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkRedOnly));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNtVry));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSelQtyNxt));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(PickedPage.txtRecipientPge)) 
			{
				objStatus+=true;
				String objDetail="User is navigated to Recipient page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Recipient page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			String strHALCountRcpntPageExpMsg=verifyexpectedresult.HALCountRcpntPage;
			String strHALCountRcpntPageActMsg=UIFoundation.getText(PickedPage.txtHALCountrecpnt);
			strHALCountRcpntPageActMsg=strHALCountRcpntPageActMsg.replaceAll("\n","").replace("\r","");
			System.out.println("LocalPickupActMsg="+strHALCountRcpntPageActMsg);
			System.out.println("LocalPickupExpMsg="+strHALCountRcpntPageExpMsg);
			if(strHALCountRcpntPageActMsg.equalsIgnoreCase(strHALCountRcpntPageExpMsg))
			{
				objStatus+=true;
				String objDetail="HAL Count is Displayed in the Recipient Page ";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="HAL Count is Not Displayed in the Recipient Page ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}	
			if (objStatus.contains("false"))
			{				
				String objDetail="ValidateHALCountinPickedRecipient  test case failed ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{
				String objDetail="ValidateHALCountinPickedRecipient test case executed succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}			
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
}