

package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyBehaviourOfContinueButtonInRecipientPage extends Mobile {	

	
	/***************************************************************************
	 * Method Name : VerifyBehaviourOFContinueButtonInRecipientPage()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh. 
	 * Purpose : 
	 * TM-895,
	 ****************************************************************************
	 */

	public static String verifyBehaviourOFContinueButtonInRecipientPage() {
		String objStatus = null;
		   String screenshotName = "Scenarios_verifyBehaviourOFContinueButtonInRecipientPage_Screenshot.jpeg";
		  
		try {
			log.info("The execution of the method verifyBehaviourOFContinueButtonInRecipientPage started here ...");
			UIFoundation.waitFor(3L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(2L);
                    
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameRequiredInRecipientPage) && UIFoundation.isDisplayed(FinalReviewPage.txtStreetAddressRequired) && UIFoundation.isDisplayed(FinalReviewPage.txtCityRequired) && UIFoundation.isDisplayed(FinalReviewPage.txtStateRequiredInRecipientPage) && UIFoundation.isDisplayed(FinalReviewPage.txtZipRequiredInRecipientPage) && UIFoundation.isDisplayed(FinalReviewPage.txtPhoneRequired) )
			{
				  objStatus+=true;
			      String objDetail="Verified  the behavior of the Continue button, if any mandatory data is not entered in the Recipient page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      getlogger().pass(objDetail);
				  System.out.println("Verified  the behavior of the Continue button, if any mandatory data is not entered in the Recipient page");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the behavior of the Continue button, if any mandatory data is not entered in the Recipient page is failed";
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName+"RecipientAddress", objDetail);
			       getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				   System.err.println("Verify the behavior of the Continue button, if any mandatory data is not entered in the Recipient page is failed ");
			}

			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

}
