package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;

public class VerifyExpandedlearnaboutcontentisDispforListPginMobile extends Mobile {	

	
	/***************************************************************************
	 * Method Name			: VerifyExpandedContentForListpage()
	 * Created By			: Chandrashekhar K B
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String VerifyExpandedContentForListpage()
	{
		String objStatus=null;
		String screenshotName = "Scenarios__VerifyExpandedContentForListpage.jpeg";
		
		try
		{			
			getlogger().info("The execution of the method ratingStarsDisplayedProperlyInMyWine started here ...");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnExpandListContent));
			UIFoundation.waitFor(4L);
			if (UIFoundation.isDisplayed(ListPage.txtListContentExpanded)&&UIFoundation.isDisplayed(ListPage.btnMinimizeListContent)) {
				objStatus+=true;				
				String objDetail = "Expanded Learn about content is displayed with (X) icon";
				getlogger().pass(objDetail);
				} else {
					objStatus+=false;			
					String objDetail="Expanded Learn about content is Not displayed with (X) icon";
					getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
				}	
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMinimizeListContent));
			UIFoundation.waitFor(4L);
			if (!UIFoundation.isDisplayed(ListPage.txtListContentExpanded)&&UIFoundation.isDisplayed(ListPage.btnExpandListContent)) {
				objStatus+=true;				
				String objDetail = "Learn about content is collapsed and displayed with plus (+) icon ";
				getlogger().pass(objDetail);
				} else {
					objStatus+=false;			
					String objDetail="Learn about content is Not collapsed ";
					getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
				}	
			if (objStatus.contains("false")){			
				objStatus+=false;
				String objDetail="VerifyExpandedlearnaboutcontentisDispforListPginMobile Test case Failed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			} else {								
				objStatus+=true;
				String objDetail="VerifyExpandedlearnaboutcontentisDispforListPginMobile Test case is passed";
				getlogger().pass(objDetail);
				return "Pass";
			}	
		} catch(Exception e) {	
			getlogger().error("there is an exception arised during the execution of the method VerifyExpandedContentForListpage "+ e);
			return "Fail";	
		}
	}	
}
