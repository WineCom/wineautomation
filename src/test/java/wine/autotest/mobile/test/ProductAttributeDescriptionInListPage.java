package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class ProductAttributeDescriptionInListPage extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: aboutProffessionalandProductAttributeDescriptionInPIP()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String productAttributeDescriptionInListPage()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_productAttributeInList.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		try
		{
			log.info("The execution of the method productAttributeDescriptionInListPage started here ...");
			
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtCaberNet));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkmainEntityOfPage));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.txtProductAttributeName));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(CartPage.txtProductAttributeTitle) && UIFoundation.isDisplayed(CartPage.txtProductAttributeContent))
			{
				objStatus+=true;
				String objDetail="A popup with explanations of the icons that a given product is associated with, is displayed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				getlogger().pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="A popup with explanations of the icons that a given product is associated with, is not displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			log.info("The execution of the method productAttributeDescriptionInListPage ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method productAttributeDescriptionInListPage "+ e);
			return "Fail";
			
		}
	}	
}
