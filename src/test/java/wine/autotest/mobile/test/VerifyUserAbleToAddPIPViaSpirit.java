package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyUserAbleToAddPIPViaSpirit extends Mobile {	

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
		
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "ratingUsername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: RatingStarsDisplayedProperlyInMyWine()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String ratingStarsDisplayedProperlyInMyWine()
	{
		String objStatus=null;
		
		   String screenshotName = "Scenarios__ratingStarMyWine.jpeg";									
		
		try
		{			
			log.info("The execution of the method ratingStarsDisplayedProperlyInMyWine started here ...");
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			UIFoundation.waitFor(1L);			
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.MainNavAccSignIn));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.spnRareProducts));
			UIFoundation.waitFor(1L);	
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.spnRareProducts));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkPipProduct));	
			UIFoundation.waitFor(1L);		
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgUserRating));	
			UIFoundation.waitFor(1L);		
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgObj_MyWine));
					
			UIFoundation.waitFor(2L);	
			/*objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.imgMyWineUserRating));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgMyWineUserRating));		
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgRatingStarTextArea));			
			objStatus+=String.valueOf(UIFoundation.isElementDisplayed(ListPage.txtTextAreaCount));
			objStatus+=String.valueOf(UIFoundation.isElementDisplayed(ListPage.btnCancelInMyWine));
			objStatus+=String.valueOf(UIFoundation.isElementDisplayed(ListPage.btnSaveInMyWine));*/
			
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstProdNameLink));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.imgClearStarRating));
									
			
			log.info("The execution of the method ratingStarsDisplayedProperlyInMyWine ended here ...");
			if (objStatus.contains("false"))
			{
							
				objStatus+=false;
				 String objDetail="Verify the 'Rating stars' are displayed properly  on clicking the rating stars for already rated product in My Wine test case is failed";
				  ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				  getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				   UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
			else
			{								
				objStatus+=true;
				String objDetail="Verify the 'Rating stars' are displayed properly  on clicking the rating stars for already rated product in My Wine test case is executed successfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method ratingStarsDisplayedProperlyInMyWine "+ e);
			return "Fail";
			
		}
	}	
}
