package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;;

public class CreditCardInfoIsUpdatedInPaymentMethods extends Mobile {

	
	/***************************************************************************
	 * Method Name			: addPaymentMethod()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 ****************************************************************************
	 */

	public static String addPaymentMethod()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios__PaymentMethod.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
							+ screenshotName;
		try
		{
			log.info("The execution of the method addAddress started here ...");
			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			//objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavTabSignOut"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.txtPaymentMethods));
			UIFoundation.webDriverWaitForElement(ThankYouPage.lnkCreditCardEdit, "Clickable", "", 50);
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkCreditCardEdit));
			UIFoundation.waitFor(2L);
			UIFoundation.clearField(ThankYouPage.txtEnterNameField);
			UIFoundation.waitFor(2L);
		
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtEditCVVField, "CardCvid"));
			String expectedEditedName=UIFoundation.setObjectEditShippingAddress(ThankYouPage.txtEditNameField, "firstName");
			expectedEditedName=expectedEditedName.replaceAll(" ", "");
			UIFoundation.scrollDownOrUpToParticularElement(ThankYouPage.btnSaveCreditCard);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.btnSaveCreditCard));
			UIFoundation.waitFor(1L);
			String actualEditedName=UIFoundation.getText(ThankYouPage.txtUserPayementName);
			actualEditedName=actualEditedName.replaceAll(" ", "");
			if(actualEditedName.equalsIgnoreCase(expectedEditedName))
			{
				  objStatus+=true;
			      String objDetail="Verified that user is able to update Credit Card info in Payment methods";
			      getlogger().pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify that user is able to update Credit Card info in Payment methods  is failed";	
				   getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"addressBook", objDetail);
				  
			}
			log.info("The execution of the method addAddress ended here ...");	
			if (objStatus.contains("false") && (!UIFoundation.isDisplayed(ThankYouPage.txtPaymentMethodsHeader)))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method addPaymentMethod "+ e);
			return "Fail";
		}
	}
	
}
