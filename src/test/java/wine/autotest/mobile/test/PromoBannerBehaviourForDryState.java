package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class PromoBannerBehaviourForDryState extends Mobile {
	

	/***************************************************************************
	 * Method Name			: PromoBannerBehaviourForDryState()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String promoBannerBehaviourForDryState()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_promoBannerBehaviourForDryState.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
			String stateName=null;
		try
		{
			log.info("The execution of the method promoBannerBehaviourForDryState started here ...");
			UIFoundation.SelectObject(ListPage.cboSelectState, "dryState");
			UIFoundation.waitFor(10L);
			String expectedPromoBanner = verifyexpectedresult.dryStatePromoBar;
			String actualPromoBanner=UIFoundation.getText(ListPage.TxtPromoBannerDry);
			stateName=actualPromoBanner.substring(49);
			if(actualPromoBanner.contains(expectedPromoBanner))
			{
				objStatus+=true;
				String objDetail="Due to state regulations, we cannot ship wine to" +stateName+". message is displayed.";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Due to state regulations, we cannot ship wine to" +stateName+". message is not displayed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}


			log.info("The execution of the method promoBannerBehaviourForDryState ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method superscriptInProductPrice "+ e);
			return "Fail";
			
		}
	}	
	
}
