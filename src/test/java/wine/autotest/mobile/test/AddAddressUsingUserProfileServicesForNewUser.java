package wine.autotest.mobile.test;
import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class AddAddressUsingUserProfileServicesForNewUser extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: addAddress()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 ****************************************************************************
	 */

	public static String addAddress()
	{
		String objStatus=null;
		
		String screenshotName = "Scenarios_AddressBook_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try
		{
			log.info("The execution of the method addAddress started here ...");
		
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnMainNavAccTab);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavAccTab));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddressBook));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewAddress));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtAddressFullName, "fullName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "dryAddress"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "dryCity"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "dryState"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "dryZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnAddressSave);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnAddressSave));
			UIFoundation.waitFor(3L);
		
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnVerifyContinueButton);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinueButton));
			UIFoundation.waitFor(14L);
			log.info("The execution of the method addAddress ended here ...");	
			
			if(UIFoundation.isDisplayed(FinalReviewPage.txtAddressBookHeader)){
			      objStatus+=true;
			      String objDetail="Address Book Header is displayed";
			      getlogger().pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
			      objStatus+=false;
			      String objDetail="Address Book Header is not displayed";
			      getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			      UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false"))
			{
				System.out.println("Adding address using user profile services for new user test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Adding address using user profile services for new user test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{			
			log.error("there is an exception arised during the execution of the method addAddress "+ e);
			return "Fail";
		}
	}


	
}
