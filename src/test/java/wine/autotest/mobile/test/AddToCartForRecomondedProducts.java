package wine.autotest.mobile.test;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class AddToCartForRecomondedProducts extends Mobile {
	

	
	
	/***************************************************************************
	 * Method Name			: verifyAddToCartButtonForRecommendedProducts()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * TC ID				: TM-289
	 ****************************************************************************
	 */
	
	
	public static String verifyAddToCartButtonForRecommendedProducts()
	{
		String objStatus = null;
		  String screenshotName = "Scenarios_twoGiftCert_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
			ArrayList<String> arrayList= new ArrayList<String>();	
		try {
		    log.info("The execution of the method verifyAddToCartButtonForRecommendedProducts started here ...");
					
			if(!UIFoundation.isDisplayed(ListPage.btnMainNavButton))
			 {
			    getDriver().navigate().refresh();
			    UIFoundation.waitFor(2L);
			 }
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkPipProduct);
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkPipProduct));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgRateProduct));
			UIFoundation.waitFor(2L);			
			objStatus += String.valueOf(UIFoundation.isDisplayed(CartPage.spnAddRecommendedProducts));		
		
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.spnAddRecommendedProducts));
			UIFoundation.waitFor(1L);
			String recProduct=UIFoundation.getText(CartPage.spnRecommendedProduct);				
			
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnAddToCartAlert);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.btnAddToCartAlert));
			UIFoundation.waitFor(2L);	
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.spnUserRating));
			UIFoundation.waitFor(1L);	
			objStatus += String.valueOf(UIFoundation.mouseHover(CartPage.spnMouseOveruserRating));
			UIFoundation.waitFor(1L);	
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.imgClearStarRating));
			UIFoundation.waitFor(2L);	
			
		//	UIFoundation.scrollDownOrUpToParticularElement(driver, "AddToCartButton");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnAddToCart));
			UIFoundation.waitFor(2L);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(2L);							
			java.util.List<WebElement> wel=	getDriver().findElements(By.xpath("(//span[@class='prodItemInfo_name'])"));
			for(int i=0;i<=wel.size()-1;i++)
			{
				String st=wel.get(i).getText();			
				arrayList.add(st);				
			}
			if(arrayList.contains(recProduct))
			{				
				objStatus+=true;
				String objDetail="Add to cart Button is displayed and user is able to add the recommended products to the Cart successfully " ;
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} 
			else
			{				
				objStatus+=false;
				String objDetail="Add to cart Button is not displayed and user is not able to add the recommended products to the Cart " ;
				ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
						
			log.info("The execution of the method verifyAddToCartButtonForRecommendedProducts ended here ...");
			
			if (objStatus.contains("false")) {
				
				return "Fail";
				
			} else {
				return "Pass";			}			
		
		} catch (Exception e) {
			return "Fail";
		}
	}	
	
	
	/***************************************************************************
	 * Method Name			: rateTheStarsAndVerifyRecommendedProducts()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * TC ID				: TM-292
	 ****************************************************************************
	 */
	

	public static String rateTheStarsAndVerifyRecommendedProducts()
	{
		String objStatus = null;
		  try {
			log.info("The execution of the method rateTheStarsAndVerifyRecommendedProducts started here ...");
			
					
			getDriver().get("https://qwww.wine.com/product/substance-cabernet-sauvignon-2016/414487");
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgRateProduct));

	//		objStatus += String.valueOf(UIFoundation.clickObject(driver, "rateProduct"));
			UIFoundation.waitFor(2L);			
			objStatus += String.valueOf(UIFoundation.isDisplayed(CartPage.spnAddRecommendedProducts));		
		
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.spnAddRecommendedProducts));
			UIFoundation.waitFor(1L);
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.imgClearStarRating));
			UIFoundation.waitFor(2L);
			
			log.info("The execution of the method rateTheStarsAndVerifyRecommendedProducts ended here ...");
			
			if (objStatus.contains("false")) {
				System.out.println("Recommended products is not displayed beneath the rating stars");
				return "Fail";
				
			} else {
				System.out.println("Recommended products is  displayed beneath the rating stars");
				return "Pass";
			}
			
		
		} catch (Exception e) {
			return "Fail";
		}
	}	

	
	
	/***************************************************************************
	 * Method Name			: verifyTheCumuletiveRatingChanges()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * TC ID				: TM-3648
	 ****************************************************************************
	 */
	

	public static String verifyTheCumuletiveRatingChanges(WebDriver driver)
	{
		String objStatus = null;
		  System.getProperty("user.dir");
		try {
			log.info("The execution of the method rateTheStarsAndVerifyRecommendedProducts started here ...");
			
			if(!UIFoundation.isDisplayed(ListPage.btnMainNavButton))
			 {
			    driver.navigate().refresh();
			    UIFoundation.waitFor(2L);
			 }
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkPipProduct);
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkPipProduct));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgRateProduct));
			UIFoundation.waitFor(1L);	
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgRateProduct4));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.imgRatedProduct4));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgRateProduct2));
			UIFoundation.waitFor(1L);			
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgRatedProduct2));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(CartPage.spnAddRecommendedProducts));		
		
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.spnAddRecommendedProducts));
			UIFoundation.waitFor(1L);		
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnAddToCartAlert);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.btnAddToCartAlert));
			UIFoundation.waitFor(1L);	
			objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.spnUserRating));
			objStatus += String.valueOf(UIFoundation.mouseHover(CartPage.spnUserRating));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.imgClearStarRating));
		
			
			log.info("The execution of the method rateTheStarsAndVerifyRecommendedProducts ended here ...");
			
			if (objStatus.contains("false")) {
				System.out.println("Recommended products is not displayed beneath the rating stars");
				return "Fail";
				
			} else {
				System.out.println("Recommended products is  displayed beneath the rating stars");
				return "Pass";
			}
			
		
		} catch (Exception e) {
			return "Fail";
		}
	}
}
