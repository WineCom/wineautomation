/*package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.AddProductToCartPage;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.OrderCreationPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyFunctionalityOfDryStateSaveItemsForLater extends Mobile {	
	
	
	*//***************************************************************************
	 * Method Name : login() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh.
	 * Purpose     : 
	 ****************************************************************************
	 *//*
	
	public static String login(WebDriver driver)
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try
		{
			
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(6L); 
			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "dryUserName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50);
			String actualtile=driver.getTitle();
			String expectedTile = verifyexpectedresult.homePageTitle;
			
			if(actualtile.equalsIgnoreCase(expectedTile))
			{
				objStatus+=true;
			}else
			{
				objStatus+=false;
		    	String objDetail="Failed to login";
		    	UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
		    //	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to login";
	    	UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
	   // 	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}

	static ArrayList<String> productsInCart = new ArrayList<String>();
	static ArrayList<String> productsInSaveForLater = new ArrayList<String>();

	*//***************************************************************************
	 * Method Name : orderSummaryForNewUser() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 *//*
	public static String productDetailsPresentInCart(WebDriver driver) {
		String products1 = null;
		String products2 = null;
		String products3 = null;
		String products4 = null;
		String products5 = null;

		try {

			System.out.println("=======================Products added in to the cart  =====================");
			if (!UIFoundation.getText(driver, "FirstProductInCart").contains("Fail")) {
				products1 = UIFoundation.getText(driver, "FirstProductInCart");
				System.out.println("1) " + products1);
				productsInCart.add(products1);

			}

			if (!UIFoundation.getText(driver, "SecondProductInCart").contains("Fail")) {
				products2 = UIFoundation.getText(driver, "SecondProductInCart");
				System.out.println("2) " + products2);
				productsInCart.add(products2);

			}

			if (!UIFoundation.getText(driver, "ThirdProductInCart").contains("Fail")) {
				products3 = UIFoundation.getText(driver, "ThirdProductInCart");
				System.out.println("3) " + products3);
				productsInCart.add(products3);

			}

			if (!UIFoundation.getText(driver, "FourthProductInCart").contains("Fail")) {
				products4 = UIFoundation.getText(driver, "FourthProductInCart");
				System.out.println("4) " + products4);
				productsInCart.add(products4);

			}

			if (!UIFoundation.getText(driver, "FifthProductInCart").contains("Fail")) {
				products5 = UIFoundation.getText(driver, "FifthProductInCart");
				System.out.println("5) " + products5);
				productsInCart.add(products5);

			}
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}

	*//***************************************************************************
	 * Method Name : shippingDetails() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 *//*

	public static String dryStateShippingDetails(WebDriver driver) {
		String objStatus = null;
		String prodInSaveForLater = null;
		int productCount = 0;
		
		String screenshotName = "Scenarios_SaveItems_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method dryStateShippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			if(UIFoundation.isDisplayed(driver, "CheckoutButton"))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "CheckoutButton"));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "obj_Checkout"));
			}
                          UIFoundation.waitFor(2L);
                          objStatus+=String.valueOf(ApplicationDependent.recipientEdit(driver));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "selectRecipientAdr"));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(driver, "shippingAddressEdit")){
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "shippingAddressEdit"));
			UIFoundation.waitFor(4L);
			UIFoundation.clearField(driver, "StreetAddress");
			objStatus += String.valueOf(UIFoundation.setObject(driver, "StreetAddress", "Address1"));
			UIFoundation.clearField(driver, "City");
			objStatus += String.valueOf(UIFoundation.setObject(driver, "City", "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(driver, "shipState", "State"));
			UIFoundation.javaScriptClick(driver, "shipState");
			UIFoundation.clearField(driver, "ZipCode");
			objStatus += String.valueOf(UIFoundation.setObject(driver, "ZipCode", "ZipCode"));
			UIFoundation.clearField(driver, "PhoneNum");
			objStatus += String.valueOf(UIFoundation.setObject(driver, "PhoneNum", "PhoneNumber"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "shippingAddressSave"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "VerifyContinueButton"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "RecipientContinue"));
			UIFoundation.waitFor(5L);
			}
			if(UIFoundation.isDisplayed(driver, "RecipientEidt"))
			{
				UIFoundation.scrollDownOrUpToParticularElement(driver, "RecipientEidt");
				objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "RecipientEidt"));
				UIFoundation.waitFor(1L);
			}

			objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "AddNewAddressLink"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "ShippingTyp"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(driver, "FirstName", "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(driver, "LastName", "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(driver, "StreetAddress", "dryAddress"));
			objStatus += String.valueOf(UIFoundation.setObject(driver, "City", "dryCity"));
			objStatus += String.valueOf(UIFoundation.SelectObject(driver, "obj_State", "dryState"));
			objStatus += String.valueOf(UIFoundation.setObject(driver, "obj_Zip", "dryZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(driver, "PhoneNum", "PhoneNumber"));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(driver, "ShipContinueButton");
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "ShipContinueButton"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "SuggestedAddress"));
			UIFoundation.waitFor(5L);
			UIFoundation.scrollDownOrUpToParticularElement(driver, "VerifyContinueButton");
			if(UIFoundation.isDisplayed(driver, "VerifyContinueButton"))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(driver, "VerifyContinueButton"));
				UIFoundation.waitFor(10L);
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(driver, "VerifyContinueButtonShipRecpt");
			if(UIFoundation.isDisplayed(driver, "VerifyContinueButtonShipRecpt"))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(driver, "VerifyContinueButtonShipRecpt"));
				UIFoundation.webDriverWaitForElement(driver, "VerifyContinueButtonShipRecpt", "Invisible", "", 50);
			}
			
						
		//	objStatus += String.valueOf(UIFoundation.clickObject(driver, "VerifyContinueButton"));
		//	UIFoundation.waitFor(12L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "ShopInKyState"));
			UIFoundation.webDriverWaitForElement(driver, "ContinueShopping", "Clickable", "", 50);
			productCount = Integer.parseInt(UIFoundation.getText(driver, "CartCount"));
			UIFoundation.waitFor(5L);
			boolean status = UIFoundation.isDisplayed(driver, "ContinueShopping");
			UIFoundation.waitFor(3L);
			int saveForLaterCount = Integer.parseInt(UIFoundation.getText(driver, "SaveForLaterCount"));
			for (int i = 1; i <= saveForLaterCount; i++) {
				prodInSaveForLater = driver.findElement(By.xpath("//section[@class='saveForLater']/ul/li[" + i + "]/div[2]/div[3]/a/span")).getText();
						
				productsInSaveForLater.add(prodInSaveForLater);
				UIFoundation.waitFor(1L);
			}
			if ((productCount == 0 ) && productsInSaveForLater.containsAll(productsInCart)) {
				System.out.println(
						"Verify the functionality of �Save items for later� in state change dialog test case executed successfully");
				  objStatus+=true;
			      String objDetail="Functionality of �Save items for later� is verified";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			
			} else {
				System.out.println(
						"Verify the functionality of �Save items for later� in state change dialog test case is failed");

			       objStatus+=false;
			       String objDetail="Functionality of �Save items for later� is not verified";
			       UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			}
			
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "ContinueShopping"));
			UIFoundation.waitFor(2L);
			String expectedPAgeTitle=objExpectedRes.getProperty("homePageTitle");
			String actualHomePageTitle=driver.getTitle();
			if (expectedPAgeTitle.equalsIgnoreCase(actualHomePageTitle)) {
				
				  objStatus+=true;
			      String objDetail="Verified user is navigated to home page, on clicking 'Continue Shopping' button when the cart is empty";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verified user is navigated to home page, on clicking 'Continue Shopping' button when the cart is empty");
			} else {
				 objStatus+=false;
			       String objDetail="Verify user is navigated to home page, on clicking 'Continue Shopping' button when the cart is empty is failed";
			       UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
				System.out.println("Verify  user is navigated to home page, on clicking 'Continue Shopping' button when the cart is empty is failed");
			}
			
			log.info("The execution of the method dryStateShippingDetails ended here ...");


			log.info("The execution of the method dryStateShippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method dryStateShippingDetails " + e);
			return "Fail";
		}

	}


}
*/