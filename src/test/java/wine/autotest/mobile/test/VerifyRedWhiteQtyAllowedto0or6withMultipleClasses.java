package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyRedWhiteQtyAllowedto0or6withMultipleClasses extends Mobile {
	
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{			
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(1L);		
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "compassUser"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50);
			String actualtile=getDriver().getTitle();			
			String expectedTile = verifyexpectedresult.shoppingCartPageTitle;	
			if(actualtile.equalsIgnoreCase(expectedTile))
			{
				objStatus+=true;
			}else
			{
				objStatus+=false;
		    	String objDetail="Failed to login";
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to login";
	    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}

	
	/***********************************************************************************************************
	 * Method Name : VerifyRedWhiteQtySetto0or6() 
	 * Created By  : Ramesh S
	 * Reviewed By : Chandrashekhar
	 * Purpose     : The purpose of this method is to Verify the user able to set Red/White Quantity to 0 ro 6.' 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String VerifyRedWhiteQtySetto0or6() {
		String objStatus = null;
		String screenshotName = "Scenarios_VerifyRedWhiteQtySetto0or6_Screenshot.jpeg";
		System.getProperty("user.dir");
		try {
			log.info("Execution of the method VerifyRedWhiteQtySetto0or6 started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedCompassTiles));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSignUpPickedWine));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNovice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineJourneyNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizWineTypRedWhite));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineTypeNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnVarietalTypNxt));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoChardonnaylLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoSauvignonBlancDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnVarietalTypWhiteNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
			UIFoundation.waitFor(4L);	
			if(UIFoundation.isDisplayed(PickedPage.rdoPickedQuizNtVry)) {
				objStatus+=true;
				String objDetail="User is navigated to adventurous quiz next page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="User is not navigated to adventurous quiz next page";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizVery));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.btnMnthTypNxt)) {
				objStatus+=true;
				String objDetail="User is navigated to Month select quiz next page";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="User is not navigated to Month select quiz nexr page";
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.txtSubscriptionSettingsPge)) {
				objStatus+=true;
				String objDetail="User is navigated to Subscription Settings quiz next page";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="User is not navigated to Subscription Settings quiz next page";
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAddRedBottleCount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAddRedBottleCount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAddRedBottleCount));
			UIFoundation.waitFor(1L);
			String strRedBottleCount = UIFoundation.getText(PickedPage.spnRedCount);
			if(strRedBottleCount!=null) {
			ReportUtil.addTestStepsDetails("Increased Red Bottle Count "+strRedBottleCount, "Pass", "");
			}else {
				ReportUtil.addTestStepsDetails("Red Bottle Count Not Increased "+strRedBottleCount, "Fail", "");
			}
			
			Integer.parseInt(strRedBottleCount);
			//White Wine calculation
			UIFoundation.waitFor(1L);
			String strWhiteBottleCount = UIFoundation.getText(PickedPage.spnWhiteCount);
			if(strWhiteBottleCount!=null) {
			ReportUtil.addTestStepsDetails("Decreased  White Bottle Count "+strWhiteBottleCount, "Pass", "");
			}else {
				ReportUtil.addTestStepsDetails("White Bottle Count Not Decreased "+strWhiteBottleCount, "Fail", "");
			}
			System.out.println("strRedBottleCount :"+strRedBottleCount);
			System.out.println("strWhiteBottleCount :"+strWhiteBottleCount);
			Integer.parseInt(strWhiteBottleCount);
			if(strRedBottleCount.contains("6") && strWhiteBottleCount.contains("0")) {
				objStatus+=true;
				String objDetail="The Red/White Bottle count can be set to 0 and 6";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println(objDetail);
			}else {
				objStatus+=false;
				String objDetail="The Red/White Bottle count can not be set to 0 and 6";
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false"))
			{
				String objDetail="Validate the user able to set Red/White Bottel count to 0 or 6 test case failed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Validate the user able to set Red/White Bottel count to 0 or 6 test case executed successfully";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}	
}
