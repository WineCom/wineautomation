package wine.autotest.mobile.test;

import java.io.IOException;
import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyWarningMessageForShipTodayProductsAndListProducts extends Mobile {
	


	/***************************************************************************
	 * Method Name : addShipTodayProdTocrt() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addShipTodayProdTocrt() {
		String objStatus = null;
		String expectedWarningMsg = null;
		String actualWarningMsg = null;
		
		String screenshotName = "Scenarios_WarningMessage_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			UIFoundation.waitFor(5L);
            //UIFoundation.SelectObject(driver, "SelectState", "floridaState");
           // UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.javaSriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFeaturedMainTab));
            objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnksmallprod));

			/*addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProductToCart));
			}

			addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSecondProductToCart));
			}
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnThirdProductToCart);
			addToCart3 = UIFoundation.getText(ListPage.btnThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.javaSriptClick(ListPage.btnThirdProductToCart));
			}
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnFourthProductToCart);
			addToCart4 = UIFoundation.getText(ListPage.btnFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.javaSriptClick(ListPage.btnFourthProductToCart));
			}
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnFifthProductToCart);
			addToCart4 = UIFoundation.getText(ListPage.btnFifthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.javaSriptClick(ListPage.btnFifthProductToCart));
			}*/
            
            objStatus+=String.valueOf(UIBusinessFlow.addproductstocart(2));	
            UIFoundation.waitFor(2L);	
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.txtCabernetSauvignon);
			UIFoundation.scrollUp();
			UIFoundation.waitFor(2L);
						
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(8L);
			
			expectedWarningMsg = verifyexpectedresult.shiptodayandlistproducts;
		//	expectedWarningMsg = objExpectedRes.getProperty("shiptodayandlistproducts");
			actualWarningMsg = UIFoundation.getText(ListPage.txtShipTodayWarningMsg);
			
			if (actualWarningMsg.contains(expectedWarningMsg)) {
				System.out.println(actualWarningMsg);
				System.out.println(
						"Verify warning message for shiptoday products and others products test case is executed successfully ");
				objStatus+=true;
				String objDetail="Actual and expected Warning message are same";
				getlogger().pass(objDetail);
			    
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			
			} else {
				System.out.println(actualWarningMsg);
				objStatus+=false;
			    String objDetail="Actual and expected Warning message are not same";
				System.err.println(
						"Verify warning message for shiptoday products and others products test case is failed ");
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				
				
			   
			    UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			
			UIFoundation.clickObject(ListPage.imgWineLogo);
			UIFoundation.SelectObject(ListPage.cboSelectState, "State");
            UIFoundation.waitFor(10L);
            
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			return "Fail";
		}
	}


	
	

}