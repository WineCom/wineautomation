package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class ValidateTheSubscriptionFlowForHomeAddress extends Mobile {
	

	
	/******************************************************************************************
	 * Method Name : verifyHomeAddrFlowForPickedUpWine
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh
	 * Purpose     : This method validates local pickup address with picked up wine
	 * 
	 ******************************************************************************************/
		
		public static String verifyHomeAddrFlowForPickedUpWine() {
			
		String objStatus = null;
		String screenshotName = "Scenarios_verifyLocalPickUpFlowForPickedUpWine_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method verifyHomeAddrFlowForPickedUpWine started here .........");
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(PickedPage.btnRecipientHeader)) {
								
				objStatus+=true;
				String objDetail="Subscription flow 'Recipient' page is displayed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{								  
				  objStatus+=false;
				  String objDetail="Subscription flow 'Recipient' page should is not displayed";
				  ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				  getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				  UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}		
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.radShippingTyp));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(PickedPage.dwnShipToState, "State"));
			UIFoundation.clickObject(PickedPage.dwnShipToState);		
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.btnShipContinue));	     
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(PickedPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(PickedPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(PickedPage.btnDeliveryContinue, "Invisible", "", 70);
			}
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(PickedPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(PickedPage.txtFedexBillingAddrss)) {
				//	objStatus+=String.valueOf(UIFoundation.setObject(OMSPage."NewBillingAddress", "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingAddrss, "Address1"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingCity, "BillingCity"));
					objStatus+=String.valueOf(UIFoundation.SelectObject(PickedPage.dwnFedexBillingState, "BillingState"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingZip, "BillingZipcode"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
					UIFoundation.waitFor(3L);
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthYear, "birthYear"));
			}
			
			UIFoundation.waitFor(1L);
			if(UIFoundation.isDisplayed(PickedPage.btnPaymentContinue)) {
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.btnPaymentContinue));
			}
				
				UIFoundation.waitFor(8L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkAcceptTermsAndConditions));
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnEnrollInPicked));
				UIFoundation.webDriverWaitForElement(PickedPage.txtHeadlinePicked, "element", "", 50);
				if(UIFoundation.isDisplayed(PickedPage.txtHeadlinePicked)){
					objStatus+=true;
					String objDetail="User is successfully enrolled to compass with Home address and Glad to meet you screen is displayed.";
					logger.pass(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}else{
					objStatus+=false;
					String objDetail="User is not successfully enrolled with Home address to compass and Glad to meet you screen not displayed";
					logger.fail(objDetail); 
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
				log.info("Execution of the method verifyHomeAddrFlowForPickedUpWine ended here .........");
				if (objStatus.contains("false"))
				{
					System.out.println("Validate The Subscription Flow For Local Pickup Address test case failed");
					return "Fail";
				}
				else
				{
					System.out.println("Validate The Subscription Flow For Local Pickup Address test case executed successfully");
					return "Pass";
				}
			}catch(Exception e)
			{
				log.error("there is an exception arised during the execution of the method"+ e);
				return "Fail";
			}
		}	
}
