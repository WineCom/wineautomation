package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.test.Mobile;;

public class InvalidGiftCertificate extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: invalidPromoCode()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String invalidGiftCardValidation()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String expectedErrorMsg=null;
		String actualErrorMsg=null;
		
		String screenshotName = "Scenarios_ErrorMessage_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try
		{
			log.info("The execution of the method invalidGiftCardValidation started here ...");
			System.out.println("============Order summary ===============");
			UIFoundation.waitFor(2L);
			expectedErrorMsg = verifyexpectedresult.inValidGiftCardmsg;
			
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.chkGiftCheckbox);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.chkGiftCheckbox));
			objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "InvalidGiftCertificate"));
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.chkGiftCheckbox);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnGiftApply));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.txtGiftErrorCode);
			UIFoundation.waitFor(5L);
			actualErrorMsg=UIFoundation.getText(CartPage.txtGiftErrorCode);
			UIFoundation.waitFor(2L);
			if(actualErrorMsg.equalsIgnoreCase(expectedErrorMsg))
			{
				System.out.println(expectedErrorMsg);
			    objStatus+=true;
			    String objDetail="Actual and expected error message are same";
			    getlogger().pass(objDetail);
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}
			else
			{
				System.err.println(actualErrorMsg);
			    objStatus+=false;
		        String objDetail="Actual and expected error message are not same";
		        getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());	       
		        UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				
			}
			log.info("The execution of the method invalidGiftCardValidation ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method invalidGiftCardValidation "+ e);
			return "Fail";
			
		}
	}


	
	
	
	
}