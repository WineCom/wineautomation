package wine.autotest.mobile.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyTheBehaviourOfTheBillingAddresChechBox extends Mobile {	

	static boolean isObjectPresent=true;	

	/***************************************************************************
	 * Method Name : verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh, 
	 * Purpose     : The purpose of this method is to validate the error message displayed
	 * TM-625,
	 ****************************************************************************
	 */
	
	public static String verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected() {

		String objStatus = null;			
		String screenshotName = "Scenarios_verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected_Screenshot.jpeg";
		
		try {
			
			log.info("The execution of the method verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected started here ...");	
			
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(8L);			             
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(12L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtOrderAgain));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));			
			UIFoundation.waitFor(1L);					
			if(UIFoundation.isDisplayed(FinalReviewPage.txtMustSelectAddress))
			{
				objStatus+=true;
				String objDetail="User Must select address before proceeding error message displayed successfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");			   
			}else
			 {
			       objStatus+=false;
			       String objDetail="select address before proceeding not dsplayed";
			       getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			 }				
			
			log.info("The execution of the method verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected " + e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : checkBoxSelectedDefault()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh, 
	 * Purpose     : 
	 * jira ID	   : TM-673
	 ****************************************************************************
	 */
	
	
	public static String checkBoxSelectedDefault() {
		String objStatus=null;
	
		String screenshotName = "Scenarios_cartPage_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

			try {
				log.info("The execution of the method checkBoxSelectedDefault started here ...");
				UIFoundation.waitFor(6L);
				
				if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
				{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
				}else {
					objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
				}
				UIFoundation.waitFor(5L);	
				objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());	           				
				if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
					UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 70);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkBillPaymentEdit))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkBillPaymentEdit);
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkBillPaymentEdit));
					UIFoundation.waitFor(1L);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
				{
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
					UIFoundation.waitFor(1L);
				}				
				if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
				{
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));					
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				}
			
				if(UIFoundation.isDisplayed(FinalReviewPage.chkBillingAndShippingCheckbox))
				 {
				      objStatus+=true;
				      String objDetail="Billing same as Shipping checkbox is selected by default succesfully";
                       getlogger().pass(objDetail);
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");			   
				 }else
				 {					
				       objStatus+=false;
				       String objDetail="Billing same as Shipping checkbox is not selected by default";
				       getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				 }
				
				log.info("The execution of the method checkBoxSelectedDefault ended here ...");
				if ( objStatus.contains("false")) {
					System.out.println(" Order creation with existing account test case is failed");
					return "Fail";
				} else {
					return "Pass";
				}
			} catch (Exception e) {
				
				log.error("there is an exception arised during the execution of the method checkBoxSelectedDefault "
						+ e);
				return "Fail";
			}
	
	}
	
	/***************************************************************************
	 * Method Name : unCheckBoxSelected()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh, 
	 * Purpose     : 
	 * TM-
	 ****************************************************************************
	 */
	
	public static String unCheckBoxSelected() {
		String objStatus=null;
		String screenshotName = "Scenarios_unCheckBoxSelected_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

			try {
				log.info("The execution of the method unCheckBoxSelected started here ...");
				UIFoundation.waitFor(6L);
				
				if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
				{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
				}else {
					objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
				}
				UIFoundation.waitFor(5L);
				objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
				UIFoundation.waitFor(1L);
				
				if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
					UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 70);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkBillPaymentEdit))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkBillPaymentEdit);
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkBillPaymentEdit));
					UIFoundation.waitFor(1L);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
				{
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
					UIFoundation.waitFor(1L);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
				{
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
					
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				}
				if(UIFoundation.isSelected(FinalReviewPage.chkBillingAndShippingCheckbox))				{

					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox));
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingAddress));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingSuite));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingCity));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtBillinState));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtobj_BillingZip));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingPhone));
					objStatus+=true;
					String objDetail="All fields are displayed when click on BillingAndShippingCheckbox ";
					getlogger().pass(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}
			 else
			 {
			       objStatus+=false;
			       String objDetail="All fields are not displayed";
			       getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			 }
				log.info("The execution of the method unCheckBoxSelected ended here ...");
				if ( objStatus.contains("false")) {
					System.out.println(" Order creation with existing account test case is failed");
					return "Fail";
				} else {
					return "Pass";
				}
			} catch (Exception e) {
				
				log.error("there is an exception arised during the execution of the method unCheckBoxSelected "
						+ e);
				return "Fail";
			}
	
	}
	
	
	/***************************************************************************
	 * Method Name : shippingDetails() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh
	 * Purpose     : The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 */

	public static String shippingDetails() {
		String objStatus = null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		try {
			UIFoundation.waitFor(2L);
			log.info("The execution of the method shippingDetails started here ...");
			System.out.println("============Order summary in CART page===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(2L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
		 //   objStatus+=String.valueOf(ApplicationDependent.recipientEdit(driver));
			
			UIFoundation.waitFor(12L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.javaScriptClick(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(10L);
			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue))
			{				
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(20L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.waitFor(15L);
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

}
