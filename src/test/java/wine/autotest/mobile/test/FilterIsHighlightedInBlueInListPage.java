package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class FilterIsHighlightedInBlueInListPage extends Mobile {

	
	/***************************************************************************
	 * Method Name			: FilterIsHighlightedInBlueInListPage()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String filterIsHighlightedInBlueInListPage() {
		String objStatus=null;
		   String screenshotName = "Scenarios__FilterHigh.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
							+ screenshotName;
				
		try {
			log.info("The execution of the method filterIsHighlightedInBlueInListPage started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtCaberNet));
			UIFoundation.waitFor(8L);
		
			if(UIFoundation.isDisplayed(ListPage.txtVarietalFilterName))
			{
				  objStatus+=true;
			      String objDetail="'Filter Name' is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			     getlogger().pass(objDetail);
				
			}
			else
			{
				   objStatus+=false;
				   String objDetail="'Filter Name' is not displayed";
				  getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				   UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}	
			if(UIFoundation.isDisplayed(ListPage.btnVarietalFilterClose))
			{
				  objStatus+=true;
			      String objDetail="'Close icon' is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			     getlogger().pass(objDetail);
			}
			else
			{
				   objStatus+=false;
				   String objDetail="'Close icon' is not displayed";
				  getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}	
			log.info("The execution of the method FilterIsHighlightedInBlueInListPage ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
				
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method filterIsHighlightedInBlueInListPage "+ e);
			return "Fail";
		}
	}	
}
