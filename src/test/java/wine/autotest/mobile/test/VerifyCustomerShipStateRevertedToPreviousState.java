package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyCustomerShipStateRevertedToPreviousState extends Mobile {


	static boolean isObjectPresent=false;

	/***************************************************************************
	 * Method Name			: customerShipstateRevertedToPreviousStateInSideShoppingCart()
	 * Created By			: Chandra shekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * jira ID	            : TM-410
	 ****************************************************************************
	 */
	public static String customerShipstateRevertedToPreviousStateInSideShoppingCart() {


		String objStatus=null;
		String objDetail=null;
		String screenshotName = "Scenarios_customerShipstateRevertedToPreviousStateInSideShoppingCart_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

		try {
			log.info("The execution of the method customerShipstateRevertedToPreviousStateInSideShoppingCart started here ...");

			UIFoundation.waitFor(3L);			
			String cartCountBefortStateChange=UIFoundation.getText(ListPage.btnCartCount);
			UIFoundation.waitFor(1L);
			String getBeforeChangeState=UIFoundation.getText(ListPage.cboGetState);								
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.cboObj_ChangeState, "dryState"));				
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.isDisplayed(ListPage.btnContinueShipToKY));			
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCancleStayIn));
			UIFoundation.waitFor(8L);
			String cartCountAfterStateChange=UIFoundation.getText(ListPage.btnCartCount);
			String getAfterChangeState=UIFoundation.getText(ListPage.cboGetState);	

			if(cartCountBefortStateChange.contains(cartCountAfterStateChange) && getBeforeChangeState.contains(getAfterChangeState))
			{
				objStatus+=true;
			    objDetail="Customer's ship to state is reverted to the previously selected state and products are not removed from cart: excecuted succesfully";
			    getlogger().pass(objDetail);
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				   objStatus+=false;
				   objDetail="Customer's ship to state is not reverted to the previously selected state";
				   getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				   ReportUtil.addTestStepsDetails(objDetail, "Failed", "");

			}

			log.info("The execution of the method customerShipstateRevertedToPreviousStateInSideShoppingCart ended here ...");
			if(objStatus.contains("false"))
			{

				return "Fail";
			}
			else
			{	
				return "Pass";
			}

		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method customerShipstateRevertedToPreviousStateInSideShoppingCart "+ e);
			return "Fail";
		}

	}

	/***************************************************************************
	 * Method Name			: customerShipstateRevertedToPreviousStateOutSideShoppingCart()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * jira ID	            : TM-412
	 ****************************************************************************
	 */
	public static String customerShipstateRevertedToPreviousStateOutSideShoppingCart() {

		String objStatus=null;
		String screenshotName = "Scenarios_customerShipstateRevertedToPreviousStateOutSideShoppingCart_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method customerShipstateRevertedToPreviousStateOutSideShoppingCart started here ...");

			UIFoundation.waitFor(3L);				
			//	driver.navigate().refresh();			
			String cartCountBefortStateChange=UIFoundation.getText(ListPage.btnCrtCount);
			UIFoundation.waitFor(1L);
			String getBeforeChangeState=UIFoundation.getText(ListPage.cboGetState);								
			UIFoundation.waitFor(5L);

			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(5L);
			//	objStatus+=String.valueOf(ApplicationDependent.recipientEdit(driver));

			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEid)){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEid));
				UIFoundation.waitFor(2L);  
			}                
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkShippingAddressEdit));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.cboRecipientState, "dryState"));
			//    UIFoundation.clickObject(FinalReviewPage. "shipState");
			UIFoundation.clearField(FinalReviewPage.txtZip);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtZip, "dryZipCode"));
			UIFoundation.waitFor(1L);		

			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShippingAddressSave));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinueButtonShipRecpt)){
				UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinueButtonShipRecpt));
				UIFoundation.waitFor(10L);
			}

			if(UIFoundation.isDisplayed(FinalReviewPage.btnShipContinue)){
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
				UIFoundation.waitFor(10L);
			}else {
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnVerifyContinue));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(10L);
			}

			objStatus+=String.valueOf(UIFoundation.isDisplayed(ListPage.btnContinueShipToKY));			
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCancleStayIn));
			UIFoundation.waitFor(8L);			
			objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.imgWineLogo));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.imgWineLogo));
			UIFoundation.waitFor(1L);

			String cartCountAfterStateChange=UIFoundation.getText(ListPage.btnCartCount);
			String getAfterChangeState=UIFoundation.getText(ListPage.cboGetStateInHomePage);	

			if(cartCountBefortStateChange.contains(cartCountAfterStateChange) && getBeforeChangeState.contains(getAfterChangeState))
			{
				objStatus+=true;
				String objDetail="Verify the stay in previous state functionality, if ship state is changed inside the shopping cart (via shopping cart page) excecuted succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}
			else
			{
				objStatus+=false;
				String objDetail="Verify the stay in previous state functionality, if ship state is changed inside the shopping cart (via shopping cart page) failed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			}

			log.info("The execution of the method customerShipstateRevertedToPreviousStateOutSideShoppingCart ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method customerShipstateRevertedToPreviousStateOutSideShoppingCart "+ e);
			return "Fail";
		}
	}	
}
