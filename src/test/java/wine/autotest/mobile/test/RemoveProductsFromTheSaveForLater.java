package wine.autotest.mobile.test;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;


public class RemoveProductsFromTheSaveForLater extends Mobile {
	static ArrayList<String> expectedItemsName=new ArrayList<String>();
	static ArrayList<String> actualItemsName=new ArrayList<String>();
	static String products1=null;
	static String products2=null;
	static String products3=null;
	static String cartCountBeforeLogout=null;
	static int cartCountBefreLogout=0;
	static String cartCountAfterLogin=null;
	static int cartCountAftrLogin=0;
	
	
	
	
	
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
	
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.MainNavAccountTab));
//			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(6L); 
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernameMobile"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.waitFor(6L);
			getDriver().navigate().refresh();
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			UIFoundation.waitFor(6L);			

			System.out.println("objStatus login:"+objStatus);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}	
	
	/***************************************************************************
	 * Method Name			: addProductsToCart()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String addProductsToCart(WebDriver driver) {
		
		
		String objStatus=null;
		try {
			log.info("The execution of the method addProductsToCart started here ...");
			driver.navigate().refresh();
		//	UIFoundation.SelectObject(driver, "SelectState", "State");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkBordexBlends));
			UIFoundation.waitFor(2L);
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFirstProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnSecondProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnThirdProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFourthProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFifthProductToCart));*/
			objStatus+=String.valueOf(UIBusinessFlow.addproductstocart(2));				
			
			UIFoundation.waitFor(2L); 
			UIFoundation.scrollUp();
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSrt);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnCartCount));
			log.info("The execution of the method addProductsToCart ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}

	}
	
	/***************************************************************************
	 * Method Name			: moveProductsToSaveForLater()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String moveProductsToSaveForLater() {

		String objStatus=null;
		try {
			log.info("The execution of the method saveForLater started here ...");
	     	objStatus+=String.valueOf(UIBusinessFlow.validationForRemoveProductsSaveForLater());
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnRemoveThirdProductFromSave));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnObjRemove));
			
			System.out.println("=========================Products available in save for later section Before  logout================================");
			if(!UIFoundation.getText(CartPage.spnFirstProductInSaveFor).contains("Fail"))
			{
				products1=UIFoundation.getText(CartPage.spnFirstProductInSaveFor);
				System.out.println("1) "+products1);
				expectedItemsName.add(products1);
			}
			if(!UIFoundation.getText(CartPage.spnSecondProdcutInSaveFor).contains("Fail"))
			{
				products2=UIFoundation.getText(CartPage.spnSecondProdcutInSaveFor);
				System.out.println("2) "+products2);
				expectedItemsName.add(products2);
			}
			if(!UIFoundation.getText(CartPage.spnThirdProductInSaveFor).contains("Fail"))
			{
				products3=UIFoundation.getText(CartPage.spnThirdProductInSaveFor);
				System.out.println("3) "+products3);
				expectedItemsName.add(products3);
			}
			
			BaseTest.logout();
			log.info("The execution of the method saveForLater ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method saveForLater "+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: productsAvailableInSaveForLater()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String productsAvailableInSaveForLater() {
		String objStatus=null;
		String screenshotName = "Scenarios_ProductMismatched_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		
		try {
			log.info("The execution of the method saveForLater started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernameMobile"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnCrtCount);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCrtCount));
			UIFoundation.waitFor(3L);
			System.out.println("=========================Products available in save for later section after login================================");
			if(!UIFoundation.getText(CartPage.spnFirstProductInSaveFor).contains("Fail"))
			{
				products1=UIFoundation.getText(CartPage.spnFirstProductInSaveFor);
				System.out.println("1) "+products1);
				actualItemsName.add(products1);
			}
			if(!UIFoundation.getText(CartPage.spnSecondProdcutInSaveFor).contains("Fail"))
			{
				products2=UIFoundation.getText(CartPage.spnSecondProdcutInSaveFor);
				System.out.println("2) "+products2);
				actualItemsName.add(products2);
			}
			if(!UIFoundation.getText(CartPage.spnThirdProductInSaveFor).contains("Fail"))
			{
				products3=UIFoundation.getText(CartPage.spnThirdProductInSaveFor);
				System.out.println("3) "+products3);
				actualItemsName.add(products3);
			}
			

			log.info("The execution of the method saveForLater ended here ...");
			if(expectedItemsName.containsAll(actualItemsName)){
				 objStatus+=true;
			     String objDetail="All products are available in save for later section";
			     getlogger().pass(objDetail);
			     ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}else{
				 objStatus+=false;
			     String objDetail="Mismatched products name in save for later section";
			   //  UIFoundation.captureScreenShot(screenshotpath, objDetail);
			     getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			     UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			
			if(objStatus.contains("false"))
			{
				System.out.println("mismatched products name in save for later section");
				return "Fail";
			}
			else
			{	System.out.println("all products are available in save for later section");
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method saveForLater "+ e);
			return "Fail";
		}
	}
	
}

