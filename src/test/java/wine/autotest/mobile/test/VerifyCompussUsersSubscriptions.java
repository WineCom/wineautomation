package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyCompussUsersSubscriptions extends Mobile {
	

	
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{			
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(1L);		
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "compassUser"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50);
			String actualtile=getDriver().getTitle();			
			String expectedTile = verifyexpectedresult.shoppingCartPageTitle;	
			if(actualtile.equalsIgnoreCase(expectedTile))
			{
				objStatus+=true;
			}else
			{
				objStatus+=false;
		    	String objDetail="Failed to login";
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to login";
	    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	
	/***************************************************************************
	 * Method Name			: verifySubscriptions()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifySubscriptions()
	{
		String objStatus=null;
		String getTextTagretPrice=null;
		String getTagretPrice=null;
		String getMyRangeForm=null;
		String getLowRange=null;
		String geHighRange=null;
		String getRedAmount=null;
		String getRedPrice=null;
		String getwhiteAmount=null;
		String getWhitePrice=null;

		String verifyTextTagretPrice=null;
		String verifyTagretPrice=null;
		String VerifyMyRangeForm=null;
		String verifyLowRange=null;
		String verifyHighRange=null;
		String verifyRedAmount=null;
		String verifyRedPrice=null;
		String verifyWhiteAmount=null;
		String verifyWhitePrice=null;
		
		String screenshotName = "Scenarios_verifySubscriptions_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{			
			log.info("The execution of the method verifySubscriptions started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			UIFoundation.waitFor(2L);	
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.linPickedSetting));
		    UIFoundation.waitFor(1L);	
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyWelcomPicked));
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.cmdVerifyRedWine));
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.cmdVerifyWhiteWine));
		    UIFoundation.waitFor(1L);   
		    getTextTagretPrice=UIFoundation.getText(PickedPage.txtTargetPrice);
            getTagretPrice=UIFoundation.getText(PickedPage.txtGetTaggetPrice);
			getMyRangeForm=UIFoundation.getText(PickedPage.txtVerifyMyRangeForm);
			getLowRange=UIFoundation.getText(PickedPage.txtVerifyLowRange);
			geHighRange=UIFoundation.getText(PickedPage.txtVerifyHighRange);
			getRedAmount=UIFoundation.getText(PickedPage.txtRedAmount);
			getRedPrice=UIFoundation.getText(PickedPage.txtRedPrice);
			getwhiteAmount=UIFoundation.getText(PickedPage.txtWhiteAmount);
			getWhitePrice=UIFoundation.getText(PickedPage.txtWhitePrice);
		    
		    UIFoundation.scrollDownOrUpToParticularElement(PickedPage.lblCancleSubscription);
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.lblCancleSubscription));
		    UIFoundation.waitFor(1L);	
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.lblPickedCancel));
		    UIFoundation.waitFor(2L);	
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.cmdCancleSubscription));		    		   	    
		    UIFoundation.waitFor(2L);	
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.cmdCloseSubscription));
		    UIFoundation.waitFor(2L);		      
		    UIFoundation.scrollDownOrUpToParticularElement(PickedPage.txtVerifyPicked);
		    UIFoundation.waitFor(1L);
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyPicked));
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtRestartSubscription));
		    UIFoundation.waitFor(2L);
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.txtRestartSubscription));
		    UIFoundation.waitFor(2L);
		    verifyTextTagretPrice=UIFoundation.getText(PickedPage.txtTargetPrice);
		    verifyTagretPrice=UIFoundation.getText(PickedPage.txtGetTaggetPrice);
		    VerifyMyRangeForm=UIFoundation.getText(PickedPage.txtVerifyMyRangeForm);
		    verifyLowRange=UIFoundation.getText(PickedPage.txtVerifyLowRange);
		    verifyHighRange=UIFoundation.getText(PickedPage.txtVerifyHighRange);
		    verifyRedAmount=UIFoundation.getText(PickedPage.txtRedAmount);
		    verifyRedPrice=UIFoundation.getText(PickedPage.txtRedPrice);
		    verifyWhiteAmount=UIFoundation.getText(PickedPage.txtWhiteAmount);
		    verifyWhitePrice=UIFoundation.getText(PickedPage.txtWhitePrice);
		    UIFoundation.waitFor(1L);
			
		    if(getTextTagretPrice.equals(verifyTextTagretPrice)&& getTagretPrice.contains(verifyTagretPrice) )
		    {		    				
				objStatus+=true;
				String objDetail = "Targer prices are matched";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		    }
		    else
		    {		    			    	
		    	objStatus+=false;
		    	String objDetail="Targer prices are not matched";
		    	ReportUtil.addTestStepsDetails(objDetail, "fail", "");
		    	getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		    	
		    }
		    if(getMyRangeForm.equals(VerifyMyRangeForm)&& getLowRange.contains(verifyLowRange)&& geHighRange.contains(verifyHighRange) )
		    {		    		
		    	objStatus+=true;
				String objDetail = "Range prices are matched";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");					
		    }
		    else
		    {
		    	objStatus+=false;
		    	String objDetail="Range prices are not matched";
		    	logger.fail(objDetail);
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		    	
		    }
		    if(getRedAmount.equals(verifyRedAmount)&& getRedPrice.contains(verifyRedPrice) )
		    {
		    	objStatus+=true;
				String objDetail = "RedAmount and Red Prices are matched";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					
		    }
		    else
		    {
		    	objStatus+=false;
		    	String objDetail = "RedAmount and Red Prices are not matched";
		    	logger.fail(objDetail);
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		    	
		    }
		    if(getwhiteAmount.equals(verifyWhiteAmount)&& getWhitePrice.contains(verifyWhitePrice) )
		    {
		    	objStatus+=true;
		    	String objDetail = "WhiteAmount and White Prices are  matched";
		    	logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					
		    }
		    else
		    {
		    	objStatus+=false;
		    	String objDetail = "White Amount and White Prices are not  matched";
		    	logger.fail(objDetail);
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);    	
		    }
			log.info("The execution of the method verifySubscriptions ended here ...");
			if (objStatus.contains("false"))
			{
				objStatus+=false;
		    	String objDetail="Delivered via email' is not displayed in order history";
		    	logger.fail(objDetail);
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);		
				return "Fail";
			}
			else
			{
				  objStatus+=true;
					String objDetail = "Delivered via email' is displayed in order history";
					logger.pass(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to verifySubscriptions";
	    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			log.error("there is an exception arised during the execution of the method verifySubscriptions "+ e);
			return "Fail";
			
		}
	}
	
}
