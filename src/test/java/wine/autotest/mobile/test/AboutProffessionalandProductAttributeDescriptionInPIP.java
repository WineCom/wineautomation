package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class AboutProffessionalandProductAttributeDescriptionInPIP extends Mobile {
	

	
	
	/***************************************************************************
	 * Method Name			: AboutProffessionalandProductAttributeDescriptionInPIP()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String aboutProffessionalandProductAttributeDescriptionInPIP()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_SocialMedisLink.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			log.info("The execution of the method verifyTheSocialMediaLinks started here ...");
			
			if(!UIFoundation.isDisplayed(LoginPage.MainNavButton))
			 {
			    getDriver().navigate().refresh();
			    UIFoundation.waitFor(4L);
			 }
						
			getDriver().get("https://qwww.wine.com/product/chateau-lynch-bages-2016/202440");
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(LoginPage.txtToolTipComponet))
			{
				UIFoundation.clckObject(LoginPage.txtToolTipComponet);
				UIFoundation.waitFor(1L);
			}
			
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.imgWineRatings));
			if(UIFoundation.isDisplayed(ListPage.spnModalWindow))
			{
				objStatus+=true;
				String objDetail="'About Professional Ratings' popup is displayed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="'About Professional Ratings' popup is not displayed";	
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnModalWindowClose));
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.spnProductAttributeName));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.spnProductAttributeContent))
			{
				objStatus+=true;
				String objDetail="A popup with explanations of the icons that a given product is associated with, is displayed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="A popup with explanations of the icons that a given product is associated with, is not displayed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method verifyTheSocialMediaLinks ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method aboutProffessionalandProductAttributeDescriptionInPIP "+ e);
			return "Fail";
			
		}
	}

}
