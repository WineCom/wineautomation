package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.test.Mobile;;





public class RemoveFewProductsFromTheCart extends Mobile {
	

	static boolean isObjectPresent=false;
	
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String captureOrdersummary()
	{
		String objStatus=null;
		String subTotalBefore=null;
		String subTotalAfter=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String screenshotName = "Scenarios_captureOrdersummary_Screenshot.jpeg";
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before removing products from the cart===============");
			subTotalBefore=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotalBefore);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			UIFoundation.getText(CartPage.spnRemoveFirstProduct);
			UIFoundation.getText(CartPage.spnSecondProductPrice);
			UIFoundation.waitFor(3L);
			if(!UIFoundation.getText(CartPage.spnRemoveSecondProduct).contains("Fail"))
			{
				UIFoundation.waitFor(2L);				
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnRemoveSecondProduct));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnObjRemove));
			}
			if(!UIFoundation.getText(CartPage.spnRemoveFirstProduct).contains("Fail"))
			{
				UIFoundation.waitFor(2L);			
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnRemoveFirstProduct));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnObjRemove));
			}	
			UIFoundation.waitFor(3L);
			subTotalAfter=UIFoundation.getText(CartPage.spnSubtotal);
		//	ApplicationDependent.removerProductPrice(driver, subTotalBefore, subTotalAfter, secondProductPrice, thirdProductPrice);
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary after removing products from the cart===============");
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotalAfter);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			log.info("The execution of the method captureOrdersummary started here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Remove products from the cart test case is failed");
				objStatus+=false;
				String objDetail="Remove products from the cart test case is failed";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println("Remove products from the cart test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Remove products from the cart test case is executed successfully");
				objStatus+=true;
				String objDetail="Remove products from the cart test case is executed successfully";
                getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");								
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}
	}

}
