package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyTheContentsOfVerifyAddressDialog extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : shippingDetails()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh
	 * Purpose     : The purpose of this method is to fill the
	 * shipping address of the customer
	 * TM-645,649,646
	 ****************************************************************************
	 */

	public static String shippingDetails() {
		

		String objStatus = null;
		   String screenshotName = "Scenarios_VerifySuggested_Screenshot.jpeg";
			
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(6L);
								
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstNameAD"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastNameAD"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1AD"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "CityAD"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "StateAD"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtReceipentZipCode, "ZipCodeAD"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumberAD"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			

			if(UIFoundation.isDisplayed(FinalReviewPage.txtOriginalAddress) && UIFoundation.isDisplayed(FinalReviewPage.txtSuggestedAddress))
			{
				  objStatus+=true;
			      String objDetail="'Use this Shipping Address' checkbox should be available and should be unmarked by default";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      getlogger().pass(objDetail);
				  System.out.println("'Use this Shipping Address' checkbox should be available and should be unmarked by default");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="'Use this Shipping Address' checkbox should be available and not unmarked by default";
				   UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				   getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				   System.err.println("'Use this Shipping Address' checkbox should be available and not unmarked by default");
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(10L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.spnRecipientChangeAddress))
			{
				  objStatus+=true;
			      String objDetail="Verified  the Continue button functionality in Verify Address dialog";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      getlogger().pass(objDetail);
				  System.out.println("Verified  the Continue button functionality in Verify Address dialog");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the Continue button functionality in Verify Address dialog test case is failed";			     
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"ContinueButtonVerifyAddress", objDetail);
			       getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				   System.err.println("Verify the Continue button functionality in Verify Address dialog");
			}


			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {
		
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}	
	}	
	
}
