package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyTheContentsInTheOrderPage extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : verifyTheContentOntheOrderPage()
	 * Created By : Chandrashekhar
	 *  Reviewed By : Ramesh
	 *  Purpose : The purpose of this method is to verify the detail information displayed on the orderpage after the place an order.
	 * TM-2197
	 ****************************************************************************
	 */

	public static String verifyTheContentOntheOrderPage() {
		String objStatus = null;
		   String screenshotName = "Scenarios_verifyTheContentOntheOrderPage_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try {
			log.info("The execution of the method verifyTheContentOntheOrderPage started here ...");
			UIFoundation.waitFor(3L);
         
			if(UIFoundation.isDisplayed(ThankYouPage.spnTickSymbol) && UIFoundation.isDisplayed(ThankYouPage.spnOrderConfirmation) && UIFoundation.isDisplayed(ThankYouPage.spnthanksOnOrderPage) && UIFoundation.isDisplayed(ThankYouPage.spnverifyTextOrderNumberIs) && UIFoundation.isDisplayed(ThankYouPage.spnverifyOrderAgain) && UIFoundation.isDisplayed(ThankYouPage.spnVerifyTextSendThisWine))
			{
				  objStatus+=true;
			      String objDetail="All the fields are Verified in the order page successfully.";
			      getlogger().pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				 
			}
			else
			{
				   objStatus+=false;
				   String objDetail="All the fields are not Verified in the order page.";	
				   getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"ContinueButtonVerifyAddress", objDetail);
	  		}
			log.info("The execution of the method verifyTheContentOntheOrderPage ended here ...");

			if (objStatus.contains("false")) {
			
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method verifyTheContentOntheOrderPage " + e);
			return "Fail";
		}

	}
	
}
