package wine.autotest.mobile.test;


import java.io.IOException;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class ItemsNotEligibleForPromoCodeRedemption extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: itemsNotEligibleForPromoCodeRedemption()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 * TM-1924
	 ****************************************************************************
	 */
	public static String itemsNotEligibleForPromoCodeRedemption() {
		String objStatus = null;
			
		   String screenshotName = "Scenarios_itemsNotEligibleForPromoCodeRedemption_.jpeg";
		   
		try {
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtSearchProduct));
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtSearchProduct, "notEligibleForPromocode"));
			UIFoundation.waitFor(10L);
			UIFoundation.hitEnter(ListPage.btnNotEligiblepromocodeRedemptionProduct);
			UIFoundation.waitFor(12L);
			objStatus+=String.valueOf(UIBusinessFlow.addproductstocart(2));	
			UIFoundation.waitFor(3L);
			/*addToCart2 = UIFoundation.getText(driver, "SecondProductToCart");
			if (addToCart2.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(driver, "SecondProductToCart"));
			}*/
			
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lblShowOutOfStock);
			  UIFoundation.scrollUp();
			//UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaSriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(ListPage.txtPromoCode))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtPromoCode, "PromecodeMinimumAmount"));
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkPromoCodeApply));
				UIFoundation.waitFor(5L);
			}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtObj_PromoCode));
				objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtPromoCode, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkPromoCodeApply));
				UIFoundation.waitFor(5L);
			}
			if(UIFoundation.isDisplayed(ListPage.txtMinimumOfferAmount))
			{
				objStatus+=true;
				String objDetail="Verified appropriate error message is displayed ,when none of the items in the cart are eligible for the promo code redemption";
				ReportUtil.addTestStepsDetails("Verified appropriate error message is displayed ,when none of the items in the cart are eligible for the promo code redemption", "Pass", "");
				getlogger().pass(objDetail);
			}else
			{
				objStatus+=false;
				String objDetail="Verify appropriate error message is displayed ,when none of the items in the cart are eligible for the promo code redemption test case is failed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
}
