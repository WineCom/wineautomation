package wine.autotest.mobile.test;


import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyLimitPerCustomerForPreSaleProduct extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : addPreSaleProdTocrt()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose :
	 * Jira Id  :   TM-5013
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addPreSaleProdTocrt() {
		String objStatus = null;
		String limitPerCustomer=null;
		 String screenshotName = "Scenarios_addPreSaleProdTocrt.jpeg";
		try {	
			log.info("The execution of the method ValidatePreSaleBlurbDisplayedIn started here ...");
			if(!UIFoundation.isDisplayed(ListPage.btnMainNavButton))
			{
				getDriver().navigate().refresh();
				UIFoundation.waitFor(4L);
			}
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFeaturedMainTab));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtBordexFeatures));				
			UIFoundation.waitFor(5L);	
			UIFoundation.scrollDown();
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(CartPage.txtLimitCount));
			UIFoundation.waitFor(5L);	
			WebElement ele=getDriver().findElement(By.xpath("(//div[@class='prodItemInfo_stock']//div//div[@class='prodItemLimit']/span[@class='prodItemLimit_count'])[1]"));
			String limitCount=ele.getText();			
			UIFoundation.waitFor(2L);					
			objStatus+=String.valueOf(UIBusinessFlow.addPipLimitProduct());
			
			limitPerCustomer=UIFoundation.getText(CartPage.txtSoldIncrement);
			UIFoundation.waitFor(1L);		
			if (UIFoundation.isDisplayed(CartPage.txtPreSale) && limitPerCustomer.equals(limitCount)) {
				objStatus+=true;
				String objDetail="Pre-sale and Sold increment text is Displayed in PIP Page";
                getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}
			else
			{	
				objStatus+=false;
				String objDetail="Pre-sale and Sold increment text not  Displayed in PIP Page";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgUserRating));	
			UIFoundation.waitFor(1L);		
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgObj_MyWine));					
			UIFoundation.waitFor(2L);	
		//	if (UIFoundation.isDisplayed(CartPage.txtPreSale) && UIFoundation.isDisplayed(CartPage.txtSoldIncrement)) {
			if (UIFoundation.isDisplayed(CartPage.txtPreSale) && limitPerCustomer.equals(limitCount)) {
				objStatus+=true;
				String objDetail="Pre-sale and Sold increment text Displayed in my wine Page";
                getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}
			else
			{	
				objStatus+=false;
				String objDetail="Pre-sale and Sold increment text not Displayed in my wine Page";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstProdNameLink));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.imgClearStarRating));			
			UIFoundation.waitFor(3L);	
			log.info("The execution of the method ValidatePreSaleBlurbDisplayedIn ended here ...");
			
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

}
