package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyLikeSectionNtDisplayedTheVarietalPreferenceReviewPage extends Mobile {

	
	/***********************************************************************************************************
	 * Method Name : verifyLikeSectionInReviewPage() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh
	 * Purpose     : 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String verifyLikeSectionInReviewPage() {
		String objStatus = null;
		String screenshotName = "Scenarios_verifyLikeSectionInReviewPage_Screenshot.jpeg";
		System.getProperty("user.dir");
		try {
			log.info("Execution of the method verifyLikeSectionInReviewPage started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedCompassTiles));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSignUpPickedWine));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNovice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineJourneyNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizWineTypRed));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineTypeNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnZinfendaldisLike));					
			UIFoundation.waitFor(1L);					
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnVarietalTypNxt));
			UIFoundation.waitFor(1L);
			if(!UIFoundation.isDisplayed(PickedPage.iconLikdThumsUp)) {
				String objDetail="Like section is not displayed when user does not provide like preference";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				objStatus+=true;
			}else {
								
				objStatus+=false;
				String objDetail="Like section is displayed when user does not provide like preference";
				ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}							
			
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{					
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method verifyLikeSectionInReviewPage"+ e);
			return "Fail";
		}
	}


}
