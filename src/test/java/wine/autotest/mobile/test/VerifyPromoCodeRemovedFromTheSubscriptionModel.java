package wine.autotest.mobile.test;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyPromoCodeRemovedFromTheSubscriptionModel extends Mobile {
	

	static String expectedState=null;
	static String actualState=null;
	/***************************************************************************
	 * Method Name : subscriptionModelPage() 
	 * Created By : Chandrashekhar 
	 * Reviewed By : Ramesh
	 * Purpose : The purpose of this method is to Create.
	 ****************************************************************************
	 */

	public static String subscriptionModelPage() {
		String objStatus = null;
		
		try {
			log.info("The execution of method subscriptionModelPage started here");
			UIFoundation.waitFor(1L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedCompassTiles));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedPromoCode));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnApply));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPickedPromoClose));
			
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSignUpPickedWine));
			UIFoundation.waitFor(5L);
			
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyFirsrPickedPage));			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkNoVoice));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifySeconePickedPage));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkRedOnly));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyThirdPickedPage));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.txtPickedWrapNextThirdPage);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.txtPickedWrapNextThirdPage));	
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyForthPickedPage));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.txtPickedWrapNextFourthPage));
			UIFoundation.waitFor(1L);
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.lblNotVery));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.txtPickedWrapNextFifthPage));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.txtPickedWrapNextSixthPage));
			UIFoundation.waitFor(1L);
			
					
			log.info("The execution of the method subscriptionModelPage ended here ...");
			if (objStatus.contains("false") ) {
				
				return "Fail";
			} else {
				
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("subscriptionModelPage  test case is failed");
			log.error("there is an exception arised during the execution of the method subscriptionModelPage " + e);
			return "Fail";
		}
	}
	
	
	/***************************************************************************
	 * Method Name : shippingDetails() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh,
	 * Purpose     : The purpose of this method is to fill the shipping address of the customer
	 ****************************************************************************
	 */
	static int expectedShippingMethodCharges;
	static String Shipping=null;
	
	public static String shippingDetails() {
		String objStatus = null;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.radShippingTyp));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(PickedPage.dwnBillingSelState, "State"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.btnShipContinue));
			UIFoundation.waitFor(13L);
			if(UIFoundation.isDisplayed(PickedPage.btnVerifyContinueShipRecpt)){
		
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.btnVerifyContinueShipRecpt));
			UIFoundation.webDriverWaitForElement(PickedPage.btnVerifyContinueShipRecpt, "Clickable", "", 50);
			}
			if(UIFoundation.isDisplayed(PickedPage.btnDeliveryContinue)){
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(PickedPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}
	
	/***************************************************************************
	 * Method Name : removePromoCodeFromSubscriptionModel() 
	 * Created By : Chandrashekhar
	 * Reviewed By : Ramesh,
	 *  Purpose : 
	 * 
	 ****************************************************************************
	 */
	public static String removePromoCodeFromSubscriptionModel() {
		String objStatus = null;
		
		String screenshotName = "removePromoCodeFromSubscriptionModel.jpeg";
		

		try {			
			log.info("The execution of the method removePromoCodeFromSubscriptionModel started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(PickedPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.chkBillingAndShippingCheckbox);
		    UIFoundation.javaScriptClick(PickedPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(PickedPage.txtBirthMonth))
			{
			   objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthYear, "birthYear"));
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.btnPaymentContinue);
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPaymentContinue));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.chkTermsAndCondition);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkTermsAndCondition));
			UIFoundation.webDriverWaitForElement(PickedPage.cmdEnrollInPicked, "Invisible", "", 10);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.cmdEnrollInPicked));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.lblPickedLogo));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.btnRemovePromoCode);
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.btnRemovePromoCode));
			UIFoundation.waitFor(1L);
			           
			log.info("The execution of the method removePromoCodeFromSubscriptionModel ended here ...");
			if (objStatus.contains("false")) {
				
						
				objStatus+=false;
				String objDetail="PromoCode is applyed and not removed from the subscription model: failed";
				ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
				
			} else {
								
				objStatus+=true;
				String objDetail="PromoCode is applyed and removed from the subscription model";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method subscription " + e);
			return "Fail";
		}
	}
}
