
package wine.autotest.mobile.test;
import java.io.IOException;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;


public class AddProdcutsToCartCaptureOrder extends Mobile {


	static boolean isObjectPresent=false;

	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Chandrashekhar.
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String searchProductWithProdName() {
		String objStatus=null;
		try {

			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtsearchProduct));
			objStatus += String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "Wind"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSearchTypeList));
			UIFoundation.waitFor(12L);
			// System.out.println(UIFoundation.getText(driver,"obj_AddToCart"));
			// UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(4L);
			// System.out.println(UIFoundation.getText(driver,"AddAgain"));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";

			} else {

				return "Pass";
			}

		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	
	public static String addprodTocrt() {
		String objStatus = null;
		try {			
			UIFoundation.waitFor(1L);

			if(!UIFoundation.isDisplayed(ListPage.btnMainNavButton))
			{
				getDriver().navigate().refresh();
				UIFoundation.waitFor(4L);
			}
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(4L);			
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));		
						
			UIFoundation.waitFor(2L); 
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false")) {
				return "Fail";
				} else {
				return "Pass";
				}
		} catch (Exception e) {
			return "Fail";
		}
	}
	

	/**************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	/*public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;

		try {
			UIFoundation.waitFor(1L);

			if(!UIFoundation.isDisplayed(ListPage.btnMainNavButton))
			{
				getDriver().navigate().refresh();
				UIFoundation.waitFor(4L);
			}
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(4L);
			addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFirstProductToCart));
				UIFoundation.waitFor(1L);
			}

			addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnSecondProductToCart));
				UIFoundation.waitFor(1L);
			}

			addToCart3 = UIFoundation.getText(ListPage.btnThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnThirdProductToCart));
				UIFoundation.waitFor(1L);
			}

			addToCart4 = UIFoundation.getText(ListPage.btnFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFourthProductToCart));
				UIFoundation.waitFor(1L);
			}

			UIFoundation.waitFor(2L);		
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}*/



	public static String addProductsTocrt() {
		String objStatus = null;
		try {
			UIFoundation.waitFor(1L);

			if(!UIFoundation.isDisplayed(ListPage.btnMainNavButton))
			{
				getDriver().navigate().refresh();
				UIFoundation.waitFor(4L);
			}
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(4L);
			UIFoundation.waitFor(4L);			
			objStatus+=String.valueOf(UIBusinessFlow.addproductstocart(2));		
						
			UIFoundation.waitFor(2L); 
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));

			UIFoundation.waitFor(2L);		
			
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}



	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Chandrashekhar.
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderSummaryForNewUser() {
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;

		try {

			System.out.println("=======================Products added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);

			}

			if(!UIFoundation.getText(ListPage.btnSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductInCart);
				System.out.println("2) "+products2);

			}

			if(!UIFoundation.getText(ListPage.btnThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductInCart);
				System.out.println("3) "+products3);

			}

			if(!UIFoundation.getText(ListPage.btnFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.btnFourthProductInCart);
				System.out.println("4) "+products4);

			}

			if(!UIFoundation.getText(ListPage.btnFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.btnFifthProductInCart);
				System.out.println("5) "+products5);

			}
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("============Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHnadling));
			System.out.println("Total Before Tax :"+totalPriceBeforeSaveForLater);
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: orderSummaryForExistingUser()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderSummaryForExistingUser() {
		String products1=null;

		try {

			System.out.println("=======================Products Added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);

			}
			else
			{
				System.out.println("No products are added into the cart");
			}

			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("=======================Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHnadling));
			System.out.println("Total price of all items :"+totalPriceBeforeSaveForLater);
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: productDetailsPresentInCart()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String productDetailsPresentInCart() {
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;

		try {

			System.out.println("=======================Products added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);

			}

			if(!UIFoundation.getText(ListPage.btnSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductInCart);
				System.out.println("2) "+products2);

			}

			if(!UIFoundation.getText(ListPage.btnThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductInCart);
				System.out.println("3) "+products3);

			}

			if(!UIFoundation.getText(ListPage.btnFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.btnFourthProductInCart);
				System.out.println("4) "+products4);

			}

			if(!UIFoundation.getText(ListPage.btnFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.btnFifthProductInCart);
				System.out.println("5) "+products5);

			}
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}

}
