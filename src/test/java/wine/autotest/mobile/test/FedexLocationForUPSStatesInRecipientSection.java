package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.test.Mobile;;

public class FedexLocationForUPSStatesInRecipientSection extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: fedexLocationForUPSStatesInRecipientSection()
	 * Created By			: chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */

	public static String fedexLocationForUPSStatesInRecipientSection() {
		String objStatus=null;
		   String screenshotName = "Scenarios_fedexPickUpLocation_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		try {
			log.info("The execution of the method fedexLocationForUPSStatesInRecipientSection started here ...");
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(4L);                    
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkReciptEidt));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddressLink));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkShipToFedex));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode,"fedExUPSZipcode"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearchButton));
			UIFoundation.waitFor(10L);
			if(!UIFoundation.isDisplayed(FinalReviewPage.txtfedexLocalPickUpIsNotAvailable))
			{
				 objStatus+=true;
			      String objDetail="Fedex locations are displayed for UPS State.";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      getlogger().pass(objDetail);
				  System.out.println("Fedex locations are displayed for UPS State.");
			}else{
				 	objStatus+=false;
				   String objDetail=" Fedex locations are not displayed for UPS State. is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"PaymentSection", objDetail);
			       getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				   System.err.println("Fedex locations are not displayed for UPS State is failed ");
			}
			log.info("The execution of the method fedexLocationForUPSStatesInRecipientSection ended here ...");

			if (objStatus.contains("false")) {
				
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method fedexLocationForUPSStatesInRecipientSection "
					+ e);
			return "Fail";
		}

	}
}
