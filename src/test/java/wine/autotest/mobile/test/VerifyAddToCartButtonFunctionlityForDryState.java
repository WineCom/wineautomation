package wine.autotest.mobile.test;

import java.io.IOException;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyAddToCartButtonFunctionlityForDryState extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : orderSummaryForNewUser() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String verifyAddToCartButtonFunctionality() {
		String objStatus = null;

		try {
			
/*			UIFoundation.SelectObject(driver, "SelectState", "dryState");
			UIFoundation.waitFor(10L);*/
			UIFoundation.SelectObject(ListPage.cboSelectState, "dryState");
			UIFoundation.waitFor(3L);
			getDriver().navigate().refresh();
			UIFoundation.waitFor(3L);
			UIFoundation.SelectObject(ListPage.cboSelectState, "dryState");
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(2L);
			System.out.println("=======================verify the  �Add To Cart� button is not displayed when dry state is selected  =====================");
			if (UIFoundation.getText(ListPage.btnAddToCartFirstButton).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for first product");
				

			}

			if (UIFoundation.getText(ListPage.btnAddToCartSecondButton).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for second product");
				

			}

			if (UIFoundation.getText(ListPage.btnAddToCartThirdButton).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for third product");
				

			}

			if (UIFoundation.getText(ListPage.btnAddToCartFourthButton).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for fourth product");
				

			}

			if (UIFoundation.getText(ListPage.btnAddToCartFifthButton).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for fifth product");
				

			}
			if (objStatus.contains("false")) {
				 String objDetail="Add to Cart' button is not displayed for DryState failed ";
				   ReportUtil.addTestStepsDetails(objDetail, "Failed", "");
				   logger.fail(objDetail);
				 return "Fail";

			} else {
				 String objDetail="Add to Cart' button is displayed for DryState";
				   ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				   logger.pass(objDetail);
				return "Pass";

			}
		} catch (Exception e) {
			return "Fail";
		}
	}
}

