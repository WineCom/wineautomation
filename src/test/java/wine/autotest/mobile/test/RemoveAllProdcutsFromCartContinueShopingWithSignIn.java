package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;


public class RemoveAllProdcutsFromCartContinueShopingWithSignIn extends Mobile {

	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Chandra shekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	static int countProducts=0;
	public static String addprodTocrt() {
		String objStatus = null;
		boolean isElementPresent=false;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(2L);

			UIFoundation.waitFor(4L);			
			objStatus+=String.valueOf(UIBusinessFlow.addproductstocart(2));		
						
			UIFoundation.waitFor(2L); 
			UIFoundation.waitFor(4L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lblShowOutOfStock);
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			isElementPresent=UIFoundation.isDisplayed(LoginPage.LoginEmail);
			if(isElementPresent)
			{
				objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernameMobile"));
				objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
				UIFoundation.waitFor(3L);
			}
		//	objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "CartBtnCount"));
			UIFoundation.waitFor(4L);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: removeProductsFromTheCart()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String removeProductsFromTheCart()
	{
		String objStatus=null;
		String expected=null;
		String actual=null;
		
		String screenshotName = "Scenarios_HomeTitle_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			for(int i=0;i<=countProducts;i++)
			{
				UIFoundation.clckObject(CartPage.spnRemove);
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(CartPage.spnObjRemove);
				UIFoundation.waitFor(1L);
			}
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnContinueShopping));
			UIFoundation.waitFor(2L);
		
			expected = verifyexpectedresult.homePageTitle;
			actual=getDriver().getTitle();
			log.info("The execution of the method removeProductsFromTheCart started here ...");
			
			if(expected.equalsIgnoreCase(actual)){
				  objStatus+=true;
			      String objDetail="Actual and expected title is same";
			      getlogger().pass(objDetail);			      
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			    String objDetail="Actual and expected title is not same";
			    getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		    UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}
	}



}
