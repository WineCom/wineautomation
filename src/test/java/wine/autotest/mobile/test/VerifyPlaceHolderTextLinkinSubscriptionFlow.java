package wine.autotest.mobile.test;

import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.*;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;

public class VerifyPlaceHolderTextLinkinSubscriptionFlow extends Mobile {
	/***********************************************************************************************************
	 * Method Name : enrollForPickedUpWine() 
	 * Created By  : Aravind Radhakrishnan
	 * Purpose     : The purpose of this method is to navigate compass tiles and 'Proceed to Enrollment' 
	 * 
	 ************************************************************************************************************
	 */
	public static String enrollForPickedUpWine() {
		String objStatus = null;
		String screenshotName = "Scenarios_verifyUserIsAbleToEnrollForSubscription_Screenshot.jpeg";
		try {
			log.info("Execution of the method verifyUserIsAbleToEnrollForSubscription started here .........");
			getDriver().navigate().refresh();
			UIFoundation.waitFor(2L);	
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavAccTab));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.lnkPickedSetting);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetting));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdSignUpCompass));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkNoVoice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkRedOnly));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNtVry));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSelQtyNxt));
			UIFoundation.waitFor(4L);
			if (objStatus.contains("false"))
			{				
				String objDetail="Verify SignUp Functionalities Flow test case failed";
				System.out.println(objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Verify SignUp Functionalities Flow test case executed successfully";
				System.out.println(objDetail);
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
}
	catch(Exception e)
{
	log.error("there is an exception arised during the execution of the method"+ e);
	return "Fail";
}		
	}
	/************************************************************************************************
	 * Method Name : verifyRecipientSectionAfterSubscriptionFlow
	 * Created By  : Aravind Radhakrishnan
	 * Purpose     : 
	 * 
	 ************************************************************************************************/
	public static String verifyRecipientSectionAfterSubscriptionFlow() {
		String objStatus = null;
		String screenshotName = "Scenarios_verifyUserIsAbleToEnrollForSubscription_Screenshot.jpeg";
		try {
			log.info("Execution of the method verify Recepient address started here .........");
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(PickedPage.lnkAddNewAddress))
			{objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkAddNewAddress));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoShipToHome));
			}
			else
			{
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoShipToHome));
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(PickedPage.dwnReceiveState, "State"));
			UIFoundation.clickObject(PickedPage.dwnReceiveState);
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtReceipentZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus+= String.valueOf(UIFoundation.clickObject(PickedPage.btnShipContinue));
			UIFoundation.waitFor(11L);
			if(UIFoundation.isDisplayed(PickedPage.btnShipContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.btnVerifyContinue));
				UIFoundation.webDriverWaitForElement(PickedPage.btnVerifyContinue, "Invisible", "", 50); 
			}
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(PickedPage.btnDeliveryContinue)){
				UIFoundation.scrollDownOrUpToParticularElement(PickedPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(PickedPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			if (objStatus.contains("false"))
			{				
				String objDetail="Verify Recepient address test case failed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				return "Fail";
			}
			else
			{				
				String objDetail="Verify Recepient address test case executed successfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
	/************************************************************************************************
	 * Method Name : verifyCreditcarddetails
	 * Created By  : Aravind Radhakrishnan
	 * Purpose     : 
	 * 
	 ************************************************************************************************/
	public static String verifyCreditcarddetails() {
		String objStatus = null;
		String screenshotName = "Scenarios_verifyUserIsAbleToEnrollForSubscription_Screenshot.jpeg";
		try {
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.rdoRedeemGiftcard));
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			UIFoundation.clearField(PickedPage.txtRedeemedGiftCard);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtRedeemGiftcardNumber, "GiftCertificate1"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkRedeemGiftcardApply));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.txtValidCreditCard);
			UIFoundation.waitFor(2L);
			String ValidcredicardExpMsg=verifyexpectedresult.ValidCreditCard;
			String ValidcredicardActMsg=UIFoundation.getText(PickedPage.txtValidCreditCard);
			if(ValidcredicardActMsg.equalsIgnoreCase(ValidcredicardExpMsg))
			{
				objStatus+=true;
				String objDetail="Please provide a valid credit card is Displayed in the Payment section";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="Please provide a valid credit card is Not Displayed in the Payment section";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+"warningMsgShip", objDetail);
			}
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.lnkAddCreditCard));
			if(UIFoundation.isDisplayed(PickedPage.lnkAddCreditCard))
			{
				objStatus+=true;
				String objDetail="Add a Credit Card Link is Displayed in the Payment section";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="Add a Credit Card Link is Not Displayed in the Payment section";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+"warningMsgShip", objDetail);
			}
			
			if (objStatus.contains("false"))
			{				
				String objDetail="VerifyPlaceHolderTextLinkinSubscriptionFlow  test case failed ";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="VerifyPlaceHolderTextLinkinSubscriptionFlow test case executed succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
			
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
}
