package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyErrorDisplayedForInvalidProductId extends Mobile {
	

	

	/***************************************************************************
	 * Method Name			: verifyErrorDisplayedForInvalidProductId()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String verifyErrorDisplayedForInvalidProductId() {
		String objStatus=null;
		   String screenshotName = "Scenarios__ProductInvalidId.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
							+ screenshotName;
					//WebDriverWait wait=null;
		try {
			log.info("The execution of the method verifyErrorDisplayedForInvalidProductId started here ...");
			UIFoundation.waitFor(3L);
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtCaberNet));
			UIFoundation.waitFor(6L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkAddProductToPIP));
			UIFoundation.waitFor(2L);
			String productURl=getDriver().getCurrentUrl();
			getDriver().get(productURl+"james");
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.txtEror404);
			if(UIFoundation.isDisplayed(ListPage.txtEror404))
			{
				  objStatus+=true;
			      String objDetail="'Error 404' is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			     getlogger().pass(objDetail);
				
			}
			else
			{
				   objStatus+=false;
				   String objDetail="'Error 404' is not displayed";
				  getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
								 
			}	
			log.info("The execution of the method verifyErrorDisplayedForInvalidProductId ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
				
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method verifyErrorDisplayedForInvalidProductId "+ e);
			return "Fail";
		}

	}
}

