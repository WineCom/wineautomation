package wine.autotest.mobile.test;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import wine.autotest.mobile.test.ProductListFilteration;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.Initialize;
import wine.autotest.mobile.test.BaseTest;
import wine.autotest.fw.utilities.UIFoundation;

import wine.autotest.fw.utilities.GlobalVariables;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.XMLData;

public class Mobile  extends GlobalVariables{

	
	static int numberOfTestCasePassed=0;
	static int numberOfTestCaseFailed=0;
	public static String TestScriptStatus="";
	public static String teststarttime=null;
	
	/***************************************************************************
	 * Method Name			: loadFiles()
	 * Created By			: Ramesh S
	 * Reviewed By			: Chandrashekhar
	 * Purpose				: The purpose of this method  is to load .properties 
	 * 						   files.
	 ****************************************************************************
	 */
	
	@BeforeClass
	@Parameters({"browser", "testEnvironment", "teststate"})
	public static void loadFiles(String browser, String testEnvironment, String teststate)
	{ 
		String teststarttime=null;
		try
		{
				log=Logger.getLogger("wine Automation ...");
				teststarttime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				environmentUrl=testEnvironment;
				state=teststate;
				ReportUtil.createReport(MobileReportFileName, teststarttime,environmentUrl);
				UIBusinessFlow.deleteFile();
				xmldata=new XMLData();
				url=UIFoundation.property(configurl);
				UIFoundation.screenshotFolderName("screenshots");
				ReportUtil.startScript("Scenarios");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name			: ExtentReport()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method  is to generate extend report
	 ****************************************************************************
	 */
	@BeforeTest
	public void ExtentReport()
	{
		try
		{
			//Extend Report
			ExtentHtmlReporter reporter =new ExtentHtmlReporter(ExtentMobileReportFileName);
	        report =new ExtentReports();
	        report.attachReporter(reporter);
	        reporter.config().setDocumentTitle("Platform Automation");
	        reporter.config().setReportName("Mobile Execution Report");
	       
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	}
	
	/***************************************************************************
	 * Method Name			: LaunchBrowser()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: This method is used to launch the browser.
	 ****************************************************************************
	 */
	@BeforeMethod
	@Parameters({"browser"})
	public static void Launchbrowser(String browser)
	{ 
			try
		{				
				
				log=Logger.getLogger("Launch Browser ...");
				ReportUtil.deleteDescription();
				Initialize.launchBrowser(browser);
				objStatus = null;
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name			: TC01_BaseTest
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: this method is responsible for execution of all the 
	 * 						  scenarios.
	 ****************************************************************************
	 */
	public static String startTime=null;
	public static String endTime=null;
	public static String objStatus = null;
	
	@Test
	public static void TC01_BaseTest()
	{
			
			try
			{				
				startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
				Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                String[] m1=  Method.split("_");
                testCaseID = m1[0];
                methodName = m1[1];
                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.login());
					objStatus += String.valueOf(BaseTest.logout());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC02_productFilteration()
	{
			
			try
			{
				startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                String[] m1=  Method.split("_");
                testCaseID = m1[0];
                methodName = m1[1];
                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
			
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(ProductListFilteration.verifyOnlyThreeFiltersAreVisible());
					objStatus += String.valueOf(ProductListFilteration.verifyPagination());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	
	@Test
	public static void TC03_orderCreationWithExistingAccount()
	{			
			try
			{
               startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
				Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                String[] m1=  Method.split("_");
                testCaseID = m1[0];
                methodName = m1[1];
                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
                
                objStatus += String.valueOf(Initialize.navigate());
    			objStatus += String.valueOf(BaseTest.login());
    			objStatus += String.valueOf(OrderCreationWithExistingAccount.searchProductWithProdName());
    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForExistingUser());
    			objStatus += String.valueOf(OrderCreationWithExistingAccount.checkoutProcess());
    			
    			
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC04_orderCreationWithNewAccount()
	{
		
			try
			{
                startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
				Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                String[] m1=  Method.split("_");
                testCaseID = m1[0];
                methodName = m1[1];
                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
                objStatus += String.valueOf(Initialize.navigate());
    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());					
    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
    			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
    			objStatus += String.valueOf(OrderCreationWithNewAccount.addNewCreditCard());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					
					
			}
				catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC05_saveForLaterWithoutSignIn()
	{
		
			try
			{
                startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
				Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                String[] m1=  Method.split("_");
                testCaseID = m1[0];
                methodName = m1[1];
                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");getlogger().assignCategory("Mobile");
					
            	objStatus += String.valueOf(Initialize.navigate());					
    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
    			objStatus += String.valueOf(UIBusinessFlow.validationForSaveForLater());
    						
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC06_saveForLaterWithSignIn()
	{
		
			try
			{
                startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
				Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                String[] m1=  Method.split("_");
                testCaseID = m1[0];
                methodName = m1[1];
                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
					
                objStatus += String.valueOf(Initialize.navigate());	
    			objStatus += String.valueOf(BaseTest.login());
    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
    			objStatus += String.valueOf(UIBusinessFlow.validationForSaveForLater());				
					
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC07_orderCreationWihNewAccountBillingAddress()
	{
		
			try
			{
               startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
				Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                String[] m1=  Method.split("_");
                testCaseID = m1[0];
                methodName = m1[1];
                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
                objStatus += String.valueOf(Initialize.navigate());	
    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());										
    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
    			objStatus += String.valueOf(OrderCreationWihNewAccountBillingAddress.shippingDetails());
    			objStatus += String.valueOf(OrderCreationWihNewAccountBillingAddress.addNewCreditCard());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	public static void TC08_orderCreationWihExistingAccountBillingAddress()
	{		
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
	            	objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(BaseTest.login());								
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
	    			objStatus += String.valueOf(OrderCreationWihExistingAccountBillingAddress.shippingDetails());
	    			objStatus += String.valueOf(OrderCreationWihExistingAccountBillingAddress.addNewCreditCard());
	    						
	    			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC09_orderCreationWithPromoCodeForExistingUser()
	{		
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");		
					
					objStatus += String.valueOf(Initialize.navigate());
					
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
					objStatus += String.valueOf(OrderCreationWithPromoCodeForExistingUser.captureOrdersummary());
					objStatus += String.valueOf(OrderCreationWithPromoCodeForExistingUser.checkoutProcess());
					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC10_orderCreationWithPromoCodeForNewUser()
	{		
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
	                
	                objStatus += String.valueOf(Initialize.navigate());	
	    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());					
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithPromoCodeForNewUser.captureOrdersummary());
	    			objStatus += String.valueOf(OrderCreationWithPromoCodeForNewUser.shippingDetails());
	    			objStatus += String.valueOf(OrderCreationWithPromoCodeForNewUser.addNewCreditCard());

									
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC11_removeFewProductsFromTheCart()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());					
			objStatus += String.valueOf(RemoveFewProductsFromTheCart.captureOrdersummary());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			


		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public static void TC12_productsInCartAfterRemove()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
	               			
	    			objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(BaseTest.login());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(ProductsInCartAfterRemove.captureOrdersummary());
	    			objStatus += String.valueOf(ProductsInCartAfterRemove.productsAvailableInTheCart());
	    			
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC13_removeProductsFromTheSaveForLater()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(RemoveProductsFromTheSaveForLater.login());			
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());																				
					objStatus += String.valueOf(RemoveProductsFromTheSaveForLater.moveProductsToSaveForLater());
					objStatus += String.valueOf(RemoveProductsFromTheSaveForLater.productsAvailableInSaveForLater());
								
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	public static void TC14_forgotPassword()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(ForgotPassword.forgotPassword());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");	
							
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	@Parameters({"browser"})
	public static void TC15_userProfileCreationWithDesiredState(String browser)
	{	
		
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UserCreationWithDesiredState.userProfileCreation());
					objStatus += String.valueOf(Initialize.closeApplication());					
					Initialize.launchBrowser(browser);
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UserCreationWithDesiredState.login());
					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	@Parameters({"browser"})
	public static void TC16_userProfileCreationWithCheckoutProcess(String browser)
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus+=String.valueOf(UserCreationWithCheckoutProcess.addProductToCartFromListPage());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(UserCreationWithCheckoutProcess.userProfileCreation());
					objStatus += String.valueOf(BaseTest.logout());
					objStatus += String.valueOf(Initialize.closeApplication());				
					Initialize.launchBrowser(browser);
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UserCreationWithCheckoutProcess.login());
						
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	@Parameters({"browser"})
	public static void TC17_userProfileCreationMyWine(String browser)
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UserProfileCreationWithMyWine.userProfileCreation());
					objStatus += String.valueOf(BaseTest.logout());
					objStatus += String.valueOf(Initialize.closeApplication());					
					Initialize.launchBrowser(browser);
					objStatus+=String.valueOf(Initialize.navigate());
					objStatus+=String.valueOf(UserProfileCreationWithMyWine.login());
					objStatus+=String.valueOf(Initialize.closeApplication());		
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC18_orderCreationWithGiftCardForExistinguser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");			
						
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.login());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithGiftCardForExistinguser.captureOrdersummary());
	    			objStatus += String.valueOf(OrderCreationWithGiftCardForExistinguser.checkoutProcess());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	public static void TC19_orderCreationWithGiftCardForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
	                
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.captureOrdersummary());
	    			objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.shippingDetails());
	    			objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.addNewCreditCard());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC20_verifyStewardshipDiscount()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
				
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyStewardshipDiscount.stewardLogin());
					objStatus += String.valueOf(VerifyStewardshipDiscount.productIconCheck());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyStewardshipDiscount.captureOrdersummary());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC21_sortingOption()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
				
	               objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(SortingOptionInWineListPage.sortingMostPopular());
	    			objStatus += String.valueOf(SortingOptionInWineListPage.sortingMostInteresting());
	    			objStatus += String.valueOf(SortingOptionInWineListPage.sortingCustomerRating());
	    		//	objStatus += String.valueOf(SortingOptionInWineListPage.sortingProfessionalRating());
	    			objStatus += String.valueOf(SortingOptionInWineListPage.sortingOptionsAtoZ());
	    			objStatus += String.valueOf(SortingOptionInWineListPage.sortingOptionsZtoA());
	    			objStatus += String.valueOf(SortingOptionInWineListPage.sortingOptionsLtoH());
	    			objStatus += String.valueOf(SortingOptionInWineListPage.sortingOptionsHtoL());
	    			objStatus += String.valueOf(SortingOptionInWineListPage.sortingOldToNew());
	    			objStatus += String.valueOf(SortingOptionInWineListPage.sortingNewToOld());
	    			objStatus += String.valueOf(SortingOptionInWineListPage.savings());
	    			objStatus += String.valueOf(SortingOptionInWineListPage.justIn());
					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
							
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC22_verifyDiscountPriceInRedColorAndGrandTotalInBold()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");	
	                
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());		
	    			objStatus += String.valueOf(VerifyDiscountPriceInRedColorAndGrandTotalInBoldColor.captureOrdersummary());

								
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC23_searchProductWithCharacter()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(SearchProductByCharacter.searchProductWithProdName());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");								
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC24_removeAllProductsFromTheCartContinueShoppingWithoutSignIn()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(RemoveAllProdcutsFromCartContinueShopingWithoutSignIn.addprodTocrt());
					objStatus += String.valueOf(RemoveAllProdcutsFromCartContinueShopingWithoutSignIn.removeProductsFromTheCart());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");								
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC25_removeAllProductsFromTheCartContinueShoppingWithSignIn()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					objStatus += String.valueOf(RemoveAllProdcutsFromCartContinueShopingWithSignIn.addprodTocrt());
					objStatus += String.valueOf(RemoveAllProdcutsFromCartContinueShopingWithSignIn.removeProductsFromTheCart());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");								
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC26_giftRecipientEmailValidation()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
										
	            	objStatus += String.valueOf(Initialize.navigate());
	    		//	objStatus += String.valueOf(UIBusinessFlow.login());
	            	objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());					
	    			objStatus += String.valueOf(GiftReceipientEmailValidation.editRecipientPlaceORder());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(GiftReceipientEmailValidation.verifyGiftRecipientEmail());
	    			
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
										
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	 @Test
	public static void TC27_applyAndRemoveromoCodeForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");	
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
				    objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(ApplyAndRemovePromoCodeForExistingUser.captureOrdersummary());		
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
								
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC28_applyAndReovePromoCodeForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(ApplyAndRemovePromoCodeForNewUser.captureOrdersummary());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
								
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}

	
	@Test
	public static void TC29_quantityPickerInCartPage()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(QuantityPicker.quantityPick());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
							
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC30_invalidPromoCode()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.login());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(InvalidPromoCode.invalidPromoCode());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC31_verifySalesTaxInCartandFinalReviewPage()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.login());			
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(VerifySalesTaxInCartAndFinalReviewPage.verifySalesTaxInCartPage());
	    			objStatus += String.valueOf(VerifySalesTaxInCartAndFinalReviewPage.verifySalesTaxInFinalReviewPage());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC32_quantityPickerInPIP()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(QuantityPickerFromPIP.verifyQuantityPickerInCartPage());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");								
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC33_finalReviewPageContinueShopping()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(FinalReviewPageContinueShopping.verifyContinueShopping());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC34_invalidGiftCardValidation()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					

	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.login());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(InvalidGiftCertificate.invalidGiftCardValidation());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");								
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC35_VerifyWarningMsgaddShipTodayProductsAndListProductsToCart()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyWarningMessageForShipTodayProductsAndListProducts.addShipTodayProdTocrt());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");								
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC36_verifyWarningMsgForDryStateShippingAddress()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");	
				
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(VerifyFunctionlityOfDryStateStayInPreviousState.login());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(VerifyFunctionlityOfDryStateStayInPreviousState.dryStateShippingDetails());
	    			
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");						
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC37_verifyWarningMsgForOnlyPreSaleProducts()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
				//	objStatus += String.valueOf(VerifyWarningMsgForOnlyPreSaleProductsToCart.addPreSaleProdTocrt());
					objStatus += String.valueOf(VerifyWarningMsgForOnlyPreSaleProductsToCart.addPreSaleProdTocrt());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	
	@Test
	public static void TC38_verifyErrorMsgForUsedGiftCard()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
					  					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.login());					
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    		    objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(VerifyErrorMsgForUsedGiftCard.usedGiftCardValidation());
	    			
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
			
	@Test
	public static void TC39_verifyWarningMsgForDryStateSaveForLater()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
					/*objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyFunctionalityOfDryStateSaveItemsForLater.login(driver));
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyFunctionalityOfDryStateSaveItemsForLater.productDetailsPresentInCart(driver));
					objStatus += String.valueOf(VerifyFunctionalityOfDryStateSaveItemsForLater.dryStateShippingDetails(driver));
							*/
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}	
			
	@Test
	public static void TC40_verifyDryStateProductsUnavailableFunctionality()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
	                
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyDryStateProductsUnavailableFunctionality.verifyproductUnavailabileFunctionality());
		
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
			
	@Test
	public static void TC41_verifyAddToCartButtonFunctionlityForDryState()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyAddToCartButtonFunctionlityForDryState.verifyAddToCartButtonFunctionality());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");							
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC42_verifyWarningMsgForRemoveShipTodayProductsFromCart()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyWarningMsgForRemoveShipTodayProductFromCart.addShipTodayProdTocrt());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	
	@Test
	public static void TC43_HandelsXBottelsPerCustome()
	{	
		 
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(HandelsXBottelsPerCustomer.addLimitProdTocrtForStateDE());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC44_CreateAccountButtonInActive()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(CreateAccountButtonInactive.createButtonInActive());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	public static void TC45_VerifyInvalidEmailInCreateAccountForm()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyInValidEmailInCreateAccountForm.inValidEmail());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC46_HandleSoldInIncrementOfXProducts()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(HandleSoldInIncrementOfXProducts.addIncrementProdTocrt());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");							
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC47_SortOptionsOfGiftCard()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
				
					objStatus += String.valueOf(SortFunctionalityOfGiftCards.sortingOptionsAtoZ());			
					objStatus += String.valueOf(SortFunctionalityOfGiftCards.sortingOptionsZtoA());
					objStatus += String.valueOf(SortFunctionalityOfGiftCards.sortingOptionsLtoH());
					objStatus += String.valueOf(SortFunctionalityOfGiftCards.sortingOptionsHtoL());
					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC48_VerifyOutOFStockCheckboxChecked()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyOutOFStockCheckBoxChecked.searchProductWithProdName());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");						
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC49_VerifyProductsAreListedInAlphbeticalOrder()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyProductsAreListedInAlphbeticalOrder.verifyProductsAreListedAlbhabeticalOrder());
								 
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC50_PasswordValidationLessThanSixChar()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
	                
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(PasswordValidationLessThanSixChar.passwordValidationLessThanSixChar());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		
								
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC51_verifyEditFunctionalityofShippingAddress()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());					
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyEditFunctionalityofShippingAddress.recipientAddressEdit());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");								
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC52_orderCreationWithFedExAddress()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
										
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
					objStatus += String.valueOf(OrderCreationWithFedexAddress.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithFedexAddress.addNewCreditCard());
					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC53_verifyAddGiftWrappingToCartFunctionality()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());					
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyAddGiftWrappingToCartFunctionality.verifyGiftWrapping());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC54_addProductsToCartWithoutSelectingState()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");	
	           
				objStatus += String.valueOf(Initialize.navigateWithoutSelectinState());
				objStatus += String.valueOf(UIBusinessFlow.login());
			    objStatus += String.valueOf(VerifyChooseStateDialogPrompted.addprodTocrt());
			    objStatus += String.valueOf(VerifyChooseStateDialogPrompted.checkoutProcess());
			    endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");	
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC55_verifyStewardshipProgramAddedToCart()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(VerifyStewardshipProgramAddedToCart.addStewardshipToCart());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC56_verifyItemWithoutQuantityPicker()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyItemWithoutQuantityPicker.verifyQuantityPicker());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC57_verifyUnderVarietalFilters()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyMenuVarietalFilters.verifyVarietalMenu());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	
	
	@Test
	public static void TC58_verifySaveCreditCardInfoCheckboxUncheckedFunctionality()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifySaveCreditCardInfoCheckboxUnchecedFunctionality.verifySaveCrdeitCardInfo());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
									
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}

	
	@Test
	public static void TC59_verifySaveCreditCardInfoCheckboxCheckedFunctionality()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.login());		
	    			objStatus += String.valueOf(VerifySaveCreditCardInfoCheckboxCheckedFunctionality.addprodTocrt());
	    			objStatus += String.valueOf(VerifySaveCreditCardInfoCheckboxCheckedFunctionality.verifySaveCrdeitCardInfo());
	    			
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
												
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}

	
	@Test
	public static void TC60_verifyCollapsedPanelInShippingForm()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.login());		
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(VerifyCollapsedPanelInShippingForm.verifyCollapsedPanel());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}

	
	@Test
	public static void TC61_verifyStateChangeDialog()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");	
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyStateChangeDialog.verifyStateChangrDialog());
					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");	
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC62_verifySuggestedShippingAddressWidget()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
	            	objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(VerifySuggestedShippingAddressWidget.verifySuggestedAddress());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");									
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC63_verifyOptionTryVinatageAndRemoveButton()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");	
	            	objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());		
	    			objStatus += String.valueOf(VerifyOptionTryVinatageAndRemoveButton.searchProductWithProdName());	    			
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	
	@Test
	public static void TC64_orderCreationWithRegionForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(OrderCreationWithRegionForExistingUser.addprodTocrt());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithRegionForExistingUser.checkoutProcess());
					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	public static void TC65_orderCreationWithRegionForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());				
	    			objStatus += String.valueOf(OrderCreationWithRegionForNewUser.addprodTocrt());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
	    			objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.addNewCreditCard());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC66_orderCreationWithProductFilterationForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");	
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(OrderCreationWithFilteringTheProductsForExistingUser.productFilter());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());			
	    			objStatus += String.valueOf(OrderCreationWithFilteringTheProductsForExistingUser.checkoutProcess());
										
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC67_orderCreationWithProductFilterationForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
	    			objStatus += String.valueOf(OrderCreationWithProductFilterationForNewUser.productFilter());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
	    			objStatus += String.valueOf(OrderCreationWithProductFilterationForNewUser.addNewCreditCard());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");														
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC68_orderCreationWithSpecialProductsForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
	                objStatus += String.valueOf(Initialize.navigate());
	                objStatus += String.valueOf(OrderCreationWithSpecialProductsForExistingUser.specialProducts());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithSpecialProductsForExistingUser.checkoutProcess());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");																			
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC69_orderCreationWithSpecialProductsForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
	    			objStatus += String.valueOf(OrderCreationWithSpecialProductsForNewUser.specialProducts());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
	    			objStatus += String.valueOf(OrderCreationWithSpecialProductsForNewUser.addNewCreditCard());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC70_orderCreationWithTopRatedProductsForExistingUser()
	{	
			try
			{
				   startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(OrderCreationWithTopRatedForExistingUser.sortingOptionsTopRated());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithTopRatedForExistingUser.checkoutProcess());
	    			
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");										
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC71_orderCreationWithTopRatedProductsForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
	    			objStatus += String.valueOf(OrderCreationWithTopRatedForNewUser.sortingOptionsTopRated());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
	    			objStatus += String.valueOf(OrderCreationWithTopRatedForNewUser.addNewCreditCard());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");										
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	public static void TC72_orderCreationWithGiftsForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");	
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.login());
	    			objStatus += String.valueOf(OrderCreationWithGiftsForExistingUser.addGiftsToTheCart());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithGiftsForExistingUser.checkoutProcess());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC73_orderCreationWithGiftsForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
					
	            	objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());		
	    			objStatus += String.valueOf(OrderCreationWithGiftsForNewUser.addGiftsToTheCart());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
	    			objStatus += String.valueOf(OrderCreationWithGiftsForNewUser.addNewCreditCard());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC74_orderCreationWithMyWineForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(OrderCreationWithMyWineForexistingUSer.login());
	    			objStatus += String.valueOf(OrderCreationWithMyWineForexistingUSer.addMyWineProdTocrt());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithMyWineForexistingUSer.checkoutProcess());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");														
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC75_orderCreationWithShippingMethodForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");	
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(OrderCreationWithMyWineForexistingUSer.login());
	    			objStatus += String.valueOf(OrderCreationWithMyWineForexistingUSer.addMyWineProdTocrt());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithMyWineForexistingUSer.checkoutProcess());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");															
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC76_orderCreationWithShippingMethodForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
					objStatus += String.valueOf(OrderCreationWithShippingMethodForNewUser.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithShippingMethodForNewUser.addNewCreditCard());
					
										
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC77_orderCreationWithEditFunctionalityInFinalReviewPageForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
	            	objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.login());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithEditFunctionalityInFinalReviewPageForExistingUser.checkoutProcess());
										
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC78_orderCreationWithEditFunctionalityInFinalReviewPageForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");	
					
	                objStatus += String.valueOf(Initialize.navigate());		
	    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	    			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
	    			objStatus += String.valueOf(OrderCreationWithEditFunctionalityInFinalReviewPageForNewUser.shippingDetails());
	    			objStatus += String.valueOf(OrderCreationWithEditFunctionalityInFinalReviewPageForNewUser.addNewCreditCard());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");															
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
    @Test
	public static void TC79_addAddressUsingUserProfileServicesForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				
					
	            	objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.login());
	    			objStatus += String.valueOf(AddAddressUsingUserProfileServicesForExistingUser.addAddress());
	    			
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC80_addAddressUsingUserProfileServicesForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
					objStatus += String.valueOf(AddAddressUsingUserProfileServicesForNewUser.addAddress());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC81_orderCreationUsingUserProfileServicesWithStewardshipSettings()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");	
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
	    			objStatus += String.valueOf(OrderCreationUsingUserProfileServiceWithStewardshipSettings.addprodTocrt());
	    			objStatus += String.valueOf(OrderCreationUsingUserProfileServiceWithStewardshipSettings.productDetailsPresentInCart());
	    			objStatus += String.valueOf(OrderCreationWithEditFunctionalityInFinalReviewPageForNewUser.shippingDetails());
	    			objStatus += String.valueOf(OrderCreationUsingUserProfileServiceWithStewardshipSettings.addNewCreditCard());

	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC82_addPaymentMethodUsingUserProfileServicesForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					objStatus += String.valueOf(AddPaymentMethodUsingUserProfileServicesForExistingUser.addPaymentMethod());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC83_addPaymentMethodUsingUserProfileServicesForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");	
					
	                objStatus += String.valueOf(Initialize.navigate());
	    			objStatus += String.valueOf(UIBusinessFlow.login());
	    			objStatus += String.valueOf(AddPaymentMethodUsingUserProfileServicesForExistingUser.addPaymentMethod());
	                
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC84_orderCreationWithWhiteWineForNewUser()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
			objStatus += String.valueOf(OrderCreationWithWhiteWineForNewUSer.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());									
			objStatus += String.valueOf(OrderCreationWithWhiteWineForNewUSer.addNewCreditCard());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC85_orderCreationWithWhiteWineForExistingUser()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());	
			
			objStatus += String.valueOf(OrderCreationWithWhiteWineForExistingUser.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithWhiteWineForExistingUser.checkoutProcess());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC86_VerifyGiftWrappingSectionNtDisplayed()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
			objStatus += String.valueOf(VerifyGiftWrappingSection.addGiftsTocrt());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");	


		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC87_forgotPasswordValidation()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ForgotPasswordValidation.forgotPassword());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC88_forgotPasswordInvalidEmailErrorMsgValidation()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ForgotPasswordInvalidEmailMsgValidation.forgotPasswordInvalidEmailMsgValidation());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

		@Test
	public static void TC89_emailPrefernce()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(EmailPreference.saveButtonFunctionality());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC90_ratingStarsHiddenInPIPForGifts()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				
			objStatus += String.valueOf(Initialize.navigate());
			//	objStatus += String.valueOf(ApplicationDependent.login(driver));
			objStatus += String.valueOf(RatingStarsHiddenInPIPForGifts.VerifyRatingStarsHiddenInPIPForGifts());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC91_InvalidEmailGiftRecipientValidation()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				
						
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(InvalidEmailAddressInGiftRecipientEmail.editRecipientPlaceORder());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void T92_verifyPromoBarIsDisplayedInAccountPages()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyPromoBarIsDisplayedInAccountPages.verifyPromoBarIsDisplayedInAccountPages());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");


		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC93_verifyTheFunctionalityOfAddAddressLink()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForExistingUser());
			objStatus += String.valueOf(VerifyTheFunctionalityOfAddAddressLink.verifyTheFunctionalityOfAddAddressLink());
			
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC94_verifyTheContentsOfVerifyAddressDialog()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());		
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(VerifyTheContentsOfVerifyAddressDialog.shippingDetails());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					


		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC95_preferredAdressFunctionality()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UIBusinessFlow.loginPrefferedAddress());		
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(PreferredAdress.preferredAdressFunctionality());			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC96_verifyErrorMsgForAddingNewCreditCard()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());								
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyErrorMsgForAddingNewCreditCard.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC97_verifyEditFunctionalityForHomeAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyEditFunctionalityForHomeAddress.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyEditFunctionalityForHomeAddress.editHomeAddress());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC98_verifyEditFunctionalityForFedexeAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyTheEditFunctionalityForFedExAddress.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheEditFunctionalityForFedExAddress.editFedexAddress());															

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC99_verifyBehaviourOFContinueButtonInRecipientPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			String Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];								
				logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
				objStatus += String.valueOf(Initialize.navigate());
				objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
				objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				objStatus += String.valueOf(VerifyBehaviourOfContinueButtonInRecipientPage.verifyBehaviourOFContinueButtonInRecipientPage());

				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC100_verifyTheGiftOptionSummaryRecipientSection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheGiftOptionSummaryRecipientSection.verifyTheGiftOptionSummaryRecipientSection());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC101_newArrivalAlertsInUserProfile()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(NewArrivalAlertsInUserProfile.newArrivalAlertsInUserProfile());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");										

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC102_updateShippingAddressInAddressBook()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");            

			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UpdateShippingAddressInAddressBook.login());
			objStatus += String.valueOf(UpdateShippingAddressInAddressBook.updateShippingAddressInAddressBook());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				


		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC103_productsInsaveForLaterNtStayedBackShipToSateChange()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(ProductsInsaveForLaterNotStayedBackShipToSateChange.productsInsaveForLate());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			


		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC104_stewardshipUpsellInFinalReviewPAgeForStandardShipping()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(StewardshipUpsellInFinalReviewPAgeForStandardShipping.shippingDetails());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		


		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC105_itemsNtEligibleForPromoCodeRedemption()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");			

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ItemsNotEligibleForPromoCodeRedemption.itemsNotEligibleForPromoCodeRedemption());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	@Test
	public static void TC106_verifyUserIsNavigatedToAppropriateTextUrl()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyUserIsNavigatedToAppropriateTextUrl.verifyUserIsNavigatedToAppropriateTextUrl());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC107_verifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(VerifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping.verifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC108_verifyErrorMsgInvalidZipCodeForFedexeAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyErrorMsgInvalidZipCodeForFedexeAddress.invalidZipCodeInFedexAddress());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC109_verifyProductsAreListedInAlphabeticalOrder()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyProductsAreListedInAlphabeticalOrder.verifyProductsAreListedInAlphabeticalOrder());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public static void TC110_verifyErrorMessageInTheLoginComponent()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyErrorMsgInLoginComponent.verifyErrorMsgInLoginComponent());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC111_updateAccountInfoWithOutDate()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UpdateAccountInfoWithOutDate.login());	    			
			objStatus += String.valueOf(UpdateAccountInfoWithOutDate.updateAccountInfoWithOutDate());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC112_aboutProffessionalandProductAttributeDescriptionInPIP()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AboutProffessionalandProductAttributeDescriptionInPIP.aboutProffessionalandProductAttributeDescriptionInPIP());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC113_errorMessageDisplayedForTheUserBelow21Years()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(ErrorMessageDisplayedForTheUserBelow21Years.errorMessageDisplayedForTheUserBelow21Years());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC114_verifyTheSocialMediaLinks()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyTheSocialMediaLinks.verifyTheSocialMediaLinks());	    			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC115_stewardshipAutoRenewalOption()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyStewardshipDiscount.stewardLogin());
			objStatus += String.valueOf(StewardshipAutoRenewalOption.stewardshipAutoRenewalOption());									
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC116_newAlertSectionForCurrentlyUnavailable()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(NewAlertSectionForCurrentlyUnavailable.newAlertSectionForCurrentlyUnavailable());									
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC117_promoCodesThatDntMeetMinimumAmount()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(PromoCodesThatDntMeetMinimumAmount.addprodTocrt());
			objStatus += String.valueOf(PromoCodesThatDntMeetMinimumAmount.enterPromoCode());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC118_giftMessageRemainingCharacterCountdown()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login()); 
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(GiftMessageRemainingCharacterCountdown.editRecipientEnterGiftMessage());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC119_productAttributeDescriptionInListPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ProductAttributeDescriptionInListPage.productAttributeDescriptionInListPage());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC120_viewAllWineForDifferentStates()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ViewAllWineForDifferentStates.viewAllWineForDifferentStates());										
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC121_superscriptInProductPrice()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(SuperscriptInProductPrice.superscriptInProductPrice());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	@Test
	public static void TC122_ABVandFoundLowerPrice()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ABVandFoundLowerPrice.aBVandFoundLowerPrice());								
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC123_collapsedDeliverySectionWhenThePreSaleItemIsAdded()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(CollapsedDeliverySectionWhenThePreSaleItemIsAdded.addPreSaleprodTocrt());
			objStatus += String.valueOf(CollapsedDeliverySectionWhenThePreSaleItemIsAdded.verifyDeliverySection());					
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC124_removeCreditCard()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(RemoveCreditCard.removeCreditCard());										
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC125_promoBannerBehaviourForDryState()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(PromoBannerBehaviourForDryState.promoBannerBehaviourForDryState());										
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC126_sortOrderInNewArrivalAlerts()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(SortOrderInNewArrivalAlerts.sortedArrivalLogin());
			objStatus += String.valueOf(SortOrderInNewArrivalAlerts.verifyTheSortingInNewArrival());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC127_fedexLocationForUPSStatesInRecipientSection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(FedexLocationForUPSStatesInRecipientSection.fedexLocationForUPSStatesInRecipientSection());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC128_creditCardInfoIsUpdatedInPaymentMethods()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(CreditCardInfoIsUpdatedInPaymentMethods.addPaymentMethod());										
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC129_showOutOfStocksAndRecommenderProdutcs()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ShowOutOfStocksAndRecommenderProdutcs.showOutOfStocksAndRecommenderProdutcs());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC130_verifyTheAddressInRecipientPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyTheAddressInRecipientPage.recipientLogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheAddressInRecipientPage.verifyTheAddressInRecipientPage());										
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	
	@Test
	public static void TC131_verifyErrorDisplayedForInvalidProductId()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyErrorDisplayedForInvalidProductId.verifyErrorDisplayedForInvalidProductId());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC132_filterIsHighlightedInBlueInListPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(FilterIsHighlightedInBlueInListPage.filterIsHighlightedInBlueInListPage());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC133_verifyTheAddressGetsDeletedPermanently()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheAddressGetsDeletedPermanently.shippingDetails());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC134_addressGetsSavedOnceOnAddingNewAddressInUserProfile()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddressGetsSavedOnceOnAddingNewAddressInUserProfile.login());
			objStatus += String.valueOf(AddressGetsSavedOnceOnAddingNewAddressInUserProfile.addAddressInAddressBook());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC135_deletePreferredCreditCardUserProfile()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(DeletePreferredCreditCardUserProfile.addPaymentMethod());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC136_removePreferredCreditCardDuringCheckout()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(RemovePreferredCreditCardDuringCheckout.shippingDetails());
			objStatus += String.valueOf(RemovePreferredCreditCardDuringCheckout.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}



	@Test
	public static void TC137_verifyShipToTthisAddressSelectionChanged()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(VerifyShipToTthisAddressSelectionChanged.loginPrefferedAddress());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyShipToTthisAddressSelectionChanged.preferredAdresSelChanged());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC138_enteredGiftCardGiftCertificateSection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(EnteredGiftCardGiftCertificateSection.enterGiftCertificate());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC139_VerifyMultipleGiftCardCanBeReedemed()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyMultipleGiftCardCanBeReedemed.verifyMoreThanTwoGiftCardReedemed());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyMultipleGiftCardCanBeReedemed.addNewCreditCard());
			objStatus += String.valueOf(VerifyMultipleGiftCardCanBeReedemed.verifyOlyTwoGftCrdSelForSingleOrder());
			objStatus += String.valueOf(VerifyMultipleGiftCardCanBeReedemed.verifyAdditionalGftCrdIsUnChked());		
			objStatus += String.valueOf(Initialize.navigate());			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC140_userSelectsToPayWithDifferentCard()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");				

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UserSelectsToPayWithDifferentCard.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(UserSelectsToPayWithDifferentCard.userSelectsToPayWithDifferentCard());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC141_responsiveHeaderMenu()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ResponsiveHeaderMenu.varietalResponse());					
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC142_verifySortFunctionalitiesUnderMyWine()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifySortFunctionalitiesUnderMyWine.login());
			objStatus += String.valueOf(VerifySortFunctionalitiesUnderMyWine.addMyWineProdTocrt());	

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC143_wireUpCalender()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(CalenderDispalyedSixMonth.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(WireUpCalender.wireUpCalender());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC144_multipleTrackingOrderNumber()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(MultipleTrackingOrderNumber.login());
			objStatus += String.valueOf(MultipleTrackingOrderNumber.addprodTocrt());
			objStatus += String.valueOf(MultipleTrackingOrderNumber.multipleTrackingOrderNumber());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	

	@Test
	public static void TC145_giftBagIsDisplayedAsLineItemInShoppingCartPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			//	objStatus += String.valueOf(GiftBagIsDisplayedAsLineItemInShoppingCartPage.login());
				objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
				objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				objStatus += String.valueOf(GiftBagIsDisplayedAsLineItemInShoppingCartPage.addGiftBag());
				
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC146_createNewAccountWhileRatingTheProduct()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(CreateNewAccountWhileRatingTheProduct.createNewAccountWhileRatingTheProduct());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC147_existingCustomerLinkInNewCustomerRegisterPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ExistingCustomerLinkInNewCustomerRegisterPage.existingCustomerLinkInNewCustomerRegisterPage());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC148_verifyRatingOptionIsDisplayedInSliderBar()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyRatingOptionIsDisplayedInSliderBar.verifyRatingOptionIsDisplayedInSliderBar());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public static void TC149_continueWithFacebook()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ContinueWithFacebook.continueWithFacebook());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC150_verifyTheShipsOnStatementInPIP()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());	    			
			objStatus += String.valueOf(VerifyTheShipsOnStatementInPIP.verifyTheShipsOnStatementInPIP());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	} 

	
	@Test
	public static void TC151_verifyRedWarningMessageAppearInCart()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());				 
			objStatus += String.valueOf(VerifyWarningMsgForOnlyPreSaleProductsToCart.addPreSaleAdNormalProdTocrt());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC152_modalLightBoxWithXIcon()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyTheAddressInRecipientPage.recipientLogin());
			objStatus += String.valueOf(ModalLightBoxWithXIcon.modalLightBoxWithXIcon());
			objStatus += String.valueOf(ModalLightBoxWithXIcon.lightBoxInRecipientPage());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}



	@Test
	public static void TC153_limitReached()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(LimitReached.limitReached());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public static void TC154_ratingStarsDisplayedProperlyInMyWine()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(RatingStarsDisplayedProperlyInMyWine.login());
			objStatus += String.valueOf(RatingStarsDisplayedProperlyInMyWine.ratingStarsDisplayedProperlyInMyWine());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public static void TC155_verifyTheFourAddressAndCreditcards()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(OrderCreationWithExistingAccount.searchProductWithProdName());
			objStatus += String.valueOf(VerifyTheFourAddressAndCreditcards.verifyTheFourAddressAndCreditcards());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	@Test
	public static void TC156_verifyMilitaryOfficeBillingAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(VerifyMilitaryOfficeBillingAddress.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());			
			objStatus += String.valueOf(VerifyMilitaryOfficeBillingAddress.addNewCreditCard());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC157_verifyShipToDateUpdated()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");             

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyShipToDateUpdated.verifyShipToDateUpdated());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC158_verifyOrderOfProductsIconInPIP()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyOrderOfProductsIconInPIP.verifyOrderOfProductsIconInPIP());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC159_shipSoonCheckBox()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ShipSoonCheckbox.shipSoonCheckbox());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC160_verifyLocalPickupInformationDisplayedInCollapsedDeliverySection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");   

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyLocalPickupInformationDisplayedInCollapsedDeliverySection.addFedExAddress());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC161_verifyFedexDisplaysProperGoogleAdresses()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");             

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.loginR());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyFedexDisplaysProperGoogleAdresses.verifyFedexAddressInGoogleMaps());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	@Test
	public static void TC162_verifyBillingPhoneNumberIsRequiredField()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.loginR());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyBillingPhoneNumberIsRequiredField.addnewcreditCardWithoutPhoneNumberInPaymentMethod());
			objStatus += String.valueOf(VerifyBillingPhoneNumberIsRequiredField.addnewcreditCardWithoutPhoneNumberInPaymentMethod());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC163_verifyAddressSuggestioIsDisplayedAndAddressNtAutoUpdated()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.loginR());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyAddressSuggestioIsDisplayedAndAddressNtAutoUpdated.verifyAddressSuggestioIsDisplayed());


			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC164_verifyAboutProfRatingsHeaderisDispInProf()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");  

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyAboutProfRatingsHeaderisDispInProf.verifyprofessionalRating());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC165_passwordExceptsMoreThan15Char()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");   

			objStatus += String.valueOf(Initialize.navigate());	    	
			objStatus += String.valueOf(PasswordExceptsMoreThan15Char.verifyPasswordExceptsMoreThan15Char());
			objStatus += String.valueOf(PasswordExceptsMoreThan15Char.loginusingSamepassword());


			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC166_verifyDiffGiftBagTot()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyDiffGiftBagTot.addprodTocrt());
			objStatus += String.valueOf(VerifyDiffGiftBagTot.verifyDiffGiftBagTotInOrderSummary());


			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC167_verifyProductsAreRemovedInMyWine()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyProductsAreRemovedInMyWine.login());
			objStatus += String.valueOf(VerifyProductsAreRemovedInMyWine.verifyProductsAreRemovedInMyWine());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC168_verify30LocationsAreDispInFedexAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");             

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(Verify30LocationsAreDispInFedexAddress.login());
			objStatus += String.valueOf(Verify30LocationsAreDispInFedexAddress.verify30LocationsAreDispInFedexAddress());	                
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC169_verifyDefaultGiftBagButtonIsUnchecked()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");             

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyDefaultGiftBagButtonIsUnchecked.verifyNoGiftBagButtonIsUnchecked());
			objStatus += String.valueOf(VerifyDefaultGiftBagButtonIsUnchecked.verifyGiftbagName());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC170_verifyMyWineSignInNEditLinkForGiftMessage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");             

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyMyWineSignInNEditLinkForGiftMessage.signInModalForMyWinePage());
			objStatus += String.valueOf(VerifyMyWineSignInNEditLinkForGiftMessage.editOptionForGiftMessage());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC171_VerifyPromoBarIsDispInSignAndThankYouPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");         

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyPromoBarIsDispInSignAndThankYouPage.promoBarInSignPageandCreateAct());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyPromoBarIsDispInSignAndThankYouPage.promobarDispInThankYouPage());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC172_VerifyShippingCostAppliedToOrderSummary()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyShippingCostAppliedToOrderSummary.shippingCostAppliedToOrderSummary());		                               

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Test
	public static void TC173_verifyAlertDisplayedWhenProdAdded()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());	
			objStatus += String.valueOf(VerifyAddToCartAlertIsDispWhenProdIsAdded.verifyAddToCartAlertInHomePage());
			objStatus += String.valueOf(VerifyAddToCartAlertIsDispWhenProdIsAdded.verifyAddToCartAlertInListPage());
			objStatus += String.valueOf(VerifyAddToCartAlertIsDispWhenProdIsAdded.verifyAddToCartAlertInPiptPage());
			objStatus += String.valueOf(VerifyAddToCartAlertIsDispWhenProdIsAdded.verifyAddToCartAlertInMyWinePage());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC174_VerifyAddToCartButtonDisplayedForRecommendedProducts()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddToCartForRecomondedProducts.verifyAddToCartButtonForRecommendedProducts());                               

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC175_VerifyRecommendedProductsAfterRatingTheStars()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddToCartForRecomondedProducts.rateTheStarsAndVerifyRecommendedProducts()); 			                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC176_VerifySaveForLaterOnClickingContinue_ShipToKY()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(SaveForLater.verifySaveForLaterOnClickingShipToKY());					
	                       

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC177_VerifyProductMovedToCartFromSaveForLaterOnClickingMoveToCart()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			
			objStatus += String.valueOf(Initialize.navigate());
		//	objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(OrderCreationWithSpecialProductsForNewUser.specialProducts());			
			objStatus += String.valueOf(SaveForLater.VerifyProductMovedToCartFromSaveForLater());
			                               

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC178_verifyCustomerShipstateRevertedToPreviousStateInsideShoppingCart()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());	
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());					
			objStatus += String.valueOf(VerifyCustomerShipStateRevertedToPreviousState.customerShipstateRevertedToPreviousStateInSideShoppingCart());				
			                         

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC179_verifyCustomerShipstateRevertedToPreviousStateOutsideShoppingCart()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());	
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());		
			objStatus += String.valueOf(VerifyCustomerShipStateRevertedToPreviousState.customerShipstateRevertedToPreviousStateOutSideShoppingCart());				
		                  
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}	
		
	@Test
	public static void TC180_verifyTheBehaviorOfTheContinueButtonIfAddressNtSelectedInRecipientPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
     		objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
     		objStatus += String.valueOf(VerifyTheBehaviourOfTheBillingAddresChechBox.shippingDetails());   		
     		objStatus += String.valueOf(VerifyTheBehaviourOfTheBillingAddresChechBox.verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected());
				
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC181_verifyTheBillingAddressCheckboxIsCheckedByDefault()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());	
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheBehaviourOfTheBillingAddresChechBox.checkBoxSelectedDefault());
		//	objStatus += String.valueOf(VerifyCustomerShipStateRevertedToPreviousState.customerShipstateRevertedToPreviousStateOutSideShoppingCart(driver));				
		                          

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC182_verifyBillingAddressDisplayedOnUncheckingTheBillingSameAsShipping()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());	
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheBehaviourOfTheBillingAddresChechBox.unCheckBoxSelected());	                               

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC183_verifyAddressGetsDeletPermanentlyFromTheShippingAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddressGetsSavedOnceOnAddingNewAddressInUserProfile.login());
			objStatus += String.valueOf(AddressGetsSavedOnceOnAddingNewAddressInUserProfile.removeAddressInAddressBook());                       

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC184_verifyContentInTheOrderPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());			
			objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.shippingDetails());
			objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.addNewCreditCard());
			objStatus += String.valueOf(VerifyTheContentsInTheOrderPage.verifyTheContentOntheOrderPage());                              

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC185_verifyShippinAndHandalingChargesAppliedToTheNtStewardshipUser()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForExistingUser());
			objStatus += String.valueOf(VerifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping.verifyShippingAndHandalingChargesAreAppliedToTheNonStewardshipUser());                              

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC186_stewardUpsellshouldMatchShippingAndHandalingCharges()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(StewardshipUpsellInFinalReviewPAgeForStandardShipping.shippingDetails());                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC187_verifyGoBackButtonIsDispInFedexWindow()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyFedexDisplaysProperGoogleAdresses.fedexLocationGoBackButtonDisplayed());                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}	
	
	@Test
	public static void TC188_validatePromoCodeAppliedMessageDisplayed()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(PromoCodesThatDntMeetMinimumAmount.addprodTocrt());
			objStatus += String.valueOf(PromoCodesThatDntMeetMinimumAmount.validatePromoCodeMessageDisplayed());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace(); 
		}
	}
	
	@Test
	public static void TC189_verifyTheRareProductAttributeIconDisplayed()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(SpiritsRareProducts.validateRareProductIcon());                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Test
	public static void TC190_verifyPromoCodeAndGiftCardErrorMassage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(VerifyInvalidPromoCodeAndGiftCard.invalidPromoCode());
			objStatus += String.valueOf(VerifyInvalidPromoCodeAndGiftCard.invalidGiftCardValidation());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC191_verifyCorporateGiftLinkDisplayedUnderCustomerCareSection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyAddAntherTextAndCorporateGiftsLink.verifyCorperateLinksInCustomerCareSection());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC192_VerifyTatVarietalNRegOriginTextInPIPAreTheLinksToListPages()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyTatVarietalNRegOriginTextInPIPAreTheLinksToListPages.varietalNRegionLinkInPip());                           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC193_VerifySimilarRecmondedProdAredispAbvTheWineMakerNtesForOutOfStockInPip()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifySimilarRecmondedProdAredispAbvTheWineMakerNtesForOutOfStockInPip.recomendedProdDisplayedForOutOfStock());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC194_verifyTheConfirmationBannerForAddedProduct()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());	
			objStatus += String.valueOf(VerifyConfirmationBannerDisplayedWhenAddingProductToCart.confirmationBannerDisplayedForAddedProducts());                          

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC195_verifyFilterByLinkDisplayed()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyFilterByLinkDisplayed.verifyOnlyThreeFiltersAreVisible());
			objStatus += String.valueOf(VerifyFilterByLinkDisplayed.verifyMoreFiltersElementsAreVisible());                            

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC196_verifyUserIsNtGettingPickUpInformationMultipleTimes()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyUserIsNotGettingPickUpInformationMultipleTimes.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyUserIsNotGettingPickUpInformationMultipleTimes.verifyPickUpInformation());                            

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC197_loginUsingFacebook()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(LoginUsingFacebook.loginUsingFacebook());                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC198_verifyLovalPickUpFinderInFooterMobileSection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(Verify30LocationsAreDispInFedexAddress.login());
			objStatus += String.valueOf(Verify30LocationsAreDispInFedexAddress.verifyLocalPickupFinderInFooterSection());                          

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC199_forgotPasswordDuringCheckOut()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(ForgotPassword.forgotPassWordDuringCheckOut());                            

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC200_verifyEditFunctionalityofPreferredAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyEditFunctionalityofShippingAddress.preferredAddressEdit());  
			

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC201_verifyCustomerCareLinkinHeader()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyHeaderandFooterLinks.VerifyCustomerCareHeader());			                           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC202_verifyLinkinFooter()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyHeaderandFooterLinks.VerifyLinksinFooter());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC203_VerifyStewardshipStdShiping()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyStewardshipMember.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyStewardshipMember.VerifyDefaultstewardshipStdShipping());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC204_reportingAddressPopUpforNewPickUpLocation()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyShippingaddress.verifyHomePickUpLocationAddress());                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC205_VerifyUserNtabletoSelectSaturdayForShippinginAlaska()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.loginOtherState("StateAK"));
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(CalenderDispalyedSixMonth.AlaskaStateSaturdayCalenderSelect());                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
		
	@Test
	public static void TC206_VerifyUserNtabletoSelectSaturdayForShippinginHawaii()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.loginOtherState("StateHI"));
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(CalenderDispalyedSixMonth.HawaiiStateSaturdayCalenderSelect());                            

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC207_verifyAddToMyWineFunctionalityinPIP()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");		
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyAddToMyWineFunctionality.verifyAddToMyWineFunctionalityinPIP());                            

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC208_verifyAddToMyWineFunctionalityinPIPforRating()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyAddToMyWineFunctionality.verifyAddToMyWineFunctionalityinPIPforRating());			                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC209_VerifySigninModelinMyWineforListandPIP()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifySigninModelinMyWineforListandPIP.signInModalForMyWineforListandPIP());                        

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC210_ValidateOrderHistoryProgressBar()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyOrderHistoryPage.OrderHistoryProgressBarValidation());			                            

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC211_OrderHistoryShippingAmountValidation()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyOrderHistoryPage.OrderHistoryShippingAmountValidation());                           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC212_verifyCopyRighttxtinFooter()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyHeaderandFooterLinks.VerifyCopyrightsinFooter());                           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC213_VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard.addingNewCardInPaymentSection());
			objStatus += String.valueOf(VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard.searchProductWithProdName());
			objStatus += String.valueOf(VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard.verifyDOBFiledPresent());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC214_verifyTheElementsDisplayedOverTheHeroImage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyTheElementsDisplayedOverTheHeroImage.verifyTheElementsDisplayedOverTheHeroImage());			                       

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC215_calenderDispalyedSixMonth()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(CalenderDispalyedSixMonth.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(CalenderDispalyedSixMonth.calenderSixMnthInDeliverySection());                          

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC216_alphabeticalOrderMovingFromSaveForLaterToCart()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AlphabeticalOrderMovingFromSaveForLaterToCart.alphabeticalOrderMovingFromSaveForLaterToCart());
			objStatus += String.valueOf(VerifyProductsAreListedInAlphabeticalOrder.verifyProductsAreListedInAlphabeticalOrder());                     

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC217_VerifyPromoBannerInHomePage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyPromoBannerinHomePage.VerifypromoBanner());	
	
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC218_VerifyStewardshipMessageNtDisplayedinCartPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyStewardshipMember.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyStewardshipMember.verifyStewardshipPromoNotDisplayed());           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public static void TC219_verifyGiftCardAccountDisplayedInThePaymentOption()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());			
			objStatus += String.valueOf(VerifyTwoGiftCardAccountDisplayedInPaymentOption.verifyTwoGiftCardReedemed());			
			objStatus += String.valueOf(VerifyTwoGiftCardAccountDisplayedInPaymentOption.verifyTwoGiftCardAccountsDisplayed());     

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC220_VerifyUserAbleToAddGifrWrappingMessageAndEmailAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyUserAbleToAddGifrWrappingMessageAndEmailAddress.verifyTheUserAbleToAddGiftAddress());    

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC221_verifyTheOfferPageIsDisplayed()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyTheOfferPageIsDisplayedWhenNewUerClickOnUserProfile.offerPageDisplayed());
	                           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC222_verifyTheElementsDisplayedInThankyouPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyElementDisplayedInThankYouScreen.offerPageDisplayed());
			objStatus += String.valueOf(VerifyElementDisplayedInThankYouScreen.shippingDetails());
			objStatus += String.valueOf(VerifyElementDisplayedInThankYouScreen.addNewCreditCard());                    

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Test
	public static void TC223_verifyStewardshipDiscountDisplayedNegetiveInHistory()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyStewardshipDiscountDisplayedNegetiveInHistory.stewardLogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(VerifyStewardshipDiscountDisplayedNegetiveInHistory.shippingDetails());
			objStatus += String.valueOf(VerifyStewardshipDiscountDisplayedNegetiveInHistory.addNewCreditCard());                    

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC224_verifyDeliveryCalenderDisplayedWhenUserSelectsDeliveryAnyway()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UIBusinessFlow.userProfileCreation());			
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyDeliveryCalenderDisplayedWhenUserSelectsDeliveryAnyway.shippingDetails());			             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC225_VerifyGftCardBalDispOrdrSummaryUponRedeem()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyGftCardBalDispInOrdrSummaryUponRedeem.addProductToCartLessThnGftCrdPrice());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyGftCardBalDispInOrdrSummaryUponRedeem.verifyGftCardBalDispInOrdrSummaryUponRedeem());                 

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Test
	public static void TC226_VerifyGftCrdAccIsDispInPymntOpt()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyGftCrdAccIsDispInPymntOpt.verifyGftCrdAccAndBalDispInPaymntOpt());
			                

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC227_VerifyGftCrdSucessMsgDispAftrGftCrdApplied()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyGftCrdSucessMsgDispAftrGftCrdApplied.verifyGiftCardAppliedSuccessMsg());		                 

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC228_VerifyTheRecipientSectionUIUnderSubscriptionFlow()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifySignUpFunctionalitiesFlow.verifyUserIsAbleToEnrollForSubscription());
			objStatus += String.valueOf(VerifyTheRecipientSectionUIUnderSubscriptionFlow.verifyRecipientSectionAfterSubscriptionFlow());
					
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC229_ValidateTheSubscriptionFlowForLocalPickupAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifySignUpFunctionalitiesFlow.verifyUserIsAbleToEnrollForSubscription());
			objStatus += String.valueOf(ValidateTheSubscriptionFlowForLocalPickupAddress.verifyLocalPickUpFlowForPickedUpWine());
		            
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC230_VerifySubscriptionSettingsSummIsDispInChckoutNRecipientPaymnt()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifySignUpFunctionalitiesFlow.verifyUserIsAbleToEnrollForSubscription());
			objStatus += String.valueOf(VerifySubscriptionSettingsSummIsDispInChckoutNRecipientPaymnt.verifySubscriptionSettingsInChckout());
			                

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC231_VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyGftCardBalDispInOrdrSummaryUponRedeem.addProductToCartLessThnGftCrdPrice());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection.verifyZeroBalGftCrdIsRemovedInPymntOpt());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC232_ValidateTheSubscriptionFlowForHomeAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifySignUpFunctionalitiesFlow.verifyUserIsAbleToEnrollForSubscription());
			objStatus += String.valueOf(ValidateTheSubscriptionFlowForHomeAddress.verifyHomeAddrFlowForPickedUpWine());			
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC233_VerifyDetailedTermsAndConditionsPageIsDispOnClickingReadTermsLnk()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(PickedWineGetStarted.enrollForPickedUpWine());
			objStatus += String.valueOf(VerifyDetailedTermsAndConditionsPageIsDispOnClickingReadTermsLnk.verifyDetailedTermsDispOnClickingTermsLnk());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC234_verifyDeliveryViaEmailDisplayed()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyDeliveryViaEmailDisplayed.addGiftsToTheCart());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(VerifyDeliveryViaEmailDisplayed.shippingDetails());
			objStatus += String.valueOf(VerifyDeliveryViaEmailDisplayed.addNewCreditCard());			           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC235_verifyCancelCompassUserSubscription()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyCompussUsersSubscriptions.login());
			objStatus += String.valueOf(VerifyCompussUsersSubscriptions.verifySubscriptions());			
	
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC236_verifyGiftCardRedeemedDisplayedInTheOrderSummary()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());			
			objStatus += String.valueOf(VerifyGiftCardRedeemedDisplayedInOrderSummary.applyGiftCode());			
			objStatus += String.valueOf(VerifyGiftCardRedeemedDisplayedInOrderSummary.verifyGiftCardAmountInOrderSummary());					               

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC237_verifyGiftCardRemainingIsNtDisplayedInTheOrderSummary()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());			
			objStatus += String.valueOf(VerifyGiftCardRemainingIsNtDisplayedInTheOrderSummary.applyGiftCode());			
			objStatus += String.valueOf(VerifyGiftCardRemainingIsNtDisplayedInTheOrderSummary.verifyGiftCardAmountInOrderSummary());            

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC238_verifyCompassUserNtAbleToEnrollForDryState()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyCompassUserNotAbleToEnrollForDryState.offerPageDisplayed());
			objStatus += String.valueOf(VerifyCompassUserNotAbleToEnrollForDryState.shippingDetails());
			objStatus += String.valueOf(VerifyCompassUserNotAbleToEnrollForDryState.addNewCreditCard());               

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC239_VerifyLikedandDisLikedRedWhiteWineinAnythingElseSectionAndTotalPricewithQtyvalidation()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(ValidateRedWhiteVarietalPreffinAnythingElseSec.VerifyLikeandDislikeinAnythingElseSec());
			              

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC240_ValidateCompassEnrollmentFlowforWhiteWine()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforWhiteWine.verifyUserIsAbleToEnrollForWhiteWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforWhiteWine.verifyLocalPickUpForPickedUpWine());         

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC241_ValidateCompassEnrollmentFlowforRedWine()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(ValidateEnrollmentFlowforRedWine.enrollForPickedUpWine());
			objStatus += String.valueOf(ValidateEnrollmentFlowforRedWine.verifyLocalPickUpForPickedUpWine());                   

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC242_ValidateCompassEnrollmentFlowforBothRedandWhiteWineWithSubscriptionSettingsandPymtMethodEdit()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.enrollForPickedUpWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.verifyLocalPickUpForPickedUpWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.EditSubscriptioninPickedSettings());			
		//	objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.EditPaymentMethodinPickedSettings());          

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC243_VerifyChangeDelvyDateandRecipientAddressinPickedSettingpage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.enrollForPickedUpWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.verifyLocalPickUpForPickedUpWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.ChangedDeliveryDateinPickedSettings());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.ChangeRecipientAddressinPickedSettings());
		    
		
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC244_VerifyMySommPageDisplayedthroughPickedSettingNav()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyMySommPageDisplayedthroughNav.login());
			objStatus += String.valueOf(VerifyMySommPageDisplayedthroughNav.VerifyMySommPageDisplayedthroughNavigation());
		
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public static void TC245_verifyDescriptionDisplayedInRecipientSectionWhenSelectLocalPickup()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyDescriptionDisplayedInRecipientSectionWhenSelectLocalPickup.offerPageDisplayed());
			objStatus += String.valueOf(VerifyDescriptionDisplayedInRecipientSectionWhenSelectLocalPickup.shippingDetails());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC246_verifyContentDisplayedUnderMySOMMSection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyContenDisplayedUnderMySOMMSection.login());
			objStatus += String.valueOf(VerifyContenDisplayedUnderMySOMMSection.verifySubscriptions());			
						
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC247_VerifyContentforYourSubscriptioninPickedSetting()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyContentforYourSubscriptioninPickedSettings.RedWhiteWineSubscriptionflow());
			objStatus += String.valueOf(VerifyContentforYourSubscriptioninPickedSettings.verifyLocalPickUpForPickedUpWine());
			objStatus += String.valueOf(VerifyContentforYourSubscriptioninPickedSettings.ValidateContentforYourSubscriptioninPickedSettings());
						
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC248_VerifyRescheduleSubscriptioninPickedSetting()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyRescheduleSubscriptioninPickedSetting.enrollForPickedUpWine());
			objStatus += String.valueOf(VerifyRescheduleSubscriptioninPickedSetting.verifyLocalPickUpForPickedUpWine());
			objStatus += String.valueOf(VerifyRescheduleSubscriptioninPickedSetting.ChangedDeliveryDateinPickedSettings());			
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC249_VerifyUserAbletoSetRedWhiteBottleCountto0or6()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
			objStatus += String.valueOf(VerifyRedWhiteQtyAllowedto0or6withMultipleClasses.VerifyRedWhiteQtySetto0or6());
					
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC250_VerifyRedWhiteButtonPresentinPriceQtyWidgetPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(RedWhiteButtonPresentinPriceQuantityWidget.FunctionalityRedorWhiteButtoninPriceQtyPage());
		 
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC251_VerifyLikeDislikeNtSelectedinVeritalpreferencePage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyLikeDislikeSectionNotDisplayedinVaritalPreference.LikeDisLikeNotSelectedinVarietalPage());
		    			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC252_verifyDislikeSectionNtDisplayedInReviewPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyDislikeSectionNtDisplayedTheVarietalPreferenceReviewPage.verifyDisLikeSectionInReviewPage());
		   		    
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC253_verifyLikeSectionNtDisplayedInReviewPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyLikeSectionNtDisplayedTheVarietalPreferenceReviewPage.verifyLikeSectionInReviewPage());
		  
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC254_verifyErrorMessageDisplayedOnClickingConfirmButton()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyCompussUsersSubscriptions.login());
			objStatus += String.valueOf(VerifyErrorMessageDisplayedUponClickingConfirmButton.verifyErrorMessageDisplayed());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC255_verifyVaritalPreferenceSectionAvailable()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			  		
			objStatus += String.valueOf(VerifyVaritalPreferenceSectionAvailableWhenSommIsNotAssigned.offerPageDisplayed());
			objStatus += String.valueOf(VerifyVaritalPreferenceSectionAvailableWhenSommIsNotAssigned.shippingDetails());
			objStatus += String.valueOf(VerifyVaritalPreferenceSectionAvailableWhenSommIsNotAssigned.addNewCreditCard());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC256_verifyTheDisplayOfSpiritMenu()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyTheDisplayOfSpiritsMenuInTheWineHomePage.validateSpiritMenu());	
		    
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC257_verifySpiritsShowsAsOutOfStock()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifySpiritsShowsOutOfCartWhenSelectedOutSideState.verifyOutOfStockSpirits());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC258_verifyFiltersSupportingSpirits()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyFilterSupportingSpirits.validateTheSpiritsFilters());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC259_verifyUserAbleToAddSpiritProductViaPIP()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyUserAbleToAddPIPViaSpirit.login());
			objStatus += String.valueOf(VerifyUserAbleToAddPIPViaSpirit.ratingStarsDisplayedProperlyInMyWine());
		    
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Test
	public static void TC260_verifyEstimatedDeliveryAndChangeDate()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());	
			objStatus += String.valueOf(VerifyTxtEstimatedDeliveryAndChangeDate.verifyEstimatedDeliveryText());					           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	
	@Test
	public static void TC261_verifyTheLimitPerCustomerForPresaleProduct()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());				
			objStatus += String.valueOf(VerifyLimitPerCustomerForPreSaleProduct.addPreSaleProdTocrt());					           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Test
	public static void TC262_verifyPreSaleBlurbDisplayedInDeliverySection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());			
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());				
			objStatus += String.valueOf(VerifyPreSaleBlurbDisplayedInDeliverySection.addPreSaleProdTocrt());
			objStatus += String.valueOf(VerifyPreSaleBlurbDisplayedInDeliverySection.ValidatePreSaleBlurbDisplayedIn());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC263_removePromoCodeFromTheSubscriptionModel()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile");		
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());						  		
			objStatus += String.valueOf(VerifyPromoCodeRemovedFromTheSubscriptionModel.subscriptionModelPage());
			objStatus += String.valueOf(VerifyPromoCodeRemovedFromTheSubscriptionModel.shippingDetails());
			objStatus += String.valueOf(VerifyPromoCodeRemovedFromTheSubscriptionModel.removePromoCodeFromSubscriptionModel());
			
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC264_verifySurveyMonkeyQuizRemoved()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger);getlogger().assignCategory("Mobile"); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
										  		
			objStatus += String.valueOf(VerifySurveyMonkeyIsRemovedOnPickedThankyouScreen.subscriptionModelPage());
			objStatus += String.valueOf(VerifyPromoCodeRemovedFromTheSubscriptionModel.shippingDetails());
			objStatus += String.valueOf(VerifySurveyMonkeyIsRemovedOnPickedThankyouScreen.validateSurveyMonkeyRemoved());
			
			
			

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name			: VerifyExpandedlearnaboutcontentisDispforListPginMobile()
	 * Created By			: Chandrashekhar K B
	 * Reviewed By			: 
	 * Jira Id				: 
	 ****************************************************************************
	 */
	@Test
	public static void TC265_VerifyExpandedlearnaboutcontentisDispforListPginMobile()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile"); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyExpandedlearnaboutcontentisDispforListPginMobile.VerifyExpandedContentForListpage());
		    
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC266_OrderCreationWithOrderAgainWithSameProduct() 
	 * Created By  : Ramesh S
	 * Reviewed By : 
	 * Purpose     : To Verify the Order Again Functionality.
	 * Jira id     : TM-3390
	 ****************************************************************************
	 */
	@Test 
	public static void TC266_OrderCreationWithOrderAgainWithSameProduct() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Desktop");
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());	
			objStatus += String.valueOf(OrderCreationWithOrderAgain.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
            objStatus += String.valueOf(OrderCreationWithOrderAgain.checkoutProcess());
            objStatus += String.valueOf(OrderCreationWithOrderAgain.orderAgainToTheSameLocation()); 			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			 
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
		
	/***************************************************************************
	 * Method Name : TC260_WhetherHoldOptionforGreater300()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 *   * Jira id     : TM-4758
	 ****************************************************************************
	 */
	@Test 
	public static void TC260_WhetherHoldOptionforGreater300() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Desktop");
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(WhetherHoldScenarios.loginOtherState("StateCO"));
			objStatus += String.valueOf(WhetherHoldScenarios.addprodTocrt());
			objStatus += String.valueOf(WhetherHoldScenarios.VerifyWeatherHoldforGreater300());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***********************************************************************************************
	 * Method Name :  TC268_PromoCodeValidatioinOnboardingandPaymentPage() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     : 
	 * Jira id	   :  TM-4974, TM-4986
	 ************************************************************************************************
	 */
	@Test 
	public static void TC268_PromoCodeValidatioinOnboardingandPaymentPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Desktop");
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(ValidatePromoBarforPickedPages.promobarinOnboardingPage());
			objStatus += String.valueOf(ValidatePromoBarforPickedPages.verifyPromocodeAppliedinPymtPage());	    
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  : TC269_VerifyPlaceHolderTextLinkinSubscriptionFlow()
	 *  Created By  : Ramesh S
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test
	public static void TC269_VerifyPlaceHolderTextLinkinSubscriptionFlow()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	               logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile"); 	
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
					objStatus += String.valueOf(VerifyPlaceHolderTextLinkinSubscriptionFlow.enrollForPickedUpWine());
					objStatus += String.valueOf(VerifyPlaceHolderTextLinkinSubscriptionFlow.verifyRecipientSectionAfterSubscriptionFlow());
					objStatus += String.valueOf(VerifyPlaceHolderTextLinkinSubscriptionFlow.verifyCreditcarddetails());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	/***************************************************************************
	 * Method Name  : TC270_ValidateCustomStylingforDefaultModel()
	 *  Created By  : Ramesh S
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC270_ValidateCustomStylingforDefaultModel() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());    
            objStatus += String.valueOf(ValidateCustomStylingforDefaultModel.enrollForPickedUpWineandvalidatepromobar());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name  : TC271_VerifyAuthScreenDispinCompassSubscriptionFlow()
	 *  Created By  : Ramesh S
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC271_VerifyAuthScreenDispinCompassSubscriptionFlow() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());    
            objStatus += String.valueOf(VerifyAuthScreenisDispinCompassSubscriptionFlow.enrollpickedandverifyauthscreen());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  : TC272_VerifyAuthScreenisDispinCompassSubscriptionFlowWhenUserSignedin()
	 *  Created By  : Ramesh S
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC272_VerifyAuthScreenisDispinCompassSubscriptionFlowWhenUserSignedin() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
            objStatus += String.valueOf(VerifyAuthScreenisDispinCompassSubscriptionFlowWhenUserSignedin.enrollpickedandverifyauthscreen());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  : TC273_ValidateHALCountinPickedRecipient()
	 *  Created By  : Ramesh S
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC273_ValidateHALCountinPickedRecipient() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
            objStatus += String.valueOf(ValidateHALCountinPickedRecipient.enrollForPickedUpWineandvalidateHAlcount());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  : TC274_ValidateHALCountinLocalPickupandRecipient()
	 *  Created By  : Ramesh S
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC274_ValidateHALCountinLocalPickupandRecipient() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
            objStatus += String.valueOf(ValidateHALCountinLocalPickupandRecipient.ValidateHAlCountinLocalPickupfinder());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
            objStatus += String.valueOf(ValidateHALCountinLocalPickupandRecipient.ValidateHAlCountinRecepientPage());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 *  Method Name : TC275_VerifyReasonReqtooptoutStewardRenew()
	 *  Created By  : Ramesh S
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC275_VerifyReasonReqtooptoutStewardRenew() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(VerifyReasonReqtooptoutStewardRenew.login());
            objStatus += String.valueOf(VerifyReasonReqtooptoutStewardRenew.VerifySTWRDCancelMembership());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  :TC276_VerifyKeepMembershipFunctionalityDuringStewardOptOutFlow()
	 *  Created By  : Ramesh S
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC276_VerifyKeepMembershipFunctionalityDuringStewardOptOutFlow() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(VerifyKeepMembershipFunctionalityDuringStewardOptOutFlow.login());
            objStatus += String.valueOf(VerifyKeepMembershipFunctionalityDuringStewardOptOutFlow.VerifyKeepMembershipFunctionality());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  :TC277_VerifyStewardAccountPageDispForStewardUser()
	 *  Created By  : Ramesh S
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC277_VerifyStewardAccountPageDispForStewardUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(VerifyStewardAccountPageDispForStewardUser.login());
            objStatus += String.valueOf(VerifyStewardAccountPageDispForStewardUser.VerifyStewardsettingsPage());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  :TC278_VerifyStewardAccountPageDispForNonStewardUser()
	 *  Created By  : Chandrashekar K B
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC278_VerifyStewardAccountPageDispForNonStewardUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
            objStatus += String.valueOf(VerifyStewardAccountPageDispForNonStewardUser.VerifyStewardsettingsPage());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  :TC279_ValidateStewardshipLifeTimeSavings()
	 *  Created By  : Chandrashekar K B
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC279_ValidateStewardshipLifeTimeSavings() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(ValidateStewardshipLifeTimeSavings.login());
            objStatus += String.valueOf(ValidateStewardshipLifeTimeSavings.Verifysavingsgreaterthan49$andcolor());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  :TC280_VerifyStewardRenewoptoutFunctionalityFlow()
	 *  Created By  : Chandrashekar K B
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC280_VerifyStewardRenewoptoutFunctionalityFlow() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(VerifyStewardRenewoptoutFunctionalityFlow.login());
            objStatus += String.valueOf(VerifyStewardRenewoptoutFunctionalityFlow.CancelStewardmembership());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  :TC281_ValidateNoReferaFriendlinkforStateOH()
	 *  Created By  : Chandrashekar K B
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC281_ValidateNoReferaFriendlinkforStateOH() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(ValidateNoReferaFriendlinkforStateOH.ValidateReferaFriendLnk());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  :TC282_VerifyGiftCardFunctionalityinPaymentSecofPickedSubscriptionFlow()
	 *  Created By  : Chandrashekar K B
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC282_VerifyGiftCardFunctionalityinPaymentSecofPickedSubscriptionFlow() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
            objStatus += String.valueOf(VerifyGiftCardFunctionalityinPaymentSecofPickedSubscriptionFlow.AddGiftcard());
            objStatus += String.valueOf(VerifyGiftCardFunctionalityinPaymentSecofPickedSubscriptionFlow.enrollForPickedUpWineandValidateGiftcard());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  :TC283_VerifyRedeemedGiftCardDisponPickedSettingsPage()
	 *  Created By  : Chandrashekar K B
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC283_VerifyRedeemedGiftCardDisponPickedSettingsPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(VerifyRedeemedGiftCardDisponPickedSettingsPage.login());
            objStatus += String.valueOf(VerifyRedeemedGiftCardDisponPickedSettingsPage.navigatetoPickedandValidateGiftcard());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  :TC284_VerifyGiftcardRedeemFunctionalityinPickedSubscriptionFlow()
	 *  Created By  : Chandrashekar K B
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC284_VerifyGiftcardRedeemFunctionalityinPickedSubscriptionFlow() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
            objStatus += String.valueOf(VerifyGiftcardRedeemFunctionalityinPickedSubscriptionFlow.enrollForPickedUpWineandRedeemGiftCard());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  :TC285_VerifyUserAbletoApplyGiftinPickedSettingsPage()
	 *  Created By  : Chandrashekar K B
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC285_VerifyUserAbletoApplyGiftinPickedSettingsPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(VerifyUserAbletoApplyGiftinPickedSettingsPage.login());
            objStatus += String.valueOf(VerifyUserAbletoApplyGiftinPickedSettingsPage.navigatetoPickedandApplyGiftCard());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  :TC286_VerifyGiftCardBalanceisGreaterthan0$isDisponPaymentSecofPickedSubscriptionFlow()
	 *  Created By  : Chandrashekar K B
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC286_VerifyGiftCardBalanceisGreaterthan0$isDisponPaymentSecofPickedSubscriptionFlow() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(UIBusinessFlow.login());
            objStatus += String.valueOf(VerifyGiftCardBalanceisGreaterthan0$isDisponPaymentSecofPickedSubscriptionFlow.AddProducttoCart());
            objStatus += String.valueOf(VerifyGiftCardBalanceisGreaterthan0$isDisponPaymentSecofPickedSubscriptionFlow.ApplyGiftCardandplaceOrder());
            objStatus += String.valueOf(VerifyGiftCardBalanceisGreaterthan0$isDisponPaymentSecofPickedSubscriptionFlow.Enrolltopickedandverifygiftcard());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name  :TC287_VerifyEditButtontakesUsertoPricemixPageRatherthanBeginningofQuiz()
	 *  Created By  : Chandrashekar K B
	 *  Reviewed By : 
	 *  Purpose     :
	 ****************************************************************************
	 */
	@Test 
	public static void TC287_VerifyEditButtontakesUsertoPricemixPageRatherthanBeginningofQuiz() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
           logger=report.createTest(Method);Threadlogger.set(logger); getlogger().assignCategory("Mobile");  
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(UIBusinessFlow.login());
            objStatus += String.valueOf(VerifyEditButtontakesUsertoPricemixPageRatherthanBeginningofQuiz.Enrolltopickedandverifyeditbutton());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name			: Closebrowser()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is to close the browser.
	 ****************************************************************************
	 */
	@AfterMethod
	public static void Closebrowser()
	{ 
			try
		{
				if (objStatus.contains("Fail"))
				{
				 	numberOfTestCaseFailed++;
				 	ReportUtil.writeTestResults(testCaseID, methodName, "Fail", startTime, endTime);
				}
				else
				{
					numberOfTestCasePassed++;
					ReportUtil.writeTestResults(testCaseID, methodName, "Pass", startTime, endTime);
				}
				log=Logger.getLogger("Close Browser ...");
				Initialize.closeApplication();
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name			: EndReport()
	 * Created By			: Ramesh S
	 * Reviewed By			: Chandrashekhar
	 * Purpose				: The purpose of this method is to close the browser and end the report.
	 * ****************************************************************************
	 */
	 @AfterTest
     public void CloseBrowserAndEndReport()
     {
		 try {
		
			 	report.flush(); 
		 }catch(Exception e)
			{
				e.printStackTrace();
			}
           }

	/***************************************************************************
	 * Method Name			: endScenariosExecution()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: This method responsible for updating the end time to 
	 * 						  customized html report
	 ****************************************************************************
	 */
	
	@AfterSuite
	public static void endScenariosExecution()
	{
		String endTime=null;
		try
		{
			ReportUtil.endScript(numberOfTestCasePassed,numberOfTestCaseFailed);
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			ReportUtil.updateEndTime(endTime);
			UIFoundation.waitFor(3L);
			System.out.println("=======Mobile Cart test case details==============");
			System.out.println("Total no test case Passed:"+numberOfTestCasePassed);
			System.out.println("Total no test case Failed:"+numberOfTestCaseFailed);
			//**********************Sending Reort via Mail***************************************************
		//  MonitoringMail.sendEmail("drinkwinecom@gmail.com","drinkwinecom@gmail.com",ReportFileName,"PFA");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
