package wine.autotest.mobile.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class ViewAllWineForDifferentStates extends Mobile {
	

	/***************************************************************************
	 * Method Name			: viewAllWineForDifferentStates()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String viewAllWineForDifferentStates()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_viewAllWineForDifferentStates.jpeg";
			
		try
		{
			log.info("The execution of the method viewAllWineForDifferentStates started here ...");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtPinotNoir));
		
			UIFoundation.waitFor(6L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkAddProductToPIP));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkViewAllProdusts));
			UIFoundation.waitFor(4L);
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkViewAllProdusts));
			UIFoundation.waitFor(5L);
			String expectedListPageURL = verifyexpectedresult.viewAllWineUrl;
			String actualListPageUrlBefore=getDriver().getCurrentUrl();
			UIFoundation.waitFor(2L);
			if(actualListPageUrlBefore.contains(expectedListPageURL))
			{
				objStatus+=true;
				String objDetail="User is navigated to the winery list page";
				getlogger().pass(objDetail);
				
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");			
			}else{
				objStatus+=false;
				String objDetail="User is unable navigated to the winery list page";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
		
			/*UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnMainNavButton));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.SelectObject(ListPage.cbo_SelectState, "AllWineState"));
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkAddProductToPIP));
			UIFoundation.waitFor(2L);
			
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.txtViewAllWine));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtViewAllWine));
			UIFoundation.waitFor(2L);
			String actualListPageUrlAfter=getDriver().getCurrentUrl();
			UIFoundation.waitFor(3L);
			if(actualListPageUrlBefore.equalsIgnoreCase(actualListPageUrlAfter)){
				objStatus+=true;
				String objDetail="User is navigated to the same list page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");			
			}else{
				objStatus+=false;
				String objDetail="User is unable navigated to the same list page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName+"sameListPage", objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}*/
			log.info("The execution of the method viewAllWineForDifferentStates ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method viewAllWineForDifferentStates "+ e);
			return "Fail";
			
		}
	}	
}
