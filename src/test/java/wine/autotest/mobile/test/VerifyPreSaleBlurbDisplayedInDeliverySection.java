package wine.autotest.mobile.test;

import java.io.IOException;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyPreSaleBlurbDisplayedInDeliverySection extends Mobile {



	/***************************************************************************
	 * Method Name : addPreSaleProdTocrt()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addPreSaleProdTocrt() {
		String objStatus = null;
		try {	

			if(!UIFoundation.isDisplayed(ListPage.btnMainNavButton))
			{
				getDriver().navigate().refresh();
				UIFoundation.waitFor(4L);
			}
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFeaturedMainTab));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtBordexFeatures));				
			UIFoundation.waitFor(2L);	

			UIFoundation.waitFor(4L);			
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));		

			UIFoundation.waitFor(2L); 
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);


			UIFoundation.waitFor(3L);		

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : ValidatePreSaleBlurbDisplayedIn()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose :
	 * Jira Id     : TM-5143
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String ValidatePreSaleBlurbDisplayedIn() {
		String objStatus = null;
		String expected = null;
		String blurbDips = null;
		String screenshotName = "Scenarios_ValidatePreSaleBlurbDisplayedIn.jpeg";
		try {	
			log.info("The execution of the method ValidatePreSaleBlurbDisplayedIn started here ...");
			expected = verifyexpectedresult.preSaleBlurb;

			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.javaScriptClick(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(6L);

			blurbDips=UIFoundation.getText(FinalReviewPage.txtblurbDisplay);	

			if(blurbDips.contains(expected))
			{
				objStatus+=true;
				String objDetail="Pre-Sale blurb is displayed in the delivery section";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				getlogger().pass(objDetail);
			}else {
				objStatus+=false;
				String objDetail="Pre-Sale blurb is not displayed in the delivery section:Failed";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}

			log.info("The execution of the method ValidatePreSaleBlurbDisplayedIn ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

}

