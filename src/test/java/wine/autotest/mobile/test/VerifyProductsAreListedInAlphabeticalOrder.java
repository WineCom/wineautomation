package wine.autotest.mobile.test;

import java.util.ArrayList;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyProductsAreListedInAlphabeticalOrder extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: verifyProductsAreListedInAlphabeticalOrder()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * TM-549
	 ****************************************************************************
	 */
	public static boolean verifyProductsAreListedInAlphabeticalOrder()
	{
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> arrayListYr=new ArrayList<String>();
		   boolean isSorted = true;
			String screenshotName = "Scenarios_AlphabeticalOrder_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				arrayList.add(products1.replaceAll("[0-9]", ""));
				arrayListYr.add(products1.replaceAll("[^0-9]", ""));
				System.out.println("1) "+products1);
	
			}
			
			if(!UIFoundation.getText(ListPage.btnSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductInCart);
				arrayList.add(products2.replaceAll("[0-9]", ""));
				arrayListYr.add(products2.replaceAll("[^0-9]", ""));
				System.out.println("2) "+products2);

			}
			
			if(!UIFoundation.getText(ListPage.btnThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductInCart);
				arrayList.add(products3.replaceAll("[0-9]", ""));
				arrayListYr.add(products3.replaceAll("[^0-9]", ""));
				System.out.println("3) "+products3);

			}
			
			if(!UIFoundation.getText(ListPage.btnFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.btnFourthProductInCart);
				arrayListYr.add(products4.replaceAll("[^0-9]", ""));
				arrayList.add(products4.replaceAll("[0-9]", ""));
				System.out.println("4) "+products4);

			}
			
			if(!UIFoundation.getText(ListPage.btnFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.btnFifthProductInCart);
				arrayList.add(products5.replaceAll("[0-9]", ""));
				arrayListYr.add(products5.replaceAll("[^0-9]", ""));
				System.out.println("5) "+products5);

			}
		
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if((arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) > 0) && (arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1))<0)) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {			    	 					
					String objDetail="Products are listed in alphabetical order";
					getlogger().pass(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					return true;
			    }
			    else
			    {
			    						
					String objDetail="Products names are not sorted in alphabetical order";
					ReportUtil.addTestStepsDetails(objDetail, "fail", "");
					getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					return false;
			    }
		}catch(Exception e)
		{
			return false;
			
		}	
	}
}
