package wine.autotest.mobile.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyGftCardBalDispInOrdrSummaryUponRedeem extends Mobile {
	

	/***************************************************************************
	 * Method Name : addProductToCartLessThnGftCrdPrice() 
	 * Created By  : Rakesh C S
	 * Reviewed By : Chandrashekar 
	 * Purpose     : Add product to cart that is less the gift card amount
	 ****************************************************************************
	 */

	public static String addProductToCartLessThnGftCrdPrice() {
		String objStatus=null;
		try {
			log.info("The execution of the addProductToCartLessThnGftCrdPrice method strated here");
			if(!UIFoundation.isDisplayed(ListPage.btnMainNavButton))
			 {
			    getDriver().navigate().refresh();
			    UIFoundation.waitFor(4L);
			 }
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortOptions));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortLtoH));
			UIFoundation.waitFor(12L);
						
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(4));						
			UIFoundation.waitFor(2L); 
			
			/*addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFirstProductToCart));
				UIFoundation.waitFor(1L);
			}*/
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			UIFoundation.scrollUp();
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Clickable", "", 50);
			
			
			log.info("The execution of the method applyGiftCode started here ...");
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			UIFoundation.giftCertificateNumber();
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(20L);
				
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGiftCheckbox));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(20L);				
			}
			
						
			
			System.out.println("addprodTocrt : "+objStatus);
			
			
			
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : VerifyGftCardBalDispInOrdrSummaryUponRedeem() 
	 * Created By  : Rakesh
	 * Reviewd By  : Chandrasekar 
	 * Purpose     : verifies the gift card and remaining balance 
	 ****************************************************************************
	 */
	
	public static String verifyGftCardBalDispInOrdrSummaryUponRedeem() {
		String objStatus = null;
		String total=null;
		String giftCardAmt = null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		try {
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			 
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			   {
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			   }
			//PaymentContinue
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary in the Final Review Page  ===============");
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			String expTot = total;
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnCartEditButton));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Clickable", "", 50);
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.chkGiftCheckbox);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.chkGiftCheckbox));
			objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnGiftCardApplyng);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnGiftApply));
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(CartPage.txtGiftCertificateSuccessMsg)){
								
				objStatus+=true;
				String objDetail="Verified gift card added sucessfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{					 
				 
				 objStatus+=false;
				 String objDetail="Gift card did not add sucessfully";
				 ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				 getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}			
			UIFoundation.waitFor(6L);
			String expGiftCardRem = UIFoundation.getText(CartPage.txtGiftCrdRem);
			String txtGiftCardRem = expGiftCardRem.replaceAll("[$,]", "");
			double actRemCardBal = Double.parseDouble(txtGiftCardRem);
			double actRemCardBalTot = actRemCardBal;
			//System.out.println("Total:                 "+total);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangePayment));
			UIFoundation.waitFor(2L);
			giftCardAmt = UIFoundation.getText(CartPage.txtGiftCrdAmt);
			String txtGiftCartAmt = giftCardAmt.replaceAll("[$,]", "");
			double totalGftAmount = Double.parseDouble(txtGiftCartAmt);
			String txtSubtotal = expTot.replaceAll("[$,]", "");
			double SubtotalAmount = Double.parseDouble(txtSubtotal);
			double expRemGftCardBal = totalGftAmount-SubtotalAmount;
			expRemGftCardBal=Math.round(expRemGftCardBal*100.0)/100.0;
			if(expRemGftCardBal==actRemCardBalTot) {
				objStatus+=true;
				String objDetail="Verified Gift card Original amount and Remaining Balance is displayed properly";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else{
				objStatus+=false;
				String objDetail="Gift card Original amount and Remaining Balance is not displayed properly";
				logger.fail(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}	
			log.info("The execution of the method verifyGftCardBalDispInOrdrSummaryUponRedeem ended here ...");
			if (objStatus.contains("false"))
			{
				String objDetail="verifyGftCardBalDispInOrdrSummaryUponRedeem test scripts failed";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				logger.fail(objDetail);
				System.out.println("verifyGftCardBalDispInOrdrSummaryUponRedeem test script is failed");
				return "Fail";
			}
			else
			{
				String objDetail="verifyGftCardBalDispInOrdrSummaryUponRedeem test script excecuted succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("verifyGftCardBalDispInOrdrSummaryUponRedeem test script excecuted succesfully");
				logger.pass(objDetail);
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}	
}
