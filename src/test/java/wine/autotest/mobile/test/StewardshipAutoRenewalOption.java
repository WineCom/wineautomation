package wine.autotest.mobile.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class StewardshipAutoRenewalOption extends Mobile {	

	
	/***************************************************************************
	 * Method Name : StewardshipAutoRenewalOption() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh.
	 * Purpose : 
	 ****************************************************************************
	 */
	
	public static String stewardshipAutoRenewalOption()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_stewardshipRenewal_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			
			log.info("The execution of the method stewardshipAutoRenewalOption started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavAccTab));
			UIFoundation.waitFor(6L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkStewardshipSetting));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnStewardsCancel));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnStewardsYesCancel));
			
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.cboStewardsConfirmCancle));
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnStewardsConfirmCanclation));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnStewardshipSubmitClose));
			UIFoundation.waitFor(1L);
			
			if(UIFoundation.isDisplayed(ListPage.txtStewardsExpiryOn) && (UIFoundation.isDisplayed(ListPage.btnStewardshipRenew))){
				  objStatus+=true;				  
			      String objDetail="'Your StewardShip membership will expire on' is displyed in the 'Stewardship Settings' section";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				
			}else{
				 objStatus+=false;
			       String objDetail="'Your StewardShip membership will expire on' is displyed in the 'Stewardship Settings' section is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(2L);		
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnStewardshipRenew));
				
			log.info("The execution of the method stewardshipAutoRenewalOption ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Verify the 'Stewardship settings' section for the existing stewardship member when the user cancels the auto-renewal option";
	    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			log.error("there is an exception arised during the execution of the method stewardshipAutoRenewalOption "+ e);
			return "Fail";
			
		}
	}
}
