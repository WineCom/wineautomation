package wine.autotest.mobile.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class GiftBagIsDisplayedAsLineItemInShoppingCartPage extends Mobile {



	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */

	public static String login()
	{
		String objStatus=null;

		try
		{

			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(2L);	
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "giftBagUserName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}

		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";

		}
	}




	/***************************************************************************
	 * Method Name			: verifyDiffGiftBagTotInOrderSummary()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String addGiftBag()
	{
		String objStatus=null;
		String screenshotName = "VerifyDiffGiftBagTotInOrderSummary.jpeg";


		try {
			log.info("Verify Diff GiftBag Tot In Order Summarymethos started here");

			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(10L);			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(5L);

			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(26L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));

				WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					UIFoundation.clickObject(FinalReviewPage.chkBillingAndShippingCheckbox);
					UIFoundation.waitFor(3L);
				}
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(8L);
			}
			objStatus+=UIFoundation.clickObject(FinalReviewPage.spnGiftoptionsR);
			WebElement gift = getDriver().findElement(By.xpath("//main/section[@class='checkoutMainContent']//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label/input[@class='formWrap_checkbox giftOptions_checkbox']"));
			if(!gift.isSelected()){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtRecipientGiftCheckbox));
			}
			UIFoundation.waitFor(3L);

			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtGiftbag399));
			String giftBag=UIFoundation.getText(FinalReviewPage.spnGiftBag); 
			giftBag=giftBag.replace(" ", ".");
			double giftBagValue=Double.parseDouble(giftBag.substring(0,4));
			System.out.println("GiftBag value for each products is :"+ giftBagValue);
			String giftWrapCnt=UIFoundation.getText(FinalReviewPage.txtGiftWrapOptionCount);
			int giftWrapCount= Integer.parseInt(giftWrapCnt.replaceAll("[^0-9]", ""));
			System.out.println("Gift Wrap added to: "+giftWrapCount + " item");
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(3L);
			}

			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnViewCart)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnViewCart));
				UIFoundation.waitFor(3L);
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.spnProdItemgiftIcon);			
			if(UIFoundation.isDisplayed(FinalReviewPage.spnProdItemgiftIcon)) {
				objStatus+=true;
				String objDetail="Gift order summary and gift bag icon is displayed succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Gift bag is added to cart and order summary details");
			}
			else
			{
				objStatus+=false;
				String objDetail="Gift order summary or gift bag icon is not displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				System.err.println("Gift order summary or gift bag icon is not added");
			}
			//objStatus+=String.valueOf(UIFoundation.clickObject(driver, "GiftoptionsR"));
			String giftbagsumminrecipient=UIFoundation.getText(FinalReviewPage.spnGiftWrappingSection);
			double giftbagprice=Double.parseDouble(giftbagsumminrecipient.substring(1,5));
			double giftbagtot=giftWrapCount*giftBagValue;

			String dString = Double.toString(giftbagtot);
			String giftbag = dString.substring(0, 4);
			double giftbagtotal=Double.parseDouble(giftbag);
			if(giftbagtotal==giftbagprice) {
				objStatus+=true;
				String objDetail="Gift bag added for N items is correct";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("GiftBag total for "+giftWrapCount +" item is "+ giftbagprice);
			}
			else
			{
				objStatus+=false;
				String objDetail="Gift bag added for N items is Incorrect";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(1L);
			//	UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.HideCart);
			//	UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtShoppingCart);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnCartEditButton));
			UIFoundation.waitFor(4L);
			String giftbagsummincart=UIFoundation.getText(FinalReviewPage.spnGiftWrappingSection);
			if(giftbagsumminrecipient.equals(giftbagsummincart)) {
				objStatus+=true;
				String objDetail="Gift bag summary in Recipient and Shopping Cart page are same";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Gift bag summary in recipient and shopping cart page are same");
			}
			else
			{
				objStatus+=false;
				String objDetail="Gift bag summary in recipient and shopping cart page are not same";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				System.out.println("Gift bag summary in recipient and shopping cart page are not same");
			}
			objStatus+=String.valueOf(UIFoundation.SelectObject(CartPage.txtProductQuantity, "quantity"));
			UIFoundation.waitFor(2L);

			System.out.println("============Gift Wrapping Summary after increasing the quantity in Shopping cart====================");
			System.out.println("Gift Wrapping : "+UIFoundation.getText(FinalReviewPage.spnGiftWrappingSection));
			UIFoundation.waitFor(5L);

			System.out.println("============Gift Wrapping Summary after increasing the quantity in Recipient page====================");
			System.out.println("Gift Wrapping : "+UIFoundation.getText(FinalReviewPage.spnGiftWrappingSection));
			UIFoundation.waitFor(1L);
			log.info("The execution of the method verifyGiftWrapping ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}


		}catch (Exception e) {
			System.out.println("The error massage is"+e);

			log.error("there is an exception arised during the execution of the method verifyGiftWrapping "
					+ e);
			return "Fail";
		}

	}	

}
