package wine.autotest.mobile.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.mobile.pages.*;

public class VerifyGiftcardRedeemFunctionalityinPickedSubscriptionFlow extends Mobile {
	
	/***********************************************************************************************************
	 * Method Name : enrollForPickedUpWineandRedeemGiftCard() 
	 * Created By  : Chandrashekar K B
	 * Purpose     : The purpose of this method is to navigate compass tiles and 'Proceed to Enrollment' and Redeem  
	 *               Gift card
	 ************************************************************************************************************
	 */
	public static String enrollForPickedUpWineandRedeemGiftCard() {
		String objStatus = null;
		String screenshotName = "Scenarios_enrollForPickedUpWineandRedeemGiftCard_Screenshot.jpeg";
		try {
			log.info("Execution of the method VerifyGiftCardFunctionalityinPaymentSecofPickedSubscriptionFlow started here .........");
			objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.imgpicked));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.imgpicked));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(PickedPage.cmdSignUpCompass)) 
			{
				objStatus+=true;
				String objDetail="User is navigated to Picked offer page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Picked offer page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdSignUpCompass));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkNoVoice));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkRedOnly));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNtVry));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSelQtyNxt));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(PickedPage.txtRecipientPge)) 
			{
				objStatus+=true;
				String objDetail="User is navigated to Recipient page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Recipient page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(PickedPage.dwnReceiveState, "State"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.rdoRedeemGiftcard));
			if(UIFoundation.isDisplayed(PickedPage.txtRedeemGiftcardNumber)) 
			{
				objStatus+=true;
				String objDetail="Gift Card Redeem Section Displayed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				String giftCert1=UIFoundation.giftCertificateNumber();
				XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
				UIFoundation.clearField(PickedPage.txtRedeemedGiftCard);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtRedeemGiftcardNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkRedeemGiftcardApply));
				UIFoundation.waitFor(2L);
			}
			else
			{
				objStatus+=false;
				String objDetail="Gift Card Redeem Section Not Displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			if(UIFoundation.isDisplayed(PickedPage.txtRedeemedGiftCard)) 
			{
				objStatus+=true;
				String strGiftremain=String.valueOf(UIFoundation.getText(PickedPage.txtRedeemedGiftCard));
				String objDetail="Gift card Added to payment section GiftCard:-"+strGiftremain;
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Gift Card Redeem Section Not Displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
		

			if (objStatus.contains("false"))
			{				
				String objDetail="VerifyGiftcardRedeemFunctionalityinPickedSubscriptionFlow  test case failed ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{
				String objDetail="VerifyGiftcardRedeemFunctionalityinPickedSubscriptionFlow test case executed succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}	
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
}
