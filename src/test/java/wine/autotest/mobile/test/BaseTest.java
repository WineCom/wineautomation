package wine.autotest.mobile.test;


import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.test.Mobile;
import wine.autotest.mobile.pages.LoginPage;


public class BaseTest extends Mobile{

	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
	
			
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.MainNavAccountTab));
//			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(6L); 
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernameMobile"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.waitFor(6L);
			getDriver().navigate().refresh();
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			UIFoundation.waitFor(6L);
			
//			UIFoundation.scrollDownOrUpToParticularElement(LoginPage.MainNavAccountTab);
//			UIFoundation.waitFor(1L);
//			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.MainNavAccountTab));
			System.out.println("objStatus login:"+objStatus);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}	
	/***************************************************************************
	 * Method Name			: logout()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Logout from the Wine.com 
	 * 						  application
	 ****************************************************************************
	 */

	public static String logout()
	{
		String objStatus=null;
		try
		{
			log.info("The execution of the method logout started here ...");
			getDriver().navigate().refresh();
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			UIFoundation.scrollDownOrUpToParticularElement(LoginPage.MainNavTabSignOut);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavTabSignOut));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignoutLink));
			System.out.println("objStatus logout:"+objStatus);
			log.info("The execution of the method logout ended here ...");	
			if (objStatus.contains("false"))
			{
				System.out.println("Logout test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Logout test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method logout "+ e);
			return "Fail";
		}
	}
	
	/**************************************************************************
	 * Method Name			: userProfileCreation()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Create new user account
	 ****************************************************************************
	 */
	
	public static String userProfileCreation() {
		String objStatus=null;
		try {
			log.info("The execution of method create Account started here");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.JoinNowButton));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.FirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.Email,"email"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.Password, "accPassword"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.CreateAccountButton));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(LoginPage.MainNavTabSignOut);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavTabSignOut));
			System.out.println("objStatus userProfileCreation:"+objStatus);
			log.info("The execution of the method create Account ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("User profile creation  test case is failed");
				return "Fail";
			} else {
				System.out.println("User profile creation  test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method create account "
					+ e);
			return "Fail";
		}
	}
	
}
