package wine.autotest.mobile.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.test.Mobile;;


public class OrderCreationWihExistingAccountBillingAddress extends Mobile {
	

	static boolean isObjectPresent=false;
	

/***************************************************************************
 * Method Name			: shippingDetails()
 * Created By			: Chandrashekhar
 * Reviewed By			: Ramesh.
 * Purpose				:  The purpose of this method is to fill the shipping 
 * 						  address of the customer
 ****************************************************************************
 */

public static String shippingDetails() {
	String objStatus=null;
	try {
		log.info("The execution of the method shippingDetails started here ...");
		
		if(UIFoundation.isDisplayed(CartPage.btnObjCheckout))
		{
		objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
		}else {
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
		}
		

		UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
		objStatus += String.valueOf(UIBusinessFlow.recipientEdit());
		
		if (UIFoundation.isDisplayed(FinalReviewPage.radShiptoaddressOne)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.radShiptoaddressOne));
			UIFoundation.waitFor(10L);
		}
		UIFoundation.waitFor(10L);
		if (UIFoundation.isDisplayed(FinalReviewPage.btnContinuetostate)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnContinuetostate));
			UIFoundation.waitFor(10L);
		}
		if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.waitFor(10L);
		}
	
		if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
			UIFoundation.waitFor(10L);
		}
		if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard)) {
					
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
		}
		if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
		    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			}
		if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
		UIFoundation.waitFor(1L);
		}
		
		log.info("The execution of the method shippingDetails ended here ...");

		if (objStatus.contains("false")) {
			
			return "Fail";
		} else {
			
			return "Pass";
		}
	} catch (Exception e) {
		log.error("there is an exception arised during the execution of the method shippingDetails "
				+ e);
		return "Fail";
	}

}
/***************************************************************************
 * Method Name			: addNewCreditCard()
 * Created By			: Chandrashekhar
 * Reviewed By			: Ramesh,
 * Purpose				: The purpose of this method is to add the new credit 
 * 						  card details for billing process
 ****************************************************************************
 */
	public static String addNewCreditCard() {
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
					
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method addNewCreditCard started here ...");
			
			UIFoundation.waitFor(5L);
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(5L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage. lnkAddNewCard))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkAddNewCard);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage. lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(2L);
			WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingAddress, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingSuite, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingCity, "BillingCity"));
			objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.txtBillinState, "BillingState"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_BillingZip, "BillingZipcode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingPhone, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			   {
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			   }
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.txtSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.txtShippingHandaling);
			total=UIFoundation.getText(FinalReviewPage.txtTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage. txtOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			 getDriver().navigate().refresh();
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrderButton);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrderButton));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrderButton, "Invisible", "", 50);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
		
			if(orderNum!="Fail")
		   {
		      objStatus+=true;
		      String objDetail="Order number is placed successfully";
		      getlogger().pass(objDetail);
		      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		      
		      System.out.println("Order number is placed successfully: "+orderNum);   
		   }else
		   {
		       objStatus+=false;
		       String objDetail="Order number is null and cannot placed successfully";
		       getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		    
		   }
			log.info("The execution of the method addNewCreditCard ended here ...");	
			if (objStatus.contains("false")) {
				
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard "
					+ e);
			return "Fail";
		}
		
	}
}
