package wine.autotest.mobile.test;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifySurveyMonkeyIsRemovedOnPickedThankyouScreen extends Mobile {
	

	
	static String expectedState=null;
	static String actualState=null;
	/***************************************************************************
	 * Method Name : subscriptionModelPage() 
	 * Created By : Chandrashekhar 
	 * Reviewed By : Ramesh
	 * Purpose : The purpose of this method is to Create.
	 ****************************************************************************
	 */

	public static String subscriptionModelPage() {
		String objStatus = null;
		
		try {
			log.info("The execution of method subscriptionModelPage started here");
			UIFoundation.waitFor(1L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedCompassTiles));
			UIFoundation.waitFor(5L);		
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSignUpPickedWine));
			UIFoundation.waitFor(5L);
			
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyFirsrPickedPage));			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkNoVoice));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifySeconePickedPage));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkRedOnly));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyThirdPickedPage));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.txtPickedWrapNextThirdPage);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.txtPickedWrapNextThirdPage));	
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyForthPickedPage));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.txtPickedWrapNextFourthPage));
			UIFoundation.waitFor(1L);
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.lblNotVery));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.txtPickedWrapNextFifthPage));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.txtPickedWrapNextSixthPage));
			UIFoundation.waitFor(1L);
			
					
			log.info("The execution of the method subscriptionModelPage ended here ...");
			if (objStatus.contains("false") ) {
				
				return "Fail";
			} else {
				
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("subscriptionModelPage  test case is failed");
			log.error("there is an exception arised during the execution of the method subscriptionModelPage " + e);
			return "Fail";
		}
	}	
	
	/***************************************************************************
	 * Method Name : validateSurveyMonkeyRemoved() 
	 * Created By : Chandrashekhar
	 * Reviewed By : Ramesh,
	 *  Purpose : 
	 * 
	 ****************************************************************************
	 */
	public static String validateSurveyMonkeyRemoved() {
		String objStatus = null;
		
		String screenshotName = "validateSurveyMonkeyRemoved.jpeg";
		

		try {			
			log.info("The execution of the method validateSurveyMonkeyRemoved started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(PickedPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.chkBillingAndShippingCheckbox);
		    UIFoundation.javaScriptClick(PickedPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(PickedPage.txtBirthMonth))
			{
			   objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthYear, "birthYear"));
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.btnPaymentContinue);
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPaymentContinue));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.chkTermsAndCondition);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkTermsAndCondition));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.cmdEnrollInPicked));
			UIFoundation.waitFor(5L);	
			
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtGetEmail));
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.btnAppStore));
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.btnGooglePlay));
			UIFoundation.waitFor(1L);
			UIFoundation.waitFor(1L);
			           
			log.info("The execution of the method validateSurveyMonkeyRemoved ended here ...");
			if (objStatus.contains("false")) {
				
						
				objStatus+=false;
				String objDetail="PromoCode is applyed and not removed from the validateSurveyMonkeyRemoved: failed";
				ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
				
			} else {
								
				objStatus+=true;
				String objDetail="PromoCode is applyed and removed from the validateSurveyMonkeyRemoved";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method validateSurveyMonkeyRemoved " + e);
			return "Fail";
		}
	}
}
