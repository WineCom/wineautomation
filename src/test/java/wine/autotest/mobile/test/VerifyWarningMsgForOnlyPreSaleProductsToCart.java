package wine.autotest.mobile.test;

import java.io.IOException;
import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyWarningMsgForOnlyPreSaleProductsToCart extends Mobile {
	


	/***************************************************************************
	 * Method Name : addPreSaleProdTocrt()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addPreSaleAdNormalProdTocrt() {
		String objStatus = null;
		String screenshotName = "Scenarios_validationForOnlyPreSaleProducts_Screenshot.jpeg";

		try {
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtSearchProduct));
			objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtSearchProduct, "preSale"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtPreSaleProduct));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnAddToCart));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(4L);			
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCartCount));
			UIFoundation.waitFor(3L);
			
			
			
			if (UIFoundation.isDisplayed(CartPage.txtPreSaleProd)) {
				System.out.println("Pre-sale text is Displayed for first product");
				objStatus+=true;
				String objDetail="Pre-sale text is Displayed for first product";
                getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");			
				
			}
			else
			{
				
				System.out.println("Product is not pre-sale product");		
				objStatus+=false;
				String objDetail="Product is not pre-sale product";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
				
			
	//		UIBusinessFlow.validationForOnlyPreSaleProducts();
		//	ApplicationDependent.verifyClockIconPresent(driver);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : addPreSaleProdTocrt()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addPreSaleProdTocrt() {
		String objStatus = null;

		try {
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtSearchProduct));
			objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtSearchProduct, "preSale"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtPreSaleProduct));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnAddToCart));
			UIFoundation.waitFor(3L);
			/*objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(4L);			
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));*/
			
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCartCount));
			UIFoundation.waitFor(3L);
			UIBusinessFlow.validationForOnlyPreSaleProducts();
		//	ApplicationDependent.verifyClockIconPresent(driver);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

}
