package wine.autotest.mobile.test;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.mobile.pages.*;


public class VerifyReasonReqtooptoutStewardRenew extends Mobile {
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Ramesh S  
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	 
	public static String login()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_keepMeSignedIn.jpeg";
	try
		{
		log.info("The execution of the method login started here ...");	
		objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.MainNavAccountTab));
		UIFoundation.waitFor(3L); 
		objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "Stewardusername"));
		objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
		UIFoundation.waitFor(3L);
		driver.navigate().refresh();
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
		UIFoundation.waitFor(2L);
		System.out.println("objStatus login:"+objStatus);
		log.info("The execution of the method login ended here ...");
		if (objStatus.contains("false"))
		{
			String objDetail="Login test case is failed";
			System.out.println(objDetail);
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			return "Fail";
		}
		else
		{			
		String objDetail="Login test case is executed successfully";
		System.out.println(objDetail);
		getlogger().pass(objDetail);
		ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			return "Pass";			
		}
	}catch(Exception e)
	{	
		log.error("there is an exception arised during the execution of the method login "+ e);
		return "Fail";		
	}
}	
	/***********************************************************************************************************
	 * Method Name : VerifySTWRDCancelMembership()() 
	 * Created By  : Ramesh S
	 * Purpose     : 
	 * 
	 ************************************************************************************************************
	 */
	public static String VerifySTWRDCancelMembership() {
		String objStatus = null;
		String screenshotName = "Scenarios_VerifySTWRDCancelMembership_Screenshot.jpeg";
		try {
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavAccTab));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkStewardshipSetting));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnstewardshipHeader)) 
			{
				objStatus+=true;
				String objDetail="User is navigated to Stewardship Account  page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Stewardship Account  page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.btnstewardsCancelMembership));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(UserProfilePage.btnstewardsCancelMembership));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(UserProfilePage.btnStwmodalCancelMembership)) 
			{
				objStatus+=true;
				String objDetail="Lifetime Stewardship savings modal is Displayed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else{
				objStatus+=false;
				String objDetail="Lifetime Stewardship savings modal is Not Displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.btnStwmodalCancelMembership));
			if(UIFoundation.isDisplayed(UserProfilePage.btnStwmodalConfrimCancelMembership)) 
			{
				objStatus+=true;
				String objDetail="Reason collection modal is Displayed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else{
				objStatus+=false;
				String objDetail="Reason collection modal is Not Displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.btnStwmodalConfrimCancelMembership));		
			if(UIFoundation.isDisplayed(UserProfilePage.spnStwmodalCancelerror)) 
			{
				objStatus+=true;
				String  strErrormsgSteward=(UIFoundation.getText(UserProfilePage.spnStwmodalCancelerror));
				String objDetail="Error Msg is Displayed:-"+strErrormsgSteward;
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else
			{
				objStatus+=false;
				String objDetail="Error Msg is Not Displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			if (objStatus.contains("false"))
			{				
				String objDetail="VerifyReasonReqtooptoutStewardRenew  test case failed ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{
				String objDetail="VerifyReasonReqtooptoutStewardRenew test case executed succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}	
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}	
}