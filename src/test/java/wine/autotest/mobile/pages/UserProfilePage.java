package wine.autotest.mobile.pages;

import org.openqa.selenium.By;

public class UserProfilePage {
	
	public static By imgWineLogo = By.xpath("(//a[@class='wineLogo'])[1]");
	public static By lnkOrderStatus = By.xpath("(//a[text()='Order Status'])[1]");
	public static By lnkAddressBook = By.xpath("(//a[text()='Address Book'])[1]");
	public static By lnkPaymentMethods = By.xpath("(//a[text()='Payment Methods'])[1]");
	public static By lnkStewardshipSet = By.xpath("(//a[text()='StewardShip Settings'])[1]");
	public static By lnkEmailPreferences = By.xpath("(//a[text()='Email Preferences'])[1]");
	public static By lnkAccountInfo = By.xpath("(//a[text()='Account Info'])[1]");
	
	//StewardShip Settings
	public static By spnstewardshipHeader = By.xpath("//h1[text()='StewardShip Settings']");
	public static By spnstewardshiplandingHeader = By.xpath("//h1[text()='Unlimited FREE SHIPPING']");
	public static By txtstewardslifesavings = By.xpath("//section/div[@class='stewardshipSavingsOverview_savings']");
	public static By btnStwmodalCancelMembership = By.xpath("//button[@class='btn btn-fullWidth btn-rounded btn-outline-blue btn-processing stewardshipOptOut_cancelMembershipBtn']");
	public static By txtStwmodallifesavings = By.xpath("//div[@class='stewardshipOptOut_savingsText']");
	public static By btnStwmodalKeepMembership = By.xpath("//button[@class='btn btn-fullWidth btn-rounded btn-blue btn-processing stewardshipOptOut_keepMembershipBtn js-closeModal']");
	public static By btnStwmodalConfrimCancelMembership = By.xpath("//button[@class='btn btn-fullWidth btn-rounded btn-blue btn-processing stewardshipOptOut_confirmCancelation']");
	public static By spnStwmodalHeader= By.xpath("//div/section/h2[@class='stewardshipOptOut_header']");
	public static By btnStwmodaloptoutKeepMembership = By.xpath("//button[@class='btn btn-fullWidth btn-rounded btn-outline-blue btn-processing js-closeModal']");
	public static By btnstewardsCancelOptionFirst = By.xpath("//label/span[@class='stewardshipOptOutCancel_text'][1]");
	public static By spnStwmodalCancelerror = By.xpath("//div/p[@class='stewardshipOptOutCancel_errorMessage']");
	public static By spnStwmodalAddDetailsExp = By.xpath("//span[@class='stewardshipOptOutCancel_expDate']");
	public static By txtAnythingTellUs = By.xpath("//textarea[@class='formWrap_textarea stewardshipOptOut_cancelMoreTextArea']");
	public static By btnStwmodalSubmitandClose = By.xpath("//button[@class='btn btn-fullWidth btn-rounded btn-blue btn-processing stewardshipOptOutCancel_submit js-closeModal']");
	public static By spnstewardoptout = By.xpath("//h1[text()='StewardShip Settings']");
	public static By btnStwshipRenew = By.xpath("//button[@class='stewardshipRenew_btn btn btn-red btn-small btn-rounded']");
	public static By btnstewardsCancelMembership = By.xpath("//button[text()='Cancel Membership']");

}
