package wine.autotest.fw.utilities;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;

public class ObjectMap {
	public static Properties prop=null;
	
	/***************************************************************************
	 * Method Name			: ObjectMap()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to get object details
	 * 						  from property file
	 ****************************************************************************
	 */
	
	public ObjectMap(String FileName)
	{
		FileInputStream fin=null;
		try
		{
			fin=new FileInputStream(FileName);
			prop=new Properties();
			prop.load(fin);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name			: getLocator()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to get the locator value
	 * 						 and locator name
	 ****************************************************************************
	 */
	
	public static By getLocator(String logicalname)
	{
		By by=null;
		try
		{
			String locatorinfo=prop.getProperty(logicalname);
			String locator[]=locatorinfo.split(">");
			String locatorname=locator[0];
			String locatorvalue=locator[1];
			if(locatorname.equalsIgnoreCase("id"))
			{
				by=By.id(locatorvalue);
			}
			else if(locatorname.equalsIgnoreCase("name"))
			{
				by=By.name(locatorvalue);
			}
			else if (locatorname.equalsIgnoreCase("linktext"))
			{
				by=By.linkText(locatorvalue);
			}
			else if (locatorname.equalsIgnoreCase("xpath"))
			{
				by=By.xpath(locatorvalue);
			}
			else if (locatorname.equalsIgnoreCase("cssselector"))
			{
				by=By.cssSelector(locatorvalue);
			}
			else if (locatorname.equalsIgnoreCase("partiallinktext"))
			{
				by=By.partialLinkText(locatorvalue);
			}
			else if (locatorname.equalsIgnoreCase("tagname"))
			{
				by=By.tagName(locatorvalue);
			}
			else
			{
				System.out.println("Invalid locator name found !!!!");
			}
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return by;
	}

}
