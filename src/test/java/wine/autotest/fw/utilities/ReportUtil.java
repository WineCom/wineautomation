package wine.autotest.fw.utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;

import wine.autotest.desktop.test.Desktop;

public class ReportUtil extends Desktop {
	
	public static String ResultReportFile;
	public static String ResultReportFolderPath;
	public static String scenarioName;
	public static ArrayList<String> testscriptid=new ArrayList<String>();
	public static ArrayList<String> testdescription=new ArrayList<String>();
	public static ArrayList<String> testmethod=new ArrayList<String>();
	public static ArrayList<String> testpkgname=new ArrayList<String>();
	public static ArrayList<String> teststatus=new ArrayList<String>();
	public static ArrayList<String> testscreenshot=new ArrayList<String>();
	public static ArrayList<String> objDescription=new ArrayList<String>();
	public static ArrayList<String> objStatus=new ArrayList<String>();
	public static ArrayList<String> screenshot=new ArrayList<String>();
	public static String appUrl=null;
	
	/***************************************************************************
	 * Method Name			: createReport()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static void createReport(String FileName,String teststarttime,String environment)
	{
		BufferedWriter bw=null;
		try
		{
			ResultReportFile=FileName;
			ResultReportFolderPath=ResultReportFile.substring(0, ResultReportFile.lastIndexOf("\\"));
			String appName="Wine Platform";
			String browserName="Chrome";
			String url=environmentUrl;
			if(url.equalsIgnoreCase("q2m"))
			{
				appUrl="https://q2m.wine.com";
			}
			else if(url.equalsIgnoreCase("prod"))
			{
				appUrl="https://m.wine.com";
			}
			else if(url.equalsIgnoreCase("qm"))
			{
				appUrl="https://qm.wine.com";
			}
			else
			{
				appUrl="https://qwww.wine.com";
			}
			
			bw=new BufferedWriter(new FileWriter(ResultReportFile));
			bw.write("<html>");
			bw.write("<head><title>Wine Automation Results</title></head>");
			bw.newLine();
			bw.write("<body>");
			bw.write("<h1 align=center>Wine Automation Results</h1>");
			bw.write("<table border=2>");
			bw.write("<h3><u>Automation Summary</u></h3>");
			bw.write("<tr>");
			bw.write("<th width=150 bgcolor=#708090>Item Name</th>");
			bw.write("<th width=250 bgcolor=#708090>Item Value</th>");
			bw.write("</tr>");
			bw.write("<tr>");
			bw.write("<td width=150 bgcolor=#D2B48C>Application Name</td>");
			bw.write("<td width=250 bgcolor=#c0e3c0>"+appName+"</td>");
			bw.write("</tr>");
			bw.write("<tr>");
			bw.write("<td width=150 bgcolor=#D2B48C>Browser Name</td>");
			bw.write("<td width=250 bgcolor=#c0e3c0>"+browserName+"</td>");
			bw.write("</tr>");
			bw.write("<tr>");
			bw.write("<td width=150 bgcolor=#D2B48C>URL</td>");
			bw.write("<td width=250 bgcolor=#c0e3c0>"+appUrl+"</td>");
			bw.write("</tr>");
			bw.write("<tr>");
			bw.write("<td width=150 bgcolor=#D2B48C>Environment</td>");
			bw.write("<td width=250 bgcolor=#c0e3c0>"+environment+"</td>");
			bw.write("</tr>");
			bw.write("<tr>");
			bw.write("<td width=150 bgcolor=#D2B48C>Start Time</td>");
			bw.write("<td width=250 bgcolor=#c0e3c0>"+teststarttime+"</td>");
			bw.write("</tr>");
			bw.write("<tr>");
			bw.write("<td width=150 bgcolor=#D2B48C>End Time</td>");
			bw.write("<td width=250 bgcolor=#c0e3c0>END_TIME</td>");
			bw.write("</tr>");
			bw.newLine();
			bw.write("</table>");
			
			bw.write("</body>");
			bw.write("</html>");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				bw.flush();
				bw.close();
			}catch(Exception e)
			{
				
			}
		}
	}
	
	/***************************************************************************
	 * Method Name			: startScript()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static void startScript(String ScenarioName)
	{
		BufferedWriter bw=null;
		try
		{
			scenarioName=ScenarioName.replaceAll(" ", "_");
			bw=new BufferedWriter(new FileWriter(ResultReportFile,true));
			bw.write("<html>");
			bw.newLine(); 
			bw.write("<body>");
			bw.write("<table border=2 width=100%>");
			bw.write("<h3><u>TestScript Execution Details</u></h3>");
			bw.write("<tr>");
			bw.write("<th  width=15% bgcolor=#708090>Testcase ID</th>");
			bw.write("<th width=20% bgcolor=#708090>Testcase Name</th>");
			bw.write("<th width=15% bgcolor=#708090>Status</th>");
			bw.write("<th width=25% bgcolor=#708090>Test Start Time</th>");
			bw.write("<th width=25% bgcolor=#708090>Test End Time</th>");
			bw.write("</tr>");
			
			bw.write("</body>");
			bw.write("</html>");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				bw.flush();
				bw.close();
			}catch(Exception e)
			{
				
			}
		}
	}
	
	/***************************************************************************
	 * Method Name			: endScript()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static void endScript(int testPassed, int testfail)
	{
		BufferedWriter bw=null;
		try
		{
			bw=new BufferedWriter(new FileWriter(ResultReportFile,true));
			bw.write("</table>");
			bw.write("<head>");
			bw.write("<table border=2>");
			bw.write("<h3><u>TestScript Execution Summary</u></h3>");
			bw.write("<tr>");
			bw.write("<td width=250 bgcolor=#D2B48C>Total No. Test Case Passed</td>");
			bw.write("<td width=150 bgcolor=#3D9140>"+testPassed+"</td>");
			bw.write("</tr>");
			bw.write("<tr>");
			bw.write("<td width=250 bgcolor=#D2B48C>Total No. Test Case Failed</td>");
			bw.write("<td width=150 bgcolor=#CD0000>"+testfail+"</td>");
			bw.write("</tr>");
			bw.write("</head>");
			bw.newLine();
			bw.write("</table>");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				bw.flush();
				bw.close();
			}catch(Exception e)
			{
				
			}
		}
	}
	
	/***************************************************************************
	 * Method Name			: addTestDetails()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static void addTestDetails(String tsid,String desc,String methodname,String pkgname,String status,String screenshotpath)
	{
		try
		{
			testscriptid.add(tsid);
			testdescription.add(desc);
			testmethod.add(methodname);
			testpkgname.add(pkgname);
			teststatus.add(status);
		//	testscreenshot.add(screenshotpath);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name			: writeTestResults()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static void writeTestResults(String tcid,String testcasename,String status,String starttime,String endtime)
	{
		BufferedWriter bw=null;
		String FileName=null;
		String reportFileName = null;
		try
		{
//			FileName=scenarioName+"_"+testcasename+"_Detail_Results.html";
			FileName=tcid+"_"+testcasename+"_Detail_Results.html";
			reportFileName=ResultReportFolderPath+"\\"+FileName;
			bw=new BufferedWriter(new FileWriter(reportFileName));
			bw.write("<html>");
			bw.write("<head><title>"+testcasename+" Detail Results </title></head>");
			bw.write("<body>");
			bw.write("<h1 align=center>"+testcasename+" Detail Results </h1>");
			bw.newLine();
			bw.write("<table width=100% border=2>");
			bw.write("<h3><u>"+tcid+" "+testcasename+" Detail Results </u></h3>");
//			bw.write("<tr>");
//			bw.write("<th width=10%>TestScriptID</th>");
//			bw.write("<th width=20%>Description</th>");
//			bw.write("<th width=15%>MethodName</th>");
//			bw.write("<th width=25%>PackageName</th>");
//			bw.write("<th width=10%>Status</th>");
//			bw.write("<th width=20%>ScreenshotPath</th>");
//			bw.write("</tr>");
//			if (testmethod !=null)
//			{
//				for(int i=0;i<testmethod.size();i++)
//				{
//					bw.write("<tr>");
//					bw.write("<td width=10%>"+testscriptid.get(i)+"</td>");
//					bw.write("<td width=20%>"+testdescription.get(i)+"</td>");
//					bw.write("<td width=15%>"+testmethod.get(i)+"</td>");
//					bw.write("<td width=25%>"+testpkgname.get(i)+"</td>");
//					if (teststatus.get(i).equalsIgnoreCase("pass"))
//					{
//						bw.write("<td width=10%>"+teststatus.get(i)+"</td>");
//						bw.write("<td width=20%>&nbsp</td>");
//					}
//					else
//					{
//						bw.write("<td width=10%>"+teststatus.get(i)+"</td>");
//						bw.write("<td width=20%><a href=file:///"+testscreenshot.get(i)+">ScreenshotPath</a></td>");
//					}
//					bw.write("</tr>");
//				}
//			}
			
			bw.write("<tr>");
			bw.write("<th width=6% bgcolor=#708090>Description</th>");
			bw.write("<th width=3% bgcolor=#708090>Status</th>");
			bw.write("<th width=5% bgcolor=#708090>Screenshot</th>");
			bw.write("</tr>");
			
			for(int i=0;i<objDescription.size();i++)
			{
				if(objStatus.get(i).equalsIgnoreCase("pass"))
				{
					bw.write("<tr>");
					bw.write("<td width=6% bgcolor=#D2B48C>"+""+ ""+objDescription.get(i)+ ""+""+"</td>");
					bw.write("<td width=3% bgcolor=#D2B48C align=center>"+objStatus.get(i)+"</td> ");
					bw.write("<td width=5% bgcolor=#D2B48C>&nbsp</td>");
					
				}
				else
				{
					bw.write("<tr>");
					bw.write("<td width=6% bgcolor=#D2B48C>"+""+ ""+objDescription.get(i)+ ""+""+"");
				//	bw.write("<td width=15% bgcolor=#D2B48C >"+"The object"+ " '"+objDescription.get(i)+ "' "+"is not found"+"");
					bw.write("<td width=3% bgcolor=#cc0000 align=center>"+objStatus.get(i)+"</td> <br />");
					bw.write("<td width=5% bgcolor=#D2B48C><a href=file:///"+screenshot.get(i)+">ScreenshotPath</a></td>");
				}bw.write("</tr>");			
			}
			bw.write("</table>");
			bw.write("</body>");
			bw.write("</html>");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				bw.flush();
				bw.close();
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		
		try
		{
			bw=new BufferedWriter(new FileWriter(ResultReportFile,true));
			bw.write("<html>");
			bw.newLine();
			bw.write("<body>");
			bw.write("<tr>");
			bw.write("<td width=15% bgcolor=#D2B48C>"+tcid+"</td>");
			bw.write("<td width=20% bgcolor=#D2B48C>"+testcasename+"</td>");
			if(status.equalsIgnoreCase("pass"))
			{
				bw.write("<td width=15% bgcolor=#3D9140><a href=file:///"+reportFileName+">"+status+"</a></td>");
			}
			else
			{
				bw.write("<td width=15% bgcolor=#CD0000><a href=file:///"+reportFileName+">"+status+"</a></td>");
			}
			
			bw.write("<td width=25% bgcolor=#D2B48C>"+starttime+"</td>");
			bw.write("<td width=25% bgcolor=#D2B48C>"+endtime+"</td>");
			bw.write("</tr>");
			bw.write("</body>");
			bw.write("</html>");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				bw.flush();
				bw.close();
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
	 testscriptid=new ArrayList<String>();
	 testdescription=new ArrayList<String>();
	 testmethod=new ArrayList<String>();
	 testpkgname=new ArrayList<String>();
	 teststatus=new ArrayList<String>();
// testscreenshot=new ArrayList<String>();
	}

	/***************************************************************************
	 * Method Name			: updateEndTime()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static void updateEndTime(String endTime)
	{
		try
		{
			StringBuffer str=new StringBuffer();
			FileInputStream fin=new FileInputStream(ResultReportFile);
			DataInputStream dis=new DataInputStream(fin);
			InputStreamReader fr=new InputStreamReader(dis);
			BufferedReader br=new BufferedReader(fr);
			String strLine=null;
			while((strLine=br.readLine())!=null)
			{
				if (strLine.indexOf("END_TIME")!=-1)
				{
					strLine=strLine.replace("END_TIME", endTime);
				}
				str.append(strLine);
			}
			fin.close();
			FileOutputStream fout=new FileOutputStream(ResultReportFile);
			DataOutputStream out=new DataOutputStream(fout);
			out.writeBytes(str.toString());
			
			fout.close();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	/***************************************************************************
	 * Method Name			: addTestDetails()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to add test details to 
	 * 						  the arraylist
	 ****************************************************************************
	 */
	
	public static void addTestStepsDetails(String objectDescription,String objectStatus,String screenshots)
	{
		try
		{
			objDescription.add(objectDescription);
			objStatus.add(objectStatus);
			screenshot.add(screenshots);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	
	/***************************************************************************
	 * Method Name			: deleteDescription()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to add test details to 
	 * 						  the arraylist
	 ****************************************************************************
	 */

	public static void deleteDescription()
	{
		try
		{
			objDescription.removeAll(objDescription);
			objStatus.removeAll(objStatus);
			screenshot.removeAll(screenshot);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	/***************************************************************************
	 * Method Name			: addTestDetails()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to add test details to 
	 * 						  the arraylist
	 ****************************************************************************
	 */
	
	public static void extendReport()
	{
		try
		{
//            ExtentHtmlReporter reporter =new ExtentHtmlReporter(DesktopReportFileName);
//            reporter.config().setReportName("Web Automation Results");
//            reporter.config().setDocumentTitle("Test Results");
//            report =new ExtentReports();
//            report.attachReporter(reporter);
           
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
}



