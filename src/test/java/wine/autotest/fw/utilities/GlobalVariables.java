package wine.autotest.fw.utilities;


import java.util.Properties;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

public class GlobalVariables {
	
	public static ThreadLocal<WebDriver> webdriver= new ThreadLocal<WebDriver>();
	public static String testScriptXMLDataFileName;
	public static WebDriver driver;
	public static XMLData xmldata;
	public static String testdatacolumn;
	public static String environmentUrl;
	public static String state;	
	public static Properties objForgotPassword;
	public static Properties objConfig;
	public static String expectedData;
	public static String objectMapData;
	public static Logger log=null;
	public static UIFoundation appInd;
	public static String stateName=null;
	public static String recEmail=null;
	public static String recGiftMsg=null;
	public static Properties url = null;
	protected static String userEmail=null;
	public static String testCaseID=null;
	public static String methodName = null;
	public static String Method = null;
	public static ExtentReports report;
	public static ExtentTest logger;
	public static ThreadLocal<ExtentTest> Threadlogger = new ThreadLocal<ExtentTest>();

	public static String testScriptXMLTestDataFileName=System.getProperty("user.dir")+"\\src\\test\\resources\\testdata\\TestData.xml";
	public static String configurl = System.getProperty("user.dir")+"\\src\\test\\resources\\url.properties";
	public static String screenshotpath = System.getProperty("user.dir") + "\\testrunreports\\screenshots\\";
	public static String OrderFilePath = System.getProperty("user.dir") + "\\src\\test\\resources\\OrderNumber.txt";
	public static String GiftCertificate = System.getProperty("user.dir")+"\\src\\test\\resources\\giftCertificate.txt";
	
	public static String DesktopReportFileName=System.getProperty("user.dir")+"\\testrunreports\\desktopresults\\WineDesktopReport.html";
	public static String OrderReportFileName=System.getProperty("user.dir")+"\\testrunreports\\orderresults\\WineOrderReport.html";
	public static String MobileReportFileName=System.getProperty("user.dir")+"\\testrunreports\\mobileresults\\WineMobileReport.html";
	public static String SmokeReportFileName=System.getProperty("user.dir")+"\\testrunreports\\smokeresults\\WineSmokeReport.html";
		
	public static String ExtentDesktopReportFileName=System.getProperty("user.dir")+"\\ExtentReport\\DesktopResults\\ExtentWineDesktopReport.html";
	public static String ExtentMobileReportFileName=System.getProperty("user.dir")+"\\ExtentReport\\MobileResults\\ExtentWineMobileReport.html";
	public static String ExtentOrderReportFileName=System.getProperty("user.dir")+"\\ExtentReport\\OrderCreationResults\\ExtentWineOrderCreationReport.html";
	public static String ExtentSmokeReportFileName=System.getProperty("user.dir")+"\\ExtentReport\\SmokeResults\\ExtentWineSmokeReport.html";
	public static void waitFor(long seconds) {
		long miliseconds;
		try {
			miliseconds = seconds * 1000;
			Thread.sleep(miliseconds);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static WebDriver getDriver() {
	return webdriver.get();	}
	
	public static ExtentTest getlogger(){
		return Threadlogger.get();	}

	
}
