package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyStewardAccountPageDispForStewardUser extends Desktop {
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Ramesh S  
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	 
	public static String login()
	{
		String objStatus=null;
		   String screenshotName = UIFoundation.randomNumber(9)+ "_Scenarios_keepMeSignedIn.jpeg";
	try
		{
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "Stewardusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.webDriverWaitForElement(LoginPage.btnSignIn, "Invisible", "", 50);
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			objStatus+=String.valueOf(UIBusinessFlows.isObjectExistForSignIn());
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				UIFoundation.webDriverWaitForElement( LoginPage.QAUserPopUp, "Invisible", "", 50);
			}			
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				String objDetail="Login test case is failed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{			
			String objDetail="Login test case is executed successfully";
			System.out.println(objDetail);
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";			
			}			
		}catch(Exception e)
		{		
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";	
		}
	}	
	/***********************************************************************************************************
	 * Method Name : VerifyStewardsettingsPage()() 
	 * Created By  : Ramesh S
	 * Purpose     : 
	 * 
	 ************************************************************************************************************
	 */
	public static String VerifyStewardsettingsPage() {
		String objStatus = null;
		String screenshotName = UIFoundation.randomNumber(9)+ "_Scenarios_VerifyStewardsettingsPage_Screenshot.jpeg";
		try {
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkStewardshipSetting));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnstewardshipHeader)) 
			{
				objStatus+=true;
				String objDetail="User is navigated to Stewardship Account  page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Stewardship Account  page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			if (objStatus.contains("false"))
			{				
				String objDetail="VerifyStewardAccountPageDispForStewardUser  test case failed ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{
				String objDetail="VerifyStewardAccountPageDispForStewardUser test case executed succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}	
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}	
}	