package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class ValidateNoReferaFriendlinkforStateOH extends Desktop {
	
	/***********************************************************************************************************
	 * Method Name : ValidateReferaFriendLnk()() 
	 * Created By  : Chandrashekar K B
	 * Purpose     : 
	 * 
	 ************************************************************************************************************
	 */
	public static String ValidateReferaFriendLnk() {
		String objStatus = null;
		String screenshotName = UIFoundation.randomNumber(9)+ "_Scenarios_ValidateReferaFriendLnk_Screenshot.jpeg";
		try {
			objStatus+=String.valueOf(UIFoundation.SelectObject(LoginPage.dwnSelectState, "OHWineState"));
			UIFoundation.waitFor(5L);
			getDriver().navigate().refresh();
			UIFoundation.waitFor(5L);
			if(!UIFoundation.isDisplayed(ListPage.lnkReferaFriend)) 
			{
				objStatus+=true;
				String objDetail="Refer a friend link is not displayed in the Homepage";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Refer a friend link is displayed in the  Homepage";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			UIFoundation.waitFor(2L);
			if (objStatus.contains("false"))
			{				
				String objDetail="ValidateNoReferaFriendlinkforStateOH  test case failed ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{
				String objDetail="ValidateNoReferaFriendlinkforStateOH test case executed succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}	
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}	
}	