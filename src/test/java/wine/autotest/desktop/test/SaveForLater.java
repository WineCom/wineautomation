package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.UIFoundation;



public class SaveForLater extends Desktop {
	
static boolean isObjectPresent=false;
	
/***************************************************************************
 * Method Name			: saveForLaterWithoutSignIn()
 * Created By			: Vishwanath Chavan
 * Reviewed By			: Ramesh,KB
 * Purpose				: The purpose of this method is to search for product 
 * 						  with name and adding to the cart
 ****************************************************************************
 */
public static String saveForLaterWithoutSignIn() {
	
	
	String objStatus=null;
	try {
		log.info("The execution of the method saveForLater started here ...");
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.SelectObject(LoginPage.dwnSelectState, "State"));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnMainNav));
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkBordexBlends));
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkSecondProductToCart));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkThirdProductToCart));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFourthProductToCart));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFifthProductToCart));
		UIFoundation.waitFor(1L);
		UIFoundation.clckObject(ListPage.lnkSort);
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
	//	objStatus+=String.valueOf(ApplicationDependent.validationForSaveForLater(driver));
		log.info("The execution of the method saveForLater ended here ...");
		if(objStatus.contains("false"))
		{
			
			return "Fail";
		}
		else
		{	
			return "Pass";
		}
		
	} catch (Exception e) {
		log.error("there is an exception arised during the execution of the method saveForLater "+ e);
		return "Fail";
	}

}

/***************************************************************************
 * Method Name			: saveForLaterWithSignIn()
 * Created By			: Vishwanath Chavan
 * Reviewed By			: Ramesh,KB
 * Purpose				: The purpose of this method is to search for product 
 * 						  with name and adding to the cart
 ****************************************************************************
 */
public static String saveForLaterWithSignIn() {
	
	String objStatus=null;
	try {
		log.info("The execution of the method saveForLaterWithSignIn started here ...");
		
		getDriver().navigate().refresh();
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.SelectObject(LoginPage.dwnSelectState, "State"));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnMainNav));
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkBordexBlends));
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkSecondProductToCart));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkThirdProductToCart));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFourthProductToCart));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFifthProductToCart));
		UIFoundation.waitFor(1L);
		UIFoundation.clckObject(ListPage.lnkSort);
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
		System.out.println(objStatus);
		log.info("The execution of the method saveForLaterWithSignIn ended here ...");
		if(objStatus.contains("false"))
		{
			
			return "Fail";
		}
		else
		{
			
			return "Pass";
		}
		
	} catch (Exception e) {
		log.error("there is an exception arised during the execution of the method saveForLater "+ e);
		return "Fail";
	}
	}

/***************************************************************************
 * Method Name			: verifySaveForLaterOnClickingShipToKY()
 * Created By			: Chandra Shekhar
 * Reviewed By			: Ramesh
 * Purpose				:  
 *  
 ****************************************************************************
 */
public static String verifySaveForLaterOnClickingShipToKY() {

	String objStatus=null;
	try {
		log.info("The execution of the method verifySaveForLaterOnClickingShipToKY started here ...");

		getDriver().navigate().refresh();
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.dwnChangeState, "dryState"));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.isDisplayed(CartPage.btncontinueShipToKY));			
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btncontinueShipToKY));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.isDisplayed(CartPage.spnyourCartIsEmpty));	
		UIFoundation.waitFor(5L);

		System.out.println(objStatus);
		log.info("The execution of the method verifySaveForLaterOnClickingShipToKY ended here ...");
		if(objStatus.contains("false"))
		{

			return "Fail";
		}
		else
		{

			return "Pass";
		}

	} catch (Exception e) {
		log.error("there is an exception arised during the execution of the method saveForLater "+ e);
		return "Fail";
	}
}

/***************************************************************************
 * Method Name			: verifySaveForLaterOnClickingShipToKY()
 * Created By			: Chandra Shekhar
 * Reviewed By			: Ramesh
 * Purpose				:  
 *  
 ****************************************************************************
 */
public static String VerifyProductMovedToCartFromSaveForLater() {
	
	String products1=null;
	String products2=null;
	String screenshotName = "Scenarios_VerifyProductMovedToCartFromSaveForLater.jpeg";

	String objStatus=null;
	try {
		log.info("The execution of the method verifySaveForLaterOnClickingShipToKY started here ...");
		
		if(!UIFoundation.getText(CartPage.lnkFirstProductSaveForLater).contains("Fail")){
			products1=UIFoundation.getText(CartPage.lnkFirstProductInCart);
			System.out.println("1) "+products1);			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.lnkFirstProductSaveForLater));
			}
		if(!UIFoundation.getText(CartPage.lnkFirstProductInSaveFor).contains("Fail")){
			products2=UIFoundation.getText(CartPage.lnkFirstProductInSaveFor);
			System.out.println("2) "+products2);
			if(products2.contains(products1)){
				objStatus+="pass";
				String objDetail="Product is Successfully added to Save for later section";
				getlogger().pass(objDetail);
			} else {
				objStatus+="fail";
				String objDetail="Product is Not added to Save for later section";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
			}
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.lnkmoveToCartFirstLink));
		UIFoundation.waitFor(4L);
		products1=UIFoundation.getText(CartPage.lnkFirstProductInCart);
		if(products1.contains(products2)){
			objStatus+="pass";
			String objDetail="Product is Successfully Added to Cart Section";
			getlogger().pass(objDetail);
		} else {
			objStatus+="fail";
			String objDetail="Product is Not Added to Cart Section";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
		System.out.println("objStatus save:-"+objStatus);
		log.info("The execution of the method verifySaveForLaterOnClickingShipToKY ended here ...");
		if(objStatus.contains("false"))
		{
			String objDetail="Verify the functionality of 'Save for later' link in cart section is Failed";
			getlogger().fail(objDetail);
			return "Fail";
		}
		else
		{
			String objDetail="Verify the functionality of 'Save for later' link in cart section is Passed";
			getlogger().pass(objDetail);
			return "Pass";
		}

	} catch (Exception e) {
		log.error("there is an exception arised during the execution of the method saveForLater "+ e);
		return "Fail";
	}
}
}
