package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;

public class VerifyShipToDateUpdated extends Desktop {

	/***************************************************************************
	 * Method Name : VerifyShipToDateUpdated() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */

	public static String verifyShipToDateUpdated() {
		String objStatus = null;
				
		try {

			log.info("The execution of the method verifyShipToDateUpdated started here ...");
			//UIFoundation.SelectObject(driver, "SelectState", "shipDateUpdatedState");
			UIFoundation.waitFor(10L);
			//getDriver().get("https://qwww.wine.com/product/the-federalist-lodi-cabernet-sauvignon-2017/630205");
			getDriver().get("https://qwww.wine.com/product/santa-carolina-reserva-cabernet-sauvignon-2018/584958");
			UIFoundation.waitFor(3L);
			String shipDateinPIPPage = UIFoundation.getText(ListPage.spnfirstProdName);
			System.out.println("shipDateinPIPPage : "+shipDateinPIPPage);
			objStatus += String.valueOf(UIFoundation.selectLastValueFromDropdown(ListPage.dwnFirstProdQuantitySelect));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(4L);
			String shipDateinCartPage = UIFoundation.getText(CartPage.spnShipTodayWarningMsg1);
			UIFoundation.waitFor(2L);
			System.out.println("shipDateinCartPage : "+shipDateinCartPage);
			shipDateinPIPPage=shipDateinPIPPage.toLowerCase();
			shipDateinCartPage=shipDateinCartPage.toLowerCase();
			
			if((shipDateinPIPPage.contains("today"))||(shipDateinPIPPage.contains("tomorrow"))){
				if(shipDateinPIPPage.contains("today")){
					if(shipDateinCartPage.contains("today")){
						String objdetail=("Shipping date in PIP page and Cart page are same :- "+shipDateinPIPPage);
						objStatus+=true;
						ReportUtil.addTestStepsDetails(objdetail, "Pass", "");
						getlogger().pass(objdetail);	
						System.out.println(objdetail);
					}else {
						objStatus+=false;
						String objdetail=("Shipping date in PIP page and Cart page are Not same ");
						UIFoundation.captureScreenShot(screenshotpath+"shippingdatenotsame.jpeg", objdetail);
						getlogger().fail(objdetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"shippingdatearenotsame.jpeg")).build());		
						System.out.println(objdetail);	
						}	
				} if(shipDateinPIPPage.contains("tomorrow")){
					if(shipDateinCartPage.contains("tomorrow")){
						String objdetail=("Shipping date in PIP page and Cart page are same :- "+shipDateinPIPPage);
						objStatus+=true;
						ReportUtil.addTestStepsDetails(objdetail, "Pass", "");
						getlogger().pass(objdetail);	
						System.out.println(objdetail);
					}else {
						objStatus+=false;
						String objdetail=("Shipping date in PIP page and Cart page are Not same ");
						UIFoundation.captureScreenShot(screenshotpath+"shippingdatenotsame.jpeg", objdetail);
						System.out.println(objdetail);
						getlogger().fail(objdetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"shippingdatearenotsame.jpeg")).build());		
						}}}
			else {
				String dayinPIPPage = null;
				String dateinPIPPage=shipDateinPIPPage.replaceAll("[^0-9]", "");
				if(shipDateinPIPPage.contains("sun")) {dayinPIPPage="sunday";}
				if(shipDateinPIPPage.contains("mon")) {dayinPIPPage="monday";}
				if(shipDateinPIPPage.contains("tue")) {dayinPIPPage="tuesday";}
				if(shipDateinPIPPage.contains("wed")) {dayinPIPPage="wednesday";}
				if(shipDateinPIPPage.contains("thu")) {dayinPIPPage="thursday";}
				if(shipDateinPIPPage.contains("fri")) {dayinPIPPage="friday";}
				if(shipDateinPIPPage.contains("sat")) {dayinPIPPage="saturday";}
				if((shipDateinCartPage.contains(dayinPIPPage))&&(shipDateinCartPage.contains(dateinPIPPage))){
					String objdetail=("Shipping date in PIP page and Cart page are same :- "+shipDateinPIPPage);
					objStatus+=true;
					ReportUtil.addTestStepsDetails(objdetail, "Pass", "");
					System.out.println(objdetail);
					getlogger().pass(objdetail);	
				}else {
					objStatus+=false;
					String objdetail=("Shipping date in PIP page and Cart page are Not same ");
					UIFoundation.captureScreenShot(screenshotpath+"shippingdatenotsame.jpeg", objdetail);
					System.out.println(objdetail);
					getlogger().fail(objdetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"shippingdatearenotsame.jpeg")).build());
				}
			}
			log.info("The execution of the method verifyShipToDateUpdated ended here ...");
			if (objStatus.contains("false")) {
				
				System.out.println(
						"Verify ship to date is same in PIP Page and Cart page test case is failed");
				return "Fail";
			
			} else {
				System.out.println(
						"Verify ship to date is same in PIP Page and Cart page ");
				getlogger().pass("Verify ship to date is same in PIP Page and Cart page ");
				return "Pass";
			}

		} catch (Exception e) {

			log.error(
					"there is an exception arised during the execution of the method verifyShipToDateUpdated "
							+ e);
			return "Fail";

		}
	}

}
