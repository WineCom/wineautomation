package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.PickedPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.test.Desktop;

public class VerifyDislikeSectionNotDispinVarietalPreffReviewPgwhenUserDoesntprovideDislikePreference extends Desktop{
	/***************************************************************************
	 * Method Name			: VerifyDislikeSectionofCompass()
	 * Created By			: ChandraShekhar KB
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String VerifyDislikeSectionofCompass()
	{
	String objStatus=null;			
	String screenshotName = "Scenarios_VerifyDislikeSection_Screenshot.jpeg";
	try
	{			
		getlogger().info("The execution of the method VerifyDislikeSection started here ...");
		objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetting));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdSignUpCompass));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkNoVoice));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizWineTypRed));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecLike));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
		UIFoundation.waitFor(4L);
		if ((UIFoundation.isDisplayed(PickedPage.spnlikedheadertext))&&(!UIFoundation.isDisplayed(PickedPage.spnDislikedHeaderText))) {
			objStatus+=true;				
			String objDetail = "Dislikes section is Not displayed when a user doesn't provide dislike preferences";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Dislikes section is displayed when a user doesn't provide dislike preferences";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		if (objStatus.contains("false")){
			objStatus+=false;			
	    	String objDetail="VerifyDislikeSectionNotDispinVarietalPreffReviewPgwhenUserDoesntprovideDislikePreference Test case Failed";
	    	getlogger().fail(objDetail);  	
			return "Fail";
			} else{
			    objStatus+=true;				
				String objDetail = "VerifyDislikeSectionNotDispinVarietalPreffReviewPgwhenUserDoesntprovideDislikePreference Test case is passed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
				return "Pass";
			}
	}catch(Exception e)
	{
		objStatus+=false;
    	String objDetail="VerifyDislikeSectionNotDispinVarietalPreffReviewPgwhenUserDoesntprovideDislikePreference Test case Failed";
    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
    	getlogger().error("there is an exception arised during the execution of the method VerifyDislikeSectionofCompass "+ e);
		return "Fail";	
	}
}}