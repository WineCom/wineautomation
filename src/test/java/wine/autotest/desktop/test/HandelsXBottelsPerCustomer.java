package wine.autotest.desktop.test;

import java.io.IOException;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class HandelsXBottelsPerCustomer extends Desktop {
	
	/***************************************************************************
	 * Method Name : addShipTodayProdTocrt() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addLimitProdTocrt() {
		String objStatus = null;
		String screenshotName = "Scenarios_ProductLimit_Screenshot.jpeg";
			
		try {
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(5L);		
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(1));	
			UIFoundation.waitFor(2L);			
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(4L);
			UIFoundation.selectValueByValue(CartPage.dwnincreaseQuantity, "20");
			UIFoundation.waitFor(2L);
			String cartcount=(UIFoundation.getText(CartPage.btnCartCount));
			System.out.println("Cartcount before changing the state:-"+cartcount);
			UIFoundation.SelectObject(LoginPage.dwnSelectState, "limitState");
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(CartPage.btnShipcontinue)){
				UIFoundation.clickObject(CartPage.btnShipcontinue);
				UIFoundation.waitFor(4L);
			}
			 cartcount=(UIFoundation.getText(CartPage.btnCartCount));
			System.out.println("Cartcount after changing the state:-"+cartcount);
			if(UIFoundation.isDisplayed(CartPage.spnDrystateerror)&&cartcount.equalsIgnoreCase("0"))
			{
				objStatus+=true;
				System.out.println("Product limit test case is executed successfully");
				 String objDetail="Product limit test case is executed successfully";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      getlogger().pass(objDetail);
			}
			else
			{
				objStatus+=false;
			       String objDetail="Product limit test case is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				System.err.println("Product limit test case is failed");
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			return "Fail";
		}
	}

}
