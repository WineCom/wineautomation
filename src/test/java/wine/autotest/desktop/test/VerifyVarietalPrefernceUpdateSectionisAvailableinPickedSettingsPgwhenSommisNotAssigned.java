package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;

public class VerifyVarietalPrefernceUpdateSectionisAvailableinPickedSettingsPgwhenSommisNotAssigned extends Desktop {

	/***************************************************************************
	 * Method Name			: VerifyPickedSettingsPreferences()
	 * Created By			: Aravind R
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String VerifyPickedSettingsPreferences()
	{
	String objStatus=null;			
	String screenshotName = "Scenarios_VerifyPickedSettingsPreferences_Screenshot.jpeg";
	try
	{			
		getlogger().info("The execution of the method VerifyPickedSettingsPreferences started here ...");
		getDriver().get("https://qwww.wine.com/");
		UIFoundation.waitFor(10L);
		objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkPickedSetting));
		UIFoundation.waitFor(5L);
		if (UIFoundation.isDisplayed(UserProfilePage.txtPickedHeader)&&UIFoundation.isDisplayed(UserProfilePage.txtPickedbanner)) {
			objStatus+=true;				
			String objDetail = "Picked Settings page is displayed With the Banner 'We're pairing you up with a personal somm'";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Picked Settings page is Not displayed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		if (UIFoundation.isDisplayed(UserProfilePage.btnUpdateRedVarietal)) {
			objStatus+=true;				
			String objDetail = "Varietal preference update section is available below the picked account page banner";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Varietal preference update section is Not available below the picked account page banner";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		if (objStatus.contains("false")){
			objStatus+=false;			
	    	String objDetail="VerifyVarietalPrefernceUpdateSectionisAvailableinPickedSettingsPgwhenSommisNotAssingned Test case Failed";
	    	getlogger().fail(objDetail);  	
			return "Fail";
			} else{
			    objStatus+=true;				
				String objDetail = "VerifyVarietalPrefernceUpdateSectionisAvailableinPickedSettingsPgwhenSommisNotAssingned Test case is passed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
				return "Pass";
			}
	}catch(Exception e)
	{
		objStatus+=false;
    	String objDetail="VerifyVarietalPrefernceUpdateSectionisAvailableinPickedSettingsPgwhenSommisNotAssingned Test case Failed";
    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
    	getlogger().error("there is an exception arised during the execution of the method VerifyPickedSettingsPreferences "+ e);
		return "Fail";	
	}
}}