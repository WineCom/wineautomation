package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;

public class PasswordValidationLessThanSixChar extends Desktop{
	
	/***************************************************************************
	 * Method Name			: passwordValidationLessThanSixChar()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Create new user account
	 ****************************************************************************
	 */
	
	public static String passwordValidationLessThanSixChar() {
		String objStatus=null;
		String expectedPwdErrorMsg=null;
		String actualPwdErrorMsg=null;
		   String screenshotName = "Scenarios__passwordScreenshot.jpeg";
					
		try {
			log.info("The execution of method passwordValidationLessThanSixChar started here");
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			//objStatus+=String.valueOf(UIFoundation.clckObject(driver, "signInLink"));
			UIFoundation.waitFor(3L);;
			if(UIFoundation.isDisplayed(LoginPage.btnCreateAcc)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAcc));
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.txtEmail,"email"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtPassword, "pwdLessThanSix"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAccount));
			UIFoundation.waitFor(6L);
			expectedPwdErrorMsg=verifyexpectedresult.passwordErrorMsg;
			actualPwdErrorMsg=UIFoundation.getText(LoginPage.spnPasswordErrMsg);
			System.out.println("actualPwdErrorMsg :"+actualPwdErrorMsg);
			if(expectedPwdErrorMsg.equalsIgnoreCase(actualPwdErrorMsg))
			{
				objStatus+=true;
				String objDetail="Password Error mssage is validated.";
				ReportUtil.addTestStepsDetails("Password Error mssage is validated.", "Pass", "");
				getlogger().pass(objDetail);
				System.out.println("Password Error mssage is validated.");
			}else
			{
				objStatus +=false;
				String objDetail="Password Error mssage is validation test cases failed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);

				System.out.println("Password Error mssage is validation test cases failed.");
			}

			log.info("The execution of the method passwordValidationLessThanSixChar ended here ...");
			if (objStatus.contains("false") ) {
				String objDetail="Verify password validation with less than six character  test case is failed";
				System.out.println("Verify password validation with less than six character  test case is failed");
				getlogger().pass(objDetail);
				return "Fail";
			} else {
				System.out.println("Verify password validation with less than six character  test case is executed successfully");
				String objDetail="Verify password validation with less than six character  test case is executed successfully";
				getlogger().pass(objDetail);
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("Verify password validation with less than six character  test case is failed");
			log.error("there is an exception arised during the execution of the method passwordValidationLessThanSixChar "
					+ e);
			return "Fail";
		}
	}

}
