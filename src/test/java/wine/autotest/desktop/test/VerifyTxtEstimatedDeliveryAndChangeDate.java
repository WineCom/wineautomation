package wine.autotest.desktop.test;


import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;


public class VerifyTxtEstimatedDeliveryAndChangeDate extends Desktop {
	

	
		
	/***************************************************************************
	 * Method Name : verifyEstimatedDeliveryText() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh. 
	 * Purpose     : Verify the Text for Estimated Delivery and Change Date.
	 * Jira ID     :  TM-5015
	 ****************************************************************************
	 */

	public static String verifyEstimatedDeliveryText() {
		String objStatus = null;		
		String screenshotName = "verifyEstimatedDeliveryText.jpeg";
		
		try {
			UIFoundation.waitFor(2L);
			log.info("The execution of the method verifyEstimatedDeliveryText started here ...");
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));	
						
			UIFoundation.waitFor(5L);					
			if(UIFoundation.isDisplayed(FinalReviewPage.txtEstimatedDelivery) && UIFoundation.isDisplayed(FinalReviewPage.txtChangeDeliveryDate))
			{			
							
				objStatus+=true;
				String objDetail="Extimated Delivery and Change Date Displayed in the Recipient Section";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{				
				   objStatus+=false;
				   String objDetail="Extimated Delivery and Change Date not Displayed in the Recipient Section";
				   ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				   getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				   UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method verifyEstimatedDeliveryText ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method verifyEstimatedDeliveryText " + e);
			return "Fail";
		}

	}
}

