package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.test.Desktop;

public class VerifyDeliveryCalenderisDispWhenUserSelectDeliverAnywayOptn extends Desktop{
	/***************************************************************************
	 * Method Name			: VerifyDeliveryCalender()
	 * Created By			: ChandraShekhar KB
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String VerifyDeliveryCalender()
	{
	String objStatus=null;			
	String screenshotName = "Scenarios_VerifyDeliveryCalender_Screenshot.jpeg";
	try
	{			
		log.info("Execution of the method VerifyDeliveryCalender started here .........");		
		objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
		UIFoundation.waitFor(8L);
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.rdoShipToHome));
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
		objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
		objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
		objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
		objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "WeatherHoldState"));
		UIFoundation.clickObject(FinalReviewPage.dwnState);
		objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "WeatherHoldZip"));
		objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
		UIFoundation.waitFor(5L);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
		UIFoundation.waitFor(10L);
		if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
			UIFoundation.waitFor(10L);	
			}		
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.rdoDeliveranyway));
		UIFoundation.waitFor(5L);
		if (UIFoundation.isDisplayed(FinalReviewPage.lnkcalenderFirstMnth)) {
			objStatus+=true;				
			String objDetail = "'Severe Weather Advisory' is displayed";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="'Severe Weather Advisory' is Not displayed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		if (UIFoundation.isDisplayed(FinalReviewPage.lnkcalenderFirstMnth)&&UIFoundation.isDisplayed(FinalReviewPage.spncurrentDate)) {
			objStatus+=true;				
			String objDetail = "Delivery Calendar is Displayed and Earliest Possible Date is Selected";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Delivery Calendar is Not Displayed and Earliest Possible Date is Not Selected";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		UIFoundation.waitFor(5L);
		String strShppingMethod =UIFoundation.getText(FinalReviewPage.txtShippingMethod);
        System.out.println("shppingMethod :"+strShppingMethod);
        String strShippingMethodExp = verifyexpectedresult.standShippingText;
       if(strShppingMethod.equals(strShippingMethodExp)) {
        objStatus+=true;
        String objDetail = "Standard shpping method is selected";
        getlogger().pass(objDetail);
        ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
        }
        else {
        objStatus+=false;
        String objDetail = "Standard shpping method is not selected";
        getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
        UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
        }
		String strSelectedDatecolor=UIFoundation.getColor(FinalReviewPage.spndeliveryHeaderDate);
		System.out.println("strSelectedDatecolor:-"+strSelectedDatecolor);
		if (strSelectedDatecolor.equalsIgnoreCase("#388604")) {
			objStatus+=true;				
			String objDetail = "Selected date is displayed in Green text.";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Selected date is Not displayed in Green text.";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		
		if (objStatus.contains("false")){
			objStatus+=false;			
	    	String objDetail="VerifyDeliveryCalenderisDispWhenUserSelectDeliverAnywayOptn Test case Failed";
	    	getlogger().fail(objDetail);  	
			return "Fail";
			} else{
			    objStatus+=true;				
				String objDetail = "VerifyDeliveryCalenderisDispWhenUserSelectDeliverAnywayOptn Test case is passed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
				return "Pass";
			}
	}catch(Exception e)
	{
		objStatus+=false;
    	String objDetail="VerifyDeliveryCalenderisDispWhenUserSelectDeliverAnywayOptn Test case Failed";
    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
    	getlogger().error("there is an exception arised during the execution of the method VerifyDeliveryCalender "+ e);
		return "Fail";	
	}
}}