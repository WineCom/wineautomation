package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.UserProfilePage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.test.Desktop;

public class VerifyErrorMsgDispuponClickingConfirmButtonwhenUserDoesntSelectReschedulingOption extends Desktop{
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	 
	public static String login()
	{
		String objStatus=null;
	try
		{
			log.info("The execution of the method login started here ...");

			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "compassUser"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				}
			objStatus+=String.valueOf(UIBusinessFlows.isObjectExistForSignIn());
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false")){
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}	
	
	
	
	
	
	/***************************************************************************
	 * Method Name			: VerifyPickedSettingsChangedate()
	 * Created By			: ChandraShekhar KB
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String VerifyPickedSettingsChangedate()
	{
	String objStatus=null;			
	String screenshotName = "Scenarios_VerifyPickedsettingsChangedate_Screenshot.jpeg";
	try
	{			
		getlogger().info("The execution of the method VerifyPickedsettingsChangedate started here ...");
		objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkPickedSetting));
		UIFoundation.waitFor(5L);
		if (UIFoundation.isDisplayed(UserProfilePage.txtPickedHeader)) {
			objStatus+=true;				
			String objDetail = "Picked Settings page is displayed";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Picked Settings page is Not displayed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.lnkChangeDate);
		objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkChangeDate));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.btnConfirmDate));
		if (UIFoundation.isDisplayed(UserProfilePage.spnSelectDateError)) {
			objStatus+=true;				
			String objDetail = "'Please select a date before confirming.' is displayed";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="'Please select a date before confirming.' is Not displayed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		if (objStatus.contains("false")){
			objStatus+=false;			
	    	String objDetail="VerifyErrorMsgDispuponClickingConfirmButtonwhenUserDoesntSelectReschedulingOption Test case Failed";
	    	getlogger().fail(objDetail);  	
			return "Fail";
			} else{
			    objStatus+=true;				
				String objDetail = "VerifyErrorMsgDispuponClickingConfirmButtonwhenUserDoesntSelectReschedulingOption Test case is passed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
				return "Pass";
			}
	}catch(Exception e)
	{
		objStatus+=false;
    	String objDetail="VerifyErrorMsgDispuponClickingConfirmButtonwhenUserDoesntSelectReschedulingOption Test case Failed";
    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
    	getlogger().error("there is an exception arised during the execution of the method VerifyPickedsettingsChangedate "+ e);
		return "Fail";	
	}
}}