package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyTheContentsOfVerifyAddressDialog extends Desktop {
	
	/***************************************************************************
	 * Method Name : shippingDetails() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to fill the
	 * shipping address of the customer
	 * TM-645,649,646
	 ****************************************************************************
	 */

	public static String shippingDetails() {
		String objStatus = null;
		   try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(6L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			

			if(!UIFoundation.isSelected(FinalReviewPage.rdoOriginalAddress))
			{
				  objStatus+=true;
			      String objDetail="'Use this Shipping Address' checkbox should be available and should be unmarked by default";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      getlogger().pass(objDetail);
				  System.out.println("'Use this Shipping Address' checkbox should be available and should be unmarked by default");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="'Use this Shipping Address' checkbox should be available and not unmarked by default";
			       UIFoundation.captureScreenShot(screenshotpath, objDetail);
			       getlogger().fail(objDetail);
				   System.err.println("'Use this Shipping Address' checkbox should be available and not unmarked by default");
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(20L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress))
			{
				  objStatus+=true;
			      String objDetail="Verified  the Continue button functionality in Verify Address dialog";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      getlogger().pass(objDetail);
				  System.out.println("Verified  the Continue button functionality in Verify Address dialog");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the Continue button functionality in Verify Address dialog test case is failed";
			       UIFoundation.captureScreenShot(screenshotpath+"ContinueButtonVerifyAddress", objDetail);
			       getlogger().fail(objDetail);
				   System.err.println("Verify the Continue button functionality in Verify Address dialog");
			}

			System.out.println(objStatus);
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {
			//	System.out.println("Verify the contents of Verify Address Dialog test case is failed");
				return "Fail";
			} else {
				//System.out.println("Verify the contents of Verify Address Dialog test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}
	

}
