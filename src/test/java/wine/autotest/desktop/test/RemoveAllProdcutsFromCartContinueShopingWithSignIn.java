package wine.autotest.desktop.test;

import org.testng.Assert;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;

public class RemoveAllProdcutsFromCartContinueShopingWithSignIn extends Desktop {
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	static int countProducts=3;
	public static String addprodTocrt() {
		String objStatus = null;
		boolean isElementPresent=false;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkRegionR));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkwashingtonWines));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(countProducts));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollUp();
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(ListPage.btnCartCount)) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			else {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			isElementPresent=UIFoundation.isDisplayed(LoginPage.txtLoginEmail);
			if(isElementPresent)
			{
				objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "username"));
				objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
				UIFoundation.waitFor(3L);
			}
			UIFoundation.waitFor(4L);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: removeProductsFromTheCart()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String removeProductsFromTheCart()
	{
		String objStatus=null;
		String expected=null;
		String actual=null;

		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			UIFoundation.waitFor(1L);
			UIFoundation.scrollUp();
			UIFoundation.waitFor(2L);
			for(int i=0;i<countProducts;i++)
			{
				UIFoundation.javaScriptClick(CartPage.lnkRemoveFirstProduct);
				//UIFoundation.waitFor(2L);
				UIFoundation.javaScriptClick(CartPage.btnRemove);
				UIFoundation.waitFor(3L);
			}
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnContinueShopping));
			UIFoundation.waitFor(5L);
			expected=verifyexpectedresult.homePageTitle;
			actual=getDriver().getTitle();
			// Assertion
			Assert.assertEquals(actual, expected);
			log.info("The execution of the method removeProductsFromTheCart started here ...");
			if (objStatus.contains("false") && !(expected.equalsIgnoreCase(actual)))
			{
				String objDetail="RemoveAllProdcutsFromCartContinueShopingWithSignIn test case Failed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"screenshotName.jpeg")).build());
				System.out.println(objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="RemoveAllProdcutsFromCartContinueShopingWithSignIn test case executed sucessfully";
			     ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			     logger.pass(objDetail);
			     System.out.println(objDetail);
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}
	}

}
