package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.order.pages.ListPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.test.Desktop;

public class VerifyUserAbletoAddSpritsviaPIP extends Desktop{
	/***************************************************************************
	 * Method Name			: VerifySpiritFilters()
	 * Created By			: ChandraShekhar KB
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String AddSpiritstoPIPandVerify()
	{
	String objStatus=null;			
	String screenshotName = "Scenarios_Verifyspiritsmenu_Screenshot.jpeg";
	try
	{			
		getlogger().info("The execution of the method verifySubscriptions started here ...");
		objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkBrandy));
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
		UIFoundation.waitFor(5L);
		String strPIPProductname =UIFoundation.getText(ListPage.spnPIPName);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkAddToMyWineFunc));
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
		UIFoundation.waitFor(2L);
		String strMyWineProductname =UIFoundation.getText(ListPage.lnkmerlotFirstProd);
		if (strPIPProductname.equalsIgnoreCase(strMyWineProductname)){
			objStatus+=true;				
			String objDetail = "Spirit added to MyWine sucessfully";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Spirit is Not added to MyWine";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkAddToMyWineFunc));
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSpirits));
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkthirdListProd));//lnkfirstListProd
		UIFoundation.waitFor(5L);
		strPIPProductname =UIFoundation.getText(ListPage.spnPIPName);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkrateProduct));
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
		UIFoundation.waitFor(2L);
		strMyWineProductname =UIFoundation.getText(ListPage.lnkmerlotFirstProd);
		if (strPIPProductname.equalsIgnoreCase(strMyWineProductname)){
			objStatus+=true;				
			String objDetail = "Spirit added to MyWine sucessfully";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Spirit is Not added to MyWine";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkAddToMyWineFunc));
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
		UIFoundation.waitFor(2L);
		strMyWineProductname =UIFoundation.getText(ListPage.lnkmerlotFirstProd);
		if (!strPIPProductname.equalsIgnoreCase(strMyWineProductname)){
			objStatus+=true;				
			String objDetail = "Spirit is removed from MyWine sucessfully";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Spirit is Not removed from MyWine";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		if (objStatus.contains("false")){
			objStatus+=false;			
	    	String objDetail="VerifyUserAbletoAddSpritsviaPIP Test case Failed";
	    	getlogger().fail(objDetail);  	
			return "Fail";
			} else{
			    objStatus+=true;				
				String objDetail = "VerifyUserAbletoAddSpritsviaPIP Test case is passed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
				return "Pass";
			}
	}catch(Exception e)
	{
		objStatus+=false;
    	String objDetail="VerifyUserAbletoAddSpritsviaPIP Test case Failed";
    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
    	getlogger().error("there is an exception arised during the execution of the method AddSpiritstoPIPandVerify "+ e);
		return "Fail";	
	}
}}