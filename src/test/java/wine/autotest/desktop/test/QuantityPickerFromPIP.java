package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.order.pages.ListPage;
import wine.autotest.fw.utilities.UIFoundation;

public class QuantityPickerFromPIP extends Desktop {
	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String verifyQuantityPickerInCartPage() {
		String objStatus=null;
		//String actualQuantity=null;
		//int expectedQuantity=null;
		String screenshotName = "Scenarios_QuantityPick_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "shafer"));
			UIFoundation.waitFor(5L);
			WebElement var=getDriver().findElement(ListPage.txtsearchProduct);
			var.sendKeys(Keys.ENTER);
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFrstProductName));
			UIFoundation.waitFor(4L);
			UIFoundation.getLastOptionFromDropDown(ListPage.dwnFirstProdQuantitySelect);
			UIFoundation.waitFor(3L);
			int expectedQuantity=Integer.parseInt(UIFoundation.getFirstSelectedValue(ListPage.dwnFirstProdQuantitySelect));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(2L);
			WebElement drop_down =getDriver().findElement(By.xpath("//select[@class='productQuantity_select']"));
			Select se = new Select(drop_down);
			int actualQuantity=Integer.parseInt(se.getOptions().get(expectedQuantity).getAttribute("value"));
			UIFoundation.waitFor(1L);
			if(expectedQuantity==actualQuantity)
			{
				objStatus+=true;
				String objDetail="Selected quantity in PIP page is matched with the cart page quantity";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				getlogger().pass(objDetail);
				System.out.println("Selected quantity in PIP page is matched with the cart page quantity");
			}
			else
			{
				objStatus+=false;
		    	String objDetail="Selected quantity in PIP page does not match with the cart page quantity";
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		    	getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println("Selected quantity in PIP page does not match with the cart page quantity");
			}
			System.out.println(objStatus);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
				
			}
			else
			{
				
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}

}
