package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.order.pages.ListPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.test.Desktop;

public class VerifyFiltersinSpirits extends Desktop{
	/***************************************************************************
	 * Method Name			: VerifySpiritFilters()
	 * Created By			: ChandraShekhar KB
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String VerifySpiritFilters()
	{
	String objStatus=null;			
	String screenshotName = "Scenarios_VerifyspiritFilters_Screenshot.jpeg";
	try
	{			
		getlogger().info("The execution of the method VerifyFiltersinSpirits started here ...");
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSpirits));
		UIFoundation.waitFor(2L);
		
		//Varietal Filter
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkNewVarietalR));
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkAllWhiskey));
		UIFoundation.waitFor(7L);
		String strFirstProductname=UIFoundation.getText(ListPage.lnkFrstProductName);
		System.out.println("First Product Name:-"+strFirstProductname);
		if (strFirstProductname.contains("Whisky")){
			objStatus+=true;				
			String objDetail = "Filter 'Varietal' is applied sucessfully";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Filter 'Varietal' is Not applied ";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.spnClearVarietalFilter));
		UIFoundation.waitFor(2L);
	    
		//Region Filter
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkRegion));
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkRegionOtherUS));
		UIFoundation.waitFor(10L);
		String strFirstProductOrigin=UIFoundation.getText(ListPage.txtFirstProductOrigin);
		System.out.println("First Product Origin:-"+strFirstProductOrigin);
		if (strFirstProductOrigin.contains("Other U.S.")){
			objStatus+=true;				
			String objDetail = "Filter 'Region' is applied sucessfully";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Filter 'Region' is Not applied ";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.spnClearRegionFilter));
		UIFoundation.waitFor(2L);
		
		//Rating and Price Filter
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkRatingAndPrice));
		WebElement RatingRightSlider = getDriver().findElement(By.xpath("(//div[@class='noUi-handle noUi-handle-upper'])[1]"));
		WebElement PriceRightSlider = getDriver().findElement(By.xpath("(//div[@class='noUi-handle noUi-handle-upper'])[2]"));
		Actions actions = new Actions(getDriver());
		actions.dragAndDropBy(RatingRightSlider, -50, 0).perform();
		actions.dragAndDropBy(PriceRightSlider, -50, 0).perform();
		String strRatingValue=UIFoundation.getText(ListPage.spnRatingFilterValue);
		strRatingValue=strRatingValue.replaceAll("[^0-9]", "");
		int intRatingValueExp=Integer.parseInt(strRatingValue);
		String strPriceValue=UIFoundation.getText(ListPage.spnPriceFilterValue);
		strPriceValue=strPriceValue.replaceAll("[^0-9]", "");
		int intPriceValueExp=Integer.parseInt(strPriceValue);
		UIFoundation.waitFor(10L);
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkRatingAndPrice));
	    strRatingValue=UIFoundation.getText(ListPage.spnFirstProductRating);
	    strRatingValue=strRatingValue.replaceAll("[^0-9]", "");
	    int intRatingValueAct=Integer.parseInt(strRatingValue);
	    //System.out.println("intRatingValueAct"+intRatingValueAct);
		strPriceValue=UIFoundation.getText(ListPage.spnproductPriceRegWhole);
		strPriceValue=strPriceValue.replaceAll("[^0-9]", "");
		int intPriceValueAct=Integer.parseInt(strPriceValue);
		//System.out.println("intPriceValueAct"+intPriceValueAct);
		if ((intRatingValueAct<=intRatingValueExp)&&(intPriceValueAct<=intPriceValueExp)){
			objStatus+=true;				
			String objDetail = "Filter 'Rating and Price' is applied sucessfully";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Filter 'Rating and Price' is Not applied ";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.spnClearRatingandPriceFilter));
		UIFoundation.waitFor(10L);
		
		//Reviewed By Filter
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkmoreFilters));
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.spnNewreviewedBy));
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkWineEnthusiast));
		UIFoundation.waitFor(10L);
		String strFirstProductReviewedBy=UIFoundation.getText(ListPage.txtFirstProductReviewedBy);
		System.out.println("First Product Reviewed By:-"+strFirstProductReviewedBy);
		if (strFirstProductReviewedBy.contains("WE")){
			objStatus+=true;				
			String objDetail = "Filter 'Reviewed By' is applied sucessfully";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Filter 'Reviewed By' is Not applied ";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.spnClearReviewedFilter));
		UIFoundation.waitFor(2L);
		
		//Size and Type Filter
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.spnNewSizeAndType));
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkSizeandTypeGreen));
		UIFoundation.waitFor(6L);
		if (UIFoundation.isDisplayed(ListPage.imgFirstProdGreenWineicon)){
			objStatus+=true;				
			String objDetail = "Filter 'Size and Type' is applied sucessfully";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Filter 'Size and Type' is Not applied ";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.spnClearSizeandTypeFilter));
		UIFoundation.waitFor(2L);
		
		//Rare and Collectible Filter
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.selFeaturedProd));
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.SelFeaturedProdRare));
		UIFoundation.waitFor(6L);
		if (UIFoundation.isDisplayed(ListPage.imgFirstProdRareWineicon)){
			objStatus+=true;				
			String objDetail = "Filter 'Rare & Collectible' is applied sucessfully";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Filter 'Rare & Collectible' is Not applied ";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.spnClearFeaturedFilter));
		UIFoundation.waitFor(2L);
		if (objStatus.contains("false")){
			objStatus+=false;			
	    	String objDetail="VerifyFiltersinSpirits Test case Failed";
	    	getlogger().fail(objDetail);  	
			return "Fail";
			} else{
			    objStatus+=true;				
				String objDetail = "VerifyFiltersinSpirits Test case is passed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
				return "Pass";
			}
	}catch(Exception e)
	{
		objStatus+=false;
    	String objDetail="VerifyFiltersinSpirits Test case Failed";
    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
    	getlogger().error("there is an exception arised during the execution of the method VerifyFiltersinSpirits "+ e);
		return "Fail";	
	}
}}