package wine.autotest.desktop.test;

import java.io.IOException;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.fw.utilities.UIFoundation;


public class VerifyWarningMessageForShipTodayProductsAndListProducts extends Desktop {

	/***************************************************************************
	 * Method Name : addShipTodayProdTocrt() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addShipTodayProdTocrt() {
		String objStatus = null;

/*		String addToCart4 = null;
		String addToCart5 = null;
		String addToCart6 = null;
		String addToCart7 = null;*/
		String expectedWarningMsg = null;
		String actualWarningMsg = null;
		   String screenshotName = UIFoundation.randomNumber(9)+"Scenarios__RemoveShipScreenshot.jpeg";
					
		try {
			
			getDriver().navigate().to("https://qwww.wine.com/search/Chasseur%20Syrah%202013/0");
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIBusinessFlows.addproductstocart(1));
			System.out.println("ShipsSoonestR");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkPinotNoir));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIBusinessFlows.addproductstocart(1));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkdiscover));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIBusinessFlows.addproductstocart(1));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(2L);
			expectedWarningMsg = verifyexpectedresult.shiptodayandlistproducts;
			actualWarningMsg = UIFoundation.getText(CartPage.spnShipTodayWarningMsg);
			if (actualWarningMsg.contains(expectedWarningMsg)) {
				objStatus+=true;
				String objDetail="Verifiy warning message for shiptoday products and others products test case is Passed";
				getlogger().pass(objDetail);
			} else {
				System.out.println(actualWarningMsg);
				objStatus+=false;
				String objDetail="Verifiy warning message for shiptoday products and others products test case is failed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			if (objStatus.contains("false")) {
				System.err.println(
						"Verify warning message for shiptoday products and others products test case is failed ");
				return "Fail";
			} else {
				System.out.println(
						"Verify warning message for shiptoday products and others products test case is executed successfully ");
				return "Pass";
			}

		} catch (Exception e) {

			return "Fail";
		}
	}

}
