package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.PickedPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.test.Desktop;

public class VerifyLikeSectionNotDispinVarietalPreffReviewPgwhenUserDoesntprovideLikePreference extends Desktop{
	/***************************************************************************
	 * Method Name			: VerifyLikeSectionofCompass()
	 * Created By			: ChandraShekhar KB
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String VerifyLikeSectionofCompass()
	{
	String objStatus=null;			
	String screenshotName = "Scenarios_VerifyLikeSectionofCompass_Screenshot.jpeg";
	try
	{			
		getlogger().info("The execution of the method VerifyLikeSectionofCompass started here ...");
		objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetting));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdSignUpCompass));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkNoVoice));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizWineTypRed));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelDisLike));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
		UIFoundation.waitFor(4L);
		if ((!UIFoundation.isDisplayed(PickedPage.spnlikedheadertext))&&(UIFoundation.isDisplayed(PickedPage.spnDislikedHeaderText))) {
			objStatus+=true;				
			String objDetail = "Likes section is Not displayed when a user doesn't provide Like preferences";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Likes section is displayed when a user doesn't provide Like preferences";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		if (objStatus.contains("false")){
			objStatus+=false;			
	    	String objDetail="VerifyLikeSectionNotDispinVarietalPreffReviewPgwhenUserDoesntprovideLikePreference Test case Failed";
	    	getlogger().fail(objDetail);  	
			return "Fail";
			} else{
			    objStatus+=true;				
				String objDetail = "VerifyLikeSectionNotDispinVarietalPreffReviewPgwhenUserDoesntprovideLikePreference Test case is passed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
				return "Pass";
			}
	}catch(Exception e)
	{
		objStatus+=false;
    	String objDetail="VerifyLikeSectionNotDispinVarietalPreffReviewPgwhenUserDoesntprovideLikePreference Test case Failed";
    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
    	getlogger().error("there is an exception arised during the execution of the method VerifyLikeSectionofCompass "+ e);
		return "Fail";	
	}
}}