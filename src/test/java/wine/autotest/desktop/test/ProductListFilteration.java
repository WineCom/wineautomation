package wine.autotest.desktop.test;


import java.util.regex.Pattern;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;


public class ProductListFilteration extends Desktop{
	
	static Pattern pattern;
	static String arrTestData[];
	static String arrObjectMap[];
	static String expectedText;
	static String expected,actual;
	static String value;
	static int expectedItemCount[]={1519,253,217,1623,271,64};
	static int actualItemCount[]=new int[expectedItemCount.length];
	static boolean isObjectPresent=false;

	
	/***************************************************************************
	 * Method Name			: verifyOnlyThreeFiltersAreVisible()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to filter the product 
	 * 						  list by selecting some product,region,rate and price 
	 * 						  and checking for the count
	 * TM-3428
	 ****************************************************************************
	 */
	
	public static String verifyOnlyThreeFiltersAreVisible()
	{
		String objStatus=null;
		try
		{
			log.info("The execution of method productFilteration started here");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkvarietal));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.imgWineLogo));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkVarietalR));
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkRegionTab));
			if(UIFoundation.isDisplayed(ListPage.lnkRegionTab)) {
				String objDetail = "Region Displayed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				getlogger().pass(objDetail);
				
			}else {
				String objDetail = "Region is not Displayed";
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"verifyOnlyThreeFiltersAreVisible.jpeg")).build());
			}
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkNewRatingAndPrice));
			System.out.println("verifyOnlyThreeFiltersAreVisible objStatus :"+objStatus);
		//	objStatus+= String.valueOf(ApplicationDependent.isObjectExistForList(driver));
			if (objStatus.contains("false"))
			{				
			String objDetail = "Verify only 3 filters are visible by default on tablet/desktop list page test case is failed";
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"verifyOnlyThreeFiltersAreVisible.jpeg")).build());
			System.out.println(objDetail);
			return "Fail";
			}
			else
			{
				String objDetail = "Verify only 3 filters are visible by default on tablet/desktop list page. test case is executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				getlogger().pass(objDetail);
				System.out.println(objDetail);
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method verifyOnlyThreeFiltersAreVisible "+e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: verifyMoreFiltersElementsAreVisible()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * TM-3429
	 ****************************************************************************
	 */
	
	public static String verifyMoreFiltersElementsAreVisible()
	{
		String objStatus=null;
		try 
		{
			log.info("The execution of method productFilteration started here");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkmoreFilters));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.spnreviewedBy));
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.spnsizeAndType));
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.spnfineWine));
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkVintage));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnHide));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(!UIFoundation.isDisplayed(ListPage.lnkVintage));
			System.out.println("productFilteration objStatus:-"+objStatus);
			if (objStatus.contains("false"))
			{
				String objDetail = "Verify all the available filters are visible on clicking More filters test case is failed";
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"verifyOnlyThreeFiltersAreVisible.jpeg")).build());
				System.out.println(objDetail);
				return "Fail";
			}
			else
			{
				String objDetail = "Verify all the available filters are visible on clicking More filters test case is executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				getlogger().pass(objDetail);
				System.out.println(objDetail);
				return "Pass";
			}
		
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method verifyMoreFiltersElementsAreVisible "+e);
			return "Fail";
		}
	}
	/*
	*//***************************************************************************
	 * Method Name			: productFilteration()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to filter the product 
	 * 						  list by selecting some product,region,rate and price 
	 * 						  and checking for the count
	 ****************************************************************************
	 *//*
	
	public static String productFilteration(WebDriver driver)
	{
		String objStatus=null;
		try
		{
			log.info("The execution of method productFilteration started here");
			ApplicationIndependent.waitFor(3L);
			objStatus += String.valueOf(ApplicationIndependent.mouseHover(driver, "varietal"));
			objStatus+=String.valueOf(ApplicationDependent.ClickObjectItems(driver, "PinotNoir"));
			ApplicationIndependent.waitFor(2L);
			objStatus+=String.valueOf(ApplicationIndependent.clickObject(driver, "Region"));
			ApplicationIndependent.waitFor(2L);
			objStatus+=String.valueOf(ApplicationIndependent.clickObject(driver, "Oregon"));
			ApplicationIndependent.waitFor(4L);
			objStatus+=String.valueOf(ApplicationIndependent.clickObject(driver, "WillametteValley"));
			ApplicationIndependent.waitFor(2L);
			objStatus+=String.valueOf(ApplicationDependent.ClickObjectItems(driver, "YamhillCarlton"));
			ApplicationIndependent.waitFor(2L);
			objStatus+=String.valueOf(ApplicationIndependent.clickObject(driver, "RatingAndPrice"));
			ApplicationIndependent.waitFor(4L);
			objStatus+=String.valueOf(ApplicationIndependent.clickObject(driver, "Price"));
			ApplicationIndependent.waitFor(7L);
			objStatus+=String.valueOf(ApplicationIndependent.clickObject(driver, "Rating"));
			ApplicationIndependent.waitFor(5L);
			objStatus+=String.valueOf(ApplicationDependent.ClickObjectItems(driver, "Done"));
			ApplicationIndependent.waitFor(2L);
		//	objStatus+= String.valueOf(ApplicationDependent.isObjectExistForList(driver));
			if (objStatus.contains("false"))
			{
				
				System.out.println("Product Filteration test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Product Filteration test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method productFilteration "+e);
			return "Fail";
		}
	}
	
	*//***************************************************************************
	 * Method Name			: verifyPagination()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to verify the pagination
	 ****************************************************************************
	 *//*
	
	public static String verifyPagination(WebDriver driver)
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of method verify pagination started here");
			objStatus+=String.valueOf(ApplicationIndependent.clickObject(driver, "MainNavButton"));
			ApplicationIndependent.waitFor(2L);
			objStatus+=String.valueOf(ApplicationIndependent.clickObject(driver, "BordexBlends"));
			ApplicationIndependent.waitFor(2L);
			objStatus+=String.valueOf(ApplicationIndependent.clickObject(driver, "ShowOutOfStock"));
			ApplicationIndependent.waitFor(2L);
			objStatus+=String.valueOf(ApplicationIndependent.clickObject(driver, "ShowOutOfStock"));
			ApplicationIndependent.waitFor(2L);
			Actions action = new Actions(driver);
			for (int j = 0; j < 4; j++) {
				action.sendKeys(Keys.END).build().perform();
				ApplicationIndependent.waitFor(3L);
			}
			Actions action = new Actions(driver);
			action.sendKeys(Keys.END).build().perform();
			ApplicationIndependent.waitFor(3L);
			objStatus+= String.valueOf(ApplicationIndependent.scrollDown(driver));
			log.info("The execution of method verify pagination ended here");
			if (objStatus.contains("false"))
			{	System.out.println("Verify pagination test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify pagination test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method verifyPagination "+e);
			return "Fail";
		}
	}*/
}
