package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class StewardshipAutoRenewalOption extends Desktop {
	
	/***************************************************************************
	 * Method Name : StewardshipAutoRenewalOption() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : 
	 ****************************************************************************
	 */
	
	public static String stewardshipAutoRenewalOption()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_stewardshipRenewal_Screenshot.jpeg";
		
		try
		{
			
			log.info("The execution of the method stewardshipAutoRenewalOption started here ...");
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkStewardshipSet));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnstewardshipHeader)) 
			{
				objStatus+=true;
				String objDetail="User is navigated to Stewardship Account  page";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Stewardship Account  page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.btnstewardsCancel));
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnstewardsCancel));
			UIFoundation.waitFor(2L);
			if((UIFoundation.isDisplayed(UserProfilePage.spnStwmodalHeader)) &&(UIFoundation.isDisplayed(UserProfilePage.txtStwmodallifesavings)))
			{
				objStatus+=true;
				String objDetail="Lifetime Stewardship savings modal is Displayed With Keep Membership and Cancel Membership";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else{
				objStatus+=false;
				String objDetail="Lifetime Stewardship savings modal is Not Displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.btnStwmodalCancelMembership));
			UIFoundation.waitFor(2L);
			if((UIFoundation.isDisplayed(UserProfilePage.spnStwmodalHeader))&& (UIFoundation.isDisplayed(UserProfilePage.btnStwmodalConfrimCancelMembership)) &&
					(UIFoundation.isDisplayed(UserProfilePage.btnStwmodaloptoutKeepMembership)))
			{
				objStatus+=true;
				String objDetail="Reason collection modal is Displayed with the options Confirm Cancelation and Keep Membership";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else{
				objStatus+=false;
				String objDetail="Reason collection modal is Not Displayed with the options";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.btnstewardsCancelOptionFirst));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.btnStwmodalConfrimCancelMembership));
			UIFoundation.waitFor(2L);
			if((UIFoundation.isDisplayed(UserProfilePage.spnStwmodalHeader))&& (UIFoundation.isDisplayed(UserProfilePage.spnStwmodalAddDetailsExp)) &&
					(UIFoundation.isDisplayed(UserProfilePage.txtAnythingTellUs)))
			{
				objStatus+=true;
				String objDetail="Additional details modal is Displayed with the options";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else{
				objStatus+=false;
				String objDetail="Additional details modal is Not Displayed with the options";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.btnStwmodalSubmitandClose));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(UserProfilePage.btnStwshipRenew)) 
			{
				objStatus+=true;
				String objDetail="StewardShip has cancelled";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="StewardShip has not cancelled";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.btnStwshipRenew));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(UserProfilePage.btnstewardsCancel) && UIFoundation.isDisplayed(UserProfilePage.spnStewardsExpiryOn)) 
			{
				objStatus+=true;
				String objDetail="'Your StewardShip membership will renew on' is displayed in the 'Stewardship Settings' section with 'Cancel' link displayed at the end.";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="'Your StewardShip membership will renew on' is displayed in the 'Stewardship Settings' section with 'Cancel' link displayed at the end is failed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			/*
if(UIFoundation.isDisplayed(UserProfilePage.spnareYouSure)){
				  objStatus+=true;
			      String objDetail="'Are you sure' text is displayed";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				
			}else{
				 objStatus+=false;
			       String objDetail="'Are you sure' text is not displayed";
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(UserProfilePage.spnremainMember)){
				  objStatus+=true;
			      String objDetail="No,Remain a member link is displayed";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				
			}else{
				 objStatus+=false;
			       String objDetail="No,Remain a member link is not displayed";
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnstewardsYesCancel));
			
			if(UIFoundation.isDisplayed(UserProfilePage.spnstewardsExpiryOn) && (UIFoundation.isDisplayed(UserProfilePage.btnstewardshipRenew))){
				  objStatus+=true;
			      String objDetail="'Your StewardShip membership will expire on' is displyed in the 'Stewardship Settings' section";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				
			}else{
				 objStatus+=false;
			       String objDetail="'Your StewardShip membership will expire on' is displyed in the 'Stewardship Settings' section is failed";
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnstewardshipRenew));
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnstewardshipYesRenew));
			if(UIFoundation.isDisplayed(UserProfilePage.spnstewardsRenewOn) && UIFoundation.isDisplayed(UserProfilePage.btnstewardsCancel) ){
				  objStatus+=true;
			      String objDetail="'Your StewardShip membership will renew on' is displayed in the 'Stewardship Settings' section with 'Cancel' link displayed at the end.";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				
			}else{
				 objStatus+=false;
			       String objDetail="'Your StewardShip membership will renew on' is displayed in the 'Stewardship Settings' section with 'Cancel' link displayed at the end is failed";
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+"renew", objDetail);
			}
			
			if(UIFoundation.isDisplayed(UserProfilePage.spnunlimitedFreeShipping)){
				  objStatus+=true;
			      String objDetail="'Unlimited free shipping' is displayed in the 'Stewardship Settings'";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				
			}else{
				 objStatus+=false;
			       String objDetail="'Unlimited free shipping' is not displayed in the 'Stewardship Settings'";
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+"renew", objDetail);
			}
			
			if(UIFoundation.isDisplayed(UserProfilePage.spndedicatedCustomerService)){
				  objStatus+=true;
			      String objDetail="'Dedicated customer service' is displayed in the 'Stewardship Settings'.";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				
			}else{
				 objStatus+=false;
			       String objDetail="'Dedicated customer service' is not displayed in the 'Stewardship Settings'.";
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+"renew", objDetail);
			}
			
			if(UIFoundation.isDisplayed(UserProfilePage.spnstewardshipHeader)){
				  objStatus+=true;
			      String objDetail="'Stewardship Settings header' is displayed in the 'Stewardship Settings'.";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				
			}else{
				 objStatus+=false;
			       String objDetail="'Stewardship Settings header' is not displayed in the 'Stewardship Settings'.";
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+"renew", objDetail);
			}*/
			log.info("The execution of the method stewardshipAutoRenewalOption ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Verify the 'Stewardship settings' section for the existing stewardship member when the user cancels the auto-renewal option";
	    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			log.error("there is an exception arised during the execution of the method stewardshipAutoRenewalOption "+ e);
			return "Fail";
			
		}
	}

}
