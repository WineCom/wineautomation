package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class ValidateCustomStylingforDefaultModel extends Desktop{
	
	
/***********************************************************************************************************
 * Method Name : enrollForPickedUpWineandvalidatepromobar() 
 * Created By  : Ramesh S
 * Purpose     : The purpose of this method is to navigate compass tiles and 'Proceed to Enrollment' and Validate
 *               Promo bar in Authentication Page and Recipient Page
 *               
 ************************************************************************************************************
 */
public static String enrollForPickedUpWineandvalidatepromobar() {
	String objStatus = null;
	String screenshotName = UIFoundation.randomNumber(9)+ "_Scenarios_enrollPickedandValidatePromobar_Screenshot.jpeg";
	try {
		log.info("Execution of the method ValidateCustomStylingforDefaultModel started here .........");
		objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.imgpicked));
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.imgpicked));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.spnPromoBar)) 
		{
			String strPromoBar=UIFoundation.getText(PickedPage.spnPromoBar);
			System.out.println("Promo bar LandPage:-"+strPromoBar);
			int promoindex=strPromoBar.indexOf("Picked");
			if(promoindex>=0)
			{
				objStatus+=true;
				String objDetail="Promo Bar is Displayed on Landing page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Promo Bar is not Displayed on Landing page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
			}else{
			objStatus+=false;
			String objDetail="Promo Bar is not Displayed on Landing page";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdSignUpCompass));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkNoVoice));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkRedOnly));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNtVry));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.txtVerifyQtyPickedPage)) 
		{
			objStatus+=true;
			String objDetail="User is navigated to Quantity and Price select quiz page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Quantity and Price select quiz page";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSelQtyNxt));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.txtAuthPge)) 
		{
			objStatus+=true;
			String objDetail="User is navigated to Authentication page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Authentication page";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
		if(UIFoundation.isDisplayed(PickedPage.spnPromoBar)) 
		{
			String strPromoBar=UIFoundation.getText(PickedPage.spnPromoBar);
			System.out.println("Promo bar AuthPage:-"+strPromoBar);
			int promoindex=strPromoBar.indexOf("Picked");
			if(promoindex>=0)
			{
				objStatus+=true;
				String objDetail="Promo Bar is Displayed on Authentication page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Promo Bar is not Displayed on Authentication page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
		}else{
			objStatus+=false;
			String objDetail="Promo Bar is not Displayed on Authentication page";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
		UIFoundation.waitFor(1L);
		objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtFirstName, "firstName"));
		objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtLastName, "lastName"));
		objStatus += String.valueOf(UIFoundation.setObjectCreateAccount(PickedPage.txtEmail,"email"));
		objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtPassword, "accPassword"));
		UIFoundation.waitFor(4L);
		objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.btnCreateAccount));
		UIFoundation.waitFor(5L);
		if(UIFoundation.isDisplayed(PickedPage.txtRecipientPge)) 
		{
			objStatus+=true;
			String objDetail="User is navigated to Receipient page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Receipient page";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
		if(UIFoundation.isDisplayed(PickedPage.spnPromoBar)) 
		{
			String strPromoBar=UIFoundation.getText(PickedPage.spnPromoBar);
			System.out.println("Promo bar RcpntPage:-"+strPromoBar);
			int promoindex=strPromoBar.indexOf("Picked");
			if(promoindex>=0)
			{
				objStatus+=true;
				String objDetail="Promo Bar is Displayed on Receipient page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}
			else{
				objStatus+=false;
				String objDetail="Promo Bar is not Displayed on Receipient page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}}
		else
		{
			objStatus+=false;
			String objDetail="Promo Bar is not Displayed on Receipient page";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
		}
		if (objStatus.contains("false"))
		{				
			String objDetail="ValidateCustomStylingforDefaultModel  test case failed ";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			return "Fail";
		}
		else
		{
			String objDetail="ValidateCustomStylingforDefaultModel test case executed succesfully";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			return "Pass";
		}
	}catch(Exception e)
	{
		log.error("there is an exception arised during the execution of the method"+ e);
		return "Fail";
	}
}
}
