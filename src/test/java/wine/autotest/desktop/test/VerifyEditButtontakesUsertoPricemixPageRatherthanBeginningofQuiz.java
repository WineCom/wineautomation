package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyEditButtontakesUsertoPricemixPageRatherthanBeginningofQuiz extends Desktop {
	/***********************************************************************************************************
	 * Method Name : Enrolltopickedandverifyeditbutton()() 
	 * Created By  : Chandrashekar K B
	 * Purpose     : 
	 * 
	 ************************************************************************************************************
	 */
	public static String Enrolltopickedandverifyeditbutton() {
		String objStatus = null;
		String screenshotName = UIFoundation.randomNumber(9)+ "_Scenarios_Enrolltopickedandverifyeditbutton_Screenshot.jpeg";
		try {
		objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.imgpicked));
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.imgpicked));
		UIFoundation.waitFor(5L);
		if(UIFoundation.isDisplayed(PickedPage.cmdSignUpCompass)) 
		{
			objStatus+=true;
			String objDetail="User is navigated to Picked offer page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Picked offer page";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdSignUpCompass));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkNoVoice));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkRedOnly));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNtVry));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSelQtyNxt));
		UIFoundation.waitFor(3L);
		if(UIFoundation.isDisplayed(PickedPage.txtRecipientHeader)) 
		{
			objStatus+=true;
			String objDetail="User is navigated to Recipient page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Recipient page";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkEditsummary));
		UIFoundation.waitFor(3L);
		if(UIFoundation.isDisplayed(PickedPage.txtVerifyQtyPickedPage)) 
		{
			objStatus+=true;
			String objDetail="User is navigated to price-mix widget page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is Not navigated to price-mix widget page";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
		if (objStatus.contains("false"))
		{				
			String objDetail="VerifyEditButtontakesUsertoPricemixPageRatherthanBeginningofQuiz  test case failed ";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			return "Fail";
		}else{
			String objDetail="VerifyEditButtontakesUsertoPricemixPageRatherthanBeginningofQuiz test case executed succesfully";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			return "Pass";
		}	}
	catch(Exception e)
	{
		log.error("there is an exception arised during the execution of the method"+ e);
		return "Fail";
	}	
}}	
