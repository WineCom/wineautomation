package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class ValidateStewardshipLifeTimeSavings extends Desktop {
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekar K B  
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	 
	public static String login()
	{
		String objStatus=null;
		   String screenshotName = UIFoundation.randomNumber(9)+ "_Scenarios_keepMeSignedIn.jpeg";
	try
		{
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "Stewardusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.webDriverWaitForElement(LoginPage.btnSignIn, "Invisible", "", 50);
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			objStatus+=String.valueOf(UIBusinessFlows.isObjectExistForSignIn());
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				UIFoundation.webDriverWaitForElement( LoginPage.QAUserPopUp, "Invisible", "", 50);
			}			
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				String objDetail="Login test case is failed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{			
			String objDetail="Login test case is executed successfully";
			System.out.println(objDetail);
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";			
			}			
		}catch(Exception e)
		{		
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";	
		}
	}	
	/***********************************************************************************************************
	 * Method Name : Verifysavingsgreaterthan49$andcolor()() 
	 * Created By  : Chandrashekar K B
	 * Purpose     : 
	 * 
	 ************************************************************************************************************
	 */
	public static String Verifysavingsgreaterthan49$andcolor() {
		String objStatus = null;
		String screenshotName = UIFoundation.randomNumber(9)+ "_Scenarios_Verifysavingsgreaterthan49$andcolor_Screenshot.jpeg";
		try {
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkStewardshipSetting));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnstewardshipHeader)) 
			{
				objStatus+=true;
				String objDetail="User is navigated to Stewardship Account  page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Stewardship Account  page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.txtstewardslifesavings));
			String strLifeSavingsact=UIFoundation.getText(UserProfilePage.txtstewardslifesavings);			
			String strColorexp=UIFoundation.getColor(UserProfilePage.txtstewardslifesavings);
			System.out.println("strColorexp:-"+strColorexp);
			String strcoloract="#388604";	
			strLifeSavingsact=strLifeSavingsact.replace("$","");
			if(strLifeSavingsact.contains(","))
			{
				strLifeSavingsact=strLifeSavingsact.replace(",","");	
			}
			 if(strLifeSavingsact.contains("."))
			 {
				 String[] parts = strLifeSavingsact.split("[.]",0);
				 strLifeSavingsact = parts[0]; 
				 }			 		 
			int intLifeSavingsAct=Integer.parseInt(strLifeSavingsact),intLifeSavingsExp=49;
			 if (intLifeSavingsAct>=intLifeSavingsExp)
				{				
					objStatus+=true;
					String objDetail="The Lifetime savings is Greater than or equal to 49$:- "+strLifeSavingsact;
					System.out.println(objDetail);
					getlogger().pass(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					 if (strcoloract.equalsIgnoreCase(strColorexp))
						{					
							objStatus+=true;
						    objDetail="The text color is Green:"+strColorexp;
							System.out.println(objDetail);
							getlogger().pass(objDetail);
							ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
						}
						else
						{	
							objStatus+=false;
							objDetail="The text color is Not Green";
							System.out.println(objDetail);
							UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
							getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());	
						}
				}
				else
				{	
					objStatus+=false;
					String objDetail="The Lifetime savings is Not Greater than or equal to 49$";
					System.out.println(objDetail);
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());				
				}
			if (objStatus.contains("false"))
			{				
				String objDetail="ValidateStewardshipLifeTimeSavings  test case failed ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{
				String objDetail="ValidateStewardshipLifeTimeSavings test case executed succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}	
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}	
}	