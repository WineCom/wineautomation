package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.order.pages.ListPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.test.Desktop;

public class VerifySpiritsMenuDispinWineHomepage extends Desktop{
	/***************************************************************************
	 * Method Name			: VerifySpiritsSection()
	 * Created By			: ChandraShekhar KB
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String VerifySpiritsSection()
	{
	String objStatus=null;			
	String screenshotName = "Scenarios_Verifyspiritsmenu_Screenshot.jpeg";
	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;	
	try
	{			
		getlogger().info("The execution of the method verifySubscriptions started here ...");
		objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
		UIFoundation.waitFor(2L);
		if (UIFoundation.isDisplayed(ListPage.lnkSpirits)){
			objStatus+=true;				
			String objDetail = "Spirits Menu is Displayed in Wine Homepage";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Spirits Menu is Not Displayed in Wine Homepage";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkWhiskey));
		UIFoundation.waitFor(4L);	
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
		UIFoundation.waitFor(4L);
		if (UIFoundation.isDisplayed(ListPage.txtDistillerNotes)){
			objStatus+=true;				
			String objDetail = "User is Navigated to PIP page and 'Distiller Note' is displayed";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="User is Navigated to PIP page and 'Distiller Note' is Not displayed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		if (objStatus.contains("false")){
			objStatus+=false;			
	    	String objDetail="VerifySpiritsMenuDispinWineHomepage Test case Failed";
	    	getlogger().fail(objDetail);  	
			return "Fail";
			} else{
			    objStatus+=true;				
				String objDetail = "VerifySpiritsMenuDispinWineHomepage Test case is passed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
				return "Pass";
			}
	}catch(Exception e)
	{
		objStatus+=false;
    	String objDetail="VerifySpiritsMenuDispinWineHomepage Test case Failed";
    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
    	getlogger().error("there is an exception arised during the execution of the method VerifySpiritsSection "+ e);
		return "Fail";	
	}
}}