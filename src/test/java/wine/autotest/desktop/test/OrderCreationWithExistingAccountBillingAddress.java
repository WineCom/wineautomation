package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.ThankYouPage;

public class OrderCreationWithExistingAccountBillingAddress extends Desktop{
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			getlogger().info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "TC01_Orderusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			getlogger().info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	

	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String checkoutProcess() {
	String objStatus=null;

	String subTotal = null;
	String shippingAndHandling = null;
	String total = null;
	String salesTax = null;
	String orderNum = null;
	String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";

		try {
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			// driver.navigate().refresh();
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIBusinessFlows.recipientEdit());
			

			if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtCVVR));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(5L);
				WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				
				}

			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(ThankYouPage.lnkOrderNumber, "Clickable", "Thanks!", 70);
			UIFoundation.waitFor(10L);
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order is placed successfully";
				UIFoundation.getOrderNumber(orderNum);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				getlogger().pass(objDetail);
				System.out.println("Order is placed successfully: " + orderNum);
			} else {
				objStatus += false;
				String objDetail = "Order is null.Order not placed successfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
System.out.println(objStatus);
			log.info("The execution of the method checkoutProcess ended here ...");
			if ( objStatus.contains("false")) {
			
				return "Fail";
			} else {
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method checkoutProcess "
					+ e);
			getlogger().error("Exception : shippingDetails menthod failed"+e);
			return "Fail";
		}
	}
	
	

}
