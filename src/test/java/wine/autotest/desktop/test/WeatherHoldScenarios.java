package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;

public class WeatherHoldScenarios extends Desktop{
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String loginOtherState(String State)
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(8L);
			UIFoundation.SelectObject(LoginPage.dwnSelectState, State);
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "weatherusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Ramesh S
	 * Purpose				: 
	 * @throws IOException 
	 ***********************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		try {			
			if(UIFoundation.isDisplayed(ListPage.lnkQAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkQAUserPopUp));
				//UIFoundation.webDriverWaitForElement(ListPage.lnkQAUserPopUp, "Invisible", "", 10);
				}
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkPinotNoir));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.lnkSortOptions,"giftSortHtoL"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(1));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false")) {
				return "Fail";
				} else {
				return "Pass";
				}
		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: VerifyWeatherHoldforGreater300()
	 * Created By			: Ramesh S 
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String VerifyWeatherHoldforGreater300()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		int Tamt;
		   String screenshotName = "Scenarios__WhetherHoldforGreater300.jpeg";
		try
		{
			
			log.info("The execution of the method WhetherHoldforGreater300 started here ...");
	
			System.out.println("============Order summary in CART page===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(2L);
			System.out.println("Tamt :"+totalBeforeTax.substring(1));
			String amount = totalBeforeTax.substring(1);
			String amt[] = amount.split(",");
			amount = amt[0]+amt[1];
			System.out.println("amount :"+amount.substring(0,3));
			Tamt = Integer.parseInt(amount.substring(0,3));
			if(Tamt>300){
				objStatus+=true;
				String objDetail = "The Total Amount is greater than $300";
				getlogger().pass(objDetail);
				System.out.println(objDetail);				
			}else {
				objStatus += false;
				String objDetail = "The Total Amount is not greater than $300";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());		
				}
				
			if(UIFoundation.isDisplayed(CartPage.btnCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}
			UIFoundation.waitFor(5L);

			objStatus+=String.valueOf(UIBusinessFlows.recipientEditDifferentState("StateCO","ZipCO"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.rdokeepWeatherHold));
			if(UIFoundation.isSelected(FinalReviewPage.rdokeepWeatherHold)) {
				objStatus+=true;
				String objDetail = "Hold until weather improves is present in Page";
				getlogger().pass(objDetail);
				System.out.println(objDetail);
			}else {
				objStatus += false;
				String objDetail = "Hold until weather improves is not present in Page";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());		
				}
			System.out.println(objStatus);
			log.info("The execution of the method WhetherHoldforGreater300 ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Hold until weather improves test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("WhetherHoldforGreater300 test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method WhetherHoldforGreater300 "+ e);
			return "Fail";
			
		}
	}

}
