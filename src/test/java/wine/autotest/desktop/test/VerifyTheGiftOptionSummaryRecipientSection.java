package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;


public class VerifyTheGiftOptionSummaryRecipientSection extends Desktop {
	
	/***************************************************************************
	 * Method Name : verifyTheGiftOptionSummaryRecipientSection() 
	 * Created By : Vishwanath
	 * Chavan Reviewed By : Ramesh,KB 
	 * Purpose : 
	 * TM-1349,1350
	 ****************************************************************************
	 */

	public static String verifyTheGiftOptionSummaryRecipientSection() {
		String objStatus = null;
		   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
			

		try {
			log.info("The execution of the method editRecipientPlaceORder started here ...");
			
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(13L);
			/*objStatus += String.valueOf(UIFoundation.clickObject(driver, "SuggestedAddress"));
			UIFoundation.waitFor(3L);*/
//			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
//				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 50);
//			}
//			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkShipRecipientEdit));
			if(UIFoundation.isSelected(FinalReviewPage.chkRecipientGiftSel)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.RecipientGiftCheckboxLabel));
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnRecipientContinue);
			objStatus += String.valueOf(UIFoundation.javaSriptClick(FinalReviewPage.btnRecipientContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Clickable", "", 20);
			System.out.println(objStatus);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				System.out.println(FinalReviewPage.lnkChangeAddress);
			}
		/*	if(UIFoundation.isDisplayed(FinalReviewPage.lnkNogiftselected))
			{
				  objStatus+=true;
			      String objDetail="Gift option summary is displayed in the Collapsed Recipient section as Add a gift message";
			      getlogger().pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      
			}
			else
			{
				   objStatus+=false;
			       String objDetail="Gift option summary is not displayed in the Collapsed Recipient section as Add a gift message";
			       getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());

			}*/
//			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
			//UIFoundation.scrollDownOrUpToParticularElement(driver, "RecipientGiftCheckbox");
			UIFoundation.waitFor(2L); 
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.RecipientGiftCheckboxLabel));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.spnRecipientGiftAdd));
			//objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnRecipientContinue);
			objStatus += String.valueOf(UIFoundation.javaSriptClick(FinalReviewPage.btnRecipientContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnRecipientContinue, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkNogiftselected))
			{
				  objStatus+=true;
			      String objDetail="Verified the gift option summary in collapsed state of Recipient section when only gift wrap is added without gift message";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      getlogger().pass(objDetail);
			      System.out.println("Verified the gift option summary in collapsed state of Recipient section when only gift wrap is added without gift message");
			}
			else
			{
				   objStatus+=false;
			       String objDetail="Verify the gift option summary in collapsed state of Recipient section when only gift wrap is added without gift message is failed";
			       getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			//objStatus += String.valueOf(UIFoundation.clickObject(driver, "RecipientGiftCheckboxLabel"));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtgiftMessageTextArea);
			UIFoundation.waitFor(2L); 
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtgiftMessageTextArea, "giftMessage"));
			UIFoundation.waitFor(2L);
			UIFoundation.clckObject(FinalReviewPage.txtReceipientEmail);
			String expectedGiftMessageText=UIFoundation.getAttribute(FinalReviewPage.txtgiftMessageTextArea);
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnRecipientContinue);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaSriptClick(FinalReviewPage.btnRecipientContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnRecipientContinue, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(10L);
			}

			String actualGiftMessage=UIFoundation.getText(FinalReviewPage.spngiftMessageText);
			if(UIFoundation.isDisplayed(FinalReviewPage.spngiftMessageSummary) && expectedGiftMessageText.equalsIgnoreCase(actualGiftMessage))
			{
				  objStatus+=true;
			      String objDetail="Verified the gift option summary in collapsed state of Recipient section when gift wrap and gift message are added to the order";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      getlogger().pass(objDetail);
			      System.out.println("Verified the gift option summary in collapsed state of Recipient section when gift wrap and gift message are added to the order");
			}
			else
			{
				   objStatus+=false;
			       String objDetail="Verify gift option summary in collapsed state of Recipient section when gift wrap and gift message are added to the order is failed";
			       getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot(screenshotpath+"giftMessage", objDetail);
			}
			System.out.println(objStatus);
			log.info("The execution of the method editRecipientPlaceORder ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method editRecipientPlaceORder "
					+ e);
			return "Fail";
		}
	}
	

}
