package wine.autotest.desktop.test;

import java.io.IOException;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;


public class VerifyAddToCartButtonFunctionlityForDryState extends Desktop {
	
	/***************************************************************************
	 * Method Name : orderSummaryForNewUser() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String verifyAddToCartButtonFunctionality() {
		
		String objStatus = null;

		try {
			
			UIFoundation.SelectObject(LoginPage.dwnSelectState, "dryState");
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkvarietal));
			UIFoundation.waitFor(2L);
/*			objStatus += String.valueOf(UIFoundation.clickObject(driver, "BordexBlends"));
			UIFoundation.waitFor(2L);*/
			System.out.println("=======================verify the  �Add To Cart� button is not displayed when dry state is selected  =====================");
			if (UIFoundation.getText(ListPage.btnAddToCartFirst).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for first product");
				

			}

			if (UIFoundation.getText(ListPage.btnAddToCartSecond).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for second product");
				

			}

			if (UIFoundation.getText(ListPage.btnAddToCartThird).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for third product");
				

			}

			if (UIFoundation.getText(ListPage.btnAddToCartFourth).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for fourth product");
				

			}

			if (UIFoundation.getText(ListPage.btnAddToCartFifth).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for fifth product");
				

			}
			if (objStatus.contains("false")) {
				getlogger().fail("verifyAddToCartButtonFunctionality test failed");
				return "Fail";
			} else {
				getlogger().pass("verifyAddToCartButtonFunctionality test Passed");
				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";
		}
	}

}
