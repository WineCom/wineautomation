package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.By;
import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.order.pages.ListPage;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyWarningMsgForRemoveShipTodayProductFromCart extends Desktop {
	
	/***************************************************************************
	 * Method Name : addShipTodayProdTocrt() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addShipTodayProdTocrt() {
		String objStatus = null;
	 	String expectedWarningMsg = null;
		String actualWarningMsg = null;
		   String screenshotName = "Scenarios__RemoveShipScreenshot.jpeg";
			
		try {
			
			getDriver().navigate().to("https://qwww.wine.com/search/Chasseur%20Syrah%202013/0");
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(1));
			UIFoundation.waitFor(2L);	
			UIFoundation.scrollUp();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkPinotNoir));
			//objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(driver, "ShipsSoonestR"));
			//objStatus+=String.valueOf(UIFoundation.clickObject(driver, "ShipsSoonestR"));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(3));
			UIFoundation.waitFor(2L);	
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			System.out.println("======================Warning message before removing Ship Today products from the cart============================");
			expectedWarningMsg = verifyexpectedresult.shiptodayandlistproducts;
			actualWarningMsg = UIFoundation.getText(CartPage.spnShipTodayWarningMsg);
			if(actualWarningMsg.contains(expectedWarningMsg))
			{
				objStatus+=true;
				System.out.println(actualWarningMsg);
				getlogger().pass(actualWarningMsg);
			}
			else
			{
				objStatus +=false;
				String objDetail="does not match with Ship today warning message";
				UIFoundation.captureScreenShot(screenshotpath+"_ChoosePhoto", objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.err.println(actualWarningMsg);
			}
			int productCount = Integer.parseInt(UIFoundation.getText(ListPage.btnCartCount));
			for (int i = productCount; i >=1; i--) {
				String 	locator="//section[@class='cartContents']/ul/li[" + i + "]/div[2]/div[3]/div[3]/div[4]/span[contains(text(),' extra processing day(s) required')]";
				if(!UIFoundation.verifyAbsentElement(getDriver(), locator).contains("Fail"))
				{
					UIFoundation.waitFor(2L);
					getDriver().findElement(By.xpath("(//span[@class='iconRoundedOutline'])[" + i + "]")).click();
					UIFoundation.waitFor(2L);
					objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.btnRemove));
					UIFoundation.scrollUp();
					UIFoundation.waitFor(3L);
				}
			}
			System.out.println("======================Warning message after removing Ship Today products from the cart============================");
			actualWarningMsg = UIFoundation.getText(CartPage.spnShipTodayWarningMsg1);
			System.out.println(actualWarningMsg);
			if (objStatus.contains("false")) {
	
				return "Fail";
			} else {
	
				return "Pass";
			}
	
		} catch (Exception e) {
	
			return "Fail";
		}
	}
}