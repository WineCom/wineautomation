package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyShippingCostAppliedToOrderSummary extends Desktop {
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "pickFormUser"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
		
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}

	
	/***************************************************************************
	 * Method Name : VerifyShippingCostAppliedToOrderSummary
	 * Created By : Vishwanath Chavan 
	 * Reviewed By : Ramesh,KB 
	 * Purpose : TM-4189
	 ****************************************************************************
	 */
	
public static String shippingCostAppliedToOrderSummary() {

String objStatus = null;
String shippingAndHandling;
String screenshotName = "shippingCostAppliedToOrderSummary.jpeg";

try {
	log.info("shipping Cost Applied To Order Summary metho started here...");
		
			  objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			  UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			  objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
			 
	System.out.println("============Order summary with shipping And Handling ===============");
	 shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
	System.out.println("Shipping & Handling:   "+shippingAndHandling);
	if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
		//objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
		UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
	}
//-------------------------------------------------StandardShip---------------------------------------
	   UIFoundation.waitFor(2L);
	   objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeDate));
	   UIFoundation.waitFor(1L);
	   objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
	   objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkNewStandardShip));
	   UIFoundation.waitFor(3L);
	   String Standard = UIFoundation.getText(FinalReviewPage.spnshippingmethodStandardtext);
	   String StandardShipPrice = (Standard.substring(19));
	   System.out.println("Standard Shipping method total is "+StandardShipPrice);
	   objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
	   UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
	   shippingAndHandling =UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
		System.out.println("Standard  Shipping & Handling:   "+shippingAndHandling);
		if(StandardShipPrice.equals(shippingAndHandling)) {
			objStatus += true;
			String objDetail = "Standard Shipping total is added to order summary";
			getlogger().pass(objDetail);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		} else {
			objStatus += false;
			String objDetail = "Standard Shipping total is not  added to order summary";
			ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
	
		//-------------------------------------------------2day---------------------------------------	
	 UIFoundation.waitFor(2L);
	   objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeDate));
	   UIFoundation.waitFor(1L);
	   objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
	   objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelShipMethodOption2Day));
	   UIFoundation.waitFor(3L);
	   String twoday = UIFoundation.getText(FinalReviewPage.spnshippingmethod2daytext);
	   String twodayShipPrice = (twoday.substring(7));
	   System.out.println("2-Day Shipping method total is "+twodayShipPrice);
	   objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
	   UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
	   shippingAndHandling =UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
		System.out.println("2 Day  Shipping & Handling:   "+shippingAndHandling);
		if(twodayShipPrice.equals(shippingAndHandling)) {
			objStatus += true;
			String objDetail = "2 Day Shipping total is added to order summary";
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		} else {
			objStatus += false;
			String objDetail = "2 Day Shipping total is not  added to order summary";
			ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}

		//-------------------------------------------------Overnight PM---------------------------------------
	objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeDate));
	UIFoundation.waitFor(4L);
	objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
	objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelShipMethodOverNightPM));
	UIFoundation.waitFor(9L);
	String overnightpm = UIFoundation.getText(FinalReviewPage.spnshippingmethodPmtext);
	String overNgtpmShipPrice = (overnightpm.substring(16));
	System.out.println("Over Night PM Shipping method total is "+overNgtpmShipPrice);
	objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
	UIFoundation.waitFor(4L);
	shippingAndHandling =UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
	UIFoundation.waitFor(2L);
	System.out.println("PM Shipping & Handling:   "+shippingAndHandling);
	if(overNgtpmShipPrice.equals(shippingAndHandling)) {
		objStatus += true;
		String objDetail = "Over Night PM Shipping total is added to order summary";
		getlogger().pass(objDetail);
		System.out.println("Shipping & Handling:   "+shippingAndHandling);
		ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
	} else {
		objStatus += false;
		String objDetail = "Over Night PM Shipping total is not  added to order summary";
		ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
		UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
	}
	
	//-------------------------------------------------Overnight AM---------------------------------------	
   objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeDate));
   UIFoundation.waitFor(1L);
   objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
   objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelShipMethodOverNightAM));
   UIFoundation.waitFor(3L);
   String overnightam = UIFoundation.getText(FinalReviewPage.spnshippingmethodAmtext);
   String overNightamShipPrice = (overnightam.substring(16));
   System.out.println("Over Night AM Ship Price:-"+overNightamShipPrice);
   objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
   UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
   shippingAndHandling =UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
	System.out.println("AM Shipping & Handling:   "+shippingAndHandling);
	if(overNightamShipPrice.equals(shippingAndHandling)) {
		objStatus += true;
		String objDetail = "Over Nigt AM Shipping total is added to order summary";
		getlogger().pass(objDetail);
		System.out.println("Shipping & Handling:   "+shippingAndHandling);
		ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
	} else {
		objStatus += false;
		String objDetail = "Over Nigt AM Shipping total is not  added to order summary";
		ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
		UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
	}
	System.out.println("shippingCostAppliedToOrderSummary : "+objStatus);
	log.info("The execution of the method verifyGiftWrapping ended here ...");
	if (objStatus.contains("false")) {
		getlogger().fail("VerifyShippingCostAppliedToOrderSummary Test case is Failed");
		return "Fail";
	} else {
		getlogger().pass("VerifyShippingCostAppliedToOrderSummary Test case is Passed");
		return "Pass";
	}

} catch (Exception e) {

	log.error("there is an exception arised during the execution of the method verifyShippingCostAppliedToOrderSummary " + e);
	return "Fail";
}

}
}

