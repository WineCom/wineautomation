package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;


public class VerifyLimitPerCustomerForPreSaleProduct extends Desktop {
	

	
	/***************************************************************************
	 * Method Name : addPreSaleProdTocrt()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose :
	 * Jira Id  :   TM-5013
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addPreSaleProdTocrt() {
		String objStatus = null;
		 String screenshotName = "Scenarios_addPreSaleProdTocrt.jpeg";
		try {	
			log.info("The execution of the method ValidatePreSaleBlurbDisplayedIn started here ...");
			

			if(UIFoundation.isDisplayed(ListPage.lnkQAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkQAUserPopUp));
				//UIFoundation.webDriverWaitForElement(ListPage.lnkQAUserPopUp, "Invisible", "", 10);
				}
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkFeatured));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkBordexFeatures));
			UIFoundation.waitFor(5L);						
	//		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkaddProductToPIP));				
			UIFoundation.scrollDown();
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(CartPage.txtLimitCount));
			UIFoundation.waitFor(5L);	
			WebElement ele=getDriver().findElement(By.xpath("(//div[@class='prodItemInfo_stock']//div//div[@class='prodItemLimit']/span[@class='prodItemLimit_count'])[1]"));
			ele.getText();			
			UIFoundation.waitFor(2L);					
			objStatus+=String.valueOf(UIBusinessFlows.addPipLimitProduct());		
			
			UIFoundation.waitFor(3L);		
			if (UIFoundation.isDisplayed(CartPage.txtPreSale) && UIFoundation.isDisplayed(CartPage.txtSoldIncrement)) {
				objStatus+=true;
				String objDetail="Pre-sale and Sold increment text is Displayed in PIP Page";
                getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}
			else
			{	
				objStatus+=false;
				String objDetail="Pre-sale and Sold increment text not  Displayed in PIP Page";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgUserRating));	
			UIFoundation.waitFor(1L);		
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));					
			UIFoundation.waitFor(2L);	
			if (UIFoundation.isDisplayed(CartPage.txtPreSale) && UIFoundation.isDisplayed(CartPage.txtSoldIncrement)) {
				objStatus+=true;
				String objDetail="Pre-sale and Sold increment text Displayed in Mywine Page";
                getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}
			else
			{	
				objStatus+=false;
				String objDetail="Pre-sale and Sold increment text not Displayed in Mywine Page";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstProdNameLink));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.imgClearStarRating));			
			UIFoundation.waitFor(3L);	
			log.info("The execution of the method ValidatePreSaleBlurbDisplayedIn ended here ...");
			
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

}
