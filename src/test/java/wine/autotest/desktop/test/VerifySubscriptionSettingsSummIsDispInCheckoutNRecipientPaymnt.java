package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;

public class VerifySubscriptionSettingsSummIsDispInCheckoutNRecipientPaymnt extends Desktop {


	/*********************************************************************************************
	 * Method Name : verifySubscriptionSettingsInChckout() 
	 * Created By  : Rakesh C S
	 * Reviewed By : Chandrashekar 
	 * Purpose     : Verifies Subscription Settings is displayed in Recipient & Payment option
	 ********************************************************************************************
	 */

public static String verifySubscriptionSettingsInChckout() {
	String objStatus=null;
	String screenshotName = "Scenarios_verifySubscriptionSettingsInChckout_Screenshot.jpeg";
	try {
		log.info("The execution of the verifySubscriptionSettingsInChckout method strated here");
		if(UIFoundation.isDisplayed(PickedPage.txtNewSubscriptionSettingsHeader)) {
			objStatus+=true;
			String objDetail="Subscription Settings' summary is  displayed checkout flow";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Subscription Settings' summary is not displayed checkout flow";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.lnkNewSubscriptionEditOpt)) {
			objStatus+=true;
			String objDetail="Subscription settings edit is   displayed";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Subscription settings edit is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtNewTargetPrice)) {
			objStatus+=true;
			String objDetail="Target price is displayed";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Target price is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtNewWineTypes)) {
			objStatus+=true;
			String objDetail="Wine type selected is displayed";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Wine type selected is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtNewFrequency)) {
			objStatus+=true;
			String objDetail="Frequency is  displayed";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Frequency is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtNewFreeShipping)) {
			objStatus+=true;
			String objDetail="Free Shipping is displayed";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Free Shipping is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.rdoShipToHome));
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
		objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
		objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
		objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
		objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
		UIFoundation.clickObject(FinalReviewPage.dwnState);
		objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipCode"));
		objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRecipientShipCont));
		if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
		UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 70);		
		}
		UIFoundation.waitFor(8L);
		if(UIFoundation.isDisplayed(PickedPage.txtNewSubscriptionSettingsHeader)) {
			objStatus+=true;
			String objDetail="Subscription Settings' summary is  displayed checkout flow";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Subscription Settings' summary is not displayed checkout flow";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.lnkNewSubscriptionEditOpt)) {
			objStatus+=true;
			String objDetail="Subscription settings edit is   displayed";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Subscription settings edit is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtNewTargetPrice)) {
			objStatus+=true;
			String objDetail="Target price is displayed";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Target price is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtNewWineTypes)) {
			objStatus+=true;
			String objDetail="Wine type selected is displayed";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Wine type selected is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtNewFrequency)) {
			objStatus+=true;
			String objDetail="Frequency is  displayed ";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Frequency is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtNewFreeShipping)) {
			objStatus+=true;
			String objDetail="Free Shipping is displayed";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Free Shipping is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
		{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtFedexBillingAddrss)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingAddrss, "Address1"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingCity, "City"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnFedexBillingState, "State"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingZip, "ZipCode"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
		}
		UIFoundation.scrollDownOrUpToParticularElement(PickedPage.btnPaymentContinue);
		UIFoundation.waitFor(1L);
		objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.btnPaymentContinue));
		UIFoundation.waitFor(8L);
		if(UIFoundation.isDisplayed(PickedPage.txtNewSubscriptionSettingsHeader)) {
			objStatus+=true;
			String objDetail="Subscription Settings' summary is  displayed checkout flow";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Subscription Settings' summary is not displayed checkout flow";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.lnkNewSubscriptionEditOpt)) {
			objStatus+=true;
			String objDetail="Subscription settings edit is displayed";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Subscription settings edit is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtNewTargetPrice)) {
			objStatus+=true;
			String objDetail="Target price is displayed";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Target price is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtNewWineTypes)) {
			objStatus+=true;
			String objDetail="Wine type selected is displayed";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Wine type selected is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtNewFrequency)) {
			objStatus+=true;
			String objDetail="Frequency is  displayed";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Frequency is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtNewFreeShipping)) {
			objStatus+=true;
			String objDetail="Free Shipping is displayed";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Free Shipping is not displayed";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}	
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkAcceptTermsandConditions));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnEnrollInPicked));
		UIFoundation.webDriverWaitForElement(PickedPage.txtHeadlinePicked, "element", "", 50);
		UIFoundation.waitFor(8L);
		if(UIFoundation.isDisplayed(PickedPage.txtHeadlinePicked)){
			objStatus+=true;
			String objDetail="User is successfully enrolled to compass with Home address and Glad to meet you screen is displayed.";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not successfully enrolled with Home address to compass and Glad to meet you screen not displayed";
			 getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/*
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCodeFedEx"));
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
		UIFoundation.waitFor(10L);
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSave));
		UIFoundation.waitFor(15L);
		if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
		{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtFedexBillingAddrss)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingAddrss, "Address1"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingCity, "BillingCity"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnFedexBillingState, "BillingState"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingZip, "BillingZipcode"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
		}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPaymentContinue));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(PickedPage.lnkSubscriptionEditOpt)) {
				objStatus+=true;
				String objDetail="Subscription settings edit is   displayed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Subscription settings edit is not displayed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(UIFoundation.isDisplayed(PickedPage.txtTargetPrice)) {
				objStatus+=true;
				String objDetail="Target price is displayed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Target price is not displayed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(UIFoundation.isDisplayed(PickedPage.txtWineTypes)) {
				objStatus+=true;
				String objDetail="Wine type selected is displayed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Wine type selected is not displayed";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}*/
			System.out.println(objStatus);
			
	log.info("The execution of the method verifySubscriptionSettingsInChckout ended here ...");
	if (objStatus.contains("false"))
	{
		String objDetail="'VerifySubscriptionSettingsSummIsDispInChckoutNRecipientPaymnt' Test case Failed ";
		getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
		System.out.println("Verified the UI for 'Subscription Settings' in Recipient & Payment section is not displayed ");
		return "Fail";
	}
	else
	{
		String objDetail="'VerifySubscriptionSettingsSummIsDispInChckoutNRecipientPaymnt' Test case executed succesfully";
		getlogger().pass(objDetail);
		ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		System.out.println("Verified the UI for 'Subscription Settings' in Recipient & Payment sectionis  displayed");
		return "Pass";
	}
}catch(Exception e)
{
	log.error("there is an exception arised during the execution of the method"+ e);
	return "Fail";
}
}
}