package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyRedeemedGiftCardDisponPickedSettingsPage extends Desktop {
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekar K B  
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	 
	public static String login()
	{
		String objStatus=null;
		   String screenshotName = UIFoundation.randomNumber(9)+ "_Scenarios_keepMeSignedIn.jpeg";
	try
		{
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "compassuser"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.webDriverWaitForElement(LoginPage.btnSignIn, "Invisible", "", 50);
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			objStatus+=String.valueOf(UIBusinessFlows.isObjectExistForSignIn());
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				UIFoundation.webDriverWaitForElement( LoginPage.QAUserPopUp, "Invisible", "", 50);
			}			
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				String objDetail="Login test case is failed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{			
			String objDetail="Login test case is executed successfully";
			System.out.println(objDetail);
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";			
			}			
		}catch(Exception e)
		{		
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";	
		}
	}
	/***********************************************************************************************************
	 * Method Name : navigatetoPickedandValidateGiftcard()() 
	 * Created By  : Chandrashekar K B
	 * Purpose     : 
	 * 
	 ************************************************************************************************************
	 */
	public static String navigatetoPickedandValidateGiftcard() {
		String objStatus = null;
		String screenshotName = UIFoundation.randomNumber(9)+ "_Scenarios_navigatetoPickedandValidateGiftcard_Screenshot.jpeg";
		try {
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetting));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(PickedPage.txtGiftremain1));
			  if(!UIFoundation.isDisplayed(PickedPage.txtGiftremain1)){
				String giftCert1=UIFoundation.giftCertificateNumber();
				XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
				String giftCert2=UIFoundation.giftCertificateNumber();
				XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate2", 1, giftCert2);
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkGift));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkGiftApply));
				UIFoundation.waitFor(6L);	
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.txtGiftNumber));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtGiftNumber, "GiftCertificate2"));
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkGiftApply));
				UIFoundation.waitFor(6L);
			  }		
			if(UIFoundation.isDisplayed(PickedPage.txtGiftremain1)){
					String strGift1remain=String.valueOf(UIFoundation.getText(PickedPage.txtGiftremain1));
					System.out.println("First Giftcard Remaining:-"+strGift1remain);
					strGift1remain=strGift1remain.replace("$","");
					if(strGift1remain.contains("."))
					 {
						 String[] parts = strGift1remain.split("[.]",0);
						 strGift1remain = parts[0]; 
						 }	
					int intGift1remain=Integer.parseInt(strGift1remain);
					if(intGift1remain>0)
						{
						objStatus+=true;
						String objDetail="First Gift Card Remaining Value is Greater than 0";
						getlogger().pass(objDetail);
						ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
						}else{
						objStatus+=false;
					String objDetail="First Gift Card Remaining Value is Not Greater than 0";
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
							}
			}
				if(UIFoundation.isDisplayed(PickedPage.txtGiftremain2)){
					String strGift2remain=String.valueOf(UIFoundation.getText(PickedPage.txtGiftremain2));
					System.out.println("First Giftcard Remaining:-"+strGift2remain);
					strGift2remain=strGift2remain.replace("$","");
					if(strGift2remain.contains("."))
					 {
						 String[] parts = strGift2remain.split("[.]",0);
						 strGift2remain = parts[0]; 
						 }
					int intGift2remain=Integer.parseInt(strGift2remain);
					System.out.println("Second Giftcard Remaining:-"+intGift2remain);
					if(intGift2remain>0)
					{
						objStatus+=true;
						String objDetail="Second Gift Card Remaining Value is Greater than 0";
						getlogger().pass(objDetail);
						ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				}else{
					objStatus+=false;
					String objDetail="Second Gift Card Remaining Value is Not Greater than 0";
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
					}
				}
			if (objStatus.contains("false"))
			{				
				String objDetail="VerifyRedeemedGiftCardDisponPickedSettingsPage  test case failed ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{
				String objDetail="VerifyRedeemedGiftCardDisponPickedSettingsPage test case executed succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}	
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
}
			