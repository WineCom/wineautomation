package wine.autotest.desktop.test;


import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.pages.PickedPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.test.Desktop;

public class VerifyBackandNextButtonFunctionalityinQuizPages extends Desktop{
	/***************************************************************************
	 * Method Name			: VerifySpiritFilters()
	 * Created By			: ChandraShekhar KB
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String VerifyPrevandNextButtonFunctionality()
	{
	String objStatus=null;			
	String screenshotName = "Scenarios_VerifyCompassprevandnextfunctionality_Screenshot.jpeg";
	try
	{			
		log.info("Execution of the method VerifyPrevandNextButtonFunctionality started here .........");		
		UIFoundation.waitFor(3L);
		UIFoundation.scrollDownOrUpToParticularElement(PickedPage.lnkPickedCompassTiles);
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.lnkPickedCompassTiles));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdSignUpCompass));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkNoVoice));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineJourneyPrev));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.btnSignUpPickedWine)) {
			objStatus+=true;
			String objDetail="User is navigated to previous sign up screen";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to previous sign up screen";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSignUpPickedWine));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineJourneyNxt));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.txtVerifySeconePickedPage)) {
			objStatus+=true;
			String objDetail="User is navigated to Wine journey quiz previous page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Wine journey quiz previous page";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizWineTypRed));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineTypePrev));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.rdoPickedQuizNovice)) {
			objStatus+=true;
			String objDetail="User is navigated to Wine type quiz previous page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Wine type quiz previous page";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineJourneyNxt));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineTypeNxt));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.rdoZinfandelLike)) {
			objStatus+=true;
			String objDetail="User is navigated to varietal type quiz next page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Wine type quiz next page";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePagePrev));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.rdoPickedQuizWineTypRed)) {
			objStatus+=true;
			String objDetail="User is navigated to Wine type quiz previous page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Wine type quiz previous page";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineTypeNxt));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.iconLikdThumsUpFrZinfandel) && (UIFoundation.isDisplayed(PickedPage.iconLikdThumsUpFrMalbec))){
			objStatus+=true;
			String objDetail="User is navigated to Wine type quiz Next page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Wine type quiz Next page";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPersonalSommPrev));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.rdoZinfandelLike)) {
			objStatus+=true;
			String objDetail="User is navigated to varietal quiz previous page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to varietal quiz previous page";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
		UIFoundation.waitFor(4L);	
		if(UIFoundation.isDisplayed(PickedPage.rdoPickedQuizNtVry)) {
			objStatus+=true;
			String objDetail="User is navigated to adventurous quiz next page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to adventurous quiz next page";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNtVry));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypPrev));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.txtPersonalSommPge)) {
			objStatus+=true;
			String objDetail="User is navigated to Personal somm  quiz previous page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Personal somm  quiz previous page";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.btnMnthTypNxt)) {
			objStatus+=true;
			String objDetail="User is navigated to Month select quiz next page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Month select quiz nexr page";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMonthTypPrev));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.rdoPickedQuizNtVry)) {
			objStatus+=true;
			String objDetail="User is navigated to adventorous quiz previous page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to adventorous quiz previous page";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.txtSubscriptionSettingsPge)) {
			objStatus+=true;
			String objDetail="User is navigated to Subscription Settings quiz next page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Subscription Settings quiz next page";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSelQtyPrev));
		UIFoundation.waitFor(4L);
		if(UIFoundation.isDisplayed(PickedPage.txtMonthTypPge)) {
			objStatus+=true;
			String objDetail="User is navigated to Month select  quiz previous page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Month select  quiz previous page";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSelQtyNxt));
		UIFoundation.waitFor(4L);
		if (objStatus.contains("false")){
			objStatus+=false;			
	    	String objDetail="VerifyBackandNextButtonFunctionalityinQuizPages Test case Failed";
	    	getlogger().fail(objDetail);  	
			return "Fail";
			} else{
			    objStatus+=true;				
				String objDetail = "VerifyBackandNextButtonFunctionalityinQuizPages Test case is passed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
				return "Pass";
			}
	}catch(Exception e)
	{
		objStatus+=false;
    	String objDetail="VerifyBackandNextButtonFunctionalityinQuizPages Test case Failed";
    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
    	getlogger().error("there is an exception arised during the execution of the method VerifyPrevandNextButtonFunctionality "+ e);
		return "Fail";	
	}
}}