package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.order.pages.ListPage;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.test.Desktop;

public class VerifySpiritshowsasOutofStockifsetforOutsideofState extends Desktop{
	/***************************************************************************
	 * Method Name			: VerifySpiritsinPIPpage()
	 * Created By			: ChandraShekhar KB
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String VerifySpiritsinPIPpage()
	{
	String objStatus=null;			
	String screenshotName = "Scenarios_VerifySpiritsinPIPpage_Screenshot.jpeg";
	try
	{			
		log.info("The execution of the method VerifySpiritshowsasOutofStockifsetforOutsideofState started here ...");
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSpirits));
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
		UIFoundation.waitFor(2L);
		UIFoundation.SelectObject(LoginPage.dwnSelectState, "dryState");
		UIFoundation.waitFor(10L);
		if (UIFoundation.isDisplayed(ListPage.spncurrentlyUnavailableProd)){
			objStatus+=true;				
			String objDetail = "PIP shows Unavailable For selected State";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="PIP shows Available for selected state";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		UIFoundation.waitFor(2L);
		if (objStatus.contains("false")){
			objStatus+=false;			
			return "Fail";
			} else{
			    objStatus+=true;							
				return "Pass";
			}
	}catch(Exception e)
	{
		objStatus+=false;
    	String objDetail="VerifySpiritsinPIPpage Method Failed";
    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
    	getlogger().error("there is an exception arised during the execution of the method VerifyFiltersinSpirits "+ e);
		return "Fail";	
	}
}
	/***************************************************************************
	 * Method Name			: VerifySpiritsinCartpage()
	 * Created By			: ChandraShekhar KB
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String VerifySpiritsinCartpage()
	{
	String objStatus=null;			
	String screenshotName = "Scenarios_VerifySpiritsinCartpage_Screenshot.jpeg";
	try
	{			
		UIFoundation.SelectObject(LoginPage.dwnSelectState, "State");
		UIFoundation.waitFor(10L);
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSpirits));
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIBusinessFlows.addproductstocart(1));
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
		UIFoundation.waitFor(3L);
		UIFoundation.SelectObject(LoginPage.dwnSelectState, "dryState");
		UIFoundation.waitFor(10L);
		if(UIFoundation.isDisplayed(CartPage.btnShipcontinue)){
			UIFoundation.clickObject(CartPage.btnShipcontinue);
			UIFoundation.waitFor(4L);
		}
		if (UIFoundation.isDisplayed(CartPage.txtCartisEmpty)){
			objStatus+=true;				
			String objDetail = "Product was Removed from the cart";
			getlogger().pass(objDetail);
			} else {
				objStatus+=false;			
				String objDetail="Product was Not Removed from the cart";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    		    	
			}
		UIFoundation.waitFor(2L);
		if (objStatus.contains("false")){
			objStatus+=false;			
	    	String objDetail="VerifySpiritshowsasOutofStockifsetforOutsideofState Test case Failed";
	    	getlogger().fail(objDetail);  	
			return "Fail";
			} else{
			    objStatus+=true;				
				String objDetail = "VerifySpiritshowsasOutofStockifsetforOutsideofState Test case is passed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
				return "Pass";
			}
	}catch(Exception e)
	{
		objStatus+=false;
    	String objDetail="VerifySpiritsinCartpage Method Failed";
    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
    	getlogger().error("there is an exception arised during the execution of the method VerifyFiltersinSpirits "+ e);
		return "Fail";	
	}
}
	}