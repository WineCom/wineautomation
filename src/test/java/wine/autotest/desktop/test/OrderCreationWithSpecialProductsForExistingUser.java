package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;



public class OrderCreationWithSpecialProductsForExistingUser extends Desktop{
	
	/***************************************************************************
	 * Method Name			: specialProducts()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String specialProducts()
	{
		String objStatus=null;

		try
		{
			
			log.info("The execution of method productFilteration started here");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(ListPage.lnkFeatured));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkSummerWinePicks));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkmoreFilters));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.spnsizeAndType));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkGreenWine));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(3));
				UIFoundation.scrollUp();
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method productFilteration "+e);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String checkoutProcess()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String totalBeforeTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		
		try
		{
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary in CART page===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(26L);
			if(UIFoundation.isDisplayed(CartPage.btnexistingCust))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnexistingCust));
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(28L);
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
			UIFoundation.waitFor(0);
			getDriver().navigate().refresh();
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(10L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(10L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtCVVR));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
			}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			
/*			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentCheckoutEdit"));
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentEdit"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver, "PaymentCVV", "CardCvid"));
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentSaveButton"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));*/
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(ThankYouPage.lnkOrderNumber, "Clickable", "Thanks!", 70);
			UIFoundation.waitFor(10L);
			  if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
	            {
	                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
	                  UIFoundation.waitFor(5L);
	                  
	            }
			String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if(orderNum!="Fail")
			{
				System.out.println("Order Number :"+orderNum);
				UIFoundation.getOrderNumber(orderNum);
		    	objStatus+=true;			
			}else
			{
				objStatus+=false;
		    	String objDetail="Order number is null.Order not placed successfully";
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			
			}
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Order creation with Product filteration for existing user  test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Order creation with Product filteration for existing user test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method checkoutProcess of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";
			
		}
	}

}
