package wine.autotest.desktop.test;


import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.UserProfilePage;
import wine.autotest.fw.utilities.UIFoundation;

public class addAddressUsingUserProfileServicesForNewUser extends Desktop {
	
	/***************************************************************************
	 * Method Name			: addAddress()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 ****************************************************************************
	 */

	public static String addAddress()
	{
		String objStatus=null;
		try
		{
			log.info("The execution of the method addAddress started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clckObject(UserProfilePage.lnkAddressBook));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkAddNewAddress));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtAddressFullName, "fullName"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtStreetAddress, "dryAddress"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtCity, "dryCity"));
			objStatus += String.valueOf(UIFoundation.SelectObject(UserProfilePage.dwnState, "dryState"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtRecipientZipCode, "dryZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnAddressSave));
			UIFoundation.waitFor(3L);
			//objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.rdoSuggestedAddress));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnVerifyContinue));
			UIFoundation.waitFor(6L);
			log.info("The execution of the method addAddress ended here ...");	
			if (objStatus.contains("false") && (!UIFoundation.isDisplayed(UserProfilePage.spnAddressBookHeader)))
			{
				
				
				String objDetail = "Adding address using user profile services for new user test case is failed";
				System.out.println(objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				return "Fail";
			}
			else
			{
				System.out.println("Adding address using user profile services for new user test case is executed successfully");
				getlogger().pass("Adding address using user profile services for new user test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method addAddress "+ e);
			return "Fail";
		}
	}

}
