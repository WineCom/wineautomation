package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class ViewAllWineForDifferentStates extends Desktop{
	/***************************************************************************
	 * Method Name			: viewAllWineForDifferentStates()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String viewAllWineForDifferentStates()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_viewAllWineForDifferentStates.jpeg";
			
		try
		{
			
			UIFoundation.waitFor(5L);
			log.info("The execution of the method viewAllWineForDifferentStates started here ...");
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkPinotNoir));
			UIFoundation.waitFor(6L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkaddSecondProductToPIP));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkNewviewAllWine));
			UIFoundation.waitFor(2L);
			String expectedpippageheader=UIFoundation.getText(ListPage.lnkHeaderviewAllWinePIP);
			expectedpippageheader=expectedpippageheader.toLowerCase();
			System.out.println("expectedpippageviewallwineheader:-"+expectedpippageheader);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkNewviewAllWine));
			UIFoundation.waitFor(2L);	
			String actualListPagetitle=getDriver().getTitle();
			actualListPagetitle=actualListPagetitle.toLowerCase();
			System.out.println("actualListPagetitle:-"+actualListPagetitle);
			UIFoundation.waitFor(2L);
			if(actualListPagetitle.contains(expectedpippageheader))
			{
				objStatus+=true;
				String objDetail="User is navigated to the winery list page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				getlogger().pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="User is Not navigated to the winery list page";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"winerpagefail.jpeg")).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkaddSecondProductToPIP));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.SelectObject(LoginPage.dwnSelectState, "AllWineState"));
			UIFoundation.waitFor(12L);
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkNewviewAllWine));
			UIFoundation.waitFor(2L);
			expectedpippageheader=UIFoundation.getText(ListPage.lnkHeaderviewAllWinePIP);
			expectedpippageheader=expectedpippageheader.toLowerCase();
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkNewviewAllWine));
			UIFoundation.waitFor(2L);
			actualListPagetitle=getDriver().getTitle();	
			actualListPagetitle=actualListPagetitle.toLowerCase();
			UIFoundation.waitFor(2L);
			if(actualListPagetitle.contains(expectedpippageheader)){
				objStatus+=true;
				String objDetail="User is navigated to the same list page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				getlogger().pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="User is Notble navigated to the same list page";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"listpagefail.jpeg")).build());
				UIFoundation.captureScreenShot(screenshotpath+"sameListPage", objDetail);
			}
			log.info("The execution of the method viewAllWineForDifferentStates ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method viewAllWineForDifferentStates "+ e);
			return "Fail";
			
		}
	}	

}
