package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ThankYouPage;


public class OrderCreationWithEditFunctionalityInFinalReviewPageForExistingUser extends Desktop {
	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String checkoutProcess() {
	
	String expected=null;
	String actual=null;
	String objStatus=null;
	String subTotal=null;
	String shippingAndHandling=null;
	String total=null;
	String salesTax=null;
	int cartConutBeforeRemovingProduct=0;
	int cartConutAfterRemovingProduct=0;
	String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
	

		try {
			log.info("The execution of the method checkoutProcess started here ...");
			expected =verifyexpectedresult.placeOrderConfirmation;
			UIFoundation.waitFor(4L);
			
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());

			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(10L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(10L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkPaymentEdit);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
			}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(1L);
				}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnCartEdit));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(CartPage.lnkRemoveFirstProduct))
			{
				System.out.println("Navigated successfully to cart section");
				UIFoundation.waitFor(1L);
				cartConutBeforeRemovingProduct=UIBusinessFlows.cartCount();
				if(!UIFoundation.getText(CartPage.lnkRemoveFirstProduct).contains("Fail"))
				{
					objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkRemoveFirstProduct));
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnRemove));
					UIFoundation.waitFor(2L);
				}
				cartConutAfterRemovingProduct=UIBusinessFlows.cartCount();
				if((cartConutBeforeRemovingProduct-1)==cartConutAfterRemovingProduct)
				{
					System.out.println("One product is removed from the cart");
					getlogger().pass("One product is removed from the cart");
				}else
				{
					System.err.println("Product is not removed the cart");
					getlogger().fail("Product is not removed the cart");
				}
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
				UIFoundation.waitFor(36L);
			}else
			{
				System.err.println("Not able to navigated  Cart section");
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnPlaceOrder))
			{
				System.out.println("Navigated successfully to Final review section");
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
				UIFoundation.webDriverWaitForElement(ThankYouPage.lnkOrderNumber, "Clickable", "Thanks!", 70);
				UIFoundation.waitFor(10L);
				String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
				objStatus+=true;
				String objDetail="Order number is placed successfully ";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				getlogger().pass(objDetail+orderNum);
				System.out.println("Order number is placed successfully: "+orderNum);
				
			}else
			{
				System.err.println("Not able to Navigate Final review section");
				objStatus+=false;
		    	String objDetail="Not able to Navigate Final review section";
		    	getlogger().fail(objDetail);
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			
			log.info("The execution of the method checkoutProcess ended here ...");
			if (!expected.equalsIgnoreCase(actual) && objStatus.contains("false")) {
				System.out.println("Order Creatio nWith Edit Functionality In Final Review Page For Existing User test case is failed");
				return "Fail";
			} else {
				System.out.println("Order Creatio nWith Edit Functionality In Final Review Page For Existing User test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method checkoutProcess "
					+ e);
			return "Fail";
		}
	}
	
	

}
