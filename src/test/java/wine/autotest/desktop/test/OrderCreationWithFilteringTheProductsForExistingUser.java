package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class OrderCreationWithFilteringTheProductsForExistingUser extends Desktop {
	
	/***************************************************************************
	 * Method Name			: productFilteration()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to filter the product 
	 * 						  list by selecting some product,region,rate and price 
	 * 						  and checking for the count
	 ****************************************************************************
	 */
	
	public static String productFilter()
	{
		String objStatus=null;
		try
		{
		
			log.info("The execution of method productFilteration started here");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(6L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkRegion));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkcalifornia));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkcaliforniaAll));
			UIFoundation.waitFor(10L);
			//UIFoundation.scrollDownOrUpToParticularElement(driver, "ThirdProductToCart");
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(3));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			//UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			System.out.println("objStatus :"+objStatus);
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method productFilteration "+e);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String checkoutProcess()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String totalBeforeTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		
		try
		{
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary in CART page===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			//
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(CartPage.btnexistingCust))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnexistingCust));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "username"));
				objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			}
			UIFoundation.waitFor(8L);  
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtCVVR));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
			    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(1L);
				}
				System.out.println("============Order summary in the Final Review Page  ===============");
				subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
				shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
				total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
				salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
				System.out.println("Subtotal:              "+subTotal);
				System.out.println("Shipping & Handling:   "+shippingAndHandling);
				System.out.println("Sales Tax:             "+salesTax);
				System.out.println("Total:                 "+total);
				UIFoundation.waitFor(2L);
				if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPlaceOrder)) {
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
					UIFoundation.webDriverWaitForElement(ThankYouPage.lnkOrderNumber, "Clickable", "Thanks!", 70);
					UIFoundation.waitFor(10L);

				}
				String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if(orderNum!="Fail")
			{
				System.out.println("Order Number :"+orderNum);
				UIFoundation.getOrderNumber(orderNum);
				getlogger().pass("Order Number :"+orderNum);
		    	objStatus+=true;			
			}else
			{
				objStatus+=false;
		    	String objDetail="Order number is null.Order not placed successfully";
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		    	getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"screenshotName.jpeg")).build());
			}
			System.out.println("objStatus :"+objStatus);
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Order creation with Product filteration for existing user  test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Order creation with Product filteration for existing user test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method checkoutProcess of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";
			
		}
	}

}
