package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class ValidateHALCountinLocalPickupandRecipient extends Desktop {
	/***********************************************************************************************************
	 * Method Name : ValidateHAlCountinLocalPickupfinder() 
	 * Created By  : Ramesh S
	 * Purpose     : 
	 * 
	 ************************************************************************************************************
	 */
	public static String ValidateHAlCountinLocalPickupfinder() {
		String objStatus = null;
		String screenshotName = UIFoundation.randomNumber(9)+ "_Scenarios_ValidateHAlCountLocalpickupSrch_Screenshot.jpeg";
		try {
			log.info("Execution of the method ValidateHALCountinLocalPickupandRecipient started here .........");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnklocalPickupFinder));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(ListPage.spnlocalPickupFinderWindow)) 
			{
				objStatus+=true;
				String objDetail="'Find a pickup locations near' Modal displayed";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="'Find a pickup locations near' Modal Not displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			String strHALCountLocalSearchPageExpMsg=verifyexpectedresult.HALCountLocalSearchPage;
			String strHALCountLocalSearchPageActMsg=UIFoundation.getText(ListPage.txtHAlCountLocalPickup);
			strHALCountLocalSearchPageActMsg=strHALCountLocalSearchPageActMsg.replaceAll("\n","").replace("\r","");
			System.out.println("HALCountLocalSearchPageExpMsg="+strHALCountLocalSearchPageExpMsg);
			System.out.println("HALCountLocalSearchPageActMsg="+strHALCountLocalSearchPageActMsg);
			if(strHALCountLocalSearchPageActMsg.equalsIgnoreCase(strHALCountLocalSearchPageExpMsg))
			{
				objStatus+=true;
				String objDetail="HAL Count is Displayed in the Local PickUp Finder ";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="HAL Count is Not Displayed in the Local PickUp Finder";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCloseLocalPickupSearch));
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false"))
			{				
	           return "Fail";
			}
			else
			{	
				return "Pass";
			}	
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}		
	}
	/***********************************************************************************************************
	 * Method Name : ValidateHAlCountinRecepientPage() 
	 * Created By  : Ramesh S
	 * Purpose     : 
	 * 
	 ************************************************************************************************************
	 */
	public static String ValidateHAlCountinRecepientPage() {
		String objStatus = null;
		String screenshotName = UIFoundation.randomNumber(9)+ "_Scenarios_ValidateHAlCountRcpntpage_Screenshot.jpeg";
		try {
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtRecipientHeader)) 
			{
				objStatus+=true;
				String objDetail="User is navigated to Recipient page";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Recipient page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.rdoShipToFedEx));		
			String strHALCountRcpntPageExpMsg=verifyexpectedresult.HALCountRcpntPage;
			String strHALCountRcpntPageActMsg=UIFoundation.getText(FinalReviewPage.txtHALCountrecpnt);
			strHALCountRcpntPageActMsg=strHALCountRcpntPageActMsg.replaceAll("\n","").replace("\r","");
			System.out.println("HALCountRcpntPageExpMsg="+strHALCountRcpntPageExpMsg);
			System.out.println("HALCountRcpntPageActMsg="+strHALCountRcpntPageActMsg);
			if(strHALCountRcpntPageActMsg.equalsIgnoreCase(strHALCountRcpntPageExpMsg))
			{
				objStatus+=true;
				String objDetail="HAL Count is Displayed in the Recipient Page ";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="HAL Count is Not Displayed in the Recipient Page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}		
			if (objStatus.contains("false"))
			{				
				String objDetail="ValidateHALCountinLocalPickupandRecipient  test case failed ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{
				String objDetail="ValidateHALCountinLocalPickupandRecipient test case executed succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}	
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}	
}
