package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyGiftCardBalanceisGreaterthan0$isDisponPaymentSecofPickedSubscriptionFlow extends Desktop {

	/***********************************************************************************************************
	 * Method Name : AddProducttoCart()() 
	 * Created By  : Chandrashekar K B
	 * Purpose     : 
	 * 
	 ************************************************************************************************************
	 */
	public static String AddProducttoCart() {
		String objStatus = null;
		String screenshotName = UIFoundation.randomNumber(9)+ "_Scenarios_AddProducttoCart_Screenshot.jpeg";
		try {
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkmyWineSelectSort));
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkPriceLowtoHigh));
			UIFoundation.waitFor(2L);
			String strFirstprodPrice =UIFoundation.getText(ListPage.lnkFirstProductPrize);
			strFirstprodPrice=strFirstprodPrice.replace("$","");
			int intFirstprizesAct=Integer.parseInt(strFirstprodPrice);
			if(intFirstprizesAct<20)
			{
				objStatus+=true;
				String objDetail="The First Product prize is less than 20$ Prize:- "+intFirstprizesAct;
				System.out.println(objDetail);
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
				UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));			
			}
			else
			{
				objStatus+=false;
				String objDetail="The First Product prize is Not less than 20$";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);	
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());			
			}
			if (objStatus.contains("false"))
			{				
				return "Fail";
			}
			else
			{
				return "Pass";
			}	
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}

			
		
	/***********************************************************************************************************
	 * Method Name : ApplyGiftCardandplaceOrder()() 
	 * Created By  : Chandrashekar K B
	 * Purpose     : 
	 * 
	 ************************************************************************************************************
	 */
	public static String ApplyGiftCardandplaceOrder() {
		@SuppressWarnings("unused")
		String objStatus = null;
		String screenshotName = UIFoundation.randomNumber(9)+ "_Scenarios_ApplyGiftCardandplaceOrder_Screenshot.jpeg";
		try {
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			String giftCert2=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate2", 1, giftCert2);
			String giftCert3=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate3", 1, giftCert3);
			String giftCert4=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate4", 1, giftCert4);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(4L);	
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtGiftNumber));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate2"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(4L);
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtGiftNumber));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate3"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(4L);
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtGiftNumber));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate4"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(CartPage.spngiftCertificateSuccessMsg)){
				objStatus+=true;
				String objDetail="Multiple gift cards are applied succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Multiple gift cards are not applied";		
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(6L);
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				UIFoundation.clickObject(FinalReviewPage.txtCVVR);
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				UIFoundation.clickObject(FinalReviewPage.txtCVV);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
			    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
			}
				if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				}
				UIFoundation.waitFor(4L);
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
				UIFoundation.waitFor(15L);
				String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
				if(orderNum!="Fail")
				{
					objStatus+=true;
					String objDetail="Order is placed successfully :";
					UIFoundation.getOrderNumber(orderNum);
					getlogger().pass(objDetail+orderNum);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					System.out.println("Order is placed successfully: "+orderNum);
					return "Pass";
				}else
				{
					objStatus+=false;
			    	String objDetail="Order is null and cannot placed successfully";
			    	getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			    	return "Fail";
				}}catch(Exception e){
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
	/***********************************************************************************************************
	 * Method Name : Enrolltopickedandverifygiftcard()() 
	 * Created By  : Chandrashekar K B
	 * Purpose     : 
	 * 
	 ************************************************************************************************************
	 */
	public static String Enrolltopickedandverifygiftcard() {
		String objStatus = null;
		String screenshotName = UIFoundation.randomNumber(9)+ "_Scenarios_Enrolltopickedandverifygiftcard_Screenshot.jpeg";
		try {
			getDriver().get("https://qwww.wine.com/");
			UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.imgpicked));
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.imgpicked));
		UIFoundation.waitFor(3L);
		if(UIFoundation.isDisplayed(PickedPage.cmdSignUpCompass)) 
		{
			objStatus+=true;
			String objDetail="User is navigated to Picked offer page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Picked offer page";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdSignUpCompass));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkNoVoice));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkRedOnly));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNtVry));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSelQtyNxt));
		UIFoundation.waitFor(3L);
		if(UIFoundation.isDisplayed(PickedPage.lnkEditShippingAddress)) 
		{
			objStatus+=true;
			String objDetail="User is navigated to Recipient page";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="User is not navigated to Recipient page";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
		if(UIFoundation.isDisplayed(PickedPage.lnkChangeAddress))
		{
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkChangeAddress));
			UIFoundation.waitFor(3L);	
		}			
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkEditShippingAddress));
		UIFoundation.waitFor(3L);
		objStatus += String.valueOf(UIFoundation.SelectObject(PickedPage.dwnshipState, "State"));
		UIFoundation.clickObject(PickedPage.dwnshipState);
		UIFoundation.waitFor(2L);
		UIFoundation.clickObject(PickedPage.txtZipCode);
		UIFoundation.clearField(PickedPage.txtZipCode);
		UIFoundation.setObject(PickedPage.txtZipCode, "ZipCode");
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnshippingAddressSave));
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnContinueShip));
		if((UIFoundation.isDisplayed(PickedPage.txtGiftremain1))&&(UIFoundation.isDisplayed(PickedPage.txtGiftremain2))&&(UIFoundation.isDisplayed(PickedPage.txtGiftremain3))&&(UIFoundation.isDisplayed(PickedPage.txtGiftremain4)))
		{
			objStatus+=true;
			String objDetails="Four Active Gift Cards are Displayed";
			getlogger().pass(objDetails);
			ReportUtil.addTestStepsDetails(objDetails, "Pass", "");
			if(UIFoundation.isDisplayed(PickedPage.txtGiftremain1)){
				String strGift1remain=String.valueOf(UIFoundation.getText(PickedPage.txtGiftremain1));	
				strGift1remain=strGift1remain.replace("$","");
				if(strGift1remain.contains("."))
					{
					String[] parts = strGift1remain.split("[.]",0);
					strGift1remain = parts[0]; 
					}	
				int intGift1remain=Integer.parseInt(strGift1remain);
				if(intGift1remain>0)
					{
					objStatus+=true;
					String objDetail="First Gift Card Remaining Value is Greater than 0 Remaining:- "+strGift1remain;
					getlogger().pass(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
					}else{
						objStatus+=false;
						String objDetail="First Gift Card Remaining Value is Not Greater than 0";
						UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
						getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
						}
					}
			if(UIFoundation.isDisplayed(PickedPage.txtGiftremain2)){
			String strGift2remain=String.valueOf(UIFoundation.getText(PickedPage.txtGiftremain2));
			strGift2remain=strGift2remain.replace("$","");
				if(strGift2remain.contains("."))
					{
					String[] parts = strGift2remain.split("[.]",0);
					strGift2remain = parts[0]; 
					}
				int intGift2remain=Integer.parseInt(strGift2remain);
				if(intGift2remain>0)
					{
					objStatus+=true;
					String objDetail="Second Gift Card Remaining Value is Greater than 0 Remaining:- "+strGift2remain;
					getlogger().pass(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
					}else{
						objStatus+=false;
						String objDetail="Second Gift Card Remaining Value is Not Greater than 0";
						UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
						getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
						}
					}
		if(UIFoundation.isDisplayed(PickedPage.txtGiftremain3)){
			String strGift3remain=String.valueOf(UIFoundation.getText(PickedPage.txtGiftremain3));
			strGift3remain=strGift3remain.replace("$","");
			if(strGift3remain.contains("."))
			{
				 String[] parts = strGift3remain.split("[.]",0);
				 strGift3remain = parts[0]; 
				 }	
			int intGift3remain=Integer.parseInt(strGift3remain);
			if(intGift3remain>0)
				{
				objStatus+=true;
				String objDetail="Third Gift Card Remaining Value is Greater than 0 Remaining:- "+strGift3remain;
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				}else{
				objStatus+=false;
			String objDetail="Third Gift Card Remaining Value is Not Greater than 0";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
					}
			}
		if(UIFoundation.isDisplayed(PickedPage.txtGiftremain4)){
			String strGift4remain=String.valueOf(UIFoundation.getText(PickedPage.txtGiftremain4));
			strGift4remain=strGift4remain.replace("$","");
			if(strGift4remain.contains("."))
			 {
				 String[] parts = strGift4remain.split("[.]",0);
				 strGift4remain = parts[0]; 
				 }
			int intGift4remain=Integer.parseInt(strGift4remain);
			if(intGift4remain>0)
			{
				objStatus+=true;
				String objDetail="Fourth Gift Card Remaining Value is Greater than 0 Remaining:- "+strGift4remain;
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
		}else{
			objStatus+=false;
			String objDetail="Fourth Gift Card Remaining Value is Not Greater than 0";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
		}}else{
			objStatus+=false;
			String objDetail="Gift Cards are Not Displayed";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());	
			}
			if (objStatus.contains("false"))
			{				
				String objDetail="VerifyGiftCardBalanceisGreaterthan0$isDisponPaymentSecofPickedSubscriptionFlow  test case failed ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}else{
				String objDetail="VerifyGiftCardBalanceisGreaterthan0$isDisponPaymentSecofPickedSubscriptionFlow test case executed succesfully";
				getlogger().pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}	}
		catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}	
}}	