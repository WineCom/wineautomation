package wine.autotest.desktop.order.pages;

import org.openqa.selenium.By;


public class LoginPage {
	
	public static By imgWineLogo = By.xpath("(//a[@class='wineLogo'])[3]");
	public static By StateChangeDailog = By.xpath("//p[@class='cartTransfer_headline']");
	public static By btnAccount = By.xpath("//span[@class='accountBtn']");
	public static By btnManiNav = By.xpath("//input[@class='mainNavBtn']");
//	public static By lnkAccSignIn = By.xpath("//li[@class='headerAccountMenu_listItem mainNavList_itemLink-accntSignIn']");
	public static By lnkAccSignIn = By.xpath("//span[@class='accountBtn']");
	public static By checkboxLabel = By.xpath("//label[@class='formWrap_label formWrap_checkboxLabel signinForm_checkboxLabel']");
	public static By checkboxUncheck = By.xpath("//input[@class='formWrap_checkbox signInForm_checkbox']");
	public static By txtLoginEmail = By.xpath("//input[@name='email']");
	public static By txtLoginPassword = By.xpath("//input[@name='password']");
	public static By btnSignIn = By.xpath("//button[text()='Sign In']");
	public static By QAUserPopUp = By.xpath("//button[text()='Close this Modal']");
	public static By lnkSignIn = By.xpath("(//a[text()='Sign in'])[2]");
	public static By lnkSignout = By.xpath("(//a[text()='Sign Out'])[1]");
	public static By txtsignIn = By.xpath("(//h2[text()='Sign In'])[1]");
	public static By lnkWahintonWinesPromoPlusSymbol = By.xpath("//section[@id='learnAboutContent']/div[@class='listPageContentContainerWrap']//button[@class='listPageContent_expand']/a[@href='#learnAboutContent']");
	public static By spnpromoBarWashingtonReg = By.xpath("//section[@class='listPageContent_bodyText']/p");
	
	public static By spnPasswordErrMsg = By.xpath("//div[@class='formWrap_errorMessage formWrap_errorMessage-showing']/p");
	public static By chkCaptcha = By.xpath("//div[@class='recaptcha-checkbox-border']");
	public static By btnActivity = By.xpath("//a[@class='activityBtn']");
	public static By btnCreateAcc= By.xpath("//a[text()='Create Account']");
	public static By txtFirstName= By.xpath("//input[@name='firstName']");
	public static By txtLastName= By.xpath("//input[@name='lastName']");
	public static By txtEmail= By.xpath("//input[@name='email']");
	public static By txtPassword= By.xpath("//input[@name='password']");
	public static By btnCreateAccount= By.xpath("//form[@class='formWrap registerForm']/button");
	public static By spnInValidEmail= By.xpath("//p[text()='Invalid email format.']");
	public static By btnverifySave= By.xpath("//button[@class='btn btn-small btn-rounded btn-red btn-processing emailPreferences_saveBtn']");
	 
	public static By btnApplyNow =By.xpath("//span[@class='promoBarModal_applyBtnText' and text()='Apply Now']");
	public static By imgApplyNowClose = By.xpath("//i[@class='promoBarModal_cancelIcon icon icon-menu-close']");
	public static By dwnSelectState =By.xpath("(//form[@class='formWrap searchBarForm']/div/div/select[@class='state_select state_select-promptsCartTransfer'])[1]");
	
	public static By dwnSelectStateConfirm =By.xpath("//div/a[@class='btn btn-large btn-rounded btn-red btn-processing cartTransfer_saveBtn']");
	
	public static By lnktoolTipComponet = By.xpath("(//a[@class='tooltipComponent_text tooltipComponent_link'])[1]");
	public static By AWUSiteCantReached = By.xpath("//h1[text()='This site can\\u2019t be reached']");
	public static By spninvalidLoginErrorMsg = By.xpath("//p[text()='Invalid email address or password.']");
	public static By OrdersHistory = By.xpath("//div[@class='headerAccountMenu']//a[text()='Order Status']");
	public static By AddressBook = By.xpath("//div[@class='headerAccountMenu']//a[text()='Address Book']");
	public static By PaymentMethods = By.xpath("//div[@class='headerAccountMenu']//a[text()='Payment Methods']");
	public static By EmailPreferences = By.xpath("//div[@class='headerAccountMenu']//a[text()='Email Preferences']");
	
	public static By obj_ForgotPasowrd = By.xpath("//a[text()='Forgot your password?']");
	public static By obj_NewToWine = By.xpath("//p[@class='modalWindow_title signInWrap_secondaryTitle']");
	public static By lnkSignInFacebook = By.xpath("(//a[@class='btn btn-large btn-rounded btn-facebook formWrap_btn signInWrap_facebook']");
	public static By spnpromoBannerDry = By.xpath("//div[@class='bannerMessage bannerMessage_dryState']");
	
	//FacebookLogin
	//public static By btncontinueWithFacebook = By.xpath("//a[@class='btn btn-large btn-rounded btn-facebook formWrap_btn signInWrap_facebook']");
	public static By btncontinueWithFacebook = By.xpath("//a[@class='btn btn-large btn-rounded btn-outline formWrap_btn signInWrap_facebook']");
	public static By txtfacebookEmail = By.xpath("//input[@id='email']");
	public static By txtfacebookPass = By.xpath("//input[@id='pass']");
	public static By btnfacebookLogin = By.xpath("//button[@name='login']");
	public static By lnkForgotPassword = By.xpath("//a[@class='formWrap_link formWrap_link-blue signinForm_forgotPassword']");
	public static By txtForgotEmail = By.xpath("//input[@class='resetPasswordForm_email resetPasswordForm_input formWrap_input']");
	public static By btnForgotContinue = By.xpath("//button[@class='btn btn-large btn-rounded btn-red btn-processing formWrap_btn resetPasswordForm_btn']");
	public static By spnCheckUrEmail = By.xpath("//h3[@class='resetPasswordSuccess_headline']");
	public static By txtEnterEmailAddMsg = By.xpath("//p[@class='resetPasswordForm_disclaimer']");
	
}
