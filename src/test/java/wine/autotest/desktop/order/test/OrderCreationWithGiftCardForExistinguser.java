package wine.autotest.desktop.order.test;

import org.openqa.selenium.By;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.desktop.pages.LoginPage;

import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.UIFoundation;

public class OrderCreationWithGiftCardForExistinguser extends OrderCreation {

	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			getlogger().info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "TC13_Orderusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			getlogger().info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: chandrashekhar 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String captureOrdersummary()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String giftAccount=null;		  				
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before applying Gift card code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			String giftCert=UIFoundation.giftCertificateNumber();
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				getDriver().findElement(By.xpath("//fieldset[@class='orderCodesForm_group orderCodesForm_groupGift formWrap_group']//input[@class='formWrap_input orderCodesForm_input orderCodesForm_inputGift']")).sendKeys(giftCert);
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(7L);			
				objStatus+=String.valueOf(UIFoundation.isElementDisplayed(CartPage.spngiftCertificateSuccessMsg));
				}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
				getDriver().findElement(By.xpath("//fieldset[@class='orderCodesForm_group orderCodesForm_groupGift formWrap_group']//input[@class='formWrap_input orderCodesForm_input orderCodesForm_inputGift']")).sendKeys(giftCert);
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(7L);					
				objStatus+=String.valueOf(UIFoundation.isElementDisplayed(CartPage.spngiftCertificateSuccessMsg));
				}
			System.out.println("============Order summary after applying Gift card code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			giftAccount=UIFoundation.getText(CartPage.spnGiftCertificatePrice);
			if(giftAccount.equalsIgnoreCase("fail")){
				System.err.println("There was an error redeeming your gift card. Please be sure you haven't redeemed it already and that you entered the correct code");
				System.out.println("Subtotal:              "+subTotal);
				System.out.println("Shipping & Handling:   "+shippingAndHandling);
				System.out.println("Total Before Tax:      "+totalBeforeTax);
				}else{
				System.out.println("Subtotal:              "+subTotal);
				System.out.println("Shipping & Handling:   "+shippingAndHandling);
				System.out.println("Gift Account:            "+giftAccount);
				System.out.println("Total Before Tax:      "+totalBeforeTax);
				}
			System.out.println("objStatus :"+objStatus);
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false")){
				return "Fail";
				}else{
				return "Pass";
				}	
		}catch(Exception e)
		{		
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";	
		}
	}
	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: chandrashekhar 
	 * Purpose				: 
	 ****************************************************************************
	 */	
	public static String checkoutProcess()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String screenshotName = "Scenarios_OrderCreation13_Screenshot.jpeg";
		try
		{
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIBusinessFlows.recipientEdit());
			if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
				}	
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
					{
					 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
					 UIFoundation.waitFor(3L);
					}
				}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
				}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(1L);
				}
				System.out.println("============Order summary in the Final Review Page  ===============");
				subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
				shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
				total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
				salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
				System.out.println("Subtotal:              " + subTotal);
				System.out.println("Shipping & Handling:   " + shippingAndHandling);
				System.out.println("Sales Tax:             " + salesTax);
				System.out.println("Total:                 " + total);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentCheckoutEdit));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPaymentCVV, "CardCvid"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentSave));
			if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue)){
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(1L);
				}		
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(ThankYouPage.lnkOrderNumber, "Clickable", "Thanks!", 60);
			UIFoundation.waitFor(10L);
			String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if(orderNum!="Fail"){
				System.out.println("Order Number :"+orderNum);
				UIFoundation.getOrderNumber(orderNum);
				}else{
				objStatus+=false;
				String objDetail="Order  is null.Order not placed successfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));		 
			if(UIFoundation.isDisplayed(OrderDetailsPage.spnUsedGiftCard)){			
				objStatus+=true;
				String objDetail = "Order is succesfully placed and  gift cards used displayed in the order History.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}else {
				objStatus+=false;
				String objDetail = "Order is succesfully placed and  Gift card used displayed in the order History.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")){
				return "Fail";
				}else{
				return "Pass";
				}
		}catch(Exception e)
		{		
			log.error("there is an exception arised during the execution of the method checkoutProcess of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";	
		}
	}
}
