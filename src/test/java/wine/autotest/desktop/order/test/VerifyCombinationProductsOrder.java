package wine.autotest.desktop.order.test;

import java.io.IOException;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.order.test.OrderCreation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.order.pages.*;

public class VerifyCombinationProductsOrder extends OrderCreation {
	
	/***************************************************************************
	 * Method Name			: addDiffProdwithDiffeBottelsizeTocrt()
	 * Created By			: Ramesh S
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addDiffProdwithDiffeBottelsizeTocrt() {
		String objStatus = null;
		try {
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkWhiskey));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkMoreOptions));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.selBottleSize));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.selStdBottleSize));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkBrandy));
			UIFoundation.waitFor(5L);
	  		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.selBottleSize));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.selHalfBottleSize));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkWhiskey));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.selBottleSize));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.selStdBottleSize));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false")) {
				return "Fail";
				} else {
					return "Pass";
				}
		} catch (Exception e) {
			return "Fail";
		}
	}
}
