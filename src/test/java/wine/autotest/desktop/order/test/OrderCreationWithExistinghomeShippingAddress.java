package wine.autotest.desktop.order.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.fw.utilities.*;


public class OrderCreationWithExistinghomeShippingAddress extends OrderCreation {

	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			getlogger().info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "orderTC78username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			getlogger().info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	
	
	
	
/*	
	*//***************************************************************************
	 * Method Name			: shippingDetails()
	 * Created By			: Chandra shekhar
	 * Purpose				:  The purpose of this method is to fill the shipping 
	 * 						  address of the customer
	 ****************************************************************************
	 *//*
	public static String shippingDetails(WebDriver getDriver()) {
		String objStatus = null;
		   String screenshotName = "Scenarios_cartPage_Screenshot.jpeg";
			
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
		
			objStatus+=String.valueOf(UIFoundation.clickObject(getDriver(), "CheckoutButton"));
			UIFoundation.waitFor(6L);
			objStatus+=String.valueOf(UIFoundation.clickObject(getDriver(), "ShippingTyp"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(getDriver(), "FirstName", "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(getDriver(), "LastName", "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(getDriver(), "StreetAddress", "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(getDriver(), "City", "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(getDriver(), "obj_State", "State"));
			UIFoundation.clickObject(getDriver(), "obj_State");
			objStatus += String.valueOf(UIFoundation.setObject(getDriver(), "ZipCode", "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(getDriver(), "PhoneNum", "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(getDriver(), "ShipContinueButton"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(getDriver(), "SuggestedAddress"));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(getDriver(), "VerifyContinueButton")){
				objStatus += String.valueOf(UIFoundation.clickObject(getDriver(), "VerifyContinueButton"));
				UIFoundation.waitFor(10L);
			}

			if(UIFoundation.isDisplayed(getDriver(), "localPickUp")){
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Local pickup icon is displayed next to pickup location address.", "Pass", "");
			}else{
				objStatus+=false;
				//	System.out.println("Verify the header  in the 'About Stewardship' popup in Final Review section when the user has not enrolled stewardship.");
					String objDetail="Local pickup icon is not displayed next to pickup location address.";
					UIFoundation.captureScreenShot(getDriver(), screenshotpath+"localPickUp", objDetail);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(6L);
			}

			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}*/

	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar 
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String checkoutProcess()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String totalBeforeTax=null;
		String screenshotName = UIFoundation.randomNumber(9)+"_OrderNotPlaced4278_Screenshot.jpeg";
		
			
		try
		{
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary in CART page===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			

           if(UIFoundation.isDisplayed(CartPage.btnCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}
                          UIFoundation.waitFor(2L);
			
		
//			UIFoundation.webDriverWaitForElement(getDriver(), "SignInButton", "Invisible", "", 50);
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
	        {
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
	        }
			if (UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				UIFoundation.clearField(FinalReviewPage.txtCVV);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
			}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
				{
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
				{	
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				}
	                 
	          }
	        
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkPaymentCheckoutEdit));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPaymentCVV, "CardCvid"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnPaymentSave));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(ThankYouPage.lnkOrderNumber, "Clickable", "Thanks!", 60);
			UIFoundation.waitFor(10L);
			String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if(orderNum!="Fail")
			 {
			      objStatus+=true;
			      String objDetail="Order is placed successfully";
			      UIFoundation.getOrderNumber(orderNum);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Order is placed successfully: "+orderNum);   
			 }else
			 {
			       objStatus+=false;
			       String objDetail="Order is null and Order cannot placed";
			       UIFoundation.captureScreenShot(screenshotpath, objDetail);
			       getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			 }
						
			log.info("The execution of the method checkoutProcess ended here ...");	
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{
				return "Pass";
			}
			
	}
		catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method checkoutProcess of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";
			
		}
	}
	

}
