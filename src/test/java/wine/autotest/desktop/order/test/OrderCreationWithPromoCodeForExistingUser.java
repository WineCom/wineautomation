package wine.autotest.desktop.order.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.order.test.OrderCreation;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;

import wine.autotest.fw.utilities.ReportUtil;

public class OrderCreationWithPromoCodeForExistingUser extends OrderCreation{
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			getlogger().info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "TC05_Orderusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			getlogger().info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String loginpromocode()
	{
		String objStatus=null;
		
		try
		{
			
			getlogger().info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "orderTC75username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			getlogger().info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String logingiftCertPromocode()
	{
		String objStatus=null;
		
		try
		{
			
			getlogger().info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "orderTC77username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			getlogger().info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String logingiftCert()
	{
		String objStatus=null;
		
		try
		{
			
			getlogger().info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "orderTC76username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			getlogger().info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Chandrashekhar 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String captureOrdersummary()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String promeCode=null;
		String screenshotName = "Scenarios_PromoCodeField_Screenshot.jpeg";	
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before applying promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			if(UIFoundation.isDisplayed(CartPage.txtPromoCode))
			{	
				objStatus+=true;
				String objDetail="Promo code text filed is expanded by default";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.isElementDisplayed(CartPage.spnPromoCodeApplied));
				UIFoundation.waitFor(6L);
				}else{
				objStatus+=false;
				String objDetail="Promo code text filed is not expanded by default";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeExpand));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.waitFor(5L);
				}			
			System.out.println("============Order summary after applying promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			promeCode=UIFoundation.getText(CartPage.spnPromoCodePrice);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Prome Code:            "+promeCode);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			UIBusinessFlows.discountCalculator(subTotal, promeCode);
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false")){
				return "Fail";
			}
			else{
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar 
	 * Purpose				: 
	 ****************************************************************************
	 */


	public static String checkoutProcess()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String orderNum=null;
		try
		{
			String screenshotName = UIFoundation.randomNumber(9)+"_Checkout0575_Screenshot.jpeg";
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(26L);
			if(UIFoundation.isDisplayed(CartPage.btnexistingCust)){
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnexistingCust));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "username"));
				objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
				UIFoundation.waitFor(26L); 
				}
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
				}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
				}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(1L);
				}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(ThankYouPage.lnkOrderNumber, "Clickable", "Thanks!", 60);
			UIFoundation.waitFor(10L);
			orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if(orderNum!="Fail"){
				System.out.println("Order Number :"+orderNum);
				UIFoundation.getOrderNumber(orderNum);
				}else{
				objStatus+=false;
				String ObjDetail ="Order number is null.Order not placed successfully";
				ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				getlogger().fail(ObjDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, ObjDetail);
				}
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));				
			if(UIFoundation.isDisplayed(OrderDetailsPage.spnPromo)){			
				objStatus+=true;
				String objDetail = "Order is succesfully placed and  Promo displayed in the order History.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}else {
				objStatus+=false;
				String objDetail = "Order is succesfully placed and  Promo displayed in the order History.";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")){
				return "Fail";
				}else{
				return "Pass";
				}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method checkoutProcess of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: captureSpiritOrdersummary()
	 * Created By			: Ramesh S 
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String captureSpiritOrdersummary()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String promeCode=null;
	    String screenshotName = "Scenarios_PromoCodeField_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before applying promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			if(UIFoundation.isDisplayed(CartPage.txtPromoCode)){	
				objStatus+=true;
				String objDetail="Promo code text filed is expanded by default";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.isElementDisplayed(CartPage.spnPromoCodeApplied));
				UIFoundation.waitFor(6L);
				}else{
				objStatus+=false;
				String objDetail="Promo code text filed is not expanded by default";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeExpand));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.waitFor(5L);
				}				
			System.out.println("============Order summary after applying promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			promeCode=UIFoundation.getText(CartPage.spnPromoCodePrice);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Prome Code:            "+promeCode);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			UIBusinessFlows.discountCalculator( subTotal, promeCode);	
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false")){
				return "Fail";
				}else{
				return "Pass";
				}
		}catch(Exception e)
		{	
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";	
		}
	}	
	/***************************************************************************
	 * Method Name : PromoCodeWithGiftCert() 
	 * Created By  : Ramesh S
	 * Purpose     :
	 ****************************************************************************
	 */


	public static String PromoCodeWithGiftCert() {
		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String totalBeforeTax = null;
		String promeCode = null;
		String screenshotName = "Scenarios_promoCodeApply.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
							+ screenshotName;
		try {
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before applying promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Total Before Tax:      " + totalBeforeTax);
			if(UIFoundation.isDisplayed(CartPage.txtPromoCode)){
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.isElementDisplayed(CartPage.spnPromoCodeApplied));
				UIFoundation.waitFor(5L);
				}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeExpand));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.waitFor(5L);
				}
			if (UIFoundation.isDisplayed(CartPage.spnPromoCodeApplied)) {
				objStatus+=true;
				ReportUtil.addTestStepsDetails("'Promo code applied' message is displayed when promocode is applied..", "Pass", "");
				} else {
				objStatus+=false;
				String objDetail="'Promo code applied' message is not displayed when promocode is applied.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());	
				}	
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber)){
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				}
			String Subtotal = UIFoundation.getText(CartPage.spnSubtotal);
			Subtotal = Subtotal.replaceAll("[$,]", "");
			double SubtotalAmount = Double.parseDouble(Subtotal);
			String shippingAndHandlingGift = UIFoundation.getText(CartPage.spnShippingHandling);
			shippingAndHandlingGift = shippingAndHandling.replaceAll("[$,]", "");
			double shippingAndHandlingAmount = Double.parseDouble(shippingAndHandlingGift);
			String accountCreditLabelAmunt = UIFoundation.getText(CartPage.spnaccountCreditLabelAmount);
			accountCreditLabelAmunt = accountCreditLabelAmunt.replaceAll("[$,]", "");
			double accountCreditLabelAmount = Double.parseDouble(accountCreditLabelAmunt);
			double totalAmount=SubtotalAmount+shippingAndHandlingAmount;
			if(totalAmount==accountCreditLabelAmount){
				objStatus+=String.valueOf(UIFoundation.isElementDisplayed(CartPage.spntotalAmountZero));
				objStatus+=String.valueOf(UIFoundation.isElementDisplayed(CartPage.spncreditRemainingHeader));
				}
			System.out.println("============Order summary after applying promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			promeCode=UIFoundation.getText(CartPage.spnPromoCodePrice);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Prome Code:            " + promeCode);
			System.out.println("Total Before Tax:      " + totalBeforeTax);
			UIFoundation.waitFor(3L);
			UIBusinessFlows.discountCalculator(subTotal, promeCode);		
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false")) {
				return "Fail";
				} else {
				return "Pass";
				}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method captureOrdersummary " + e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : OrderWithGiftCert() 
	 * Created By  : Ramesh S
	 * Purpose     :
	 ****************************************************************************
	 */
	public static String OrderWithGiftCert() {
		String objStatus = null;
		
		try {
			log.info("The execution of the method captureOrdersummary started here ...");			
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber)){
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				}
			String Subtotal = UIFoundation.getText(CartPage.spnSubtotal);
			Subtotal = Subtotal.replaceAll("[$,]", "");
			double SubtotalAmount = Double.parseDouble(Subtotal);
			String shippingAndHandlingGift = UIFoundation.getText(CartPage.spnShippingHandling);
			shippingAndHandlingGift = shippingAndHandlingGift.replaceAll("[$,]", "");
			double shippingAndHandlingAmount = Double.parseDouble(shippingAndHandlingGift);
			String accountCreditLabelAmunt = UIFoundation.getText(CartPage.spnaccountCreditLabelAmount);
			accountCreditLabelAmunt = accountCreditLabelAmunt.replaceAll("[$,]", "");
			double accountCreditLabelAmount = Double.parseDouble(accountCreditLabelAmunt);
			double totalAmount=SubtotalAmount+shippingAndHandlingAmount;
			if(totalAmount==accountCreditLabelAmount){
				objStatus+=String.valueOf(UIFoundation.isElementDisplayed(CartPage.spntotalAmountZero));
				objStatus+=String.valueOf(UIFoundation.isElementDisplayed(CartPage.spncreditRemainingHeader));
				}		
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false")) {
				return "Fail";
				} else {
				return "Pass";
				}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method captureOrdersummary " + e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: checkoutProcessGiftCert()
	 * Created By			: Ramesh S 
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String checkoutProcessGiftCert()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String orderNum=null;	
		try
		{
			String screenshotName = "checkoutProcessGiftCert.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(26L);
			if(UIFoundation.isDisplayed(CartPage.btnexistingCust)){
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnexistingCust));
				UIFoundation.waitFor(2L);
				}
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());		
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
				}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
				}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));				
				}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(ThankYouPage.lnkOrderNumber, "Clickable", "Thanks!", 60);
			UIFoundation.waitFor(15L);
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order is placed successfully";
				UIFoundation.getOrderNumber(orderNum);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order is placed successfully: " + orderNum);
				} else {
				objStatus += false;
				String objDetail = "Order  is null.Order not placed successfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				} 
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));				
			 if(UIFoundation.isDisplayed(OrderDetailsPage.spnUsedGiftCard)){			
				objStatus+=true;
				String objDetail = "Order is succesfully placed and  Gift Certificate displayed in the order History.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				} else {
				objStatus+=false;
				String objDetail = "Order is succesfully placed and   Gift Certificate displayed in the order History.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
			log.info("The execution of the method checkoutProcessGiftCert ended here ...");
			if (objStatus.contains("false")){
				return "Fail";
				}else{
				return "Pass";
				}	
		}catch(Exception e)
		{		
			log.error("there is an exception arised during the execution of the method checkoutProcessGiftCert of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";			
		}
	}
	/***************************************************************************
	 * Method Name			: checkoutProcessGiftCertandPromo()
	 * Created By			: Ramesh S 
	 * Purpose				: 
	 ****************************************************************************
	 */	
	public static String checkoutProcessGiftCertandPromo()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String orderNum=null;	
		try
		{
			String screenshotName = "checkoutProcessGiftCert.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(26L);
			if(UIFoundation.isDisplayed(CartPage.btnexistingCust)){
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnexistingCust));
				UIFoundation.waitFor(2L);
				}
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
				}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
				}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				}
			UIFoundation.waitFor(4L);
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(1L);
				}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(ThankYouPage.lnkOrderNumber, "Clickable", "Thanks!", 60);
			UIFoundation.waitFor(15L);
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order is placed successfully";
				UIFoundation.getOrderNumber(orderNum);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order is placed successfully: " + orderNum);
				} else {
				objStatus += false;
				String objDetail = "Order  is null.Order not placed successfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				} 
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));				
		    if(UIFoundation.isDisplayed(OrderDetailsPage.spnPromo)){			
				objStatus+=true;
				String objDetail = "Order is succesfully placed and  Promo displayed in the order History.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}else {
				objStatus+=false;
				String objDetail = "Order is succesfully placed and  Promo displayed in the order History.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
			 if(UIFoundation.isDisplayed(OrderDetailsPage.spnUsedGiftCard)){			
				objStatus+=true;
				String objDetail = "Order is succesfully placed and  Gift Certificate displayed in the order History.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}else {
				objStatus+=false;
				String objDetail = "Order is succesfully placed and   Gift Certificate displayed in the order History.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
			log.info("The execution of the method checkoutProcessGiftCert ended here ...");
			if (objStatus.contains("false")){
				return "Fail";
				}else{
				return "Pass";
				}		
		}catch(Exception e)
		{		
			log.error("there is an exception arised during the execution of the method checkoutProcessGiftCert of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";			
		}
	}
}
