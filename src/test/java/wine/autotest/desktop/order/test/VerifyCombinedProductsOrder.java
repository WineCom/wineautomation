package wine.autotest.desktop.order.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.order.test.OrderCreation;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.fw.utilities.ReportUtil;

public class VerifyCombinedProductsOrder extends OrderCreation {

	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			getlogger().info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "TC39_Orderusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			getlogger().info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: addMyWineProdTocrt()
	 * Created By			: Chandrashekhar
	 * Purpose				: The purpose of this method is to Create an order with
	 * 						  Gift cards
	 ****************************************************************************
	 */
	
	
	public static String addMyWineProdTocrt() {
		String objStatus = null;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			UIFoundation.waitFor(8L);			
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			UIFoundation.scrollUp();
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkBisinessGiftTab));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.chkshipSoon));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			UIFoundation.scrollUp();
			UIFoundation.waitFor(2L);
			getDriver().get("https://qwww.wine.com/list/gifts/birthday/7151-9009");
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.chkshipSoon));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(1));
			UIFoundation.scrollUp();
			UIFoundation.waitFor(2L);
			getDriver().get("https://qwww.wine.com/list/gifts/thank-you/7151-9026");
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.chkshipSoon));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(1));
			UIFoundation.scrollUp();
			UIFoundation.waitFor(2L);
			getDriver().get("https://qwww.wine.com/list/gifts/wine-and-food-gifts/7151-7152");
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.chkshipSoon));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(1));
			UIFoundation.scrollUp();
			UIFoundation.waitFor(2L);
			/*getDriver().get("https://qwww.wine.com/list/gifts/food-and-chocolate-gifts/7151-8992");
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(1));
			UIFoundation.waitFor(2L);*/
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkwineSet1to4bottles));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.chkshipSoon));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(1));
			UIFoundation.scrollUp();
			UIFoundation.waitFor(2L);
            objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));	
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkGiftCards));	
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			UIFoundation.scrollUp();
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false")) {
				return "Fail";
			} else {
				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar
	 * Purpose				: The purpose of this method is to Create an order with
	 * 						  Gift cards
	 ****************************************************************************
	 */
	public static String checkoutProcess() {

		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "Scenarios_OrderCreation39_Screenshot.jpeg";	
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(2L);		
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			 getDriver().navigate().refresh();
			UIFoundation.waitFor(8L);
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(5L);
				}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				  objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));			
				}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {	
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
					{
					UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
				    UIFoundation.waitFor(3L);
					}
				}
			UIFoundation.clearField(FinalReviewPage.txtCVV);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtCVV));
			UIFoundation.clearField(FinalReviewPage.txtCVV);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(1L);
				}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(ThankYouPage.lnkOrderNumber, "Clickable", "Thanks!", 60);
			UIFoundation.waitFor(10L);
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			System.out.println("orderNum :"+orderNum);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order is placed successfully";
				UIFoundation.getOrderNumber(orderNum);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order is placed successfully: " + orderNum);
				} else {
					objStatus += false;
					String objDetail = "Order is null.Order not placed successfully";
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order creation with existing account test case is failed");
				return "Fail";
				} else {
					System.out.println("Order creation with existing account test case is executed successfully");
					return "Pass";
				}
		} catch (Exception e) {
			log.error("There is an exception arised during the execution of the method getOrderDetails " + e);
			objStatus += false;
			String objDetail = "Order is null.Order not placed successfully";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}
}
