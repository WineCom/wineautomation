package wine.autotest.desktop.order.test;

import java.io.IOException;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.order.test.OrderCreation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.order.pages.*;


public class OrderCreationWithSpiritsProductandCocktail extends OrderCreation{

	/***************************************************************************
	 * Method Name			: addSpiritandCocktailprodTocrt()
	 * Created By			: Ramesh S
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addSpiritandCocktailprodTocrt() {
		String objStatus = null;
		try {
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkWhiskey));	
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
			UIFoundation.waitFor(1L);
	    	objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkCocktailRecipes));
			UIFoundation.waitFor(1L);	
			if (UIFoundation.isDisplayed(ListPage.btnCocktailFirstprodtoCart)) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.btnCocktailFirstprodtoCart);
			}
			if (UIFoundation.isDisplayed(ListPage.btnCocktailSecondprodtoCart)) {
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.btnCocktailSecondprodtoCart);
			}						
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if(!UIFoundation.isDisplayed(ListPage.btnCartCount)){
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
				UIFoundation.waitFor(3L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
				UIFoundation.waitFor(3L);
			}
			if (objStatus.contains("false")) {
				return "Fail";
			} else {
				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";
		}
	}
}
