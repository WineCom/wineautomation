package wine.autotest.desktop.order.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.UIFoundation;

public class OrderCreationWithGftProductsAlongWithGftEmailNGftMssg extends OrderCreation{

	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			getlogger().info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "TC17_Orderusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			getlogger().info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: addGiftsToTheCart()
	 * Created By			: Chandrashekhar
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String addGiftsToTheCart()
	{
		String objStatus=null;
		try
		{
			log.info("add Gift Cards method started here.");
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));		
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkGiftCards));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			UIFoundation.waitFor(2L);	
			getDriver().get("https://qwww.wine.com/list/gifts/birthday/7151-9009");
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));	
			UIFoundation.waitFor(2L);
      		getDriver().get("https://qwww.wine.com/list/gifts/thank-you/7151-9026");
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			UIFoundation.waitFor(2L);	
			getDriver().get("https://qwww.wine.com/list/gifts/wine-and-food-gifts/7151-7152");		
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			UIFoundation.waitFor(2L);	
			/*getDriver().get("https://qwww.wine.com/list/gifts/food-and-chocolate-gifts/7151-8992");
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(1));
			UIFoundation.waitFor(2L);	*/
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkwineSet1to4bottles));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(1));
			UIFoundation.waitFor(2L);	
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
		if (objStatus.contains("false")) {
			return "Fail";
			} else {
			return "Pass";
			}
	}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method productFilteration "+e);
			return "Fail";
		}
	}

	/***********************************************************************************
	 * Method Name			: OrderCreatioWithRedVelvetGftBag()
	 * Created By			: Chandrashekhar
	 * Purpose				: The purpose of this method is to  Create an order with Gifts 
	 * 						  Products by adding Gift Email and Gift message
	 * 
	 **********************************************************************************
	 */

	public static String orderCreationWithGftProdNGftEmailNGftMsg() {
		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = UIFoundation.randomNumber(9)+"_orderCreationWithGftProdNGftEmailNGftMsg.jpeg";
	    try {
	    	log.info("Order Creation  With Gft prod N Gft Message N Gft Email method started");
	    	objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnCheckout));
	        UIFoundation.waitFor(8L);  
	        if(UIFoundation.isDisplayed(FinalReviewPage.lnkaddGiftMessage)){
	        	objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkaddGiftMessage));
				UIFoundation.waitFor(3L);	
	        	}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				UIFoundation.waitFor(3L);	
	        	}  
	        UIFoundation.clearField(FinalReviewPage.txtgiftRecipientEmail);
	        UIFoundation.waitFor(1L);
	        objStatus += String.valueOf(UIFoundation.setObjectCreateAccount(FinalReviewPage.txtgiftRecipientEmail,"email"));
	        objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtgiftMessageTextArea));
	        objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtgiftMessageTextArea, "giftMessage"));
	        UIFoundation.waitFor(1L);
	        objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtgiftRecipientEmail));
	        //String enteredGftMessg = UIFoundation.getAttribute(FinalReviewPage.txtgiftMessageTextArea);
	        UIFoundation.waitFor(1L);
	        objStatus += String.valueOf(UIFoundation.javaSriptClick(FinalReviewPage.btnRecipientContinue));
	        UIFoundation.waitFor(3L);
	        if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
				}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
				}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
					{
					 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
					 UIFoundation.waitFor(3L);
					}
				}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(1L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(ThankYouPage.lnkOrderNumber, "Clickable", "Thanks!", 60);
			UIFoundation.waitFor(10L);
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order is placed successfully";
				UIFoundation.getOrderNumber(orderNum);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order is placed successfully: " + orderNum);
				} else {
				objStatus += false;
				String objDetail = "Order is null.Order not placed successfully";
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));
			//String giftMsg=UIFoundation.getText(OrderDetailsPage.spnorderHistoryGftMssg);
			System.out.println("objStatus :"+objStatus);
			log.info("order With Gft Message N Gft Email method ended here");
		if (objStatus.contains("false")){
			System.out.println("Verify the Order Creation With Gft Message And GftEmail test case failed ");
			return "Fail";
			}else{
			System.out.println("Verify the Order Creation With Gft Message And GftEmail test case executed succesfully");
			return "Pass";
			}
	}catch(Exception e)
	{
		log.error("there is an exception arised during the execution of the method "+ e);
		return "Fail";
	}

}

	}