package wine.autotest.desktop.smoke.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.OrderDetailsPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.fw.utilities.UIFoundation;

public class OrderCreationWithGftMsgNGftEmailAlongWithSilverSheerGftBag extends SmokeTest {
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			getlogger().info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "SmokeTC03username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			getlogger().info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;

		try {			

			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
	       	objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(1L);
			
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(2));
			UIFoundation.waitFor(2L);
          			
            UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if(!UIFoundation.isDisplayed(ListPage.btnCartCount)){
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
				UIFoundation.waitFor(3L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
				UIFoundation.waitFor(3L);
			}

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: OrderCreationWithGftMsgNGftEmailAlongWithGftBag()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is to  Create an order with 
	 * Gift message ,Email and Silver Sheer Gift Bag
	 * 
	 ****************************************************************************
	 */

	public static String  OrderCreationWithSilverSheerGftBag() {
	String objStatus = null;
	String subTotal = null;
	String shippingAndHandling = null;
	String total = null;
	String salesTax = null;
	String orderNum = null;
	String screenshotName = "OrderCreationWithGftMessageAndGftEmailAlongWithGftBag.jpeg";
    try {
    	log.info("Order Creation  With Silver Sheer Gft Bag started here");
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnCheckout));
        UIFoundation.waitFor(8L);  
    //    ApplicationDependent.recipientEdit(driver);
        if (UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			UIFoundation.waitFor(3L);
		}
        if(!UIFoundation.isDisplayed(FinalReviewPage.txtgiftMessageTextArea))
		{
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.chkRecipientGift));
		}
//        objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkaddGiftMessage));
        UIFoundation.clearField(FinalReviewPage.txtgiftRecipientEmail);
        UIFoundation.waitFor(1L);
        objStatus += String.valueOf(UIFoundation.setObjectCreateAccount(FinalReviewPage.txtgiftRecipientEmail,"email"));
        objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtgiftMessageTextArea));
        objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtgiftMessageTextArea, "giftMessage"));
        UIFoundation.waitFor(1L);
        objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtgiftRecipientEmail));
        UIFoundation.getAttribute(FinalReviewPage.txtgiftMessageTextArea);
        UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.rdogiftbag3_99);
        UIFoundation.waitFor(1L);
        objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdogiftbag3_99));
        UIFoundation.waitFor(1L);
        objStatus += String.valueOf(UIFoundation.javaSriptClick(FinalReviewPage.btnRecipientContinue));
        if (UIFoundation.isDisplayed(FinalReviewPage.btnViewCart)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnViewCart));
			UIFoundation.waitFor(3L);
		}
        UIFoundation.waitFor(1L);
        UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.rdogftBagTot);
        String silvrSheer = UIFoundation.getText(FinalReviewPage.rdogftBagTot);
        String expSilvrsheeramt = verifyexpectedresult.silverSheer;
        if(silvrSheer.equals(expSilvrsheeramt)) {
			objStatus += true;
			String objDetail = "Silver sheer gift bag is added succesfully";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		} else {
			objStatus += false;
			String objDetail = "Silver sheer gift bag is not added succesfully";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		}
        if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.waitFor(1L);
		}
        if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.waitFor(1L);
		}
		if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
			UIFoundation.waitFor(1L);
		}
		if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
			//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
		}
		if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			}
		if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
		UIFoundation.waitFor(1L);
		}
		System.out.println("============Order summary in the Final Review Page  ===============");
		subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
		shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
		total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
		salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
		System.out.println("Subtotal:              " + subTotal);
		System.out.println("Shipping & Handling:   " + shippingAndHandling);
		System.out.println("Sales Tax:             " + salesTax);
		System.out.println("Total:                 " + total);
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
		UIFoundation.webDriverWaitForElement(FinalReviewPage.winOfferPopup, "Clickable", "", 70);
		if(UIFoundation.isDisplayed(FinalReviewPage.winOfferPopup)) {			
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnOfferClose));
			UIFoundation.waitFor(1L);
			}
		UIFoundation.webDriverWaitForElement(ThankYouPage.lnkOrderNumber, "Clickable", "Thanks!", 70);
		UIFoundation.waitFor(10L);
		orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
		if (orderNum != "Fail") {
			objStatus += true;
			String objDetail = "Order is placed successfully";
			getlogger().pass(objDetail);
			UIFoundation.getOrderNumber(orderNum);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			System.out.println("Order is placed successfully: " + orderNum);
		} else {
			objStatus += false;
			String objDetail = "Order is null.Order not placed successfully";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));
		String giftbag=UIFoundation.getText(OrderDetailsPage.spnorderHisSilverSheer);
		UIFoundation.getText(OrderDetailsPage.spnorderHistoryGftMssg);
		System.out.println("giftbag :"+giftbag);
		String expGftbagTxt = verifyexpectedresult.gftBagText;
		System.out.println("expGftbagTxt :"+expGftbagTxt);
		if(giftbag.equals(expGftbagTxt)) {
			objStatus+=true;
			String objDetail = "silverSheer gift bag is added successfully and displayed in order history";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else {
			objStatus+=false;
			String objDetail = "SilverSheer gift bag is not added,and not displayed in order History";
			 getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
		log.info("Order Creation With Silver Sheer Gft Bag ended here");
		if (objStatus.contains("false"))
		{
			System.out.println("Verify the Order Creation With GftMsg N GftEmail Along With SilverSheerGftBag test case failed ");
			return "Fail";
		}
		else
		{
			System.out.println("Verify the the Order Creation With GftMsg N GftEmail Along With SilverSheer GftBag test case executed succesfully");
			return "Pass";
		}
	}catch(Exception e)
	{
		log.error("there is an exception arised during the execution of the method  "+ e);
		return "Fail";
	}

}
}
