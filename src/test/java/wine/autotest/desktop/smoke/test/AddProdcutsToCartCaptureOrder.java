package wine.autotest.desktop.smoke.test;

import java.io.IOException;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.ListPage;


public class AddProdcutsToCartCaptureOrder extends SmokeTest {

	
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ***********************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		try {			
			if(UIFoundation.isDisplayed(ListPage.lnkQAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkQAUserPopUp));
				//UIFoundation.webDriverWaitForElement(ListPage.lnkQAUserPopUp, "Invisible", "", 10);
				}
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkPinotNoir));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIBusinessFlows.addproductstocart(3));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollUp();
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false")) {
				return "Fail";
				} else {
				return "Pass";
				}
		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderSummaryForNewUser() {
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		
		try {
			
			System.out.println("=======================Products added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.lnkFirstProductToCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.lnkFirstProductToCart);
				System.out.println("1) "+products1);
	
			}
			
			if(!UIFoundation.getText(ListPage.lnkSecondProductToCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.lnkSecondProductToCart);
				System.out.println("2) "+products2);

			}
			
			if(!UIFoundation.getText(ListPage.lnkThirdProductToCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.lnkThirdProductToCart);
				System.out.println("3) "+products3);

			}
			
			if(!UIFoundation.getText(ListPage.lnkFourthProductToCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.lnkFourthProductToCart);
				System.out.println("4) "+products4);

			}
			
			if(!UIFoundation.getText(ListPage.lnkFifthProductToCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.lnkFifthProductToCart);
				System.out.println("5) "+products5);

			}
			
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("============Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHandling));
			System.out.println("Total Before Tax :"+totalPriceBeforeSaveForLater);
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderSummaryForExistingUser() {
		String products1=null;
		
		try {
			
			System.out.println("=======================Products Added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.lnkFirstProductToCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.lnkFirstProductToCart);
				System.out.println("1) "+products1);

			}
			else
			{
				System.out.println("No products are added into the cart");
			}
			
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("=======================Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHandling));
			System.out.println("Total price of all items :"+totalPriceBeforeSaveForLater);
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String productDetailsPresentInCart() {
		//String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		
		try {
			
			System.out.println("=======================Products added in to the cart  =====================");
			/*if(!ApplicationIndependent.getText(driver, "FirstProductInCart").contains("Fail"))
			{
				products1=ApplicationIndependent.getText(driver, "FirstProductInCart");
				System.out.println("1) "+products1);
	
			}*/
			
			if(!UIFoundation.getText(ListPage.lnkSecondProductToCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.lnkSecondProductToCart);
				System.out.println("2) "+products2);

			}
			
			if(!UIFoundation.getText(ListPage.lnkThirdProductToCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.lnkThirdProductToCart);
				System.out.println("3) "+products3);

			}
			
			if(!UIFoundation.getText(ListPage.lnkFourthProductToCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.lnkFourthProductToCart);
				System.out.println("4) "+products4);

			}
			
			if(!UIFoundation.getText(ListPage.lnkFifthProductToCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.lnkFifthProductToCart);
				System.out.println("5) "+products5);

			}
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	

}
