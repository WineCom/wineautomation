package wine.autotest.desktop.smoke.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.order.pages.CartPage;
import wine.autotest.desktop.order.pages.FinalReviewPage;
import wine.autotest.desktop.order.pages.ThankYouPage;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;

public class VerifyOrderWithVisaCard extends SmokeTest {
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			getlogger().info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "SmokeTC05username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			getlogger().info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
public static String checkoutProcess() {

	String objStatus = null;
	String subTotal = null;
	String shippingAndHandling = null;
	String total = null;
	String salesTax = null;
	String orderNum = null;
	String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
	
	try {
		log.info("The execution of the method checkoutProcess started here ...");
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
		 getDriver().navigate().refresh();
		UIFoundation.waitFor(8L);
		if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
			UIFoundation.waitFor(5L);
			}
		if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
		   objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));		
			}
		if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {	
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCardV1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "CardnumV1"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"MonthV1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"YearV1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvidV1"));
			UIFoundation.waitFor(3L);
			WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
				{
				 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
				 UIFoundation.waitFor(3L);
				}		
			}
			UIFoundation.clearField(FinalReviewPage.txtCVV);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingAddress)) {	
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingSuite, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingCity, "BillingCity"));
			objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnBillinState, "BillingState"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBillingZip, "BillingZipcode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingPhone, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			}
		if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
		System.out.println("============Order summary in the Final Review Page  ===============");
		subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
		shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
		total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
		salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
		System.out.println("Subtotal:              " + subTotal);
		System.out.println("Shipping & Handling:   " + shippingAndHandling);
		System.out.println("Sales Tax:             " + salesTax);
		System.out.println("Total:                 " + total);
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
		UIFoundation.webDriverWaitForElement(ThankYouPage.lnkOrderNumber, "Clickable", "Thanks!", 70);
		UIFoundation.waitFor(10L);
		orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
		System.out.println("orderNum :"+orderNum);
		if (orderNum != "Fail") {
			objStatus += true;
			String objDetail = "Order is placed successfully";
			UIFoundation.getOrderNumber(orderNum);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			System.out.println("Order is placed successfully: " + orderNum);
			} else {
				objStatus += false;
				String objDetail = "Order is null.Order not placed successfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}

		log.info("The execution of the method checkoutProcess ended here ...");
		if (objStatus.contains("false")) {
			System.out.println("Order creation with existing account test case is failed");
			return "Fail";
		} else {
			System.out.println("Order creation with existing account test case is executed successfully");
			return "Pass";
		}
	} catch (Exception e) {

		log.error("There is an exception arised during the execution of the method getOrderDetails " + e);
		objStatus += false;
		String objDetail = "Order number is null.Order not placed successfully";
		UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		return "Fail";
	}
}

public static String SpiritcheckoutProcess() {

	String objStatus = null;
	String subTotal = null;
	String shippingAndHandling = null;
	String total = null;
	String salesTax = null;
	String orderNum = null;
	String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
	
	try {
		log.info("The execution of the method checkoutProcess started here ...");
		
		UIFoundation.waitFor(3L);
		objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
		 getDriver().navigate().refresh();
		UIFoundation.waitFor(8L);
		
		if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
			UIFoundation.waitFor(5L);

		}
		if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
		  objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
			
	}
		if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
			
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCardV1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "CardnumV1"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"MonthV1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"YearV1"));
//			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvidV1"));
			UIFoundation.waitFor(3L);
			WebElement ele=getDriver().findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			
			}
		UIFoundation.clearField(FinalReviewPage.txtCVV);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtCVV));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvidV1"));
		if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
		UIFoundation.waitFor(1L);
		}
		System.out.println("============Order summary in the Final Review Page  ===============");
		subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
		shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
		total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
		salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
		System.out.println("Subtotal:              " + subTotal);
		System.out.println("Shipping & Handling:   " + shippingAndHandling);
		System.out.println("Sales Tax:             " + salesTax);
		System.out.println("Total:                 " + total);
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
		UIFoundation.waitFor(15L);
		orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
		System.out.println("orderNum :"+orderNum);
		if (orderNum != "Fail") {
			objStatus += true;
			String objDetail = "Order is placed successfully";
			UIFoundation.getOrderNumber(orderNum);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			getlogger().pass(objDetail);
			System.out.println("Order is placed successfully: " + orderNum);
		} else {
			objStatus += false;
			String objDetail = "Order is null.Order not placed successfully";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		}
		objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));
		UIFoundation.waitFor(20L);
		String CreditLastFour=UIFoundation.getText(OrderDetailsPage.spnCrdCardName);
		String expCardNum = verifyexpectedresult.VisaCard;
		if(CreditLastFour.equals(expCardNum)) {
			objStatus+=true;
			String objDetail = CreditLastFour+" Expected Credit card is dispalyed in Order History";
			getlogger().pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else {
			objStatus+=false;
			String objDetail = CreditLastFour+" Expected Credit card is not dispalyed in Order History";
			getlogger().fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
		log.info("The execution of the method checkoutProcess ended here ...");
		if (objStatus.contains("false")) {
			System.out.println("Order creation with existing account test case is failed");
			return "Fail";
		} else {
			System.out.println("Order creation with existing account test case is executed successfully");
			return "Pass";
		}
	} catch (Exception e) {

		log.error("There is an exception arised during the execution of the method getOrderDetails " + e);
		objStatus += false;
		String objDetail = "Order number is null.Order not placed successfully";
		UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		return "Fail";
	}
}

}





